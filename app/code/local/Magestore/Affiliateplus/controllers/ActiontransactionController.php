<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_ActiontransactionController extends Mage_Core_Controller_Front_Action {

    /**
     * @return mixed
     */
    public function creatTransactionAction() {
        // Changed By Adam 28/07/2014
        if(!Mage::helper('affiliateplus')->isAffiliateModuleEnabled()) return $this->_redirectUrl(Mage::getBaseUrl());
        
        $params = $this->getRequest()->getParams();
        $hashCode = $params['hash_code'];
        $action = Mage::getModel('affiliateplus/action')->load($params['action_id']);
        $hashCodeCompare = md5($action->getCreatedDate() . $action->getId());
        if ($hashCode == $hashCodeCompare && !$action->getIsCommission()) {
            $isUnique = 1;
            $action->setIsUnique(1)->save();
            Mage::dispatchEvent('affiliateplus_save_action_before', array(
                'action' => $action,
                'is_unique' => $isUnique,
            ));
        }
    }

}