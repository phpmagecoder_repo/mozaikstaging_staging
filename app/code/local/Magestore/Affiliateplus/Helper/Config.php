<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */
class Magestore_Affiliateplus_Helper_Config extends Mage_Core_Helper_Abstract
{
    /**
     * @param $code
     * @param null $store
     * @return mixed
     */
    public function getGeneralConfig($code, $store = null)
    {
        return Mage::getStoreConfig('affiliateplus/general/' . $code, $store);
    }

    /**
     * @param $code
     * @param null $store
     * @return mixed
     */
    public function getCommissionConfig($code, $store = null)
    {
        return Mage::getStoreConfig('affiliateplus/commission/' . $code, $store);
    }

    /**
     * @param $code
     * @param null $store
     * @return mixed
     */
    public function getDiscountConfig($code, $store = null)
    {
        return Mage::getStoreConfig('affiliateplus/discount/' . $code, $store);
    }

    /**
     * @param $code
     * @param null $store
     * @return mixed
     */
    public function getPaymentConfig($code, $store = null)
    {
        return Mage::getStoreConfig('affiliateplus/payment/' . $code, $store);
    }

    /**
     * @param $code
     * @param null $store
     * @return mixed
     */
    public function getEmailConfig($code, $store = null)
    {
        return Mage::getStoreConfig('affiliateplus/email/' . $code, $store);
    }

    /**
     * get Account and Sharing Config
     *
     * @param string $code
     * @param mixed $store
     * @return mixed
     */
    public function getSharingConfig($code, $store = null)
    {
        return Mage::getStoreConfig('affiliateplus/account/' . $code, $store);
    }

    /**
     * @param $code
     * @param null $store
     * @return mixed
     */
    public function getMaterialConfig($code, $store = null)
    {
        return Mage::getStoreConfig('affiliateplus/general/material_' . $code, $store);
    }

    /**
     * @return bool
     */
    public function disableMaterials()
    {
        return (Mage::helper('affiliateplus/account')->accountNotLogin() || !$this->getMaterialConfig('enable'));
    }

    /**
     * @param $code
     * @param null $store
     * @return mixed
     */
    public function getReferConfig($code, $store = null)
    {
        return Mage::getStoreConfig('affiliateplus/refer/' . $code, $store);
    }

    /**
     * @param $code
     * @param null $store
     * @return mixed
     */
    public function getActionConfig($code, $store = null)
    {
        return Mage::getStoreConfig('affiliateplus/action/' . $code, $store);
    }

    /**
     *Added By Adam (29/08/2016): check if allow the affiliate to get commission from his purchase
     * @param null $store
     * @return mixed
     */
    public function allowAffiliateToGetCommissionFromHisPurchase($store = null)
    {
        return $this->getCommissionConfig('allow_affiliate_get_commission_from_his_purchase', $store);
    }

}