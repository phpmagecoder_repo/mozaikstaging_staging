<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Helper_License extends Mage_Core_Helper_Abstract
{

    const EXT_CODE = 'Affiliateplus-platinum';
    const EXT_ORG_CODE = 'Affiliateplus';
    const EXT_NAME = 'Affiliate Plus Platinum';

    /**
     * @return bool
     */
    public function checkLicense()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function checkLicenseKey()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function checkLicenseKeyFrontController()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function checkLicenseKeyAdminController()
    {
        return true;
    }
}