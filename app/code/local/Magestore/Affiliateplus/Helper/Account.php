<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Helper_Account extends Mage_Core_Helper_Abstract {

    /**
     * @return bool
     */
    public function customerNotLogin() {
        return !$this->customerLoggedIn();
    }

    /**
     * @return mixed
     */
    public function customerLoggedIn() {
        return Mage::getSingleton('customer/session')->isLoggedIn();
    }

    /**
     * @return bool
     */
    public function accountNotRegistered() {
        return !$this->isRegistered();
    }

    /**
     * @return mixed
     */
    public function isRegistered() {
        return Mage::getSingleton('affiliateplus/session')->isRegistered();
    }

    /**
     * @return bool
     */
    public function accountNotLogin() {
        return !$this->isLoggedIn();
    }

    /**
     * @return mixed
     */
    public function isLoggedIn() {
        return Mage::getSingleton('affiliateplus/session')->isLoggedIn();
    }

    /**
     * get Affiliate Session
     *
     * @return Magestore_Affiliateplus_Model_Session
     */
    public function getSession() {
        return Mage::getSingleton('affiliateplus/session');
    }

    /**
     * get Affiliate Account
     *
     * @return Magestore_Affiliateplus_Model_Account
     */
    public function getAccount() {
        return $this->getSession()->getAccount();
    }

    /**
     * @return mixed
     */
    public function getNavigationLabel() {
        return $this->__('My Affiliate Account');
    }

    /**
     * @return mixed
     */
    public function getAccountBalanceFormated() {
        $scope = Mage::getStoreConfig('affiliateplus/account/balance');
        if($scope == 'website')
            return Mage::helper('core')->currency($this->getAccount()->getWebsiteBalance());
        else
            return Mage::helper('core')->currency($this->getAccount()->getBalance());
        //$baseCurrency = Mage::app()->getStore()->getBaseCurrency();
        //return $baseCurrency->format($this->getAccount()->getBalance());
    }

    /**
     * @return mixed
     */
    public function getBalanceLabel() {
        return $this->__('Balance: %s', $this->getAccountBalanceFormated());
    }

    /**
     * @return bool
     */
    public function isEnoughBalance() {
        return ($this->getAccount()->getBalance() >= Mage::helper('affiliateplus/config')->getPaymentConfig('payment_release'));
    }

    /**
     * @return bool
     */
    public function disableStoreCredit() {
        if ($this->accountNotLogin()) {
            return true;
        }
        if (Mage::helper('affiliateplus/config')->getPaymentConfig('store_credit')) {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function disableWithdrawal() {
        if ($this->accountNotLogin()) {
            return true;
        }
        if (Mage::helper('affiliateplus/config')->getPaymentConfig('withdrawals')) {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function hideWithdrawalMenu() {
        return $this->disableStoreCredit() && $this->disableWithdrawal();
    }

    /**
     * @return string
     */
    public function getWithdrawalLabel() {
        if ($this->disableWithdrawal()) {
            return 'Store Credits';
        }
        return 'Withdrawals';
    }

    
    
    //hainh 22-07-2014
    /**
     * @param $address
     * @param $paypalEmail
     * @param $customer
     * @param $notification
     * @param $referringWebsite
     * @param $successMessage
     * @param null $referredBy
     * @param null $coreSession
     * @param $keyShop
     * @return string
     */
    public function createAffiliateAccount($address, $paypalEmail, $customer, $notification, $referringWebsite, $successMessage, $referredBy=null, $coreSession=null, $keyShop) {

        $account = Mage::getModel('affiliateplus/account')
                ->setData('customer_id', $customer->getId())
                //->setData('address_id',$address->getId())
                ->setData('name', $customer->getName())
                ->setData('email', $customer->getEmail())
                ->setData('paypal_email', $paypalEmail)
                ->setData('created_time', now())
                ->setData('balance', 0)
                ->setData('total_commission_received', 0)
                ->setData('total_paid', 0)
                ->setData('total_clicks', 0)
                ->setData('unique_clicks', 0)
                ->setData('status', 1)
                ->setData('status_default', 1)
                ->setData('approved_default', 1)
                ->setData('notification', $notification)
                /*
                  hainh update for adding referring website
                  22-04-2014
                 */
                ->setData('referring_website', $referringWebsite)
                /*
                  Adam update for adding referring by to database
                  11-09-2014
                 */
                ->setData('referred_by', $referredBy)
                /*
                  Adam update for adding key shop to database
                  27/08/2016
                 */
                ->setData('key_shop', $keyShop)
        ;
        $successMessage = Mage::helper('affiliateplus/config')->getSharingConfig('notification_after_signing_up');
        $coreSession = $coreSession ? $coreSession : Mage::getSingleton('core/session');
        if (Mage::helper('affiliateplus/config')->getSharingConfig('need_approved')) {
            $account->setData('status', 2);
            $account->setData('approved', 2);
            $coreSession->setData('has_been_signup', true);
            $successMessage .= ' ' . $this->__('Thank you for signing up for our Affiliate program. Your registration will be reviewed and we\'ll inform you as soon as possible.');
        }
        if ($address)
            $account->setData('address_id', $address->getId());
        $account->setData('identify_code', $account->generateIdentifyCode());
        $account->setStoreId(Mage::app()->getStore()->getId())->save();
        $account->updateUrlKey();

        //send email
        $account->sendMailToNewAccount($account->getIdentifyCode());  //@variable identify_code
        $account->sendNewAccountEmailToAdmin();
        return $successMessage;
    }
    //end editing
    /*add by blanka*/
    /**
     * get store ids by website id
     * @param type $websiteId
     * @return store ids
     */
    public function getStoreIdsByWebsite($websiteId){
        if(is_null($websiteId))
            $websiteId = Mage::app()->getWebsite()->getId();
        $stores = Mage::getModel('core/store')->getCollection()
            ->addFieldToFilter('website_id', $websiteId)
            ->addFieldToFilter('is_active', 1)
            ->getAllIds();
        return $stores;
    }
    /*end*/

    /**
     *	Changed by Adam (19/08/2015): show list lifetimecustomer in frontend
     */
    public function notEnableLifetime() {
        if(Mage::helper('affiliateplus/config')->getCommissionConfig('life_time_sales') && $this->isLoggedIn())
            return false;
        return true;
    }
    /**
     *	Changed by Lucas (25/03/2017): Disable the substore
     */

    public function disableSubstore(){

        if(Mage::helper('affiliateplus/config')->getGeneralConfig('substore_config') && $this->isLoggedIn())
            return false;
        return true;
    }

}
