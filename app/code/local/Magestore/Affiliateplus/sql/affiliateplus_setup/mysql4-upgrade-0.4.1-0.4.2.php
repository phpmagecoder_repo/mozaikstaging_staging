<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

/*
* Added by Adam 14/08/2014
*/

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('sales/order_item'), 'affiliateplus_commission_item', 'VARCHAR(255) NULL DEFAULT NULL');
$installer->getConnection()->addColumn($installer->getTable('sales/invoice_item'), 'affiliateplus_commission_flag', 'smallint(2) NULL DEFAULT 0');
$installer->getConnection()->addColumn($installer->getTable('sales/creditmemo_item'), 'affiliateplus_commission_flag', 'smallint(2) NULL DEFAULT 0');

/* Changed By Adam 25/08/2014: dua config rel_nofollow ve ban standard*/
$installer->getConnection()->addColumn($installer->getTable('affiliateplus/banner'), 'rel_nofollow', 'smallint(6) NOT NULL default 0');

/* Changed By Adam 11/09/2014: to store refer by email into database*/
$installer->getConnection()->addColumn($installer->getTable('affiliateplus/account'), 'referred_by', 'varchar(255) default ""');

$installer->endSetup();