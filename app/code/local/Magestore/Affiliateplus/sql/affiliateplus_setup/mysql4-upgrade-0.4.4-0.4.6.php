<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

$installer = $this;
$installer->startSetup();

$installer->getConnection()->modifyColumn($installer->getTable('affiliateplus_action'), 'account_email', "varchar(255) NOT NULL default ''");
$installer->getConnection()->modifyColumn($installer->getTable('affiliateplus_action'), 'banner_title', "varchar(255) NOT NULL default ''");
$installer->getConnection()->modifyColumn($installer->getTable('affiliateplus_action'), 'domain', "varchar(255) NOT NULL default ''");
$installer->getConnection()->modifyColumn($installer->getTable('affiliateplus_action'), 'referer', "varchar(255) NOT NULL default ''");

$installer->getConnection()->modifyColumn($installer->getTable('affiliateplus_payment'), 'payment_method', "varchar(255) NOT NULL default ''");

$installer->getConnection()->modifyColumn($installer->getTable('affiliateplus_payment_verify'), 'payment_method', "varchar(255) NOT NULL default ''");
$installer->getConnection()->modifyColumn($installer->getTable('affiliateplus_payment_verify'), 'field', "varchar(255) NOT NULL default ''");


$installer->endSetup();