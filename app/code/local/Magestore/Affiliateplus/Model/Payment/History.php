<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Model_Payment_History extends Mage_Core_Model_Abstract
{
    public function _construct() {
        parent::_construct();
        $this->_init('affiliateplus/payment_history');
    }

    /**
     * @return mixed|string
     */
    public function getStatusLabel() {
        $statuses = array(
            1 =>  Mage::helper('affiliateplus')->__('Pending'),
            2 =>  Mage::helper('affiliateplus')->__('Processing'),
            3 =>  Mage::helper('affiliateplus')->__('Complete'),
            4 =>  Mage::helper('affiliateplus')->__('Canceled')
        );
        if (isset($statuses[$this->getStatus()])) return $statuses[$this->getStatus()];
        return '';
    }
}
