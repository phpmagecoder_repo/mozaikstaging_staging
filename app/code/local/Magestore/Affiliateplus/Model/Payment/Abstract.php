<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

abstract class Magestore_Affiliateplus_Model_Payment_Abstract extends Mage_Core_Model_Abstract
{
	protected $_code = '';
	protected $_store_id = null;
	
	protected $_payment;
	
	protected $_formBlockType = 'affiliateplus/payment_form';
	protected $_infoBlockType = 'affiliateplus/payment_info';
	
    public function _construct(){
        parent::_construct();
        //$this->_init('affiliateplus/payment');
    }
    
    /**
     * Set Payment for this metho
     *
     * @param Magestore_Affiliateplus_Model_Payment $value
     * @return Magestore_Affiliateplus_Model_Payment_Abstract
     */
    public function setPayment($value){
    	$this->_payment = $value;
    	return $this;
    }
    
    /**
     * get Payment model
     *
     * @return Magestore_Affiliateplus_Model_Payment
     */
    public function getPayment(){
    	return $this->_payment;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setStoreId($value){
		$this->_store_id = $value;
		return $this;
	}

    /**
     * @return null
     */
    public function getStoreId(){
		return $this->_store_id;
	}

    /**
     * @return string
     */
    public function getPaymentCode(){
		return $this->_code;
	}

    /**
     * @return string
     */
    public function getFormBlockType(){
		return $this->_formBlockType;
	}

    /**
     * @return string
     */
    public function getInfoBlockType(){
		return $this->_infoBlockType;
	}

    /**
     * @param $code
     * @return mixed
     */
    protected function _getPaymentConfig($code){
		return Mage::getStoreConfig(Magestore_Affiliateplus_Helper_Payment::XML_PAYMENT_METHODS.'/'.$this->getPaymentCode().'/'.$code,$this->getStoreId());
	}

    /**
     * @return mixed
     */
    public function isEnable(){
		return $this->_getPaymentConfig('active');
	}

    /**
     * @return int
     */
    public function calculateFee(){
		return 0;
	}

    /**
     * @param bool $includeContainer
     * @return mixed
     */
    public function getFeePrice($includeContainer = true){
		$store = Mage::app()->getStore($this->getStoreId());
		return $store->getBaseCurrency()->format($this->calculateFee(),array(),$includeContainer);
	}

    /**
     * @return mixed
     */
    public function getLabel(){
		return $this->_getPaymentConfig('label');
	}

    /**
     * @return $this
     */
    public function loadPaymentMethodInfo(){
		return $this;
	}

    /**
     * @return $this
     */
    public function savePaymentMethodInfo(){
		return $this;
	}

    /**
     * @return mixed
     */
    public function getInfoString(){
		return Mage::helper('affiliateplus/payment')->__('
			Method: %s \n
			Fee: %s \n
		',$this->getLabel(),$this->getFeePrice(false));
	}

    /**
     * @return string
     */
    public function getInfoHtml(){
		$html = Mage::helper('affiliateplus/payment')->__('Method: ');
		$html .= '<strong>'.$this->getLabel().'</strong><br />';
		$html .= Mage::helper('affiliateplus/payment')->__('Fee: ');
		$html .= '<strong>'.$this->getFeePrice(true).'</strong><br />';
		return $html;
	}

    /**
     * @return bool
     */
    public function getPaymentHelper() {
        if ($class = $this->_getPaymentConfig('helper')) {
            return Mage::helper($class);
        }
        return false;
    }
}