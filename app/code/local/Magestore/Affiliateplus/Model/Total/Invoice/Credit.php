<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Model_Total_Invoice_Credit extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    /**
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @return $this
     */
    public function collect(Mage_Sales_Model_Order_Invoice $invoice){
            // Changed By Adam 22/09/2014
        if(!Mage::helper('affiliateplus')->isAffiliateModuleEnabled()) return $this;
		//$baseDiscount = 0;
        //$discount = 0;
        
        $order = $invoice->getOrder();
        $baseOrderDiscount = $order->getBaseAffiliateCredit();
        $orderDiscount = $order->getAffiliateCredit();
        
        if ($invoice->getBaseGrandTotal() < 0.0001 || $baseOrderDiscount >= 0) {
            return $this;
        }
        $baseInvoicedDiscount = 0;
        $invoicedDiscount = 0;
        foreach ($order->getInvoiceCollection() as $_invoice) {
            $baseInvoicedDiscount += $_invoice->getBaseAffiliateCredit();
            $invoicedDiscount += $_invoice->getAffiliateCredit();
        }
        
        if ($invoice->isLast()) {
            $baseDiscount = $baseOrderDiscount - $baseInvoicedDiscount;
            $discount = $orderDiscount - $invoicedDiscount;
        } else {
//            edit by viet
            $baseOrderTotal = $order->getBaseSubtotalInclTax();// - $baseOrderDiscount;
            $baseDiscount = $baseOrderDiscount * $invoice->getBaseSubtotalInclTax() / $baseOrderTotal;
            $discount = $orderDiscount * $invoice->getBaseSubtotalInclTax() / $baseOrderTotal;
//            end by viet
            if ($baseDiscount < $baseOrderDiscount) {
                $baseDiscount = $baseOrderDiscount;
                $discount = $orderDiscount;
            }
        }
        if ($baseDiscount) {
            $baseDiscount = Mage::app()->getStore()->roundPrice($baseDiscount);
            $discount = Mage::app()->getStore()->roundPrice($discount);
            
            $invoice->setBaseAffiliateCredit($baseDiscount);
            $invoice->setAffiliateCredit($discount);
            
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseDiscount);
			$invoice->setGrandTotal($invoice->getGrandTotal() + $discount);
        }
        return $this;
	}
}
