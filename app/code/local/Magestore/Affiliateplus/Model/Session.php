<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Model_Session extends Mage_Core_Model_Session
{
    /**
     * @return mixed
     */
    public function getAccount(){
		if (!Mage::registry('load_account')){
			// $customer = $this->getCustomer();
//            $customerId = Mage::getSingleton('customer/session')->getCustomerId();
			$customerId = Mage::getSingleton('customer/session')->isLoggedIn() ? Mage::getSingleton('customer/session')->getCustomerId() : null;
			$account = Mage::getModel('affiliateplus/account')
				->setStoreId(Mage::app()->getStore()->getId());
			if (Mage::helper('affiliateplus/config')->getSharingConfig('balance') == 'global')
				$account->setBalanceIsGlobal(true);
			if ($customerId) {
                $account->loadByCustomerId($customerId);
            }
			$this->setData('account',$account);
			Mage::register('load_account',true);
		} 
		return $this->getData('account');
	}

    /**
     * @return bool
     */
    public function isRegistered(){
		if ($this->getAccount() && $this->getAccount()->getId())
			return true;
		return false;
	}

    /**
     * @return bool
     */
    public function isLoggedIn(){
		if ($this->isRegistered())
			if ($this->getAccount()->getStatus() == '1')
				return true;
		return false;
	}

    /**
     * @return mixed
     */
    public function getCustomer(){
		return Mage::getSingleton('customer/session')->getCustomer();
	}
}