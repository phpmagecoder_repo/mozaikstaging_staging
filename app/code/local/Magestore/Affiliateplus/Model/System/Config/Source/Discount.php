<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Model_System_Config_Source_Discount
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
			array('value' => '', 'label'=>Mage::helper('affiliateplus')->__('Both Affiliate program Discount and Shopping cart Discount')),
            array('value' => 'affiliate', 'label'=>Mage::helper('affiliateplus')->__('Only Affiliate program Discount')),
            array('value' => 'system', 'label' => Mage::helper('affiliateplus')->__('Only Shopping cart Discount')),
        );
    }
}
