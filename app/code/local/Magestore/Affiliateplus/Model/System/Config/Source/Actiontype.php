<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Model_System_Config_Source_Actiontype
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(){
        $types = new Varien_Object(array(
    		'actions'	=> array(
    			array('value' => '3', 'label'=>Mage::helper('affiliateplus')->__('Sale')),
                array('value' => '10', 'label'=>Mage::helper('affiliateplus')->__('Administrator')),
    		)
    	));
        Mage::dispatchEvent('affiliateplus_get_action_types',array(
    		'types'		=> $types,
    	));
        return $types->getActions();
    }

    /**
     * @return array
     */
    public function getOptionList(){
        $result = array();
        foreach($this->toOptionArray() as $option){
            $result[$option['value']] = $option['label']; 
        }
        return $result;
    }
}