<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Model_System_Config_Source_Payments
{
    /**
	 * Get Affiliate Payment Helper
	 *
	 * @return Magestore_Affiliateplus_Helper_Payment
	 */
	protected function _getPaymentHelper(){
		return Mage::helper('affiliateplus/payment');
	}

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $paymentMethods = array();
        $store = Mage::app()->getRequest()->getParam('store');
        $availableMethods = $this->_getPaymentHelper()->getAvailablePayment($store);
        foreach($availableMethods as $code => $method){
            $paymentMethods[$code] = $method->getLabel();
        }
        return $paymentMethods;
    }

}