<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

/* add by VietBQ */
class Magestore_Affiliateplus_Model_System_Config_Source_Refer {

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return array(
            array('value' => 'email', 'label' => Mage::helper('affiliateplus')->__('Email')),
            array('value' => 'facebook', 'label' => Mage::helper('affiliateplus')->__('Facebook')),
            array('value' => 'twitter', 'label' => Mage::helper('affiliateplus')->__('Twitter')),
            array('value' => 'google', 'label' => Mage::helper('affiliateplus')->__('Google')),
        );
    }

}