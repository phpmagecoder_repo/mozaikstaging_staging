<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Model_Mysql4_Action_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected $_customGroupSql = false;
    
	public function _construct() {
        parent::_construct();
        $this->_init('affiliateplus/action');
    }

    /**
     * @param $value
     * @return $this
     */
    public function setCustomGroupSql($value) {
        $this->_customGroupSql = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSelectCountSql() {
        if ($this->_customGroupSql) {
            $this->_renderFilters();
            $countSelect = clone $this->getSelect();
            $countSelect->reset(Zend_Db_Select::ORDER);
            $countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
            $countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
            $countSelect->reset(Zend_Db_Select::COLUMNS);
            $countSelect->reset(Zend_Db_Select::GROUP);
            $countSelect->columns('COUNT(DISTINCT referer, landing_page, store_id)');
            return $countSelect;
        }
        return parent::getSelectCountSql();
    }
}