<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Model_Mysql4_Payment_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	protected $_load_method_info = true;

    /**
     * @param $value
     * @return $this
     */
    public function setLoadMethodInfo($value){
		$this->_load_method_info = $value;
		return $this;
	}
	
    public function _construct()
    {
        parent::_construct();
        $this->_init('affiliateplus/payment');
    }

    /**
     * @param $storeId
     * @return $this
     */
    public function addStoreToFilter($storeId){
    	$this->getSelect()
    		->where('store_id = 0 OR store_id = ?',$storeId);
    	return $this;
    }

    /**
     * @return $this
     */
    protected function _afterLoad(){
    	parent::_afterLoad();
    	if ($this->_load_method_info)
	    	foreach ($this->_items as $item)
	    		$item->addPaymentInfo();
    	return $this;
    }
}