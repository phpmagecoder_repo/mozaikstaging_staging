<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Model_Referer extends Mage_Core_Model_Abstract
{
	protected $_eventPrefix = 'affiliateplus_referer';
    protected $_eventObject = 'affiliateplus_referer';
	
    public function _construct(){
        parent::_construct();
        $this->_init('affiliateplus/referer');
    }

    /**
     * @param $accountId
     * @param $referer
     * @param $storeId
     * @param $pathInfo
     * @return $this
     */
    public function loadExistReferer($accountId, $referer, $storeId, $pathInfo){
    	$item = $this->getCollection()
    		->addFieldToFilter('account_id',$accountId)
    		->addFieldToFilter('referer',$referer) 
    		->addFieldToFilter('store_id',$storeId)
    		->addFieldToFilter('url_path',$pathInfo)
    		->getFirstItem();
    	if ($item && $item->getId())
    		$this->setData($item->getData())
    			->setId($item->getId());
    	else 
    		$this->setData('account_id',$accountId)
    			->setData('referer',$referer)
    			->setData('store_id',$storeId)
    			->setData('url_path',$pathInfo)
    			->setId(null);
    	return $this;
    }

    /**
     * @return mixed
     */
    protected function _beforeSave(){
		if ($ipAddress = $this->getIpAddress()){
			$ipList = explode(',',$this->getIpList());
			if (!in_array($ipAddress,$ipList)){
				$this->setIpList($this->getIpList().','.$ipAddress);
				$this->setUniqueClicks($this->getUniqueClicks() + 1);
			}
			$this->setTotalClicks($this->getTotalClicks() + 1);
			$this->setIpAddress(null);
		}
    	return parent::_beforeSave();
    }
}