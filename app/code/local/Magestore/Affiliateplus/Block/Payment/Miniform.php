<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Payment_Miniform extends Mage_Core_Block_Template
{
    /**
     * @return $this
     */
    public function _prepareLayout(){
		parent::_prepareLayout();
		$this->setTemplate('affiliateplus/payment/miniform.phtml');
		return $this;
    }

    /**
     * @return mixed
     */
    public function getAccount(){
    	return Mage::getSingleton('affiliateplus/session')->getAccount();
    }

    /**
     * @return float
     */
    public function getBalance(){
        /*Changed By Adam 15/09/2014: to fix the issue of request withdrawal when scope is website*/
        $balance = 0;
        if(Mage::getStoreConfig('affiliateplus/account/balance') == 'website') {
            $website = Mage::app()->getStore()->getWebsite();
            
            $stores = $website->getStores();
            
            foreach($stores as $store) {
                $account = Mage::getModel('affiliateplus/account')->setStoreId($store->getId())->load($this->getAccount()->getId());
                $balance += $account->getBalance();
            }
        } else {
            $balance = $this->getAccount()->getBalance();
        }
        $balance = Mage::app()->getStore()->convertPrice($balance);
//        $balance = Mage::app()->getStore()->convertPrice($this->getAccount()->getBalance());
        return floor($balance * 100) / 100;
    	return round(Mage::app()->getStore()->convertPrice($this->getAccount()->getBalance()),2);
    }

    /**
     * @return mixed
     */
    public function getFormatedBalance(){
        /*Changed By Adam 15/09/2014: to fix the issue of request withdrawal when scope is website*/
        $balance = 0;
        if(Mage::getStoreConfig('affiliateplus/account/balance') == 'website') {
            $website = Mage::app()->getStore()->getWebsite();
            
            $stores = $website->getStores();
            
            foreach($stores as $store) {
                $account = Mage::getModel('affiliateplus/account')->setStoreId($store->getId())->load($this->getAccount()->getId());
                $balance += $account->getBalance();
            }
            return Mage::helper('core')->currency($balance);
        } else {
            return Mage::helper('core')->currency($this->getAccount()->getBalance());
        }
//    	return Mage::helper('core')->currency($this->getAccount()->getBalance());
    }

    /**
     * @return mixed
     */
    public function getFormActionUrl(){
        //hainh comment these lines 22-05-2014
        //$all = Mage::helper('affiliateplus/payment')->getAvailablePayment();
        //if(count($all) > 1)
            $url = $this->getUrl('affiliateplus/index/paymentForm');
        //else 
        //    $url = $this->getUrl('affiliateplus/index/confirmRequest');
        return $url;
    }

    /**
     * @return bool
     */
    public function canRequest() {
        return !Mage::helper('affiliateplus/account')->disableWithdrawal();
    }

    /**
     * @return float
     */
    public function getMaxAmount() {
        $taxRate = Mage::helper('affiliateplus/payment_tax')->getTaxRate();
        if (!$taxRate) {
            return $this->getBalance();
        }
        $balance = $this->getBalance();
        $maxAmount = $balance * 100 / (100 + $taxRate);
        return round($maxAmount, 2);
    }
}
