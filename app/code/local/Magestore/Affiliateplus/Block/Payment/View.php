<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Payment_View extends Mage_Core_Block_Template
{
    /**
     * @return mixed
     */
    public function getAccount(){
    	return Mage::getSingleton('affiliateplus/session')->getAccount();
    }
    
    /**
     * get Payment Model
     *
     * @return Magestore_Affiliateplus_Model_Payment
     */
    public function getPayment(){
    	if (!$this->hasData('payment')){
    		$payment = Mage::registry('view_payment_data');
    		$payment->addPaymentInfo();
    		$this->setData('payment',$payment);
    	}
    	return $this->getData('payment');
    }
    
    /**
     * get Payment Method
     *
     * @return Magestore_Affiliateplus_Model_Payment_Abstract
     */
    public function getPaymentMethod(){
    	return $this->getPayment()->getPayment();
    }

    /**
     * @return array
     */
    public function getStatusArray(){
    	return array(
			1	=> $this->__('Pending'),
			2	=> $this->__('Processing'),
			3	=> $this->__('Complete'),
            4	=> $this->__('Canceled'),
		);
    }

    /**
     * @return $this
     */
    public function _prepareLayout(){
		parent::_prepareLayout();
		
		if ($this->getPaymentMethod())
		if ($paymentMethodInfoBlock = $this->getLayout()->createBlock($this->getPaymentMethod()->getInfoBlockType(),'payment_method_info')){
			$paymentMethodInfoBlock->setPaymentMethod($this->getPaymentMethod());
			$this->setChild('payment_method_info',$paymentMethodInfoBlock);
		}
		
		return $this;
    }

    /**
     * @return mixed
     */
    public function getFullHistory() {
        if (!$this->hasData('collection')) {
            $collection = Mage::getResourceModel('affiliateplus/payment_history_collection')
                ->addFieldToFilter('payment_id', $this->getPayment()->getId());
            $collection->getSelect()->order('created_time DESC');
            $this->setData('collection', $collection);
        }
        return $this->getData('collection');
    }

    /**
     * @return mixed
     */
    public function getCollection() {
        return $this->getFullHistory();
    }
}
