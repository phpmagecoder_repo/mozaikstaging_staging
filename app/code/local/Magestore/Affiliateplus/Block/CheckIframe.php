<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_CheckIframe extends Mage_Core_Block_Template {

    /**
     * @return mixed
     */
    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    /**
     * @return mixed
     */
    public function getActionId() {
        $session = Mage::getSingleton('core/session');
        $actionId = $session->getData('transaction_checkiframe__action_id');
        $this->setActionId(NULL);
        return $actionId;
        ;
    }

    /**
     * @param $actionId
     */
    public function setActionId($actionId) {
        $session = Mage::getSingleton('core/session');
        $session->setData('transaction_checkiframe__action_id', $actionId);
    }

    /**
     * @return mixed
     */
    public function getHashCode() {
        $session = Mage::getSingleton('core/session');
        $hashCode = $session->getData('transaction_checkiframe_hash_code');
        $this->setHashCode(NULL);
        return $hashCode;
    }

    /**
     * @param $hashCode
     */
    public function setHashCode($hashCode) {
        $session = Mage::getSingleton('core/session');
        $session->setData('transaction_checkiframe_hash_code', $hashCode);
    }

}