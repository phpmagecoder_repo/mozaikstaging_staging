<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Credit_Cart extends Mage_Core_Block_Template
{
	/**
	 * get Helper
	 *
	 * @return Magestore_Affiliateplus_Helper_Config
	 */
	public function _getHelper(){
		return Mage::helper('affiliateplus/config');
	}
    
    /**
     * get Account helper
     *
     * @return Magestore_Affiliateplus_Helper_Account
     */
    protected function _getAccountHelper() {
        return Mage::helper('affiliateplus/account');
    }

    /**
     * @return $this
     */
    public function _prepareLayout(){
		parent::_prepareLayout();
        $this->setTemplate('affiliateplus/credit/cart.phtml');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormatedBalance() {
        $balance = $this->_getAccountHelper()->getAccount()->getBalance();
        $balance = Mage::app()->getStore()->convertPrice($balance);
        if ($this->getAffiliateCredit() > 0) {
            $balance -= $this->getAffiliateCredit();
        }
        return Mage::app()->getStore()->formatPrice($balance);
        // return $this->_getAccountHelper()->getAccountBalanceFormated();
    }
    
    /**
     * check using affiliate credit or not
     * 
     * @return boolean
     */
    public function getUseAffiliateCredit() {
        return Mage::getSingleton('checkout/session')->getUseAffiliateCredit();
    }

    /**
     * @return mixed
     */
    public function getAffiliateCredit() {
        return Mage::getSingleton('checkout/session')->getAffiliateCredit();
    }
}
