<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Sales_Credit_Order extends Mage_Sales_Block_Order_Totals
{
	public function initTotals() {
        $parent = $this->getParentBlock();
        $order = $parent->getOrder();
		if($amount = floatval($order->getAffiliateCredit())){
			$total = new Varien_Object(array(
                'code'  => 'affiliateplus_credit',
                'value' => $amount,
                'base_value'    => $order->getBaseAffiliateCredit(),
                'label' => $this->__('Paid by Affiliate Credit'),
            ));
			$parent->addTotal($total, 'subtotal');
		}
	}
}
