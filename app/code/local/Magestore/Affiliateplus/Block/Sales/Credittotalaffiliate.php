<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Sales_Credittotalaffiliate extends Mage_Sales_Block_Order_Totals
{
    /**
     * @return mixed
     */
    public function getAffiliateplusDiscount(){
		$order = $this->getOrder();
		return $order->getAffiliateplusDiscount();
	}

    /**
     * @return mixed
     */
    public function getBaseAffiliateplusDiscount(){
		$order = $this->getOrder();
		return $order->getBaseAffiliateplusDiscount();
	}
	
	public function initTotals(){
		$amount = $this->getAffiliateplusDiscount();
		if(floatval($amount)){
			$total = new Varien_Object();
			$total->setCode('affiliateplus_discount');
			$total->setValue($amount);
			$total->setBaseValue($this->getBaseAffiliateplusDiscount());
			$total->setLabel('Affiliate Discount');
			$parent = $this->getParentBlock();
			$parent->addTotal($total,'subtotal');
		}
	}

    /**
     * @return mixed
     */
    public function getOrder(){
		if(!$this->hasData('order')){
			$order = $this->getParentBlock()
				->getCreditmemo()
				->getOrder();
			$this->setData('order',$order);
		}
		return $this->getData('order');
	}
}