<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Email_Report extends Mage_Core_Block_Template
{
    /**
     * @param $statistic
     * @return array
     */
    public function prepareStatistic($statistic){
		$sales = 0;
		$transaction = 0;
		$commission = 0;
		foreach ($statistic as $sta){
			$sales += isset($sta['sales']) ? $sta['sales'] : 0;
			$transaction += isset($sta['transactions']) ? $sta['transactions'] : 0;
			$commission += isset($sta['transactions']) ? $sta['commissions'] : 0;
		}
		return array('sales' => $sales, 'transaction' => $transaction, 'commission' => $commission);
	}

    /**
     * @return array
     */
    public function getOptionLabels(){
		return array(
			'complete'	=> $this->__('Complete'),
			'pending'	=> $this->__('Pending'),
			'cancel'	=> $this->__('Canceled'),
		);
	}
}