<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Account_Login extends Mage_Core_Block_Template
{
	/**
	 * get Core Session
	 *
	 * @return Mage_Core_Model_Session
	 */
	protected function _getCoreSession(){
		return Mage::getSingleton('core/session');
	}

    /**
     * @return mixed
     */
    public function _prepareLayout(){
		return parent::_prepareLayout();
    }

    /**
     * @return null
     */
    public function getUsername(){
    	if ($loginData = $this->getLoginFormData())
    		return $loginData['email'];
    	return null;
    }

    /**
     * @return mixed
     */
    public function getLoginFormData(){
    	return $this->_getCoreSession()->getLoginFormData();
    }

    /**
     * @return mixed
     */
    public function getRegisterUrl(){
    	return $this->getUrl('affiliateplus/account/register');
    }

    /**
     * @return mixed
     */
    public function getRegisterDescription(){
    	return Mage::helper('affiliateplus/config')->getSharingConfig('register_description');
    }

    /**
     * @param $html
     * @return mixed
     */
    protected function _afterToHtml($html){
    	$this->_getCoreSession()->unsetData('login_form_data');
    	return parent::_afterToHtml($html);
    }
}