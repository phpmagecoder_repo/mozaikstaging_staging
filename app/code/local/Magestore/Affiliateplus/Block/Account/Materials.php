<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Account_Materials extends Mage_Core_Block_Template
{
    /**
     * @return mixed
     */
    public function getPageIdentifier(){
		return Mage::helper('affiliateplus/config')->getMaterialConfig('page');
	}

    /**
     * @return mixed
     */
    public function getPageId(){
		$page = Mage::getModel('cms/page');
		$pageId = $page->checkIdentifier($this->getPageIdentifier(), Mage::app()->getStore()->getId());
		return $pageId;
	}

    /**
     * @return mixed
     */
    public function getPage(){
		return Mage::getSingleton('cms/page');
	}
	
	protected function _construct(){
		parent::_construct();
		$page = Mage::getSingleton('cms/page');
		if ($pageId = $this->getPageId())
			$page->setStoreId(Mage::app()->getStore()->getId())->load($pageId);
	}

    /**
     * @return string
     */
    protected function _toHtml(){
		$helper = Mage::helper('cms');
		$processor = $helper->getPageTemplateProcessor();
		
		$html = $this->getMessagesBlock()->getGroupedHtml();
		if ($pageHeading = $this->getChild('page_content_heading')){
			$pageHeading->setContentHeading($this->getPage()->getContentHeading());
			$html .= $pageHeading->toHtml();
		}
		$html .= $processor->filter($this->getPage()->getContent());
		return $html;
	}
}