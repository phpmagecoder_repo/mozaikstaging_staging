<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Account_Edit extends Mage_Core_Block_Template
{
    /**
     * @return mixed
     */
    protected function _getSession(){
		return Mage::getSingleton('affiliateplus/session');
	}

    /**
     * @return mixed
     */
    public function customerLoggedIn(){
    	return Mage::helper('affiliateplus/account')->customerLoggedIn();
    }

    /**
     * @return mixed
     */
    public function isLoggedIn(){
    	return $this->_getSession()->isLoggedIn();
    }

    /**
     * @return mixed
     */
    public function getCustomer(){
    	return Mage::getSingleton('customer/session')->getCustomer();
    }

    /**
     * @param null $field
     * @return null
     */
    public function getFormData($field=null){
    	$formData = $this->_getSession()->getAffiliateFormData();
		if($field)
			return isset($formData[$field]) ? $formData[$field] : null;
		return $formData;
    }

    /**
     * @return $this
     */
    public function unsetFormData(){
    	$this->_getSession()->unsetData('affiliate_form_data');
    	return $this;
    }

    /**
     * @return mixed
     */
    public function getAccount(){
    	return $this->_getSession()->getAccount();
    }

    /**
     * @return mixed
     */
    public function requiredAddress(){
        $store = Mage::app()->getStore();
    	return Mage::helper('affiliateplus/config')->getSharingConfig('required_address', $store);
    }

    /**
     * @return mixed
     */
    public function requiredPaypal(){
        $store = Mage::app()->getStore();
    	return Mage::helper('affiliateplus/config')->getSharingConfig('required_paypal', $store);
    }

    /**
     * @return mixed
     */
    public function getFormattedAddress(){
		$account = $this->getAccount();
		return Mage::getModel('customer/address')->load($account->getAddressId())->format('html');
	}

    /**
     * @return mixed
     */
    public function getAddress() {
		$address = Mage::getModel('customer/address');
		$formData = $this->getFormData();
		if(isset($formData['account'])){
			$address->setData($formData['account']);
		} elseif($this->isLoggedIn()){
			$address->load($this->getAccount()->getAddressId());
		} elseif($this->customerLoggedIn()){
			if(!$address->getFirstname())
				$address->setFirstname($this->getCustomer()->getFirstname());
			if(!$address->getLastname())
				$address->setLastname($this->getCustomer()->getLastname());
		}
		return $address;
    }

    /**
     * @return mixed
     */
    public function customerHasAddresses(){
    	return $this->getCustomer()->getAddressesCollection()->getSize();
    }

    /**
     * @param $type
     * @return string
     */
    public function getAddressesHtmlSelect($type){
        if ($this->customerLoggedIn()){
            $options = array();
            foreach ($this->getCustomer()->getAddresses() as $address) {
                $options[] = array(
                    'value'=>$address->getId(),
                    'label'=>$address->format('oneline')
                );
            }

            $addressId = $this->getAddress()->getId();
            if (empty($addressId)) {
				//$address = $this->getCustomer()->getPrimaryBillingAddress();
            }

            $select = $this->getLayout()->createBlock('core/html_select')
                ->setName($type.'_address_id')
                ->setId($type.'-address-select')
                ->setClass('address-select')
                ->setExtraParams('onchange=lsRequestTrialNewAddress(this.value);')
                ->setValue($addressId)
                ->setOptions($options);

            $select->addOption('', Mage::helper('checkout')->__('New Address'));

            return $select->getHtml();
        }
        return '';
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getCountryHtmlSelect($type){
        $countryId = $this->getAddress()->getCountryId();
        if (is_null($countryId)) {
            $countryId = Mage::getStoreConfig('general/country/default');
        }
        $select = $this->getLayout()->createBlock('core/html_select')
            ->setName($type.'[country_id]')
            ->setId($type.':country_id')
            ->setTitle(Mage::helper('checkout')->__('Country'))
            ->setClass('validate-select')
            ->setValue($countryId)
            ->setOptions($this->getCountryOptions());

        return $select->getHtml();
    }

    /**
     * @return mixed
     */
    public function getRegionCollection(){
        if (!$this->_regionCollection){
            $this->_regionCollection = Mage::getModel('directory/region')->getResourceCollection()
                ->addCountryFilter($this->getAddress()->getCountryId())
                ->load();
        }
        return $this->_regionCollection;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getRegionHtmlSelect($type){
        $select = $this->getLayout()->createBlock('core/html_select')
            ->setName($type.'[region]')
            ->setId($type.':region')
            ->setTitle(Mage::helper('checkout')->__('State/Province'))
            ->setClass('required-entry validate-state')
            ->setValue($this->getAddress()->getRegionId())
            ->setOptions($this->getRegionCollection()->toOptionArray());

        return $select->getHtml();
    }

    /**
     * @return mixed
     */
    public function getCountryCollection(){
        if (!$this->_countryCollection) {
            $this->_countryCollection = Mage::getSingleton('directory/country')->getResourceCollection()
                ->loadByStore();
        }
        return $this->_countryCollection;
    }

    /**
     * @return bool|mixed
     */
    public function getCountryOptions(){
        $options    = false;
        $useCache   = Mage::app()->useCache('config');
        if ($useCache) {
            $cacheId    = 'DIRECTORY_COUNTRY_SELECT_STORE_' . Mage::app()->getStore()->getCode();
            $cacheTags  = array('config');
            if ($optionsCache = Mage::app()->loadCache($cacheId)) {
                $options = unserialize($optionsCache);
            }
        }

        if ($options == false) {
            $options = $this->getCountryCollection()->toOptionArray();
            if ($useCache) {
                Mage::app()->saveCache(serialize($options), $cacheId, $cacheTags);
            }
        }
        return $options;
    }

    /**
     * @param $html
     * @return mixed
     */
    protected function _afterToHtml($html){
    	$this->unsetFormData();
    	return parent::_afterToHtml($html);
    }

    /**
     * @return mixed
     */
    public function getCheckCustomerEmailUrl(){
    	return $this->getUrl('affiliateplus/account/checkemailregister');
    }
    
    /*Changed By Adam 12/09/2014: check referred by email before register*/
    /**
     * @return mixed
     */
    public function getCheckReferredEmailUrl(){
    	return $this->getUrl('affiliateplus/account/checkreferredemail');
    }

    /**
     * Changed By Adam (27/08/2016):
     */
    public function getCheckKeyShopUrl() {
        return $this->getUrl('affiliateplus/account/checkkeyshop');
    }

    /**
     * @author Adam (27/08/2016)
     * @return mixed
     */
    public function getAffiliateUrl(){
        $homeUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);

        return Mage::helper('affiliateplus/url')->addAccToSubstore($homeUrl);
    }
}