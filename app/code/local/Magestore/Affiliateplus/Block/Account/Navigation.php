<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Account_Navigation extends Mage_Customer_Block_Account_Navigation
{
	protected $_navigation_title = '';

    /**
     * @param $title
     * @return $this
     */
    public function setNavigationTitle($title){
		$this->_navigation_title = $title;
		return $this;
	}

    /**
     * @return string
     */
    public function getNavigationTitle(){
		return $this->_navigation_title;
	}

    /**
     * @param $name
     * @param $path
     * @param $label
     * @param bool $disabled
     * @param int $order
     * @param array $urlParams
     * @return $this
     */
    public function addLink($name, $path, $label, $disabled = false, $order = 0, $urlParams=array())
    {
    	if (isset($this->_links[$order])) $order++;
    	
    	$link = new Varien_Object(array(
            'name' 		=> $name,
            'path' 		=> $path,
            'label' 	=> $label,
            'disabled'	=> $disabled,
            'order'		=> $order,
            'url' 		=> $this->getUrl($path, $urlParams),
        ));
        
        Mage::dispatchEvent('affiliateplus_account_navigation_add_link',array(
        	'block'		=> $this,
        	'link'		=> $link,
        ));
    	
        $this->_links[$order] = $link;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLinks(){
		$links = new Varien_Object(array(
			'links'	=> $this->_links,
		));
		
		Mage::dispatchEvent('affiliateplus_account_navigation_get_links',array(
			'block'		=> $this,
			'links_obj'	=> $links,
		));
		
		$this->_links = $links->getLinks();
		
		ksort($this->_links);
		
		return $this->_links;
	}

    /**
     * @param $link
     * @return bool
     */
    public function isActive($link){
		if (parent::isActive($link)) return true;
		if (in_array($this->_activeLink,array(
				'affiliatepluslevel/index/listTierTransaction',
				'affiliatepluspayperlead/index/listleadcommission'
			)) && $this->_completePath($link->getPath()) == 'affiliateplus/index/listTransaction')
				return true;
		return false;
	}
}