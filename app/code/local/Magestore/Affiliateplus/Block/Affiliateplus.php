<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Affiliateplus extends Mage_Core_Block_Template
{
	/**
	 * get Helper
	 *
	 * @return Magestore_Affiliateplus_Helper_Config
	 */
	public function _getHelper(){
		return Mage::helper('affiliateplus/config');
	}

    /**
     * @return mixed
     */
    public function _prepareLayout(){
		return parent::_prepareLayout();
    }

    /**
     * @return $this
     */
    public function addFooterLink(){
    	$footerBlock = $this->getParentBlock();
        // Changed By Adam 28/07/2014
    	if ($footerBlock && $this->_getHelper()->getGeneralConfig('show_affiliate_link_on_frontend') && Mage::helper('affiliateplus')->isAffiliateModuleEnabled())
    		$footerBlock->addLink(
    			$this->__('Affiliates'),
    			'affiliateplus',
    			'affiliateplus',
    			true,
    			array(),
    			10
    		);
    	return $this;
    }
}