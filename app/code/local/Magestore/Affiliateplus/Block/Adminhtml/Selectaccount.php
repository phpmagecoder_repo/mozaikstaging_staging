<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Adminhtml_Selectaccount extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Magestore_Affiliateplus_Block_Adminhtml_Selectaccount constructor.
     */
    public function __construct()
  {
    $this->_controller = 'adminhtml_selectaccount';
    $this->_blockGroup = 'affiliateplus';
    $this->_headerText = Mage::helper('affiliateplus')->__('Select Account to Create Withdrawal');
     
	parent::__construct();
        $this->_removeButton('add');
  }
}