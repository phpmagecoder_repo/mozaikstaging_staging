<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Adminhtml_Account_Edit_Tab_Lifetimecustomer
    extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Magestore_Affiliateplus_Block_Adminhtml_Account_Edit_Tab_Lifetimecustomer constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('lifetimecustomergrid');
        $this->setDefaultSort('tracking_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);

    }

    /**
     * @param $column
     * @return mixed
     */
    protected function _addColumnFilterToCollection($column)
    {
        return parent::_addColumnFilterToCollection($column);
    }


    /**
     * @return mixed
     */
    protected function _prepareCollection()
    {

        $collection = Mage::getResourceModel('affiliateplus/tracking_collection')
            ->addFieldToFilter('account_id', $this->getRequest()->getParam('id'));

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return mixed
     */
    protected function _prepareColumns()
    {
        $id = $this->getRequest()->getParam('id');

        $this->addColumn('tracking_id', array(
            'header'    => Mage::helper('affiliateplus')->__('ID'),
            'width'     => '50px',
            'index'     => 'tracking_id',
            'type'  => 'number',
        ));

        $this->addColumn('customer_email', array(
            'header'    => Mage::helper('affiliateplus')->__('Email'),
            'width'     => '250px',
            'index'     => 'customer_email',
            'renderer' => 'affiliateplus/adminhtml_account_renderer_customer',
        ));

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('affiliateplus')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('affiliateplus')->__('remove'),
                        'url'       => array('base'=> '*/*/remove/id/'.$id),
                        'field'     => 'tracking_id',
                        'confirm'	=> 'Do you want to remove this customer?'

                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
            ));

        return parent::_prepareColumns();
    }

    //return url
    /**
     * @return mixed
     */
    public function getGridUrl()
    {
        return $this->getData('grid_url')
            ? $this->getData('grid_url')
            : $this->getUrl('*/*/lifetimecustomerGrid', array('_current'=>true,'id'=>$this->getRequest()->getParam('id')));

    }

    //return Magestore_Affiliate_Model_Referral
    /**
     * @return mixed
     */
    public function getAccount()
    {
        return Mage::getModel('affiliateplus/account')
            ->load($this->getRequest()->getParam('id'));
    }

}