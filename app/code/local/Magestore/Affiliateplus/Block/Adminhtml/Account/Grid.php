<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Adminhtml_Account_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Magestore_Affiliateplus_Block_Adminhtml_Account_Grid constructor.
     */
    public function __construct()
  {
      parent::__construct();
      $this->setId('accountGrid');
      $this->setDefaultSort('account_id');
      $this->setDefaultDir('DESC');
	  $this->setUseAjax(true);
      $this->setSaveParametersInSession(true);
  }

    /**
     * @return mixed
     */
    protected function _prepareCollection()
  {
  	  
      $collection = Mage::getModel('affiliateplus/account')->getCollection();
	  //add event to add more column 
	  Mage::dispatchEvent('affiliateplus_adminhtml_join_account_other_table', array('collection' => $collection));
	  
	  $storeId = $this->getRequest()->getParam('store');
	  $collection->setStoreId($storeId);
	
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

    /**
     * @return mixed
     */
    protected function _prepareColumns()
  {
  	  $currencyCode = Mage::app()->getStore()->getBaseCurrency()->getCode();
      $this->addColumn('account_id', array(
          'header'    => Mage::helper('affiliateplus')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'account_id',
		  'type'	  => 'number',
		  'filter_index'	=> 'main_table.account_id'
      ));

      $this->addColumn('name', array(
          'header'    => Mage::helper('affiliateplus')->__('Name'),
          'align'     =>'left',
          'index'     => 'name',
		  'filter_index'	=> 'main_table.name'
      ));
      
//      $this->addColumn('identify_code', array(
//          'header'    => Mage::helper('affiliateplus')->__('Identify Code'),
//          'align'     =>'left',
//          'index'     => 'identify_code',            // Day la dong de lay du lieu tu field identify_code
//		  'filter_index'	=> 'main_table.identify_code'
//      ));


      $this->addColumn('email', array(
			'header'    => Mage::helper('affiliateplus')->__('Email Address'),
			'index'     => 'email',
			'filter_index'	=> 'main_table.email'
      ));
      $substore = Mage::helper('affiliateplus/config')->getGeneralConfig('substore_config');
      if($substore ) {
          $this->addColumn('key_shop', array(
              'header' => Mage::helper('affiliateplus')->__('Key shop'),
              'index' => 'key_shop',
              'filter_index' => 'main_table.key_shop'
          ));
      }
	  $this->addColumn('balance', array(
			'header'    => Mage::helper('affiliateplus')->__('Balance'),
			'width'     => '100px',
			'align'     =>'right',
			'index'     => 'balance',
			'type'		=> 'price',
			'currency_code' => $currencyCode,
			'filter_index'	=> 'main_table.balance'
      ));
	  
	  $this->addColumn('total_commission_received', array(
			'header'    => Mage::helper('affiliateplus')->__('Commission Paid'),
			'width'     => '100px',
			'align'     =>'right',
			'index'     => 'total_commission_received',
			'type'		=> 'price',
			'currency_code' => $currencyCode,
			'filter_index'	=> 'main_table.total_commission_received'
      ));
	  
	  $this->addColumn('total_paid', array(
			'header'    => Mage::helper('affiliateplus')->__('Total Paid'),
			'width'     => '100px',
			'align'     =>'right',
			'index'     => 'total_paid',
			'type'		=> 'price',
			'currency_code' => $currencyCode,
			'filter_index'	=> 'main_table.total_paid'
      ));
		
	  /* $this->addColumn('store_id', array(
			'header'    => Mage::helper('affiliate')->__('Store view'),
			'align'     =>'left',
			'index'     =>'store_id',
			'type'      =>'store',
			'width'     => '150px',
			'store_all' =>true,
			'store_view'=>true,
	  )); */
	  
	  //add event to add more column 
	  Mage::dispatchEvent('affiliateplus_adminhtml_add_column_account_grid', array('grid' => $this));
	  
      $this->addColumn('status', array(
          'header'    => Mage::helper('affiliateplus')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
		  'filter_index'	=> 'main_table.status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
		  
      ));
	  
	  $this->addColumn('approved', array(
          'header'    => Mage::helper('affiliateplus')->__('Approved'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'approved',
		  'filter_index'	=> 'main_table.approved',
          'type'      => 'options',
          'options'   => array(
              1 => 'Yes',
              2 => 'No',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('affiliateplus')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('affiliateplus')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('affiliateplus')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('affiliateplus')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
		$storeId = $this->getRequest()->getParam('store');
        $this->setMassactionIdField('account_id');
        $this->getMassactionBlock()->setFormFieldName('account');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('affiliateplus')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('affiliateplus')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('affiliateplus/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('affiliateplus')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true, 'store'=>$storeId)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('affiliateplus')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId(), 'store' => $this->getRequest()->getParam('store')));
	}

    /**
     * @return mixed
     */
    public function getGridUrl()
	{
		return $this->getUrl('*/*/grid', array('_current'=>true));
	}

}