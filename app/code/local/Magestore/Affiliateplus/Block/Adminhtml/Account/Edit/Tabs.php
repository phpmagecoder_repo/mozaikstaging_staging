<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Adminhtml_Account_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Magestore_Affiliateplus_Block_Adminhtml_Account_Edit_Tabs constructor.
     */
    public function __construct()
	{
		parent::__construct();
		$this->setId('account_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('affiliateplus')->__('Account Information'));
	}

    /**
     * @return mixed
     */
    protected function _beforeToHtml()
	{
		$id = $this->getRequest()->getParam('id');
		
		// if(!$id){
			// $this->addTab('customer_section', array(
				// 'label'     => Mage::helper('affiliateplus')->__('Import Customer Info'),
				// 'title'     => Mage::helper('affiliateplus')->__('Import Customer Info'),
				// 'url'		=> $this->getUrl('*/*/customer',array('_current'=>true)),
		  		// 'class'     => 'ajax',
			// ));
			
		// }
		
		//event to add more tab
		Mage::dispatchEvent('affiliateplus_adminhtml_add_account_tab', array('form' => $this, 'id' => $id));
		
		$this->addTab('general_section', array(
			'label'     => Mage::helper('affiliateplus')->__('General Information'),
			'title'     => Mage::helper('affiliateplus')->__('General Information'),
			'content'   => $this->getLayout()->createBlock('affiliateplus/adminhtml_account_edit_tab_form')->toHtml(),
		));
        
        $this->addTab('form_section', array(
			'label'     => Mage::helper('affiliateplus')->__('Payment Information'),
			'title'     => Mage::helper('affiliateplus')->__('Payment Information'),
			'content'   => $this->getLayout()->createBlock('affiliateplus/adminhtml_account_edit_tab_paymentinfo')->toHtml(),
		));
		
		if($id){
			$this->addTab('transaction_section', array(
				'label'     => Mage::helper('affiliateplus')->__('History transaction'),
				'title'     => Mage::helper('affiliateplus')->__('History transaction'),
				'url'		=> $this->getUrl('*/*/transaction',array('_current'=>true)),
		  		'class'     => 'ajax',  
			));
			
			$this->addTab('payment_section', array(
				'label'     => Mage::helper('affiliateplus')->__('History Withdrawal'),
				'title'     => Mage::helper('affiliateplus')->__('History Withdrawal'),
				'url'		=> $this->getUrl('*/*/payment',array('_current'=>true)),
		  		'class'     => 'ajax',
			));

//			$this->addTab('product_section', array(
//				'label' => Mage::helper('affiliateplus')->__('Products'),
//				'title' => Mage::helper('affiliateplus')->__('Products'),
//				'url' => $this->getUrl('*/*/product', array('_current' => true)),
//				'class' => 'ajax',
//			));

			/*customize by adam 27/08/2016: add lifetime customer tab */
			if(Mage::helper('affiliateplus/config')->getCommissionConfig('life_time_sales'))
				$this->addTab('lifetimecustomer_section', array(
					'label'     => Mage::helper('affiliateplus')->__('Lifetime Customer'),
					'title'     => Mage::helper('affiliateplus')->__('Lifetime Customer'),
					'url'		=> $this->getUrl('*/*/lifetimecustomer',array('_current'=>true)),
					'class'     => 'ajax',
				));
		}
		
		$this->setActiveTab('general_section');
		return parent::_beforeToHtml();
	}

    /**
     * @param $tabId
     * @param $tab
     * @param $afterTabId
     */
    public function addTabAfter($tabId, $tab, $afterTabId)
    {
        $this->addTab($tabId, $tab);
        $this->_tabs[$tabId]->setAfter($afterTabId);
    }
}