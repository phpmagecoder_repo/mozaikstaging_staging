<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Adminhtml_Payment_Renderer_Actions
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row){
            $edit=$this->getUrl('*/*/edit', array('id' => $row->getId()));
            $cancel=$this->getUrl('*/*/cancelPayment', array('id' => $row->getId()));
            if($row->getStatus()<=2){
                return sprintf('<a href="%s" title="%s">%s</a> | <a href="%s" title="%s">%s</a>',
				$edit,
				Mage::helper('affiliateplus')->__('Edit Withdrawals'),
				Mage::helper('affiliateplus')->__('Edit'),
                                $cancel,
				Mage::helper('affiliateplus')->__('Cancel Withdrawals'),
				Mage::helper('affiliateplus')->__('Cancel')
			);
            }  else {
                return sprintf('<a href="%s" title="%s">%s</a>',
				$edit,
				Mage::helper('affiliateplus')->__('Edit Withdrawals'),
				Mage::helper('affiliateplus')->__('Edit')
			);
            }
	}
}