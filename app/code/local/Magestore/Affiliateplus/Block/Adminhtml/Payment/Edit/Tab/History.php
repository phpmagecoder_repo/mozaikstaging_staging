<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Adminhtml_Payment_Edit_Tab_History extends Mage_Adminhtml_Block_Template
{
    /**
     * @return mixed
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        return $this->setTemplate('affiliateplus/payment/history.phtml');
    }

    /**
     * @return mixed
     */
    public function getFullHistory() {
        if (!$this->hasData('collection')) {
            $collection = Mage::getResourceModel('affiliateplus/payment_history_collection')
                ->addFieldToFilter('payment_id', $this->getPayment()->getId());
            $collection->getSelect()->order('created_time DESC');
            $this->setData('collection', $collection);
        }
        return $this->getData('collection');
    }

    /**
     * @return mixed
     */
    public function getCollection() {
        return $this->getFullHistory();
    }

    /**
     * @return Varien_Object
     */
    public function getPayment() {
        if (Mage::registry('payment_data')) {
            return Mage::registry('payment_data');
        }
        return new Varien_Object();
    }
}
