<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Adminhtml_Payment_Review_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Magestore_Affiliateplus_Block_Adminhtml_Payment_Review_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->_blockGroup = 'affiliateplus';
        $this->_controller = 'adminhtml_payment_review';
		
        $this->_removeButton('reset');
        $this->_removeButton('delete');
        
        $this->_updateButton('back', 'onclick', 'backToEdit()');
        $this->_updateButton('save', 'label', Mage::helper('adminhtml')->__('Pay'));
        
        $backUrl = $this->getUrl('*/*/cancelReview', array(
            'id' => $this->getRequest()->getParam('id'),
            'store' => $this->getRequest()->getParam('store')
        ));
        $this->_formInitScripts[] = "
            function backToEdit(){
                editForm.submit('$backUrl');
            }
        ";
    }

    /**
     * @return mixed
     */
    public function getHeaderText()
    {
        return Mage::helper('affiliateplus')->__('Review your payment and pay');
    }
}