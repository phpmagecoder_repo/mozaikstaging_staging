<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Adminhtml_Banner_View extends Mage_Core_Block_Template
{
    /**
     * @return $this
     */
    protected function _beforeToHtml()
	{
		parent::_beforeToHtml();
		
		$banner = Mage::registry('banner'); 
		
		if(!$banner)
			$banner = $this->getBanner();
		if(!$banner)
			$bannerType = $this->getBannerType();
		else
			$bannerType = $banner->getTypeId();	
		if($bannerType == '1')
			$this->setTemplate('affiliateplus/banner/imageview.phtml');
		elseif($bannerType == '2')
			$this->setTemplate('affiliateplus/banner/flashview.phtml');
		
		return $this;
	}
}