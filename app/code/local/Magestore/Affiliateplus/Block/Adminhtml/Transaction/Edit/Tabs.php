<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliateplus_Block_Adminhtml_Transaction_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * Magestore_Affiliateplus_Block_Adminhtml_Transaction_Edit_Tabs constructor.
     */
    public function __construct()
	{
		parent::__construct();
		$this->setId('transaction_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('affiliateplus')->__('Transaction Information'));
	}

    /**
     * @return mixed
     */
    protected function _beforeToHtml()
	{
		// Changed By Adam 27/08/2016: create transaction from existed order
		$transaction = $this->getTransaction();
		$id = $this->getRequest()->getParam('id');
		if(!$id)
		{
			$this->addTab('form_listorder', array(
				'label'     => Mage::helper('affiliateplus')->__('Select Order'),
				'title'     => Mage::helper('affiliateplus')->__('Select Order'),
				'class'     => 'ajax',
				'url'   => $this->getUrl('*/*/listorder',array('_current'=>true,'id'=>$this->getRequest()->getParam('id'))),
			));
		}
		// End code

		$this->addTab('form_section', array(
			'label'     => Mage::helper('affiliateplus')->__('Transaction Information'),
			'title'     => Mage::helper('affiliateplus')->__('Transaction Information'),
			'content'   => $this->getLayout()->createBlock('affiliateplus/adminhtml_transaction_edit_tab_form')->toHtml(),
		));
		
		//event to add more tab
		Mage::dispatchEvent('affiliateplus_adminhtml_add_transaction_tab', array('form' => $this));
		
		return parent::_beforeToHtml();
	}

    /**
     * @param $tabId
     * @param $tab
     * @param string $afterTabId
     */
    public function addTabAfter($tabId, $tab, $afterTabId = '')
    {
        $this->addTab($tabId, $tab);
        $this->_tabs[$tabId]->setAfter($afterTabId);
    }
}