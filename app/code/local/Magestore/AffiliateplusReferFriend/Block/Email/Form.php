<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_AffiliateplusReferFriend_Block_Email_Form
    extends Magestore_AffiliateplusReferFriend_Block_Refer
{
    /**
     * @return mixed
     */
    public function getDefaultEmailContent() {
        $content = $this->_getHelper()->getReferConfig('email_content');
        $url = $this->getRequest()->getParam('url');
        $url = $url ? $url : $this->getPersonalUrl();
        return str_replace(
            array(
                '{{store_name}}',
                '{{personal_url}}',
                '{{account_name}}'
            ), array(
                Mage::app()->getStore()->getFrontendName(),
                $url,
                $this->getAccount()->getName()
            ), $content
        );
    }
}
