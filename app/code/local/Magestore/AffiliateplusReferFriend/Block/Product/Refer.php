<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_AffiliateplusReferFriend_Block_Product_Refer extends Mage_Core_Block_Template
{
    protected function _construct() {
        parent::_construct();
        $this->setTemplate('affiliateplusreferfriend/product/refer.phtml');
        $this->setData('generate_javascript', true);
    }

    /**
     * @return bool
     */
    public function getGenerateJavascript() {
        if ($this->getData('generate_javascript')) {
            $this->setData('generate_javascript', false);
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getJsonEmail() {
        $result = array(
             'yahoo' => "http://mail.yahoo.com",
             'gmail' => "http://gmail.com",
             'hotmail'	=> "http://hotmail.com",
           // 'yahoo' => $this->getUrl('affiliateplus/refer/yahoo'),
           // 'gmail' => $this->getUrl('affiliateplus/refer/gmail'),
                //'hotmail'	=> $this->getUrl('affiliateplus/refer/hotmail'),
        );
        return Zend_Json::encode($result);
    }

    /**
     * @param $product
     * @return mixed
     */
    public function getAffiliateUrl($product) {
        $productUrl = $product->getProductUrl();
        return Mage::helper('affiliateplus/url')->addAccToUrl($productUrl);
    }
}
