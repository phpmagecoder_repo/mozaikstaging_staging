<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_AffiliateplusReferFriend_Block_Product_List
    extends Mage_Catalog_Block_Product_List
{
    /**
     * @return mixed
     */
    public function isEnableShareFriend()
    {
        if ($this->hasData('is_enable_share_friend')) {
            return $this->getData('is_enable_share_friend');
        }
        if (Mage::helper('affiliateplus/account')->accountNotLogin()) {
            $this->setData('is_enable_share_friend', false);
        } else {
            $this->setData('is_enable_share_friend',
                Mage::helper('affiliateplus/config')->getReferConfig('refer_enable_product_list')
            );
        }
        return $this->getData('is_enable_share_friend');
    }

    /**
     * @param $product
     * @param bool $displayMinimalPrice
     * @param string $idSuffix
     * @return string
     */
    public function getPriceHtml($product, $displayMinimalPrice = false, $idSuffix = '')
    {
        $html = parent::getPriceHtml($product, $displayMinimalPrice, $idSuffix);
        $addProductBlock = $this->getLayout()->createBlock('affiliateplus/affiliateplus')
            ->setProduct($product)
            ->setTemplate('affiliateplus/account/productlist.phtml')->renderView();
        $blockHtml = '';
        if ($this->isEnableShareFriend()) {
            // Add share friend for product list page
            $block = Mage::getBlockSingleton('affiliateplusreferfriend/product_refer');
            $block->setProduct($product);
            $blockHtml = $block->toHtml();
        }

        $html = $blockHtml. $addProductBlock . $html;

        return $html;
    }
}
