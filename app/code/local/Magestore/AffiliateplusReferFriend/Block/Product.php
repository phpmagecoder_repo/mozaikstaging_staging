<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_AffiliateplusReferFriend_Block_Product extends Mage_Core_Block_Template
{
    /**
     * @return bool
     */
    public function isEnableShareFriend()
    {
        if (Mage::helper('affiliateplus/account')->accountNotLogin()) {
            return false;
        }
        return Mage::helper('affiliateplus/config')->getReferConfig('refer_enable_product_detail');
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * @param $product
     * @return mixed
     */
    public function getAffiliateUrl($product)
    {
        $productUrl = $product->getProductUrl();
        return Mage::helper('affiliateplus/url')->addAccToUrl($productUrl);
    }

    /**
     * @return mixed
     */
    public function getJsonEmail() {
        $result = array(
            'yahoo' => $this->getUrl('*/*/yahoo'),
            'gmail' => $this->getUrl('*/*/gmail'),
                //'hotmail'	=> $this->getUrl('*/*/hotmail'),
        );
        return Zend_Json::encode($result);
    }

    /**
     * @param $product
     * @return mixed
     */
    public function getShareIconsHtml($product) {
        $block = Mage::getBlockSingleton('affiliateplusreferfriend/product_refer');
        $block->setProduct($product);
        return $block->toHtml();
    }
}
