<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_AffiliateplusReferFriend_Block_Refer_Abstract extends Mage_Core_Block_Template
{
	/**
	 * get Contacts List to show for select
	 * 
	 * @return array
	 */
	public function getContacts(){
		return array();
	}

    /**
     * @param $contact
     * @return string
     */
    public function getEmailValue($contact){
		if (isset($contact['name']) && trim($contact['name'])){
			return $contact['name'] . '<' . $contact['email'] . '>';
		}
		return $contact['email'];
	}
}
