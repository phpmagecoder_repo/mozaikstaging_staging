<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_AffiliateplusReferFriend_Model_Refer_Gmail extends Zend_Oauth_Consumer
{
	protected $_options = null;

    /**
     * Magestore_AffiliateplusReferFriend_Model_Refer_Gmail constructor.
     */
    public function __construct(){
		$this->_config = new Zend_Oauth_Config;
		$this->_options = array(
			'consumerKey'       => $this->_getConsumerKey(),
			'consumerSecret'    => $this->_getConsumerSecret(),
			'signatureMethod'   => 'HMAC-SHA1',
			'version'           => '1.0',
			'requestTokenUrl'   => 'https://www.google.com/accounts/OAuthGetRequestToken',
			'accessTokenUrl'    => 'https://www.google.com/accounts/OAuthGetAccessToken',
			'authorizeUrl'      => 'https://www.google.com/accounts/OAuthAuthorizeToken'
		);
		$this->_config->setOptions($this->_options);
	}
	
	/**
	 * get Helper
	 *
	 * @return Magestore_Affiliateplus_Helper_Config
	 */
	public function _getHelper(){
		return Mage::helper('affiliateplus/config');
	}

    /**
     * @return mixed
     */
    protected function _getConsumerKey(){
		return $this->_getHelper()->getReferConfig('google_consumer_key');
	}

    /**
     * @return mixed
     */
    protected function _getConsumerSecret(){
		return $this->_getHelper()->getReferConfig('google_consumer_secret');
	}

    /**
     * @param $url
     */
    public function setCallbackUrl($url){
		$this->_config->setCallbackUrl($url);
	}

    /**
     * @return array|null
     */
    public function getOptions(){
		return $this->_options;
	}

    /**
     * @return mixed
     */
    public function getCoreSession(){
		return Mage::getSingleton('core/session');
	}

    /**
     * @return mixed
     */
    public function getGmailRequestToken(){
		return $this->getCoreSession()->getAffiliateGmailRequestToken();
	}

    /**
     * @param $token
     * @return $this
     */
    public function setGmailRequestToken($token){
		$this->getCoreSession()->setAffiliateGmailRequestToken($token);
		return $this;
	}

    /**
     * @return bool
     */
    public function isAuth(){
		$requestToken = $this->getGmailRequestToken();
		$request = Mage::app()->getRequest();
		if ($requestToken && $request->getParam('oauth_token') && $request->getParam('oauth_verifier'))
			return true;
		return false;
	}

    /**
     * @return mixed
     */
    public function getAuthUrl(){
		$this->setCallbackUrl(Mage::getUrl('*/*/gmail'));
		$token = $this->getRequestToken(array('scope' => 'https://www.google.com/m8/feeds/'));
		$this->setGmailRequestToken(serialize($token));
		return $this->getRedirectUrl();
	}
}