<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_AffiliateplusReferFriend_Model_System_Config_Source_Dynamiccomment_Comment extends Mage_Core_Model_Config_Data {

    /**
     * @param Mage_Core_Model_Config_Element $element
     * @param $currentValue
     * @return string
     */
    public function getCommentText(Mage_Core_Model_Config_Element $element, $currentValue) {
        if(((string)$element->source_model)=='affiliateplusreferfriend/system_config_source_parametervalue'){
        $result = "<p class='note' id='dynamic_comment'></p>";
        $result .= "<script type='text/javascript'>
            function update_commment_content()
            {
             var comment = $('dynamic_comment');
             var content = 42;
             var param = $('affiliateplus_general_url_param').getValue();
             if(param=='')param = 'acc';
             if($('affiliateplus_general_url_param_value').getValue()==1)var content = 'cfcd208495d565ef66e7dff9f98764da' ;
                comment.innerHTML = 'Ex: " . Mage::getUrl() . "?'+ param + '=' + content;        
            }

            function init_comment()
            {
              update_commment_content();
                $('affiliateplus_general_url_param_value').observe('change', function(){
                update_commment_content();
                });
                $('affiliateplus_general_url_param').observe('change', function(){
                update_commment_content();
                });
            }
            document.observe('dom:loaded', function(){init_comment();});
            </script>";
        return $result;
    }
    }

}
