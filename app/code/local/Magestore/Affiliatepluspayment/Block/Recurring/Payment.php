<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliatepluspayment_Block_Recurring_Payment extends Magestore_Affiliateplus_Block_Account_Edit {

    /**
     * @return $this
     */
    protected function _construct() {

        parent::_construct();
        if (Mage::getStoreConfig('affiliateplus_payment/recurring/enable') && count($this->getMethodArr())) {
            $this->setTemplate('affiliatepluspayment/recurring/payment.phtml');
        } else {
            $this->setTemplate('affiliateplus/account/edit.phtml');
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getMethodArr() {
        $methodpayment = array();
        if ($this->paypalActive())
            $methodpayment['paypal'] = 'PayPal';
        if ($this->moneybookerActive())
            $methodpayment['moneybooker'] = 'MoneyBookers';
        return $methodpayment;
    }

    /**
     * @return mixed
     */
    public function getRecurringPayment() {
        return $this->getAccount()->getRecurringPayment();
    }

    /**
     * @return mixed
     */
    public function getRecurringMethod() {
        return $this->getAccount()->getRecurringMethod();
    }

    /**
     * @return mixed
     */
    public function getMoneybookerEmail() {
        return $this->getAccount()->getMoneybookerEmail();
    }

    /**
     * @return mixed
     */
    public function moneybookerActive() {
        return Mage::getStoreConfig('affiliateplus_payment/moneybooker/active');
    }

    /**
     * @return mixed
     */
    public function paypalActive() {
        return Mage::getStoreConfig('affiliateplus_payment/paypal/active');
    }

    /**
     * @return bool
     */
    public function moneybookerDisplay() {
        if (!$this->paypalActive())
            return TRUE;
        if (($this->moneybookerActive() && ($this->getRecurringMethod() == 'moneybooker')))
            return TRUE;
    }

}