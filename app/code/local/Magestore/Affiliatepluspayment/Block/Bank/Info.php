<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliatepluspayment_Block_Bank_Info extends Magestore_Affiliateplus_Block_Payment_Info
{
    /**
     * @return $this
     */
    public function _prepareLayout(){
		parent::_prepareLayout();
		$this->setTemplate('affiliatepluspayment/bank/info.phtml');
		return $this;
    }

    /**
     * @return mixed
     */
    public function getBankStatement(){
        if(!$this->hasData('bank_statement')){
            $payment = $this->getPaymentMethod();
            $account = Mage::getSingleton('affiliateplus/session')->getAccount();
            $bankaccountId = $payment->getPaymentBankaccountId() ? $payment->getPaymentBankaccountId() : $payment->getBankaccountId();
            $bankaccountId = $bankaccountId ? $bankaccountId : 0;
            $verify = Mage::getModel('affiliateplus/payment_verify')->loadExist($account->getId(), $bankaccountId, 'bank');            
            
            $this->setData('bank_statement',$verify->getInfo());
        }
        return $this->getData('bank_statement');
    }
}