<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliatepluspayment_Block_Offline_Info extends Magestore_Affiliateplus_Block_Payment_Info
{
    /**
     * @return $this
     */
    public function _prepareLayout(){
		parent::_prepareLayout();
		$this->setTemplate('affiliatepluspayment/offline/info.phtml');
		return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoiceAddress(){
        if(!$this->hasData('invoice_address')){
            $payment = $this->getPaymentMethod();
            $account = Mage::getSingleton('affiliateplus/session')->getAccount();
            $addressId = $payment->getAccountAddressId() ? $payment->getAccountAddressId() : $payment->getAddressId();
            $addressId = $addressId ? $addressId : 0 ;
            $verify = Mage::getModel('affiliateplus/payment_verify')->loadExist($account->getId(), $addressId, 'offline');
            $this->setData('invoice_address',$verify->getInfo());
        }
        return $this->getData('invoice_address');
    }
}