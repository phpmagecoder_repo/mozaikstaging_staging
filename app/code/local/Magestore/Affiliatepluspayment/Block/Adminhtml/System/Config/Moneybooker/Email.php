<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliatepluspayment_Block_Adminhtml_System_Config_Moneybooker_Email extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element){
		$this->setElement($element);
		return $this->_toHtml();
	}

    /**
     * @param int $store
     * @return mixed
     */
    public function getValue($store = 0){
        $value = Mage::getStoreConfig('affiliateplus_payment/moneybooker/moneybooker_email',$store);
        return $value;
    }

    /**
     * @return string
     */
    protected function _toHtml(){
        $value = $this->getValue();
        $store = $this->getRequest()->getParam('store');
        $storeId = Mage::app()->getStore($store)->getId();
        $default = Mage::getStoreConfig('affiliateplus_payment/moneybooker/user_mechant_email_default');
        $defaultStore = Mage::getStoreConfig('affiliateplus_payment/moneybooker/user_mechant_email_default',$storeId);
        
        $valueStore = $this->getValue($storeId);
        $style = '';
        $display = '';
        $disabled = '';
        $checked = '';
        if($defaultStore) $display = 'none';
        if($default == $defaultStore) $checked = 'checked';
        if($storeId && ($value == $valueStore)) $disabled = 'disabled';
        if($default) $style = '$("row_affiliateplus_payment_moneybooker_moneybooker_email").style.display = "none"';
        return '<input id="affiliateplus_payment_moneybooker_moneybooker_email" name="groups[moneybooker][fields][moneybooker_email][value]" value="'.$valueStore.'" class=" input-text" '.$disabled.' type="text">
                <script type="text/javascript">
                    $("row_affiliateplus_payment_moneybooker_moneybooker_email").style.display="'.$display.'";
                    if($("affiliateplus_payment_moneybooker_user_mechant_email_default_inherit"))
                        $("affiliateplus_payment_moneybooker_user_mechant_email_default_inherit").checked = "'.$checked.'"
                    '.$style.'
                </script>
                ';
    }

        /**
	 * Constructor for block 
	 * 
	 */
	public function __construct(){
		parent::__construct();		
	}
}