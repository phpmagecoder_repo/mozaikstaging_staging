<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliatepluspayment_Model_Source_Period
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(){
        return array(
			array('value' => '7', 'label'=>Mage::helper('affiliateplus')->__('Weekly')),
			array('value' => '30', 'label'=>Mage::helper('affiliateplus')->__('Monthly')),
			array('value' => '365', 'label'=>Mage::helper('affiliateplus')->__('Yearly')),
			array('value' => '0', 'label'=>Mage::helper('affiliateplus')->__('Custom Period')),
        );
    }
}