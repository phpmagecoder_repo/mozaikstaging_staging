<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliatepluspayment_Model_Bankaccount extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('affiliatepluspayment/bankaccount');
    }

    /**
     * @param bool $isHtml
     * @return string
     */
    public function format($isHtml = true){
    	if ($isHtml){
    		$html = Mage::helper('affiliatepluspayment')->__('Bank: %s',$this->getName()).'<br />';
    		$html .= Mage::helper('affiliatepluspayment')->__('Account: %s',$this->getAccountName()).'<br />';
    		$html .= Mage::helper('affiliatepluspayment')->__('Acc Number: %s',$this->getAccountNumber()).'<br />';
    		if ($this->getRoutingCode())
    			$html .= Mage::helper('affiliatepluspayment')->__('Routing Code: %s',$this->getRoutingCode()).'<br />';
                if ($this->getSwiftCode())
    			$html .= Mage::helper('affiliatepluspayment')->__('SWIFT Code: %s',$this->getSwiftCode()).'<br />';
    		if ($this->getAddress())
    			$html .= Mage::helper('affiliatepluspayment')->__('Bank Address: %s',$this->getAddress()).'<br />';
            /*if ($this->getBankStatement())
    			$html .= Mage::helper('affiliatepluspayment')->__('Bank Statement <br /> %s',$this->getBankStatement()).'<br />';*/
    		return $html;
    	}
    	return sprintf('%s, %s, %s',$this->getAccountName(),$this->getAccountNumber(),$this->getName());
    }
    
    public function getBankStatement(){
        if($this->getId() ){
            $verify = Mage::getModel('affiliateplus/payment_verify')->loadExist($this->getAccountId(), $this->getId(), 'bank');
            return $verify->getInfo();
        }
        return ;
    }

    /**
     * @return mixed
     */
    public function getAccount(){
        return Mage::getSingleton('affiliateplus/session')->getAccount();
    }

    /**
     * @param $account
     * @return null
     */
    public function getBankAccounts($account){
    	if (!$account) return null;
    	$collection = $this->getCollection()
    		->addFieldToFilter('account_id',$account->getId());
    	return $collection;
    }

    /**
     * @return array|bool
     */
    public function validate(){
    	$errors = array();
    	
    	if (!$this->getName())
    		$errors[] = Mage::helper('affiliatepluspayment')->__('Bank name is empty.');
    	if (!$this->getAccountName())
    		$errors[] = Mage::helper('affiliatepluspayment')->__('Bank account name is empty.');
    	if (!$this->getAccountNumber())
    		$errors[] = Mage::helper('affiliatepluspayment')->__('Bank account number is empty.');
    	
    	if (count($errors) == 0)
    		return false;
    	return $errors;
    }
}