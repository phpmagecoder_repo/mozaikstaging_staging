<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Affiliateplus
 * @module     Affiliateplus
 * @author      Magestore Developer
 *
 * @copyright   Copyright (c) 2016 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 *
 */

class Magestore_Affiliatepluspayment_Helper_Paypal extends Magestore_Affiliateplus_Helper_Payment_Paypal
{
    /**
     * @param $data
     * @return string
     */
    public function getPaymanetUrl($data) {
        $url = $this->_getMasspayUrl();
        $i = 0;
        $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
        foreach ($data as $item) {
            $url .= '&L_EMAIL' . $i . '=' . $item['email'] . '&L_AMT' . $i . '=' . $item['amount'] . '&CURRENCYCODE' . $i . '=' . $baseCurrencyCode;
            $i++;
        }
        return $url;
    }

    /**
     * @return string
     */
    protected function _getMasspayUrl() {
        $url = $this->_getApiEndpoint();
        $url .= '&METHOD=MassPay&RECEIVERTYPE=EmailAddress';
        return $url;
    }

    /**
     * @return string
     */
    protected function _getApiEndpoint() {
        if (Mage::getStoreConfig('affiliateplus_payment/paypal/user_mechant_email_default')) {
            $isSandbox = Mage::getStoreConfig('paypal/wpp/sandbox_flag');
        } else {
            $isSandbox = Mage::getStoreConfig('affiliateplus_payment/paypal/sandbox_mode');
        }
        
        $paypalApi = $this->_getPaypalApi();
        $url = sprintf('https://api-3t%s.paypal.com/nvp?', $isSandbox ? '.sandbox' : '');
        $url .= 'USER=' . $paypalApi['api_username'] . '&PWD=' . $paypalApi['api_password'] . '&SIGNATURE=' . $paypalApi['api_signature'] . '&VERSION=62.5';
        return $url;
    }

    /**
     * @return mixed
     */
    protected function _getPaypalApi() {
        if (Mage::getStoreConfig('affiliateplus_payment/paypal/user_mechant_email_default')) {
            $data['api_username'] = Mage::getStoreConfig('paypal/wpp/api_username');
            $data['api_password'] = Mage::getStoreConfig('paypal/wpp/api_password');
            $data['api_signature'] = Mage::getStoreConfig('paypal/wpp/api_signature');
        } else {
            $data['api_username'] = Mage::getStoreConfig('affiliateplus_payment/paypal/api_username');
            $data['api_password'] = Mage::getStoreConfig('affiliateplus_payment/paypal/api_password');
            $data['api_signature'] = Mage::getStoreConfig('affiliateplus_payment/paypal/api_signature');
        }
        return $data;
    }
}