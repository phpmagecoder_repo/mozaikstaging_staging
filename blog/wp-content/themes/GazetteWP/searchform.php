<?php
/**
 * The template for displaying search forms in Twenty Eleven
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="text" class="searchField" name="s" id="s" placeholder="<?php esc_attr_e( 'Start Typing ...', 'localization' ); ?>" />
	<span class="close"><?php _e( 'close x', 'localization' ); ?></span>
</form>
