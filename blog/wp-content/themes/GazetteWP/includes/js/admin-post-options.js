jQuery(document).ready(function($){

	function check_post_type() {

		var post_type = $('#gz_post_type').val();

		$('#gz_ratings_settings_array').closest('.format-settings').hide();

		if ( 'review' == post_type )
			$('#gz_ratings_settings_array').closest('.format-settings').show();

	}

	check_post_type();
	
	$('#gz_post_type').change(function(){
		check_post_type();
	});

});