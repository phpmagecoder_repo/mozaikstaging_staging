<?php
/**
* Initialize the meta boxes. 
*/
add_action( 'admin_init', '_custom_meta_boxes' );

/**
* Post Options
*/
function _custom_meta_boxes() {

	$common_options = array(
		'id'          => 'gz_common_options',
		'title'       => 'Page Options',
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'label'       => 'Layout',
				'id'          => 'sidebar',
				'type'        => 'select',
				'std'         => 'no',
				'desc'        => '',
				'choices'     => array(
					array(
						'label'       => 'With Sidebar',
						'value'       => 'yes'
					),
					array(
						'label'       => 'No Sidebar',
						'value'       => 'no'
					)
				),
			)
		)
	);

	$post_options = array(
		'id'          => 'gz_post_options',
		'title'       => 'Post Options',
		'desc'        => '',
		'pages'       => array('post'),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'label'       => 'Layout',
				'id'          => 'sidebar',
				'type'        => 'select',
				'std'         => 'yes',
				'desc'        => '',
				'choices'     => array(
					array(
						'label'       => 'With Sidebar',
						'value'       => 'yes'
					),
					array(
						'label'       => 'No Sidebar',
						'value'       => 'no'
					)
				),
			),
			array(
				'id'      => 'gz_post_type',
				'label'   => 'Post Type',
				'desc'    => 'Choose the post type.',
				'std'     => 'regular',
				'type'    => 'select',
				'class'   => '',
				'choices' => array(
					array(
						'value' => 'regular',
						'label' => 'Regular',
					),
					array(
						'value' => 'video',
						'label' => 'Video',
					),
					array(
						'value' => 'review',
						'label' => 'Review'   
					)
				)
			),
			array (
				'id'      => 'gz_ratings',
				'label'   => 'Ratings',
				'desc'    => 'Set the ratings.',
				'std'     => '',
				'type'    => 'list_item',
				'class'   => '',
				'settings'    => array(    
					array(
						'label'       => 'Number Rating',
						'id'          => 'gz_rating_num',
						'type'        => 'text',
					),
					array(
						'label'       => 'Textual Rating',
						'id'          => 'gz_rating_text',
						'type'        => 'text',
					)
				)
			)
		)
	);  


	/**
	* Register our meta boxes using the 
	* ot_register_meta_box() function.
	*/

	ot_register_meta_box( $common_options );
	ot_register_meta_box( $post_options );

}