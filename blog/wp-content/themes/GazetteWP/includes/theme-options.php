<?php
/**
 * Initialize the options before anything else.
 */
add_action( 'admin_init', 'custom_theme_options', 1 );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
	
	/**
	 * Generate options for category colors
	 */

	// Get categories
	$cats = get_categories('hide_empty=0');

	// If there are categories
	if ( $cats ) {

		// Loop the categories
		foreach ( $cats as $cat ) {
			
			// Generate option
			$cat_color_options[] = array(
				'id'      => 'gz_cat_color_' . $cat->term_id,
				'label'   => 'Category BG Color - ' . $cat->name,
				'desc'    => 'The background color for the <strong>' . $cat->name . '</strong> category.',
				'std'     => '#000000',
				'type'    => 'colorpicker',
				'section' => 'gz_categories',
				'class'   => '',
			);

		}

	}

	/**
	 * Get a copy of the saved settings array. 
	 */
	$saved_settings = get_option( 'option_tree_settings', array() );
	
	/**
	 * Custom settings array that will eventually be 
	 * passes to the OptionTree Settings API Class.
	 */
	$custom_settings = array( 
		'contextual_help' => array(
			'sidebar'       => ''
		),
		'sections'        => array( 
			array(
				'id'          => 'general',
				'title'       => 'General'
			),
			array(
				'id'          => 'header',
				'title'       => 'Header'
			),
			array(
				'id'          => 'layout',
				'title'       => 'Layout'
			),
			array(
				'id' => 'gz_categories',
				'title' => 'Categories'   
			),
			array(
				'id' => 'gz_other',
				'title' => 'Other'
			),
			array(
				'id' => 'gz_code',
				'title' => 'Custom Code'   
			)
		),
		'settings'        => array(
			
			/**
			 * GENERAL
			 */

			array(
				'id'          => 'logo',
				'label'       => 'Logo',
				'desc'        => 'Upload the logo.',
				'std'         => '',
				'type'        => 'upload',
				'section'     => 'general',
				'class'       => ''
			),
			array(
				'id'          => 'gz_favicon',
				'label'       => 'Favicon',
				'desc'        => 'Upload the favicon.',
				'std'         => '',
				'type'        => 'upload',
				'section'     => 'general',
				'class'       => ''
			),
			array(
				'id'          => 'gz_analytics',
				'label'       => 'Analytics',
				'desc'        => 'Paste your analytics code here.',
				'std'         => '',
				'type'        => 'textarea_simple',
				'section'     => 'general',
				'class'       => ''
			),

			/**
			 * HEADER
			 */

			array(
				'id'          => 'topbarsearch',
				'label'       => 'Display Search Icon?',
				'desc'        => '',
				'std'         => '',
				'type'        => 'select',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => '',
				'choices'     => array( 
					array(
						'value'       => 'sdfasdf',
						'label'       => 'Make Your Choice :',
						'src'         => ''
					),
					array(
						'value'       => 'yes',
						'label'       => 'Yes',
						'src'         => ''
					),
					array(
						'value'       => 'no',
						'label'       => 'No',
						'src'         => ''
					)
				),
			),
			array(
				'id'          => 'youtubelink',
				'label'       => 'Youtube URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'twitterlink',
				'label'       => 'Twitter URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'facebooklink',
				'label'       => 'Facebook URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'vimeolink',
				'label'       => 'Vimeo URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'googlelink',
				'label'       => 'Google + URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'flickrlink',
				'label'       => 'Flickr URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'pinterestlink',
				'label'       => 'Pinterest URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'linkedinlink',
				'label'       => 'Linkedin URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'dribbblelink',
				'label'       => 'Dribbble URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'instagramlink',
				'label'       => 'Instagram URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),
			array(
				'id'          => 'behancelink',
				'label'       => 'Behance URL',
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'class'       => ''
			),

			/**
			 * LAYOUT
			 */

			array(
				'id'          => 'gz_single_thumb',
				'label'       => 'Single Post - Thumbnail',
				'desc'        => '',
				'std'         => 'no',
				'type'        => 'select',
				'section'     => 'layout',
				'class'       => '',
				'choices'     => array( 
					array(
						'value'       => 'yes',
						'label'       => 'Show Thumbnail',
						'src'         => ''
					),
					array(
						'value'       => 'no',
						'label'       => 'Do Not Show Thumbnail',
						'src'         => ''
					)
				),
			),
			array(
				'id'          => 'cat_page_sidebar',
				'label'       => 'Archives - Layout',
				'desc'        => '',
				'std'         => 'yes',
				'type'        => 'select',
				'section'     => 'layout',
				'class'       => '',
				'choices'     => array( 
					array(
						'value'       => 'yes',
						'label'       => 'With Sidebar',
						'src'         => ''
					),
					array(
						'value'       => 'no',
						'label'       => 'No Sidebar',
						'src'         => ''
					)
				),
			),
			array(
				'id'          => 'search_page_sidebar',
				'label'       => 'Search - Layout',
				'desc'        => '',
				'std'         => 'yes',
				'type'        => 'select',
				'section'     => 'layout',
				'class'       => '',
				'choices'     => array( 
					array(
						'value'       => 'yes',
						'label'       => 'With Sidebar',
						'src'         => ''
					),
					array(
						'value'       => 'no',
						'label'       => 'No Sidebar',
						'src'         => ''
					)
				),
			),

			/**
			 * Other
			 */
			array(
				'id'      => 'gz_slider_mousewheel',
				'label'   => 'Slider - Mousewheel',
				'desc'    => 'Enable/Disable mousewheel support for the slider.',
				'std'     => '1',
				'type'    => 'select',
				'section' => 'gz_other',
				'class'   => '',
				'choices' => array(
					array(
						'value'       => '1',
						'label'       => 'Enabled',
					),
					array(
						'value'       => '0',
						'label'       => 'Disabled',
					)
				)
			),
			array(
				'id'      => 'gz_share_twitter',
				'label'   => 'Share - Twitter Profile',
				'desc'    => 'This option is for the share functionality (the red square used to tweet or share a post). If profile supplied the tweet will have "via @profile" at the end.',
				'std'     => '',
				'type'    => 'text',
				'section' => 'gz_other',
				'class'   => '',
			),
			array(
				'id'          => 'gz_footer_bottom',
				'label'       => 'Footer Bottom (Logo + Menu)',
				'desc'        => '',
				'std'         => 'yes',
				'type'        => 'select',
				'section'     => 'gz_other',
				'class'       => '',
				'choices'     => array( 
					array(
						'value'       => 'yes',
						'label'       => 'Enable',
						'src'         => ''
					),
					array(
						'value'       => 'no',
						'label'       => 'Disable',
						'src'         => ''
					)
				),
			),

			/**
			 * CODE
			 */

			array(
				'id'      => 'gz_code_css',
				'label'   => 'Custom CSS Code',
				'desc'    => 'Enter your custom CSS code here.',
				'std'     => '',
				'type'    => 'textarea_simple',
				'section' => 'gz_code',
				'class'   => '',
			),
			array(
				'id'      => 'gz_code_js',
				'label'   => 'Custom JavaScript Code',
				'desc'    => '<p>Enter your custom JavaScript code here.</p><p>jQuery is enabled, just make sure to wrap it within:</p>jQuery(document).ready(function($){<br>&nbsp;&nbsp;&nbsp;&nbsp;/* your code here */<br> });',
				'std'     => '',
				'type'    => 'textarea_simple',
				'section' => 'gz_code',
				'class'   => '',
			),
		)
	);
	
	// Append the category color options array to the options array
	$custom_settings['settings'] = array_merge( $custom_settings['settings'], $cat_color_options );

	/* allow settings to be filtered before saving */
	$custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );
	
	/* settings are not the same update the DB */
	if ( $saved_settings !== $custom_settings ) {
		update_option( 'option_tree_settings', $custom_settings ); 
	}
	
}