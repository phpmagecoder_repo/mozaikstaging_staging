<?php

add_action( 'widgets_init', create_function( '', 'register_widget( "dd_tabbed_widget" );' ) );
class DD_Tabbed_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
			'dd_tabbed_widget', // Base ID
			'Gazette Tabbed Info', // Name
			array( 'description' => 'Show latest posts, popular posts and latest comments in a tabbed layout.' ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$amount = $instance['amount'];

		echo $before_widget;

		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;

		/* Start - Widget Content */

		?>

			<div class="tabs-container">

				<div class="tabs-nav">
					<a href="#" class="tab-nav"><?php _e( 'Recent', 'localization' ); ?></span></a>
					<a href="#" class="tab-nav"><?php _e( 'Popular', 'localization' ); ?></span></a>
					<a href="#" class="tab-nav"><?php _e( 'Comments', 'localization' ); ?></span></a>
				</div><!-- .tabs-nav -->

				<div class="tabs-content">

					<?php
						$args = array(
							'paged' => 1, 
							'post_type' => 'post',
							'posts_per_page' => $amount,
						);
						$widget_posts = new WP_Query( $args );
					?>
					<div class="tab-content">
						<ul>
							<?php if ( $widget_posts->have_posts() ) : while ( $widget_posts->have_posts() ) : $widget_posts->the_post(); ?>
								<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php endwhile; endif; wp_reset_postdata(); ?>
						</ul>
					</div><!-- .tab-content -->

					<?php
						$args = array(
							'paged' => 1, 
							'post_type' => 'post',
							'posts_per_page' => $amount,
							'orderby' => 'comment_count',
						);
						$widget_posts = new WP_Query( $args );
					?>
					<div class="tab-content">
						<ul>
							<?php if ( $widget_posts->have_posts() ) : while ( $widget_posts->have_posts() ) : $widget_posts->the_post(); ?>
								<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php endwhile; endif; wp_reset_postdata(); ?>
						</ul>
					</div><!-- .tab-content -->

					<?php
						$args = array(
							'number' => $amount,
							'status' => 'approve',
							'type' => 'comment',
						);
						$comments = get_comments($args);
					?>
					<div class="tab-content">
						<ul>
							<?php if ( ! empty( $comments ) ) : ?>
								<?php foreach($comments as $comment) : ?>
									<li><?php echo $comment->comment_author; ?> on <a href="<?php the_permalink( $comment->comment_post_ID ); ?>"><?php echo get_the_title( $comment->comment_post_ID ); ?></a></li>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>
					</div><!-- .tab-content -->

				</div><!-- .tabs-content-wrapper -->

			</div><!-- .tabs-container -->



		<?php

		/* End - Widget Content */

		echo $after_widget;

	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['amount'] = strip_tags( $new_instance['amount'] );

		return $instance;

	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		// Get values
		if ( isset( $instance[ 'title' ] ) ) $title = $instance[ 'title' ]; else $title = 'TABBED INFO';
		if ( isset( $instance[ 'amount' ] ) ) $amount = $instance[ 'amount' ]; else $amount = '3';

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'amount' ); ?>"><?php _e( 'Amount:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'amount' ); ?>" name="<?php echo $this->get_field_name( 'amount' ); ?>" type="text" value="<?php echo esc_attr( $amount ); ?>" />
		</p>
		<?php 

	}

}