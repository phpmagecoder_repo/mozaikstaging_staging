<?php

add_action( 'widgets_init', create_function( '', 'register_widget( "dd_videos_widget" );' ) );
class DD_Videos_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
			'dd_videos_widget', // Base ID
			'Gazette Latest Videos', // Name
			array( 'description' => 'Show latest video posts.' ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$amount = $instance['amount'];
		$category = $instance['category'];

		echo $before_widget;

		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;

		/* Start - Widget Content */

		if ( $category == '' || $category == 'all' ) {

			$args = array(
				'paged' => 1, 
				'post_type' => 'post',
				'posts_per_page' => $amount,				
				'meta_key' => 'gz_post_type',
				'meta_value' => 'video'
			);

		} else {

			$cat = get_term_by( 'id', $category, 'category' );

			$args = array(
				'paged' => 1, 
				'post_type' => 'post',
				'posts_per_page' => $amount,
				'category__in' => $category,
				'meta_key' => 'gz_post_type',
				'meta_value' => 'video'
			);

		}

		$widget_posts = new WP_Query( $args );

		$count = 0;

		?>

			<ul class="videos">
				
				<?php if ( $widget_posts->have_posts() ) : while ( $widget_posts->have_posts() ) : $widget_posts->the_post(); $count++; ?>

					<?php

						// Get categories
						$cats = get_the_category();
						$cats_output = '';

						// If has categories
						if ( $cats ) {

							// Loop categories
							foreach ( $cats as $cat ) {
								
								// Generate output
								$cats_output .= '<span class="categoryBadge color-category-' . $cat->term_id . '"><a href="' . get_category_link( $cat->term_id ) . '">' . $cat->cat_name . '</a></span>';

							}

						}

					?>
								
					<li class="slider-item video">

						<div class="slider-item-thumb">
							<?php echo $cats_output; ?>
							<a class="videoIcon" href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/videoIcon.png'; ?>" alt="" /></a>
							<a href="<?php the_permalink(); ?>" class="slider-item-thumb-img"><?php the_post_thumbnail( 'post-medium' ); ?></a>
						</div>

						<div class="slider-item-info">
							<div class="slider-item-title">
								<span class="slider-item-info-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
							</div>
						</div>

					</li>

				<?php endwhile; endif; ?>
			   
			</ul>

		<?php

		/* End - Widget Content */

		echo $after_widget;

		wp_reset_query();

	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['amount'] = strip_tags( $new_instance['amount'] );
		$instance['category'] = strip_tags( $new_instance['category'] );

		return $instance;

	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		// Get values
		if ( isset( $instance[ 'title' ] ) ) $title = $instance[ 'title' ]; else $title = 'Latest Videos';
		if ( isset( $instance[ 'amount' ] ) ) $amount = $instance[ 'amount' ]; else $amount = '3';
		if ( isset( $instance[ 'category' ] ) ) $category = $instance[ 'category' ]; else $category = 'all';

		// Get categories
	    $cats = get_categories('hide_empty=0');
	    $cats_options = '<option value="all">All</option>';

	    // Generate Options
	    if ( $cats ) {
	        foreach ( $cats as $cat ) {
	        	$selected = '';
	        	if ( $category == $cat->term_id ) $selected = 'selected="selected"';
	            $cats_options .= '<option value="' . $cat->term_id . '" ' . $selected . '>' . esc_attr( $cat->name ) . '</option>'; 
	        }
	    }

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'amount' ); ?>"><?php _e( 'Amount:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'amount' ); ?>" name="<?php echo $this->get_field_name( 'amount' ); ?>" type="text" value="<?php echo esc_attr( $amount ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category:' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>"> 
				<?php echo $cats_options; ?>1
			</select>
		</p>
		<?php 

	}

}