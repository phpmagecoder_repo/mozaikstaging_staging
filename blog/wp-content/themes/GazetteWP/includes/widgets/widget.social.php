<?php

add_action( 'widgets_init', create_function( '', 'register_widget( "dd_social_widget" );' ) );
class DD_Social_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
	 		'dd_social_widget', // Base ID
			'Gazette Social', // Name
			array( 'description' => 'Show followers count.' ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$rss = $instance['rss'];
		$twitter = $instance['twitter'];
		$facebook = $instance['facebook'];

		echo $before_widget;

		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;

		/* Start - Widget Content */

    	?>

    		<ul>
                                
                <li class="rssCount">

                    <a class="icon" href="<?php if ( '' != $rss ) : echo $rss; else : bloginfo( 'rss2_url' ); endif; ?>"><i class="icon-rss"></i></a>
                    <div class="info">

                        <a class="count" href="<?php if ( '' != $rss ) : echo $rss; else : bloginfo( 'rss2_url' ); endif; ?>"><?php _e( 'RSS', 'localization' ); ?></a>
                        <a class="title" href="<?php if ( '' != $rss ) : echo $rss; else : bloginfo( 'rss2_url' ); endif; ?>"><?php _e( 'Subscribe', 'localization' ); ?></a>

                    </div>
                        
                </li>
                    
                <li class="twitterCount">
                    
                    <a class="icon" href="<?php echo $twitter; ?>"><i class="icon-twitter"></i></a>
                        
                    <div class="info">

                        <a class="count" href="<?php echo $twitter; ?>"><?php _e( 'Twitter', 'localization' ); ?></a>
                        <a class="title" href="<?php echo $twitter; ?>"><?php _e( 'Follow', 'localization' ); ?></a>
                        
                    </div>
                          
                </li>
                    
                <li class="facebookCount">
                    
                    <a class="icon" href="<?php echo $facebook; ?>"><i class="icon-facebook"></i></a>
                        
                    <div class="info">

                        <a class="count" href="<?php echo $facebook; ?>"><?php _e( 'Facebook', 'localization' ); ?></a>
                        <a class="title" href="<?php echo $facebook; ?>"><?php _e( 'Like', 'localization' ); ?></a>
                        
                    </div>
                        
                </li>
                    
            </ul>

    	<?php

		/* End - Widget Content */

		echo $after_widget;		

	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['rss'] = strip_tags( $new_instance['rss'] );
		$instance['twitter'] = strip_tags( $new_instance['twitter'] );
		$instance['facebook'] = strip_tags( $new_instance['facebook'] );

		return $instance;

	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		// Get values
		if ( isset( $instance[ 'title' ] ) ) $title = $instance[ 'title' ]; else $title = 'Stay Connected';
		if ( isset( $instance[ 'rss' ] ) ) $rss = $instance[ 'rss' ]; else $rss = '';
		if ( isset( $instance[ 'twitter' ] ) ) $twitter = $instance[ 'twitter' ]; else $twitter = 'http://twitter.com/DDStudios';
		if ( isset( $instance[ 'facebook' ] ) ) $facebook = $instance[ 'facebook' ]; else $facebook = 'https://www.facebook.com/pages/DDStudios/216879595048256';

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'rss' ); ?>"><?php _e( 'Feed URL:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'rss' ); ?>" name="<?php echo $this->get_field_name( 'rss' ); ?>" type="text" value="<?php echo esc_attr( $rss ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e( 'Twitter URL:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" type="text" value="<?php echo esc_attr( $twitter ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e( 'Facebook URL:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" type="text" value="<?php echo esc_attr( $facebook ); ?>" />
		</p>
		<?php 

	}

}