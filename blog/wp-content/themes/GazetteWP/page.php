<?php 
	
	get_header(); 

	$sidebar = get_post_meta(get_the_ID(), 'sidebar', true);
	if ( ! $sidebar ) $sidebar = 'no';
	$content_class = 'sixteen';

	if ( $sidebar == 'yes' )
		$content_class = 'eleven';

?>

<div class="pageTitle nav container clearer stretched show-bg">
	
	<div class="container">
		
		<div class="sixteen columns"><h1><?php the_title(); ?></h1></div>
	
	</div><!-- .container -->

</div><!-- .pageTitle -->
	
<?php if ( have_posts()) : while ( have_posts()) : the_post(); ?>

	<div class="pageContent">

		<div class="container">

			<div class="<?php echo $content_class; ?> columns content">
				<?php the_content(); ?>
			</div>

			<?php if ( $sidebar == 'yes' ) get_sidebar( 'pages'); ?>

		</div><!-- .container -->
		
	</div><!-- .pageContent -->

<?php endwhile; else : ?>

	<div class="post box">
		<h3><?php _e( 'There is not post available.', 'localization' ); ?></h3>
	</div>

<?php endif; ?>
				
<?php get_footer(); ?>