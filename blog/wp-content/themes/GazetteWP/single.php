<?php 
	
	get_header(); 

	// Get Sidebar
	$sidebar = get_post_meta( get_the_ID(), 'sidebar', true );
	if ( ! $sidebar ) $sidebar = 'yes';
	$content_class = 'sixteen';

	// Generate Content Class (width)
	if ( 'yes' == $sidebar )
		$content_class = 'eleven';

	// Post Type
	$post_type = get_post_meta(get_the_ID(), 'gz_post_type', true);

	$ratings = false;

	// If no sidebar, get ratings
	if ( 'review' == $post_type && 'yes' != $sidebar )
		$ratings = get_post_meta( get_the_ID(), 'gz_ratings', true );
	
	// Get category
	$cats = get_the_category();
	$cats_output = '';

	// If has categories, loop them and generate output
	if ( $cats ) {
		foreach ( $cats as $cat ) {
			$cats_output .= '<span class="categoryBadge color-category-' . $cat->term_id . '"><a href="' . get_category_link( $cat->term_id ) . '">' . $cat->cat_name . '</a></span>';
		}
	}

?>

<div class="pageTitle nav container clearer stretched show-bg">

	<div class="container">

		<div class="sixteen columns"><h1><?php the_title(); echo $cats_output; ?></h1></div>

	</div>

</div>

<?php if ( have_posts()) : while ( have_posts()) : the_post(); ?>

	<div class="pageContent">

		<div class="container post-container">

			<div class="<?php echo $content_class; ?> columns content">

				<div class="singleMeta clearfix">

					<ul>

						<li><a class="author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="icon-user"></i><?php the_author(); ?></a></li>
						
						<?php if ( has_tag() ) :  ?>
							<li><a class="tags" href="#"><i class="icon-tag"></i></a><?php the_tags('', ', ', ''); ?> </li>
						<?php endif; ?>

						<li><a class="comments" href="<?php comments_link(); ?> "><i class="icon-comment"></i> <?php comments_number( __( '0 comments', 'localization' ), __( '1 comment', 'localization' ), __( '% comments', 'localization' ) ); ?> </a></li>

						<li class="share"><?php gazette_share(); ?></li>

					</ul>
 <div class="breadcrumbs-wp">
	<?php if(function_exists('bcn_display'))
	{
		bcn_display();
	}?>
</div>

				</div><!-- .singleMeta -->
		
				<?php if ( 'yes' == ot_get_option( 'gz_single_thumb', 'no' ) ) : ?>

					<div class="singleImg">
						<?php if ( $sidebar == 'yes' ) the_post_thumbnail( 'post-big' ); else the_post_thumbnail( 'post-full' ); ?>
					</div>

				<?php endif; ?>

				<?php if ( $ratings ) : ?>

					<div class="pageReview">

						<div class="widget_reviewInfo clearfix">

							<ul class="clearfix">

								<?php 
									
									// Vars
									$count = 0; 
									$real_count = 0;
									$ratings_found = count( $ratings );

									// Loop ratings
									foreach ( $ratings as $rating ) : $count++; $real_count++;

										// Rating Class
										$rating_class = 'odd';
										if ( $count == 2 ) {
											$rating_class = 'even';
											$count = 0;
										}
										if ( $real_count == $ratings_found ) 
											$rating_class .= ' result';

										?>
								
										<li class="<?php echo $rating_class; ?>">
											<div class="title">
												<span><?php echo $rating['title']; ?></span>
												<span class="categoryBadge"><?php echo $rating['gz_rating_text']; ?></span>
											</div>
											<div class="note"><?php echo $rating['gz_rating_num']; ?></div>
										</li>

								<?php endforeach; ?>

							</ul>

						</div><!-- .widget_reviewInfo -->

					</div><!-- .pageReview -->

				<?php endif; ?>

				<?php the_content(); ?>

				<?php comments_template( '', true ); ?>

			</div><!-- .content -->

			<?php if ( $sidebar == 'yes' ) get_sidebar(); ?>

		</div><!-- .container -->

	</div><!-- .pageContent -->
	
<?php endwhile; else : ?>

	<div class="post box">
		<h3><?php _e('There is not post available.', 'localization'); ?></h3>
	</div>

<?php endif; ?>

<?php get_footer(); ?>