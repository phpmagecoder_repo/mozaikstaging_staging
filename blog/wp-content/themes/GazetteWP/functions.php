<?php

define( 'THEME_PATH', get_template_directory_uri() );
if ( ! isset( $content_width ) ) $content_width = 940;
	   

/* ========================================================================================================================
	
	Load OptionTree
	
======================================================================================================================== */

require_once( get_template_directory() . '/includes/theme-options.php' );

/**
 * Optional: set 'ot_show_pages' filter to false.
 * This will hide the settings & documentation pages.
 */

add_filter( 'ot_show_pages', '__return_true' );

/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );

global $options;
$options = get_option('option_tree');

/**
 * Required: include OptionTree.
 */
include_once( get_template_directory() . '/option-tree/ot-loader.php' );

/* ========================================================================================================================
	
	Required External Files
	
======================================================================================================================== */

require_once get_template_directory() . '/includes/comment-list.php';
include 'includes/shortcodes/shortcodes.php';
include_once("includes/tgn-meta-boxes.php");

/**
 * Set up theme defaults and register support for various WordPress features.
 */

	add_action( 'after_setup_theme', 'gazette_setup' );

	if ( ! function_exists( 'gazette_setup' ) ) {
		
		function gazette_setup() {

			/**
			 * Load translations
			 */

			load_theme_textdomain( 'localization', get_template_directory() . '/languages' );

			/**
			 * Theme Support
			 */

			add_theme_support('automatic-feed-links');
			add_theme_support( 'post-thumbnails' );
			add_theme_support('nav-menus');

			/**
			 * Add custom image sizes
			 */

			add_image_size( 'slider-slide', 280, 290, true ); //slider
			add_image_size( 'slider-featured', 580, 600, true ); // slider
			add_image_size( 'post-regular', 130, 130, true ); // post listing
			add_image_size( 'post-medium', 280, 280, true ); // post listing, reviews widget, videos widget
			add_image_size( 'post-big', 805, 9999, false ); // post single
			add_image_size( 'post-full', 1180, 9999, false ); // post single (no sidebar)

			/**
			 * Register menus
			 */

			register_nav_menus( array(
				'main_menu' => __( 'Main Menu', 'localization' ),
				'right_menu' => __( 'Right Menu (Button)', 'localization'),
				'footer_menu' => __( 'Footer Menu', 'localization')
			) );

			function gazette_fallback_menu() {

				echo '<ul class="nav clearfix sf-menu sf-js-enabled sf-shadow">
					<li class="homelink"><a href="' . home_url() . '">'. __('Home', 'localization') .'</a></li>';
					wp_list_pages('title_li=&depth=3');
				echo '</ul>';

			}

		}

	} /* End check for gazette_setup function */

/**
 * dd_comment
 *
 * Used as a callback by wp_list_comments() in comments.php
 * for displaying the comments.
 */

	if ( ! function_exists( 'dd_comment' ) ) {

		function dd_comment( $comment, $args, $depth ) {

			$GLOBALS['comment'] = $comment;
			
			switch ( $comment->comment_type ) :
				
				case 'pingback' :
				case 'trackback' :
					?>
					<li class="post pingback">
						<p><?php _e( 'Pingback:', 'localization' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'localization' ), ' ' ); ?></p>
					<?php
				break;
				default :
					?>

					<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">

						<article>

							<div class="comment-inner">

								<div class="comment-author-avatar">
									<?php echo get_avatar( $comment, 100 ); ?>
								</div><!-- .comment-autor -->

								<div class="comment-main">

									<div class="comment-info clearfix">
										
										<div class="comment-meta">
											<ul>
												<li><span><?php _e( 'by', 'localization' ); ?></span><?php echo get_comment_author_link(); ?></li>
												<li><span><?php _e( 'on', 'localization' ); ?></span><?php echo get_comment_date(); ?></li>
											</ul>
										</div><!-- comment-meta -->

										<div class="comment-reply">
											<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
										</div><!-- .comment-reply -->

									</div>

									<div class="comment-content">
										<?php if ( $comment->comment_approved == '0' ) : ?>
											<p><em><?php _e( 'Your comment is awaiting moderation.', 'localization' ); ?></em></p>
										<?php endif; ?>
										<?php comment_text(); ?>
									</div>

								</div><!-- .comment-main -->

							</div><!-- .comment-inner -->


						</article>

					<?php

					break;
			endswitch;

		} // end dd_comment()

	} // end check for dd_comment()

/**
 * dd_load_more_button
 *
 * Output the HTML for the infinite posts load more button.
 */

	if ( ! function_exists( 'dd_load_more_button' ) ) {

		function dd_load_more_button() {

			?><a class="loadMore" href="#" data-finished="<?php _e( 'NO MORE ARTICLES', 'localization' ); ?>" data-loading="<?php _e( 'LOADING', 'localization' ); ?>" data-default="<?php _e( 'LOAD MORE ARTICLES', 'localization' ); ?>"><i class="icon-docs"></i><?php _e( 'LOAD MORE ARTICLES', 'localization' ); ?></a><?php

		}

	}

/* ========================================================================================================================
	
	Custom Paginattion
	
======================================================================================================================== */


function kriesi_pagination($pages = '', $range = 2)
{
	 $showitems = ($range * 2)+1;

	 global $paged;
	 if(empty($paged)) $paged = 1;

	 if($pages == '')
	 {
		 global $wp_query;
		 $pages = $wp_query->max_num_pages;
		 if(!$pages)
		 {
			 $pages = 1;
		 }
	 }

	 if(1 != $pages)
	 {
		 echo "<ul class='pagination clearfix'>";
		 if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a class='button-small grey rounded3' href='".get_pagenum_link(1)."'>&laquo;</a></li>";
		 if($paged > 1 && $showitems < $pages) echo "<a class='button-small-theme rounded3' href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

		 for ($i=1; $i <= $pages; $i++)
		 {
			 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
			 {
				 echo ($paged == $i)? "<li><span class='button-small-theme rounded3 current'>".$i."</span></li>":"<li><a class='button-small grey rounded3 inactive' href='".get_pagenum_link($i)."' >".$i."</a></li>";
			 }
		 }

		 if ($paged < $pages && $showitems < $pages) echo "<li><a class='button-small-theme rounded3' href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a></li>";
		 if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a class='button-small-theme rounded3' href='".get_pagenum_link($pages)."'>&raquo;</a></li>";
		 echo "</ul>\n";
	 }
}

// Ready for theme localisation
load_theme_textdomain( 'localization' );

/**
 * Register Sidebars
 */

	if (function_exists('register_sidebar')) {
		
		register_sidebar(array(
			'name' => 'Home Sidebar 1',
			'id' => 'sidebar-4',
			'before_widget' => '<li class="widget clearfix"><div id="%1$s" class="%2$s">',
			'after_widget' => '</div></li>',
			'before_title' => '<h5>',
			'after_title' => '</h5><hr>',
		));

		register_sidebar(array(
			'name' => 'Home Sidebar 2',
			'id' => 'sidebar-5',
			'before_widget' => '<li class="widget clearfix"><div id="%1$s" class="%2$s">',
			'after_widget' => '</div></li>',
			'before_title' => '<h5>',
			'after_title' => '</h5><hr>',
		));
		
		register_sidebar(array(
			'name' => 'Blog Single',
			'id' => 'sidebar-1',
			'before_widget' => '<li class="widget clearfix"><div id="%1$s" class="%2$s">',
			'after_widget' => '</div></li>',
			'before_title' => '<h5>',
			'after_title' => '</h5><hr>',
		));

		register_sidebar(array(
			'name' => 'Archives',
			'id' => 'sidebar-archives',
			'before_widget' => '<li class="widget clearfix"><div id="%1$s" class="%2$s">',
			'after_widget' => '</div></li>',
			'before_title' => '<h5>',
			'after_title' => '</h5><hr>',
		));

		register_sidebar(array(
			'name' => 'Pages',
			'id' => 'sidebar-2',
			'before_widget' => '<li class="widget clearfix"><div id="%1$s" class="%2$s">',
			'after_widget' => '</div></li>',
			'before_title' => '<h5>',
			'after_title' => '</h5><hr>',
		));

		register_sidebar(array(
			'name' => 'Footer',
			'id' => 'sidebar-3',
			'before_widget' => '<div class="widget four columns clearfix"><div id="%1$s" class="%2$s">',
			'after_widget' => '</div></div>',
			'before_title' => '<h5>',
			'after_title' => '</h5><hr>',
		));

		/* Loop all cats and create sidebars */

		// Get categories
	    $cats = get_categories('hide_empty=0');

	    // Generate Options
	    if ( $cats ) {
	        foreach ( $cats as $cat ) {

	        	register_sidebar(array(
					'name' => esc_attr( $cat->name ) . ' Archives',
					'id' => 'sidebar-archives-' . $cat->term_id,
					'before_widget' => '<div class="widget four columns clearfix"><div id="%1$s" class="%2$s">',
					'after_widget' => '</div></div>',
					'before_title' => '<h5>',
					'after_title' => '</h5><hr>',
				));
	        }
	    }

	}


/* ========================================================================================================================
	
	Set Custom Query
	
======================================================================================================================== */

function dd_set_query($custom_query=null) { global $wp_query, $wp_query_old, $post, $orig_post;
	$wp_query_old = $wp_query;
	$wp_query = $custom_query;
	$orig_post = $post;
}

function dd_restore_query() {  global $wp_query, $wp_query_old, $post, $orig_post;
	$wp_query = $wp_query_old;
	$post = $orig_post;
	setup_postdata($post);
}

/* ========================================================================================================================
	
	Enqueues scripts and styles for front-end.
	
======================================================================================================================== */

function gazette_scripts_styles() {

	global $wp_styles;

	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/*
	 * Adds JavaScript for handling the navigation menu hide-and-show behavior.
	 */
		
	//wp_enqueue_script("jquery"); 
       // wp_deregister_script( 'jquery' ); // deregisters the default WordPress jQuery  
       // wp_register_script('jquery', ("/wp-includes/js/jquery/jquery.js"), false);
        wp_enqueue_script('jquery');
        wp_enqueue_script( 'gazette-script', get_template_directory_uri() . '/js/script.js', array(), '1.0', true );
	wp_enqueue_script( 'gazette-superfish', get_template_directory_uri() . '/js/superfish.js', array(), '1.0', true );
	wp_enqueue_script( 'gazette-slider', get_template_directory_uri() . '/js/slider.js', array(), '1.0', true );
	wp_localize_script( 'gazette-slider', 'GazetteAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_script( 'gazette-plugins', get_template_directory_uri() . '/js/slider-plugins.js', array(), '1.0', true );
	wp_enqueue_script( 'gazette-tweet-platform', '//platform.twitter.com/widgets.js', array(), '1.0', true );

	/*
	 * Loads our main stylesheet.
	 */
		
	wp_enqueue_style( 'gazette-style', get_stylesheet_uri() );

	/*
	 * Loads other stylesheet.
	 */

	wp_enqueue_style( 'gazette-custom', get_template_directory_uri() . '/stylesheets/custom.php', array( 'gazette-style' ), '20121010' );
	wp_enqueue_style( 'gazette-fontello', get_template_directory_uri() . '/font/css/fontello.css', array( 'gazette-style' ), '20121010' );
	wp_enqueue_style( 'gazette-fontelloie7', get_template_directory_uri() . '/font/css/fontello-ie7.css', array( 'gazette-style' ), '20121010' );
  
   
}
add_action( 'wp_enqueue_scripts', 'gazette_scripts_styles' );

/**
 * Enqueue scripts and styles in the admin
 */
function gazette_scripts_admin($hook) {

	if ( 'post-new.php' == $hook )
		wp_enqueue_script( 'post-options-js', get_template_directory_uri() . '/includes/js/admin-post-options.js', array(), '1.0', true );

}
add_action( 'admin_enqueue_scripts', 'gazette_scripts_admin' );

include_once( get_template_directory() . '/includes/widgets/widget.category-posts.php' );
include_once( get_template_directory() . '/includes/widgets/widget.reviews.php' );
include_once( get_template_directory() . '/includes/widgets/widget.videos.php' );
include_once( get_template_directory() . '/includes/widgets/widget.social.php' );
include_once( get_template_directory() . '/includes/widgets/widget.tabbed.php' );


/**
 * gazette_menu_cats
 *
 * Changes the output of the right menu. Adds a class and a count for categories.
 */

add_filter( 'wp_nav_menu_objects', 'gazette_menu_cats', 10, 2 );
function gazette_menu_cats( $sorted_menu_items, $args ) {

	// If it's the right menu
	if ( 'right_menu' == $args->theme_location ) {

		// Go through all the menu items
		foreach ($sorted_menu_items as $key => $menu_item) {

			// If it's a category
			if ( 'taxonomy' == $menu_item->type ) {

				// Get category info
				$cat = get_term_by( 'id', $menu_item->object_id, 'category' );

				// Add "category" class
				$sorted_menu_items[$key]->classes[] = 'category';

				// Add posts count
				$sorted_menu_items[$key]->title .= '<span>'. $cat->count .'</span>';

			}

		}

	}

	// Return the modified array
	return $sorted_menu_items; 

}

/**
 * gazette_show_slider
 *
 * The homepage slider
 */

function gazette_show_slider() {

	$has_featured = false;
	$has_slide = false;

	ob_start();

	?>

	<div id="slider" data-mousewheel="<?php echo ot_get_option('gz_slider_mousewheel', '1'); ?>" data-offset="12">

		<div id="slider-inner">

			<?php

				$args = array(
					'paged' => 1, 
					'post_type' => 'post',
					'posts_per_page' => 1,
					'tag' => 'featured'
				);
				$slider_posts = new WP_Query( $args );

			?>

			<div id="slider-featured-wrapper">
				
				<div id="slider-featured">

					<div id="slider-featured-inner">

						<?php if ($slider_posts->have_posts()) : while ($slider_posts->have_posts()) : $slider_posts->the_post(); $has_featured = true; ?>

							<?php

								// Get tags
								$tags = get_the_tags();
								$tags_output = '';

								// If has tags
								if ( $tags ) {

									// Loop tags
									foreach ( $tags as $tag ) {
										
										// Generate output
										if ( 'featured' != $tag->name ) {
											$tags_output .= '<a href="' . get_tag_link( $tag->term_id ) . '">' . $tag->name . '</a>';
										}

									}

								}

								// Get categories
								$cats = get_the_category();
								$cats_output = '';

								// If has categories
								if ( $cats ) {

									// Loop categories
									foreach ( $cats as $cat ) {
										
										// Generate output
										$cats_output .= '<span class="categoryBadge color-category-' . $cat->term_id . '"><a href="' . get_category_link( $cat->term_id ) . '">' . $cat->cat_name . '</a></span>';

									}

								}

							?>

							<div id="slider-featured-thumb">
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'slider-featured' ); ?></a>
							</div><!-- #slider-featured-thumb -->

							<div id="slider-featured-info">
								
								 <ul class="clearfix">
							
									<li><a class="author" href="#"><i class="icon-user"></i><?php echo get_the_author_meta( 'display_name' ); ?></a></li>
									<li><a class="tags" href="#"><i class="icon-tag"></i></a><?php echo $tags_output; ?></li>
									<li><a class="comments" href="#"><i class="icon-comment"></i><?php comments_popup_link( 'No Comments', 'One Comment', '% ' . 'Comments' ); ?></a></li>
									<li class="share">
										<?php gazette_share(); ?>
									</li>
										
								</ul>
								
								<div class="slider-featured-title">
									
									 <span id="slider-featured-info-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php echo $cats_output; ?></span>
									
								</div>
							
							</div><!-- #slider-featured-info -->

						<?php endwhile; endif; ?>

					</div><!-- #slider-featured-inner -->

				</div><!-- #slider-featured -->

			</div><!-- #slider-featured-wrapper -->

			<?php

				wp_reset_query();

				// Get the ID of the featured tag (to exclude)
				$featured_tag = get_term_by('slug', 'featured', 'post_tag');
				$featured_tag = $featured_tag->term_id;

				$args = array(
					'paged' => 1, 
					'post_type' => 'post',
					'posts_per_page' => 12,
					'tag' => 'slider',
					'tag__not_in' => $featured_tag
				);
				$slider_posts = new WP_Query( $args );

			?>

			<div id="slider-main" data-foundposts="<?php echo $slider_posts->found_posts; ?>">

				<div id="slider-main-inner">

					<?php if ($slider_posts->have_posts()) : while ($slider_posts->have_posts()) : $slider_posts->the_post(); $has_slide = true; ?>

						<?php

							$post_type = get_post_meta(get_the_ID(), 'gz_post_type', true);
							$add_class = '';

							if ( $post_type == 'video' )
								$add_class = 'video';
							elseif ( $post_type == 'review' )
								$add_class = 'review';

							// Get category
							$cats = get_the_category();
							$cats_output = '';

							// If has category
							if ( $cats ) {

								// Loop categories
								foreach ( $cats as $cat ) {
									
									// Generate output
									$cats_output .= '<span class="categoryBadge color-category-' . $cat->term_id . '"><a href="' . get_category_link( $cat->term_id ) . '">' . $cat->cat_name . '</a></span>';

								}

							}

						?>

						<div class="slider-item <?php echo $add_class; ?>">

							<div class="slider-item-thumb">
								<?php echo $cats_output; ?>
								<?php if ( $post_type == 'video' ) : ?>
									<a class="videoIcon" href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/videoIcon.png'; ?>" alt="" /></a>
								<?php endif; ?>
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'slider-slide' ); ?></a>
							</div><!-- .slider-item-thumb -->

							<div class="slider-item-info">

								<?php if ( $post_type == 'review' ) : ?>

									<?php $ratings = get_post_meta( get_the_ID(), 'gz_ratings', true ); ?>

									<?php if ( $ratings ) : $ratings_found = count( $ratings ) - 1; ?>

										<span class="slider-item-review clearfix">
											<span class="note"><?php echo $ratings[$ratings_found]['gz_rating_num']; ?></span>
											<span class="description"><?php echo $ratings[$ratings_found]['gz_rating_text']; ?></span>
										</span>

									<?php endif; ?>

								<?php endif; ?>

								<div class="slider-item-title">
									<span class="slider-item-info-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
								</div>

							</div><!-- .slider-item-info -->

						</div><!-- .slider-item -->

					<?php endwhile; endif; ?>

				</div><!-- #slider-main-inner -->

			</div><!-- #slider-main -->

		</div><!-- #slider-inner -->

		<div id="slider-scroller"></div>

	</div><!-- #slider -->

	<?php

	$output = ob_get_contents();
	ob_end_clean();

	if ( $has_slide && $has_featured )
		echo $output;

	wp_reset_query();

}

/**
 * gazette_cat_colors
 *
 * Generate CSS for the category colors
 */

add_action( 'wp_head', 'gazette_cat_colors' );
function gazette_cat_colors() {

	// Get categories
	$cats = get_categories('hide_empty=0');
	$cats_css = '';
	
	// If there are categories
	if ( $cats ) {

		// Loop the categories
		foreach ( $cats as $cat ) {
			
			// Generate CSS
			$cats_css .= '.color-category-' . $cat->term_id . ' { background-color: ' . ot_get_option( 'gz_cat_color_' . $cat->term_id, '#000' ) . '; } ';

		}

	}

	// Output custom CSS if it's not empty
	if ( $cats_css != '' ) {
		?><style><?php echo $cats_css; ?></style><?php
	}

}

/**
 * gazette_share
 *
 * Social sharing
 */

function gazette_share() {

	?>
	<a class="shareBtn" href="#"><i class="icon-export"></i></a>                                
	<div class="shareIt">
		
		<a href="https://twitter.com/intent/tweet?url=<?php echo urlencode( get_permalink() ); ?>&text=<?php echo urlencode( get_the_title() ); ?><?php if ( ot_get_option( 'gz_share_twitter' ) ) : ?>&via=<?php echo urlencode( ot_get_option( 'gz_share_twitter' ) ); endif; ?>"><i class="icon-twitter"></i><?php _e( 'Twitter', 'localization' ); ?></a>
		<a href="#" onclick="return fbs_click('<?php echo get_permalink(); ?>', '<?php echo get_the_title(); ?>')"><i class="icon-facebook"></i><?php _e( 'Facebook', 'localization' ); ?></a>
		
	</div>
	<?php

}

/**
 * gazette_slider_load
 *
 * Load new posts
 */
add_action( 'wp_ajax_nopriv_gazette-load-slides', 'gazette_slider_load' );
add_action( 'wp_ajax_gazette-load-slides', 'gazette_slider_load' );
function gazette_slider_load( $atts ) {

	$offset = $_POST['offset'];

	wp_reset_query();

	// Get the ID of the featured tag (to exclude)
	$featured_tag = get_term_by('slug', 'featured', 'post_tag');
	$featured_tag = $featured_tag->term_id;

	$args = array(
		'paged' => 1, 
		'post_type' => 'post',
		'posts_per_page' => 2,
		'tag' => 'slider',
		'tag__not_in' => $featured_tag,
		'offset' => $offset
	);
	$slider_posts = new WP_Query( $args );

	$output = '<div class="slider-col">';

	if ($slider_posts->have_posts()) : while ($slider_posts->have_posts()) : $slider_posts->the_post();

		$post_type = get_post_meta(get_the_ID(), 'gz_post_type', true);
		$add_class = '';

		if ( $post_type == 'video' )
			$add_class = 'video';
		elseif ( $post_type == 'review' )
			$add_class = 'review';

		// Get category
		$cats = get_the_category();
		$cats_output = '';

		// If has category
		if ( $cats ) {

			// Loop categories
			foreach ( $cats as $cat ) {
				
				// Generate output
				$cats_output .= '<span class="categoryBadge color-category-' . $cat->term_id . '"><a href="' . get_category_link( $cat->term_id ) . '">' . $cat->cat_name . '</a></span>';

			}

		}

		ob_start();

		?>

			<div class="slider-item <?php echo $add_class; ?>">

				<div class="slider-item-thumb">
					<?php echo $cats_output; ?>
					<?php if ( $post_type == 'video' ) : ?>
						<a class="videoIcon" href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri() . '/images/videoIcon.png'; ?>" alt="" /></a>
					<?php endif; ?>
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'slider-slide' ); ?></a>
				</div><!-- .slider-item-thumb -->

				<div class="slider-item-info">

					<?php if ( $post_type == 'review' ) : ?>

						<?php $ratings = get_post_meta( get_the_ID(), 'gz_ratings', true ); ?>

						<?php if ( $ratings ) : $ratings_found = count( $ratings ) - 1; ?>

							<span class="slider-item-review clearfix">
								<span class="note"><?php echo $ratings[$ratings_found]['gz_rating_num']; ?></span>
								<span class="description"><?php echo $ratings[$ratings_found]['gz_rating_text']; ?></span>
							</span>

						<?php endif; ?>

					<?php endif; ?>

					<div class="slider-item-title">
						<span class="slider-item-info-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
					</div>

				</div><!-- .slider-item-info -->

			</div><!-- .slider-item -->

		<?php

		$output .= ob_get_contents();

		ob_end_clean();

	endwhile; endif;

	$output .= '</div>';
	
	$response = json_encode( array( 'success' => true, 'output' => $output ) );

	header( "Content-Type: application/json" );
	echo $response;

	exit;

}

/**
 * dd_social_nav
 *
 * The social icons in header.
 */

if ( ! function_exists( 'dd_social_nav' ) ) {

	function dd_social_nav() {

		$show_icons = false;

		ob_start();

		?>

		<nav class="socialNav">

			<ul>

				<?php if (ot_get_option('contactlink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('contactlink') ?>" class="mail"><i class="icon-mail"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('twitterlink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('twitterlink') ?>" class="twitter"><i class="icon-twitter"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('youtubelink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('youtubelink') ?>" class="youtube"><i class="icon-youtube"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('facebooklink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('facebooklink') ?>" class="facebook"><i class="icon-facebook"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('vimeolink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('vimeolink') ?>" class="vimeo"><i class="icon-vimeo"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('googlelink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('googlelink') ?>" class="google"><i class="icon-gplus"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('pinterestlink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('pinterestlink') ?>" class="pinterest"><i class="icon-pinterest"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('linkedinlink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('linkedinlink') ?>" class="linkedin"><i class="icon-linkedin"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('dribbblelink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('dribbblelink') ?>" class="dribbble"><i class="icon-dribbble"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('instagramlink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('instagramlink') ?>" class="instagram"><i class="icon-instagram"></i></a></li>
				<?php } ?>

				<?php if (ot_get_option('behancelink') != '') { $show_icons = true; ?>
					<li><a href="<?php echo ot_get_option('behancelink') ?>" class="behance"><i class="icon-behance"></i></a></li>
				<?php } ?>

				<?php if ( ot_get_option('topbarsearch') == 'yes' ) { $show_icons = true; ?><li><a class="search" href="#"><i class="icon-search-1"></i></a></li><?php } ?>

			</ul>

		</nav>

		<?php

		$output = ob_get_contents();
		ob_end_clean();

		if ( $show_icons ) echo $output;

	}

}

/**
 * dd_category_query_mod
 *
 * Modify the main query for the category archives
 */

function dd_category_query_mod( $query ) {

	if ( $query->is_archive && $query->is_category  ) {
		
		// Additional query params
		$gz_show = false;
		if ( isset( $_GET['gz_show'] ) )
			$gz_show = $_GET['gz_show'];

		// Show Popular
		if ( $gz_show && 'popular' == $gz_show )
			$query->set( 'orderby', 'comment_count' );

		// Show videos
		if ( $gz_show && 'videos' == $gz_show ) {
			$query->set( 'meta_key', 'gz_post_type' );
			$query->set( 'meta_value', 'video' );
		}

	}

	return $query;

}

add_action( 'pre_get_posts','dd_category_query_mod' );


/**
 * dd_custom_code
 *
 * Custom CSS and JS code from Theme Options.
 */

add_action( 'wp_footer', 'dd_custom_code');
function dd_custom_code() {
    
	if ( ot_get_option( 'gz_code_css' ) )
		echo '<style>' . ot_get_option( 'gz_code_css' ) . '</style>';

	if ( ot_get_option( 'gz_code_js' ) )
		echo '<script>' . ot_get_option( 'gz_code_js' ) . '</script>';

}

add_filter( 'term_links-post_tag', 'dd_tags_filter' );
function dd_tags_filter( $tags ) {

	foreach ( $tags as $key => $tag ) {
		
		if ( false === strpos( $tag, 'featured' ) ) { } else {
			unset( $tags[$key] );
		}

		if ( false === strpos( $tag, 'slider' ) ) { } else {
			unset( $tags[$key] );
		}

	}

	return $tags;

}

function press_this_ptype($link) {
	$post_type = 'qa_faqs';

	$link = str_replace('post-new.php', "post-new.php?post_type=$post_type", $link);
	$link = str_replace('?u=', '&u=', $link);

	return $link;
}
add_filter('shortcut_link', 'press_this_ptype', 11);

