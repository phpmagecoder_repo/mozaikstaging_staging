<?php 
	
	get_header(); 

	// Get Sidebar
	$sidebar = get_post_meta( get_the_ID(), 'sidebar', true );
	if ( ! $sidebar ) $sidebar = 'yes';
	$content_class = 'sixteen';

	// Generate Content Class (width)
	if ( 'yes' == $sidebar )
		$content_class = 'eleven';

	// Post Type
	$post_type = get_post_meta(get_the_ID(), 'gz_post_type', true);

	// Get category
	$cats = get_the_category();
	$cats_output = '';

	// If has categories, loop them and generate output
	if ( $cats ) {
		foreach ( $cats as $cat ) {
			$cats_output .= '<span class="categoryBadge color-category-' . $cat->term_id . '"><a href="' . get_category_link( $cat->term_id ) . '">' . $cat->cat_name . '</a></span>';
		}
	}

?>

<div class="pageTitle">

	<div class="container">

		<div class="sixteen columns"><h1><?php the_title(); echo $cats_output; ?></h1></div>

	</div>

</div>

<?php if ( have_posts()) : while ( have_posts()) : the_post(); ?>

	<div class="pageContent">

		<div class="container">

			<div class="<?php echo $content_class; ?> columns content">

				<div class="singleMeta clearfix">

					<ul>

						<li><a class="author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="icon-user"></i><?php the_author(); ?></a></li>
						<?php echo '<li><p class="qa-post-like">' . qa_getPostLikeLink(get_the_ID()) . '</p></li>'; ?>
						<?php $faq_cats = qa_add_categories();?>

						<?php echo '<li><p class="qa_cats">' . __('Posted in: ', 'qa-plus') . $faq_cats . '</p></li>'; ?>

						<?php if ( has_tag() ) :  ?>
							<li><a class="tags" href="#"><i class="icon-tag"></i></a><?php the_tags('', ', ', ''); ?> </li>
						<?php endif; ?>

						<li class="share"><?php gazette_share(); ?></li>

					</ul>

				</div><!-- .singleMeta -->

				<?php if ( 'yes' == ot_get_option( 'gz_single_thumb', 'no' ) ) : ?>

					<div class="singleImg">
						<?php if ( $sidebar == 'yes' ) the_post_thumbnail( 'post-big' ); else the_post_thumbnail( 'post-full' ); ?>
					</div>

				<?php endif; ?>


				<?php the_content(); ?>

				<?php // comments_template( '', true ); ?>

			</div><!-- .content -->

			<?php if ( $sidebar == 'yes' ) get_sidebar(); ?>

		</div><!-- .container -->

	</div><!-- .pageContent -->
	
<?php endwhile; else : ?>

	<div class="post box">
		<h3><?php _e('There is not post available.', 'localization'); ?></h3>
	</div>

<?php endif; ?>

<?php get_footer(); ?>