<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to dd_comment() which is
 * located in the functions.php file.
 */
?>

<?php
	/*
	 * If the current post is protected by a password and
	 * the visitor has not yet entered the password we will
	 * return early without loading the comments.
	 */
	if ( post_password_required() )
		return;
?>

	<div id="comments">

		<?php if ( have_comments() ) : ?>
			
			<h3 class="comments-title"><?php _e( 'COMMENTS', 'localization' ); ?></h3>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
				<nav role="navigation" id="comment-nav-above" class="site-navigation comment-navigation">
					<h1 class="assistive-text"><?php _e( 'Comment navigation', 'localization' ); ?></h1>
					<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'localization' ) ); ?></div>
					<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'localization' ) ); ?></div>
				</nav><!-- #comment-nav-before .site-navigation .comment-navigation -->
			<?php endif; // check for comment navigation ?>

			<ol class="comments clean-list">
				<?php wp_list_comments( array( 'callback' => 'dd_comment' ) ); ?>
			</ol><!-- .commentlist -->

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
				<nav role="navigation" id="comment-nav-below" class="site-navigation comment-navigation">
					<h1 class="assistive-text"><?php _e( 'Comment navigation', 'localization' ); ?></h1>
					<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'localization' ) ); ?></div>
					<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'localization' ) ); ?></div>
				</nav><!-- #comment-nav-below .site-navigation .comment-navigation -->
			<?php endif; // check for comment navigation ?>

		<?php else : ?>

			<div class="align-center"><?php _e( 'There are no comments published yet.', 'localization' ); ?></div>

		<?php endif ; // end if have_comments() ?>

		<?php if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
				<p class="nocomments"><?php _e( 'Comments are closed.', 'localization' ); ?></p>
		<?php endif; ?>

	</div><!-- #comments -->

	<div id="respond">

		<?php
			comment_form( array(
				'cancel_reply_link'    => 'cancel',
				'comment_notes_before' => '',
				'comment_notes_after'  => '',
				'title_reply'          => __( 'Reply', 'localization' ),
				'title_reply_to'       => __( 'Reply to %s', 'localization'),
				'comment_field'        => '<div class="comment-form-comment"><textarea id="comment" name="comment" placeholder="' . __( 'Comment', 'localization' ) . '" aria-required="true"></textarea></div>',
				'fields'               => apply_filters( 'comment_form_default_fields', array(
					'author' => '<div class="comment-form-author"><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" placeholder="' . __( 'Name', 'localization' ) . ' *" aria-required="true" /></div>',
					'email'  => '<div class="comment-form-email"><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" placeholder="' . __( 'Email', 'localization' ) . ' *" aria-required="true" /></div>',
					'url'    => '<div class="comment-form-url"><input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" placeholder="' . __( 'Website', 'localization' ) . '" /></div>' 
				)),
			)); 

		?>

	</div><!-- #respond -->
