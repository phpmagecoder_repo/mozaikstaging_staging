<?php get_header(); ?>
<div class=" nav container clearer stretched show-bg">
<?php gazette_show_slider(); ?>
</div>

<div class="homeContent">

    <div class="container">

        <div class="eight columns">

            <h5><?php _e( 'LATEST NEWS', 'localization' ); ?></h5>	
            
            <hr>

            <ul class="homePosts clearfix">

                <?php

                    global $paged;

                    $arguments = array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'paged' => $paged
                    );

                    $blog_query = new WP_Query($arguments);

                ?>

                <?php if ($blog_query->have_posts()) : while ($blog_query->have_posts()) : $blog_query->the_post(); ?>

                    <?php $thumbimg = get_post_meta(get_the_ID(), 'thumbimg', true); ?>

                    <?php

                        // Get category
                        $cats = get_the_category();
                        $cats_output = '';

                        // If has category
                        if ( $cats ) {

                            // Loop categories
                            foreach ( $cats as $cat ) {
                                
                                // Generate output
                                $cats_output .= '<span class="categoryBadge color-category-' . $cat->term_id . '"><a href="' . get_category_link( $cat->term_id ) . '">' . $cat->cat_name . '</a></span>';

                            }

                        }

                    ?>

                    <li <?php post_class('homePost'); ?>>

                        <?php if( has_post_thumbnail() ) : ?>

                            <div class="two columns alpha homePostsThumbs">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'post-regular' ); ?></a>
                            </div>    

                            <div class="six columns omega">

                        <?php else : ?>

                            <div class="eight columns alpha omega">

                        <?php endif; ?>

                            <div class="homePostsInfo">

                                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php echo $cats_output; ?></span></h1>
                                
                                <?php the_excerpt(); ?>

                                <ul>
                                    <li><a class="author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="icon-user"></i><?php the_author(); ?></a></li>
                                    <?php 
                                        if ( has_tag() ) {  
                                            $tags_output = get_the_tag_list( '', ', ', '' );
                                            if ( ! empty( $tags_output ) ) :
                                                ?>
                                                    <li><a class="tags" href="#"><i class="icon-tag"></i></a><?php echo $tags_output; ?> </li>
                                                <?php 
                                            endif;
                                        } 
                                    ?>
                                    <li><a class="comments" href="<?php comments_link(); ?> "><i class="icon-comment"></i> <?php comments_number( '0 comments', '1 comment', '% comments' ); ?> </a></li>
                                </ul>

                            </div>

                        </div>

                    </li>

                <?php endwhile; endif; ?>

            </ul>

            <?php kriesi_pagination( $blog_query->max_num_pages, 500 ); ?>

            <?php wp_reset_postdata(); ?>

            <?php dd_load_more_button(); ?>                  

        </div>

        <ul class="sidebar four columns clearfix">
            <?php if ( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'sidebar-4' ) ) : else : ?>
                <p><strong>Home Sidebar 1</strong> widget section is empty.</p>
                <p>Go to WP Admin &rarr; Appearance &rarr; Widgets and add the widgets you want.</p>
            <?php endif; ?>
        </ul>

        <ul class="sidebar four columns clearfix">
            <?php if ( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'sidebar-5' ) ) : else : ?>
                <p><strong>Home Sidebar 2</strong> widget section is empty.</p>
                <p>Go to WP Admin &rarr; Appearance &rarr; Widgets and add the widgets you want.</p>
            <?php endif; ?>
        </ul>

    </div><!-- .container -->

</div><!-- homeContent -->

<?php get_footer(); ?>