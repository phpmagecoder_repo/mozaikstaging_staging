<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<?php
/**
 * @author      MagePsycho <info@magepsycho.com>
 * @website     http://www.magepsycho.com
 * @category    using Header / Footer outside of Magento
 */
/* 
$mageFilename = '/var/zpanel/hostdata/zadmin/public_html/uwphotography_ca/app/Mage.php';
require_once $mageFilename;
#Mage::setIsDeveloperMode(true);
#ini_set('display_errors', 1);
umask(0);
Mage::init();
Mage::getSingleton('core/session', array('name'=>'frontend'));
 
$block = Mage::getSingleton('core/layout');
 
# HEAD BLOCK
$headBlock  = $block->createBlock('page/html_head')->setTemplate('page/html/head_wp.phtml');// this wont give you the css/js inclusion

# add infortis js
$headBlock->addJs('prototype/prototype.js');
$headBlock->addJs('lib/ccard.js');
$headBlock->addJs('prototype/validation.js');
$headBlock->addJs('scriptaculous/builder.js');
$headBlock->addJs('scriptaculous/effects.js');
$headBlock->addJs('scriptaculous/dragdrop.js');
$headBlock->addJs('scriptaculous/controls.js');
$headBlock->addJs('scriptaculous/slider.js');
$headBlock->addJs('varien/js.js');
$headBlock->addJs('varien/form.js');
$headBlock->addJs('varien/menu.js');
$headBlock->addJs('mage/translate.js');
$headBlock->addJs('mage/cookies.js');

# add general js
//$headBlock->addJs('infortis/jquery/jquery-1.7.2.min.js');
//$headBlock->addJs('infortis/jquery/jquery-noconflict.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.easing.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.tabs.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.accordion.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.ba-throttle-debounce.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.owlcarousel.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.cookie.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.reveal.js');
                        
# add css
$headBlock->addCss('css/styles.css');
$headBlock->addCss('css/styles-infortis.css');  
$headBlock->addCss('css/infortis/_shared/generic-cck.css');
$headBlock->addCss('css/infortis/_shared/accordion.css');
$headBlock->addCss('css/infortis/_shared/dropdown.css');
$headBlock->addCss('css/infortis/_shared/itemslider.css');
$headBlock->addCss('css/infortis/_shared/itemslider-old.css');
$headBlock->addCss('css/infortis/_shared/generic-nav.css');
$headBlock->addCss('css/infortis/_shared/icons.css');
$headBlock->addCss('css/infortis/_shared/itemgrid.css');
$headBlock->addCss('css/infortis/_shared/tabs.css');

$headBlock->addCss('css/infortis/ultra-megamenu/ultra-megamenu.css'); 
$headBlock->addCss('css/infortis/ultra-megamenu/wide.css');



$headBlock->addCss('css/icons-theme.css');
$headBlock->addCss('css/icons-social.css');
$headBlock->addCss('css/common.css');
$headBlock->addCss('css/_config/design_default.css');
$headBlock->addCss('css/override-components.css');
$headBlock->addCss('css/override-modules.css');
$headBlock->addCss('css/override-theme.css');
$headBlock->addCss('css/infortis/_shared/grid12.css');
$headBlock->addCss('css/_config/grid_default.css');
$headBlock->addCss('css/_config/layout_default.css');
$headBlock->addCss('css/reveal.css');
$headBlock->addCss('css/custom1.css');
      
$headBlock->getCssJsHtml();
$headBlock->getIncludes();
 
# HEADER BLOCK
$headerBlock = $block->createBlock('page/html_header')->setTemplate('page/html/header.phtml');

$linksBlock = $block->createBlock('cms/block')->setBlockId('block_header_top_left');
$headerBlock->setChild('block_header_top_left',$linksBlock);

$linksBlock1 = $block->createBlock('cms/block')->setBlockId('block_header_top_right');
$headerBlock->setChild('block_header_top_right',$linksBlock1);

$currencyBlock = $block->createBlock('directory/currency')->setTemplate('directory/currency.phtml');
$headerBlock->setChild('currency',$currencyBlock);


$cartBlock = $block->createBlock('cms/block')->setBlockId('external_cart_block');   // My custom cart Block
$headerBlock->setChild('cart_sidebar',$cartBlock);   

$searchBlock =  $block->createBlock('core/template')->setTemplate('catalogsearch/form.mini.phtml');
$headerBlock->setChild('topSearch',$searchBlock);

$navBlock = $block->createBlock('ultramegamenu/navigation')->setTemplate('infortis/ultramegamenu/mainmenu.phtml');
$headerBlock->setChild('topMenu',$navBlock);

$navBlockChild1 = $block->createBlock('cms/block')->setBlockId('block_header_nav_dropdown');
$navBlock->setChild('block_header_nav_dropdown',$navBlockChild1);

$navBlockChild2 = $block->createBlock('cms/block')->setBlockId('block_nav_links');
$navBlock->setChild('block_nav_links',$navBlockChild2);

$headerBlock = $headerBlock->toHtml();   */
 
?>
<head>
	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
	<title><?php
			if ( defined('WPSEO_VERSION') ) {
				wp_title();
			} else {
				if ( is_home() || is_front_page() ) {
					bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
				} else {
					wp_title( '|', true, 'right' ); bloginfo( 'name' );
				}
			}
	?></title>
       
	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php if ( is_search() ) : ?>
		<meta name="robots" content="noindex, nofollow" /> 
	<?php endif; ?>

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Atoms & Pingback
	================================================== -->

	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php if( ot_get_option( 'gz_favicon') ) : ?>
		<link rel="shortcut icon" href="<?php echo ot_get_option( 'gz_favicon'); ?>" type="image/x-icon" />		
	<?php endif; ?>
                <!--------------------------- MAGENTO HEAD START -------------------------------->
	<?php //echo $headBlock->toHtml(); ?>
        <!--------------------------- MAGENTO HEAD END -------------------------------->
        <!--------------------------- WP HEAD START -------------------------------->
	<?php wp_head(); ?>
        <!--------------------------- WP HEAD END -------------------------------->
         <script type="text/javascript" src="http://www.housingcamera.com/js/infortis/jquery/plugins/jquery.cookie.js"></script>
	<!--------------------------- START GANALYTICS -------------------------------->
        <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-2273859-1', 'housingcamera.com');
	  ga('send', 'pageview');

	</script>
        <!--------------------------- END GANALYTICS -------------------------------->
	<link rel='stylesheet' id='gazzete-custom-style-css'  href='http://www.housingcamera.com/blog/wp-content/themes/GazetteWP/stylesheets/gazzette-custom.css' type='text/css' media='all' />
        
</head>

<body data-spy="scroll" data-target=".subnav" data-offset="50" <?php body_class(); ?>>
<!-- Magento header -->
<div class="page magento-css">
        <?php //echo $headerBlock; ?>
</div>
<!-- END Magento header -->
<!--------------------------------------------->
<!--------------------------------------------->
<?php /* REMOVE WP HEADER
 <!--  Tabbed Nav -->
  <ul id="navigation">
            <li class="store"><a href="http://www.housingcamera.com/" title="Browse the Mozaik Store"><div class="caption"></div></a></li>
            <li class="blog active"><a href="http://blog.housingcamera.com/" title="Visit Our UW Photography Blog"><div class="caption"></div></a></li>
            <li class="gallery"><a href="http://www.housingcamera.com/gallery" title="Enjoy Our Gallery of UW Photos"><div class="caption"></div></a></li><li class="chat"><a href="#" onclick="return SnapEngage.startLink();" title="Chat with Us"><div class="caption"></div></a></li>
        </ul>
        <script type="text/javascript">
 	var userAgent = window.navigator.userAgent;
	if (!userAgent.match(/iPhone|iPod|Android|Mobile Safari/i)) { 
		jQuery(function() {
                jQuery('#navigation a').stop().animate({'marginLeft':'-70px'},1000);

                jQuery('#navigation > li').hover(
                    function () {
                        jQuery('a',jQuery(this)).stop().animate({'marginLeft':'-2px'},200);
                    },
                    function () {
                        jQuery('a',jQuery(this)).stop().animate({'marginLeft':'-70px'},200);
                    }
                );
            });
	}
	else{
		jQuery('#navigation').hide();
	  
	}
        </script>
	<!--  Tabbed Nav END -->

<header>	
		<div class="topBar clearfix">
			<?php dd_social_nav(); ?>
		</div>
		<div class="container">
			<div class="sixteen columns">

				<?php if ( ot_get_option( 'topbarsearch' ) == 'yes' ) get_search_form(); ?>

				<h1 class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">						
					<img src="<?php echo ot_get_option( 'logo', get_template_directory_uri() . '/images/logo.png' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
				</a></h1>

				<nav class="mainNav">

					<?php
						wp_nav_menu( array(
								'container' => false,
								'menu_class' => 'nav clearfix sf-menu sf-js-enabled sf-shadow',
								'theme_location' => 'main_menu',
								'echo' => true,
								'before' => '',
								'after' => '',
								'link_before' => '',
								'fallback_cb' => 'gazette_fallback_menu',
								'link_after' => '',
								'depth' => 0
							)
						);
					?>

				</nav><!-- .mainNav -->

				<?php dd_social_nav(); ?>

				<div class="mobileMenu big">

					<a class="mobileMenuBtn" href="#"></a>

					<nav>

						<ul>

							<?php
								wp_nav_menu( array(
									'container' => false,
									'menu_class' => '',
									'theme_location' => 'right_menu',
									'echo' => true,
									'before' => '',
									'after' => '',
									'link_before' => '',
									'fallback_cb' => 'gazette_fallback_menu',
									'link_after' => '',
									'depth' => 0
									)
								);
							?>

						</ul>

					</nav>

				</div><!-- .mobileMenu -->

			</div>

		</div>

	</header>
	*/ ?>
