<?php

	// Post Type
	$post_type = get_post_meta(get_the_ID(), 'gz_post_type', true);
	$ratings = false;

	// Get ratings
	if ( 'review' == $post_type )
		$ratings = get_post_meta( get_the_ID(), 'gz_ratings', true );

?>

<div class="one column separator">&nbsp</div>

<ul class="sidebar four columns clearfix">

	<?php if ( $ratings ) : ?>

		<li class="widget">

			<div class="widget_reviewInfo clearfix">

				<h5><?php _e( 'THE VERDICT', 'localization' ); ?></h5>	
				<hr>

				<ul class="clearfix">

					<?php 
						
						// Vars
						$count = 0; 
						$real_count = 0;
						$ratings_found = count( $ratings );

						// Loop ratings
						foreach ( $ratings as $rating ) : $count++; $real_count++;

							// Rating Class
							$rating_class = 'odd';
							if ( $count == 2 ) {
								$rating_class = 'even';
								$count = 0;
							}
							if ( $real_count == $ratings_found ) 
								$rating_class .= ' result';

							?>
					
							<li class="<?php echo $rating_class; ?>">
								<div class="title">
									<span><?php echo $rating['title']; ?></span>
									<span class="categoryBadge"><?php echo $rating['gz_rating_text']; ?></span>
								</div>
								<div class="note"><?php echo $rating['gz_rating_num']; ?></div>
							</li>

					<?php endforeach; ?>

				</ul>

			</div><!-- .widget_reviewInfo -->

		</li>   

	<?php endif; ?>

	<?php if ( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'sidebar-1' ) ) : else : ?>
						
		<?php /* Content if sidebar is empty */ ?>
		
	<?php endif; ?>

</ul>