<?php 

	get_header();

	$sidebar = ot_get_option( 'search_page_sidebar', 'yes' );
	$content_class = 'sixteen';

	if ( $sidebar == 'yes' )
		$content_class = 'eleven';

	$cat_title = single_cat_title( '', false );
	$cat_id = get_query_var( 'cat' );

	$count = 0;

	global $paged;

?>

<div class="pageTitle  nav container clearer stretched show-bg">

	<div class="container">

		<div class="sixteen columns"><h1><?php printf( __( 'Search for: %s', 'localization' ), get_search_query() ); ?></h1></div>

	</div>

</div><!-- .pageTitle -->

<div class="pageContent">

	<div class="container">

		<div class="<?php echo $content_class; ?> columns content">

			<?php if ( have_posts()) : while ( have_posts()) : the_post(); $count++; ?>

				<?php if ( $count == 1 && $paged == 0 ) : ?>

					<div class="categoryFeatured clearfix">

						<div class="four columns alpha categoryFeaturedThumb">

							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'post-medium' ); ?></a>

						</div>

						<?php if ( $sidebar == 'yes') : ?>
							<div class="seven columns omega">
						<?php else : ?>
							<div class="twelve columns omega">
						<?php endif; ?>

							<div class="categoryFeaturedInfo">

								<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
								
								<?php the_excerpt(); ?>

								<ul>

									<li><a class="author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="icon-user"></i><?php the_author(); ?></a></li>
									
									<?php if ( has_tag() ) :  ?>
										<li><a class="tags" href="#"><i class="icon-tag"></i></a><?php the_tags('', ', ', ''); ?> </li>
									<?php endif; ?>

									<li><a class="comments" href="<?php comments_link(); ?> "><i class="icon-comment"></i> <?php comments_number( __( '0 comments', 'localization' ), __( '1 comment', 'localization' ), __( '% comments', 'localization' ) ); ?> </a></li>

								</ul>

							</div>

						</div>

					</div><!-- .categoryFeatured -->

				<?php endif; ?>

				<?php if ( $count > 1 || $paged != 0 ) : ?>

					<?php if ( ( $count == 2 && $paged == 0 ) || ( $count == 1 && $paged != 0 ) ) : ?><ul class="categoryPosts"><?php endif; ?>

					<li <?php post_class( 'categoryPost' ); ?>>

						<?php if( has_post_thumbnail() ) : ?>

							<div class="two columns alpha categoryPostsThumbs">
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'post-regular' ); ?></a>
							</div>    

							<?php if ( $sidebar == 'yes') : ?>
								<div class="eight columns omega">
							<?php else : ?>
								<div class="thirteen columns omega">
							<?php endif; ?>

						<?php else : ?>

							<?php if ( $sidebar == 'yes') : ?>
								<div class="ten columns omega">
							<?php else : ?>
								<div class="fifteen columns omega">
							<?php endif; ?>

						<?php endif; ?>

								<div class="categoryPostsInfo">

									<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><span class="categoryBadge category-<?php echo $cat_id; ?>"><?php the_category(', ');?></span></h1>
									
									<?php the_excerpt(); ?>

									<ul>

										<li><a class="author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="icon-user"></i><?php the_author(); ?></a></li>
										
										<?php if ( has_tag() ) :  ?>
											<li><a class="tags" href="#"><i class="icon-tag"></i></a><?php the_tags('', ', ', ''); ?> </li>
										<?php endif; ?>

										<li><a class="comments" href="<?php comments_link(); ?> "><i class="icon-comment"></i> <?php comments_number( __( '0 comments', 'localization' ), __( '1 comment', 'localization' ), __( '% comments', 'localization' ) ); ?> </a></li>

									</ul>

								</div><!-- .categoryPostsInfo -->

							</div><!-- .columns.omega -->

							<div class="one column">
								<a class="categoryPostBtn" href="<?php the_permalink(); ?>"><i class="icon-right-open"></i></a>
							</div>

					</li>

				<?php endif; ?>

			<?php endwhile; else :?>


				<div class="pageContent">

					<h4 style="margin-top: 0; margin-bottom: 10px;"><?php _e('THERE IS NO POST AVAILABLE', 'localization'); ?></h4>

					<a href="<?php echo home_url(); ?>" class="back-to-home"><?php _e('Go Back To Homepage', 'localization'); ?> &rarr;</a>

				</div>

			<?php endif; ?> 

			</ul><!-- .categoryPosts -->

			<?php kriesi_pagination( $wp_query->max_num_pages, 500 ); ?>

			<?php dd_load_more_button(); ?>

		</div><!-- .content -->

		<?php if ( $sidebar == 'yes' ) get_sidebar( 'archives' ); ?>

	</div><!-- .container -->

</div><!-- .pageContent -->

<?php get_footer(); ?>