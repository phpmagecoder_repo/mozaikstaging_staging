jQuery(document).ready(function($) {

    /**
     * Tabs
     */

    // Hide tabs and add classes
    $('.tab-content').hide();
    $('.tab-content:first-child').show();
    $('.tab-nav:first-child').addClass('active-tab');

    // Tab nav click
    $('.tab-nav').live('click', function(e){

        e.preventDefault();
        
        $(this).addClass('active-tab').siblings('.active-tab').removeClass('active-tab');
        var tab_id = $(this).index();
        $(this).closest('.tabs-container').find('.tab-content').eq(tab_id).show().siblings('.tab-content').hide();

    });

    // Copy menu for mobile usage
    $('.mobileMenu').clone().appendTo('.topBar').removeClass('big');

    // Remove WP generated width and height for imgs
    $('.wp-post-image').removeAttr('height width');

    $('.shareBtn').on('click', function(e) {
        
        $('.shareIt').fadeToggle(200);
        $(this).toggleClass('shareActive');
        e.preventDefault();
        
    });


    $('.socialNav .search, .close').on('click', function(e) {
        
        $('#searchform').slideToggle(200);
        $('.searchField').focus();
        
        e.preventDefault();
        
    });
    
    
     $('.mobileMenuBtn').on('click', function(e) {
        
        $(this).toggleClass('mobileActive');
        $('.big nav ').fadeToggle(200);
        
        e.preventDefault();
        
    });
    
     $('.topBar .mobileMenuBtn').on('click', function(e) {
        

        $('.topBar .mobileMenu nav').fadeToggle(200);
        
        e.preventDefault();
        
    });

   function sidebar() { 
   
   $contentHeight = $('.content').outerHeight();

   $('.separator').height($contentHeight);
   
}

 function mobileMenu() {
            
        // Create the dropdown base
        $("<select />").appendTo("nav .mobileNavContainer");
        $("nav select").hide();
        
        // Create default option "Go to..."
        $("<option />", {
            "selected": "selected",
            "value"   : "",
            "text"    : "Go to..."
        }).appendTo("nav select");

        // Populate dropdown with menu items
        $("nav a").each(function() {
            var el = $(this);
            $("<option />", {
                "value"   : el.attr("href"),
                "text"    : el.text()
            }).appendTo("nav select");
        });

        $("nav select").change(function() {
            window.location = $(this).find("option:selected").val();
        });
            
        }
        
          
        function select() {
            
        
              // FOR EACH SELECT
    $('nav select').each(function() {
        
        // LET'S PUT OUR MARKUP BEFORE IT
        $(this).before('<div class="select-wrapper">');
        
        // LETS PUR OUR MARKUP AFTER IT
        $(this).after('<span class="select-container"></span></div>');
        
        // UPDATES THE INITIAL SELECTED VALUE
        var initialVal = $(this).children('option:selected').text();
        $(this).siblings('span.select-container').text(initialVal);
        
        // HIDES SELECT BUT LET THE USER STILL CLICK IT
        $(this).css({opacity: 0});  
        
        // WHEN USER CHANGES THE SELECT, WE UPDATE THE SPAN BOX
        $(this).change(function() {
            
            // GETS NEW SELECTED VALUE
            var newSelVal = $(this).children('option:selected').text();
            
            // UPDATES BOX
            $(this).siblings('span.select-container').text(newSelVal);
            
        });
        
    }); 
            
        }

    jQuery(window).load(function() {
       

   jQuery('ul.sf-menu').superfish(); 
 
mobileMenu();
select();
sidebar();

    });
    
    
    jQuery(window).resize(function() {
     
        sidebar();
      
    });

});



/* SLOBODAN'S CODE IS BELLOW */


/* Facebook share */
function fbs_click( shareUrl, shareTitle ) {
    
    // Get info
    u=shareUrl;
    t=shareTitle;

    // Open sharer
    window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');

    // Bye bye
    return false;

}

/**
 * DOM loaded
 */

    jQuery(document).ready(function($){

        /**
         * Add some classes
         */

        $('.widget_dd_category_posts_widget').addClass('widget_category');
        $('.widget_dd_reviews_widget').addClass('widget_review');
        $('.widget_dd_social_widget').addClass('widget_social');
        $('.footerWidgets .widget:last-child').addClass('last-child');


        /**
         * Infinite Scrolling
         */

        // If there are no more posts
        if ( $('li', '.pagination').length < 2 )
            $('.loadMore').addClass('loadMoreFinished').html($('.loadMore').data('finished'));

        // When the load more link is clicked
        $('.loadMore').on('click', function(e){

            // Stop default behaviour
            e.preventDefault();

            // Declare vars
            var current_page, next_page, next_page_link,
            _this = jQuery(this),
            container = '.homePosts';

            // If there are more posts
            if ( ! _this.hasClass('loadMoreFinished') ) {

                // Change text
                _this.html(_this.data('loading'));

                // Get the pages info
                if ( jQuery('body').hasClass('archive') || jQuery('body').hasClass('search') ) container = '.categoryPosts';
                current_page = $('.current', '.pagination').closest('li');
                next_page = current_page.next('li');
                next_page_link = $('a', next_page).attr('href');

                // Fetch, append and prepare for next page
                $.get(next_page_link, function(data){ 
                    
                    // Append the content
                    $(container + ' > li', data).appendTo(container);

                    // Remove current class from previous page
                    $('.current', current_page).removeClass('current');

                    // Add current class to current page
                    $('a', next_page).addClass('current');

                    // If there are no more posts
                    if ( ! next_page.next('li').length )
                        _this.addClass('loadMoreFinished').html(_this.data('finished'));
                    else
                        _this.html(_this.data('default'));

                });

            }

        });

    });


/**
 * Everything loaded
 */

    jQuery(window).load(function(){

    });