<?php 

	get_header();

	$sidebar = ot_get_option( 'cat_page_sidebar', 'yes' );
	$content_class = 'sixteen';

	if ( $sidebar == 'yes' )
		$content_class = 'eleven';

	$cat_title = single_cat_title( '', false );
	$cat_id = get_query_var( 'cat' );

	$count = 0;

	global $paged;

?>

<div class="pageTitle  nav container clearer stretched show-bg">

	<div class="container">

		<div class="sixteen columns"><h1><?php
			if ( is_category() )
				printf( __( 'Category Archives: %s', 'localization' ), single_cat_title( '', false ) );
			elseif ( is_tag() )
				printf( __( 'Tag Archives: %s', 'localization' ), single_tag_title( '', false ) );
			elseif ( is_author() ) {

				the_post();
				printf( __( 'Author Archives: %s', 'localization' ), get_the_author() );

				rewind_posts();
echo "<div style=\"color:black;\">";
echo do_shortcode('[wp_biographia]'); 
echo "</div>";

			} elseif ( is_day() )
				printf( __( 'Daily Archives: %s', 'localization' ), get_the_date() );
			elseif ( is_month() )
				printf( __( 'Monthly Archives: %s', 'localization' ), get_the_date( 'F Y' ) );
			elseif ( is_year() )
				printf( __( 'Yearly Archives: %s', 'localization' ), get_the_date( 'Y' ) );
			else
				_e( 'Archives', 'localization' );
		?></h1></div>

	</div>

</div><!-- .pageTitle -->

<div class="pageContent">

	<div class="container">

		<div class="<?php echo $content_class; ?> columns content">

			<?php if ( is_category() ) : ?>

				<div class="categoryMeta clearfix">

					<h1 class="categoryTitle color-category-<?php echo $cat_id; ?>"><a href="<?php echo get_category_link( $cat_id ); ?>"><?php echo $cat_title; ?></a></h1>

					<ul>
						<li><a class="subscribe" href="<?php echo get_category_feed_link( $cat_id, '' ); ?>"><i class="icon-rss"></i><?php _e( 'subscribe', 'localization' ); ?></a></li>
						<li><a class="popular" href="<?php echo add_query_arg( 'gz_show', 'popular' ); ?>"><i class="icon-star"></i><?php _e( 'popular', 'localization' ); ?></a></li>
						<li><a class="videos" href="<?php echo add_query_arg( 'gz_show', 'videos' ); ?>"><i class="icon-video-1"></i><?php _e( 'videos', 'localization' ); ?></a></li>					
					</ul>

				</div>

			<?php endif; ?>

			<?php if ( have_posts()) : while ( have_posts()) : the_post(); $count++; ?>

				<?php if ( $count == 1 && $paged == 0 ) : ?>

					<div class="categoryFeatured clearfix">

						<div class="four columns alpha categoryFeaturedThumb">

							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'post-medium' ); ?></a>

						</div>

						<?php if ( $sidebar == 'yes') : ?>
							<div class="seven columns omega">
						<?php else : ?>
							<div class="twelve columns omega">
						<?php endif; ?>

							<div class="categoryFeaturedInfo">

								<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
								
								<?php the_excerpt(); ?>

								<ul>

									<li><a class="author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="icon-user"></i><?php the_author(); ?></a></li>
									
									<?php 
                                        if ( has_tag() ) {  
                                            $tags_output = get_the_tag_list( '', ', ', '' );
                                            if ( ! empty( $tags_output ) ) :
                                                ?>
                                                    <li><a class="tags" href="#"><i class="icon-tag"></i></a><?php echo $tags_output; ?> </li>
                                                <?php 
                                            endif;
                                        } 
                                    ?>

									<li><a class="comments" href="<?php comments_link(); ?> "><i class="icon-comment"></i> <?php comments_number( __( '0 comments', 'localization' ), __( '1 comment', 'localization' ), __( '% comments', 'localization' ) ); ?> </a></li>

								</ul>

							</div>

						</div>

					</div><!-- .categoryFeatured -->

				<?php endif; ?>

				<?php if ( $count > 1 || $paged != 0 ) : ?>

					<?php if ( ( $count == 2 && $paged == 0 ) || ( $count == 1 && $paged != 0 ) ) : ?><ul class="categoryPosts"><?php endif; ?>

					<li <?php post_class( 'categoryPost' ); ?>>

						<?php if( has_post_thumbnail() ) : ?>

							<div class="two columns alpha categoryPostsThumbs">
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'post-regular' ); ?></a>
							</div>    

							<?php if ( $sidebar == 'yes') : ?>
								<div class="eight columns">
							<?php else : ?>
								<div class="thirteen columns">
							<?php endif; ?>

						<?php else : ?>

							<?php if ( $sidebar == 'yes') : ?>
								<div class="ten columns alpha">
							<?php else : ?>
								<div class="fifteen columns alpha">
							<?php endif; ?>

						<?php endif; ?>

								<div class="categoryPostsInfo">

									<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><span class="categoryBadge category-<?php echo $cat_id; ?>"><?php the_category(', ');?></span></h1>
									
									<?php the_excerpt(); ?>

									<ul>

										<li><a class="author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="icon-user"></i><?php the_author(); ?></a></li>
										
										<?php 
	                                        if ( has_tag() ) {  
	                                            $tags_output = get_the_tag_list( '', ', ', '' );
	                                            if ( ! empty( $tags_output ) ) :
	                                                ?>
	                                                    <li><a class="tags" href="#"><i class="icon-tag"></i></a><?php echo $tags_output; ?> </li>
	                                                <?php 
	                                            endif;
	                                        } 
	                                    ?>

										<li><a class="comments" href="<?php comments_link(); ?> "><i class="icon-comment"></i> <?php comments_number( '0 comments', '1 comment', '% comments' ); ?> </a></li>

									</ul>

								</div><!-- .categoryPostsInfo -->

							</div><!-- .columns.omega -->

							<div class="one column omega">
								<a class="categoryPostBtn" href="<?php the_permalink(); ?>"><i class="icon-right-open"></i></a>
							</div>

					</li>

				<?php endif; ?>

			<?php endwhile; else :?>


				<div class="pageContent">

					<h4 style="margin-top: 0; margin-bottom: 10px;"><?php _e('THERE IS NO POST AVAILABLE', 'localization'); ?></h4>

					<a href="<?php echo home_url(); ?>" class="back-to-home"><?php _e('Go Back To Homepage', 'localization'); ?> &rarr;</a>

				</div>

			<?php endif; ?> 

			</ul><!-- .categoryPosts -->

			<?php kriesi_pagination( $wp_query->max_num_pages, 500 ); ?>

			<?php dd_load_more_button(); ?>

		</div><!-- .content -->

		<?php if ( $sidebar == 'yes' ) get_sidebar( 'archives' ); ?>

	</div><!-- .container -->

</div><!-- .pageContent -->

<?php get_footer(); ?>