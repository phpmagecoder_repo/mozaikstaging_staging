<?php
	
	$sidebar_id = 'sidebar-archives';
	
	if ( is_category() ) {

		$cat_id = get_query_var( 'cat' );
		if ( is_active_sidebar( 'sidebar-archives-' . $cat_id ) )
			$sidebar_id = 'sidebar-archives-' . $cat_id;

	}

?>

<div class="one column separator">&nbsp</div>

<ul class="sidebar four columns clearfix">

	<?php if ( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( $sidebar_id ) ) : else : ?>
						
		<?php /* Content if sidebar is empty */ ?>
		
	<?php endif; ?>

</ul>