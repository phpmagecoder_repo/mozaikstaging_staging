<!-- Magento Footer -->
<div class="page magento-css">
        <?php
    	//$layout = Mage::getSingleton('core/layout');
     //   $footerBlock = $layout->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
        // echo $footerBlock;

	//$footerScripts =  $layout->createBlock('core/template')->setTemplate('page/html/footer_theme_scripts.phtml')->toHtml();
	 //echo $footerScripts;
        ?>
</div>
<!-- END Magento Footer -->

<?php /*
	<?php if ( 'yes' == ot_get_option ( 'gz_footer_bottom', 'yes' ) || is_active_sidebar( 'sidebar-3' ) ) : ?>
		<footer id="footer" class="<?php if ( 'yes' == ot_get_option ( 'gz_footer_bottom', 'yes' ) ) echo 'footerBottomActive'; ?>">
			<div class="container">
				<div class="sixteen columns">
					<?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
						<div class="footerWidgets sidebar clearfix">
							<?php if ( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'sidebar-3' ) ) : else : ?>
								<?php // Content if sidebar is empty  ?>
							<?php endif; ?>
						</div><!-- .footerWidgets -->
					<?php endif; ?>
					<?php if ( 'yes' == ot_get_option ( 'gz_footer_bottom', 'yes' ) ) : ?>
						<div class="footerBottom">
							<div class="footerLeft">
								<img src="<?php echo ot_get_option( 'logo', get_template_directory_uri() . '/images/logo.png' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
							</div><!-- .footerLeft -->
							<div class="footerRight">
								<?php wp_nav_menu( array( 'theme_location' => 'footer_menu' ) ); ?>
							</div><!-- .footerRight -->
						</div><!-- .footerBottom -->
					<?php endif; ?>
				</div><!-- .sixteen -->
			</div><!-- .container -->
		</footer><!-- #footer -->
	<?php endif; ?>
	<?php echo ot_get_option( 'gz_analytics' , '' ); ?>
	
*/ ?>
<?php wp_footer(); ?> 

<?php //echo Mage::getStoreConfig('design/footer/absolute_footer'); ?>
	
</body>
</html>