<?php
get_header();
$file = deskpress_template_path();

global $deskpress_data;
if ( isset( $deskpress_data['layout'] ) && isset( $deskpress_data['post_page_layout'] ) ) {

} else {
	$deskpress_data['layout']           = '2c-l-fixed';
	$deskpress_data['post_page_layout'] = '2c-r-fixed';
}


if ( get_post_type() == "product" ) {
	include $file;
} elseif ( ( is_category() || is_archive() || is_search() || is_post_type_archive( 'tps' ) ) && !use_bbpress() ) {
	$class            = 'col-sm-9 alignright';
	$sidebar_cl       = " sidebar-left";
	$hide_breadcrumbs = 0;
	if ( isset( $deskpress_data['opt_hide_breadcrumbs_archive'] ) && $deskpress_data['opt_hide_breadcrumbs_archive'] != '0' ) {
		$hide_breadcrumbs = $deskpress_data['opt_hide_breadcrumbs_archive'];
	}

	if ( $deskpress_data['layout'] == '2c-r-fixed' ) {
		$class      = "col-sm-9 alignleft";
		$sidebar_cl = " sidebar-right";
	}
	if ( $deskpress_data['layout'] == '1col-fixed' ) {
		$class      = "col-sm-12 fullwith";
		$sidebar_cl = "";
	}
	?>
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<!--remove archive top -->
			<?php

			//
			if ( use_bbpress() || is_home() || is_front_page() || is_post_type_archive( 'tps' ) || ( get_post_type() == "tps" ) ) {
				// form search
			} elseif ( 'download' == get_post_type() ) {
				get_template_part( 'templates/archive-edd', 'top' );
			} else {
				get_template_part( 'templates/archive', 'top' );
			}
			?>
			<div class="container">
				<div class="row">
					<div class="content-post <?php echo $sidebar_cl; ?>">
						<div class="<?php echo $class; ?>">
							<?php
							if ( $hide_breadcrumbs != '1' ) {
								if ( use_bbpress() || is_home() || is_front_page() || is_post_type_archive( 'tps' ) || ( get_post_type() == "tps" ) ) {
									// form search
								} else {
									deskpress_breadcrumbs();
								}
							}
							if ( get_post_type() == "download" ) {
								get_template_part( 'edd_templates/content' );
							} else {
								include $file;
							}
							?>
						</div>
						<?php
						if ( $class == "col-sm-9 alignleft" ) {
							get_sidebar();
						} elseif ( $class == "col-sm-9 alignright" ) {
							get_sidebar();
						}
						?>
					</div>
					<!--end content-post-->
				</div>
				<!-- row-->
			</div>
			<!-- container-->
		</main>
		<!-- #main -->
	</section><!-- #primary -->
<?php
} else {

	if ( is_page() || is_single() || use_bbpress() ) {
		if ( is_page_template( 'page-templates/homepage.php' ) ) {
			$class      = 'col-sm-9 alignright';
			$sidebar_cl = " sidebar-left";
			?>
			<main id="main" class="site-main main-single" role="main">
					<div class="container">
					<div class="row">
						<div class="content-post <?php echo $sidebar_cl; ?>">
							<div class="<?php echo $class; ?>">
								<?php

								include $file;
								?>
							</div>
							<?php
							if ( $class == "col-sm-9 alignleft" ) {
								get_sidebar();
							} elseif ( $class == "col-sm-9 alignright" ) {
								get_sidebar();
							}
							?>
						</div>
						<!--end content-post-->
					</div>
					<!--end row-->
				</div>
				<!--end container -->
				
			</main><!--end main-->
		<?php
		} elseif ( is_page_template( 'page-templates/contact.php' ) ) {
			get_template_part( 'templates/page', 'top' );
			include $file;
		} elseif ( is_page() ) {
			$class      = 'col-sm-9 alignright';
			$sidebar_cl = " sidebar-left";
			if ( $deskpress_data['post_page_layout'] == '2c-r-fixed' ) {
				$class      = "col-sm-9 alignleft";
				$sidebar_cl = " sidebar-right";
			}
			if ( $deskpress_data['post_page_layout'] == '1col-fixed' ) {
				$class      = "col-sm-12 fullwith";
				$sidebar_cl = "";
			}
			?>
			<main id="main" class="site-main main-single" role="main">
				<?php
				get_template_part( 'templates/page', 'top' );
				?>
				<div class="container">
					<div class="row">
						<div class="content-post <?php echo $sidebar_cl; ?>">
							<div class="<?php echo $class; ?>">
								<?php

								include $file;
								?>
							</div>
							<?php
							if ( $class == "col-sm-9 alignleft" ) {
								get_sidebar();
							} elseif ( $class == "col-sm-9 alignright" ) {
								get_sidebar();
							}
							?>
						</div>
						<!--end content-post-->
					</div>
					<!--end row-->
				</div>
				<!--end container -->
			</main><!--end main-->
		<?php
		} else {
			$class            = 'col-sm-9 alignright';
			$sidebar_cl       = " sidebar-left";
			$hide_breadcrumbs = '';
			if ( isset( $deskpress_data['opt_hide_breadcrumbs'] ) && $deskpress_data['opt_hide_breadcrumbs'] != '0' ) {
				$hide_breadcrumbs = $deskpress_data['opt_hide_breadcrumbs'];
			}

			if ( $deskpress_data['post_page_layout'] == '2c-r-fixed' ) {
				$class      = "col-sm-9 alignleft";
				$sidebar_cl = " sidebar-right";
			}
			if ( $deskpress_data['post_page_layout'] == '1col-fixed' ) {
				$class      = "col-sm-12 fullwith";
				$sidebar_cl = "";
			}
			?>
			<main id="main" class="site-main main-single" role="main">
				<?php
				if ( use_bbpress() || is_home() || is_front_page() || is_post_type_archive( 'tps' ) || ( get_post_type() == "tps" ) ) {
				} else {
					get_template_part( 'templates/content', 'top' );
				}
				?>
				<div class="container">
					<div class="row">
						<div class="content-post <?php echo $sidebar_cl; ?>">
							<div class="<?php echo $class; ?>">
								<?php
								if ( get_post_type() == "download" ) {
									get_template_part( 'edd_templates/content', 'single' );
								} else {
									include $file;
								} ?>
							</div>
							<?php
							if ( $class == "col-sm-9 alignleft" ) {
								get_sidebar();
							} elseif ( $class == "col-sm-9 alignright" ) {
								get_sidebar();
							}
							?>
						</div>
						<!--end content-post-->
					</div>
					<!--end row-->
				</div>
				<!--end container -->
			</main><!--end main-->
		<?php
		}
	} else {
		// 404
		include $file;
	}
}
?>
<?php
get_footer();
?>
