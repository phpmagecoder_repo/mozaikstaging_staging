<?php
/**
 * @package deskpress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="content-inner">
		<?php
		//global $sidebar_thumb_size;
		//do_action( 'deskpress_entry_top', $sidebar_thumb_size );

		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			?>
			<div class="post-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
					<?php the_post_thumbnail( 'medium' ); ?>
				</a>
			</div>
		<?php
		}
		$class = '';
		switch ( get_post_type() ) {
			case 'topic':
				$class = ' fa-comment ';
				break;
			case 'tps-faq':
				$class = ' fa-question ';
				break;
			case 'post':
				$class = ' fa-file-text ';
				break;
			default:
				$class = ' fa-book ';
		}
		?>
		<div class="entry-content">
			<header class="entry-header">
				<h4>
					<span><i class="fa fa-fw <?php echo $class ?>"></i></span>
					<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
						<?php if ( function_exists( 'tps_search_highlight' ) ) {
							echo tps_search_highlight( get_search_query(), the_title( '', '', false ) );
						}?>
					</a>
				</h4>
				<?php deskpress_posted_on(false); ?>
			</header>
			<!-- .entry-header -->
			<div class="entry-summary">
				<?php
				if ( function_exists( 'tps_search_highlight' ) ) {
					echo tps_search_highlight( get_search_query(), apply_filters( 'the_excerpt', get_the_excerpt() ) );
				}
				?>
			</div>
			<!-- .entry-summary -->
		</div>
	</div>
</article><!-- #post-## -->
