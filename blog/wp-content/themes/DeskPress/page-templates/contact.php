<?php
/**
 * Template Name: Page Contact
 *
 **/
?>
<div id="main-content" class="page_contact" role="main">
	<?php
	// Start the Loop.
	while ( have_posts() ) : the_post();
 		the_content();
	endwhile;
	?>
</div><!-- #main-content -->

