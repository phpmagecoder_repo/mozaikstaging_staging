<?php
/**
 * Template Name: Home Page
 *
 **/
?>
<div id="main-content" class="home-content" role="main">
	<?php
	// Start the Loop.
	while ( have_posts() ) : the_post();
		the_content();
	endwhile;
	?>
</div><!-- #main-content -->

