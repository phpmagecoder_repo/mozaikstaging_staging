<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

global $product, $woocommerce_loop, $deskpress_data, $post;
$attachment_ids = $product->get_gallery_attachment_ids();


// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}
$classes = array();
$classes[] = 'product_animated animated';

?>

<li <?php post_class( $classes ); ?>>
	<div class="item-product">
		<div class="product-hover">
			<?php
			if ( isset( $deskpress_data['product_image_hover'] ) && $deskpress_data['product_image_hover'] == "changeimages" ) {
				echo '<div class="product-image">';
			} else {
				echo '<div class="product-image flip-wrapper">';
			}?>
			<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
			<div class="box-function">
				<?php do_action( 'woocommerce_after_shop_loop_item' );?>
			</div>
			</div>
		</div>
 		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="link_hover">&nbsp;</a>
	</div>
	<h3 class="product-name"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
	<?php
	/**
	 * woocommerce_after_shop_loop_item_title hook
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );
	?>
 </li>
