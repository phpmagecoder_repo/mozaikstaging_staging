<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

get_header( 'shop' );
if (get_post_type() != 'product') {
	return;
}
/***********custom Top Images*************/
global $deskpress_data, $wp_query;
$cat_obj = $wp_query->get_queried_object();
$class = 'col-sm-9 alignright';
$sidebar_cl =" product-sidebar-left";
if (isset($deskpress_data['product_layout'] ) && $deskpress_data['product_layout'] == '2c-r-fixed') {
	$class = "col-sm-9 alignleft";
	$sidebar_cl =" product-sidebar-right";
}
if (isset($deskpress_data['product_layout'] ) && $deskpress_data['product_layout'] == '1col-fixed') {
	$class = "col-sm-12 fullwith";
	$sidebar_cl = "";
}

?>
	<div class="main-content-shop site-main" role="main">
		<?php get_template_part( 'templates/archive', 'top' ); ?>
 		<div class="container archive-product-wapper<?php echo $sidebar_cl; ?>">
			<div class="row">
				<div class="archive-product-content <?php echo $class; ?>">
					<?php
						$hide_breadcrumbs = '';
						if ( isset( $deskpress_data['opt_hide_breadcrumbs_product'] ) && $deskpress_data['opt_hide_breadcrumbs_product'] != '0' ) {
							$hide_breadcrumbs = $deskpress_data['opt_hide_breadcrumbs_product'];
						}
						if ( $hide_breadcrumbs != '1' ) {
 							woocommerce_breadcrumb();
 						}
					?>

					<?php if ( have_posts() ) : ?>

						<?php
						/**
						 * woocommerce_before_shop_loop hook
						 *
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						do_action( 'woocommerce_before_shop_loop' );
						?>
						<?php woocommerce_product_loop_start(); ?>

						<?php woocommerce_product_subcategories(); ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php wc_get_template_part( 'content', 'product' ); ?>

						<?php endwhile; // end of the loop. ?>

						<?php woocommerce_product_loop_end(); ?>
 						<?php
						/**
						 * woocommerce_after_shop_loop hook
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
						?>

					<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
						<?php wc_get_template( 'loop/no-products-found.php' ); ?>
					<?php endif; ?>
				</div>

				<?php
					/**
					 * woocommerce_sidebar hook
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					if ( $class != "col-sm-12 fullwith" ) {
						do_action( 'woocommerce_sidebar' );
					}
					?>
			</div>
		</div>
	</div>
<?php get_footer( 'shop' ); ?>