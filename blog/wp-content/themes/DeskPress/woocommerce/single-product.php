<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

get_header( 'shop' );

global $deskpress_data;
 /*********** Theme option*************/
$class = 'col-sm-9 alignright';
$sidebar_cl = " sidebar-left";
if (isset($deskpress_data['product_layout'] ) &&  $deskpress_data['product_layout'] == '2c-r-fixed' ) {
	$class      = "col-sm-9 alignleft";
	$sidebar_cl = " sidebar-right";
}
if ( isset($deskpress_data['product_layout'] ) && $deskpress_data['product_layout'] == '1col-fixed' ) {
	$class      = "col-sm-12 fullwith";
	$sidebar_cl = "";
}
 ?>
	<main id="main" class="site-main main-product" role="main">
		<?php get_template_part( 'templates/content', 'top' ); ?>
 		<div class="container content-site-main"><div class="row">
		<div class="content-post <?php echo $sidebar_cl; ?>">
 			<div class="main-content <?php echo $class; ?>">
				<?php
				$hide_breadcrumbs = '';
				if ( isset( $deskpress_data['opt_hide_breadcrumbs_product'] ) && $deskpress_data['opt_hide_breadcrumbs_product'] != '0' ) {
					$hide_breadcrumbs = $deskpress_data['opt_hide_breadcrumbs_product'];
				}
				if ( $hide_breadcrumbs != '1' ) {
  					woocommerce_breadcrumb();
  				}
				?>
			<?php
			/**
			 * woocommerce_before_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 */
			do_action( 'woocommerce_before_main_content' );
			?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php woocommerce_get_template_part( 'content', 'single-product' ); ?>
			<?php endwhile; // end of the loop. ?>
			<?php
			/**
			 * woocommerce_after_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
			?>
			</div>
			<?php

			if ( $class == "col-sm-9 alignleft" ) {
				get_sidebar();
			} elseif ( $class == "col-sm-9 alignright" ) {
				get_sidebar();
			}
			?>
		</div>
		 </div></div>
	</main>

<?php get_footer( 'shop' ); ?>