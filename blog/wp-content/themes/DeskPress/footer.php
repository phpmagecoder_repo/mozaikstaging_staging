<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package deskpress
 */

//$var = wp_get_sidebars_widgets()['footer'];
//echo "<pre>";
//print_r( $var );
//echo "</pre>";
?>
</div>
</div><!-- end #wapper-->
<!-- Magento Footer -->
<div class="page magento-css">
        <?php
        $layout = Mage::getSingleton('core/layout');
        $themename =  Mage::getSingleton('core/design_package')->getTheme('frontend');
        if($themename =='child')
        {
		  	$footerBlock = $layout->createBlock('page/html_footer')->setTemplate('page/html/wp_footer.phtml')->toHtml();
		  	  echo $footerBlock;
		  }
		  else{
		  	$footerBlock = $layout->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
		  	  echo $footerBlock;
		  }
    	
        
       

	//$footerScripts =  $layout->createBlock('core/template')->setTemplate('page/html/footer_theme_scripts.phtml')->toHtml();
	 //echo $footerScripts;
        ?>
</div>
<!-- END Magento Footer -->

<?php global $deskpress_data; ?>


<?php /* REMOVE WP FOOTER
<!-- #content -->
<?php if ( isset( $deskpress_data['show_main_bottom'] ) && $deskpress_data['show_main_bottom'] != '1' ) {
	$column_botom_right = $column_botom_left = '6';
	if ( isset( $deskpress_data['column_botom_left'] ) ) {
		$column_botom_left  = $deskpress_data['column_botom_left'];
		$column_botom_right = 12 - $deskpress_data['column_botom_left'];
	}
	if ( is_active_sidebar( 'main_bottom_1' ) || is_active_sidebar( 'main_bottom_2' ) ) : ?>
		<div class="main_bottom_area">
			<div class="container">
				<div class="row">
					<?php if ( is_active_sidebar( 'main_bottom_1' ) ) : ?>
						<div class="col-sm-<?php echo $column_botom_left; ?>">
							<?php dynamic_sidebar( 'main_bottom_1' ); ?>
						</div><!-- col-sm-6 -->
					<?php endif; ?>
					<?php if ( is_active_sidebar( 'main_bottom_2' ) ) : ?>
						<div class="col-sm-<?php echo $column_botom_right; ?>">
							<?php dynamic_sidebar( 'main_bottom_2' ); ?>
						</div><!-- col-sm-6 -->
					<?php endif; ?>
				</div>
			</div>
		</div><!--end main bottom area-->
	<?php endif;
}
?>



<footer id="colophon" class="site-footer" role="contentinfo">
	<?php if ( is_active_sidebar( 'footer_1' ) || is_active_sidebar( 'footer_2' ) || is_active_sidebar( 'footer_3' ) || is_active_sidebar( 'footer_4' ) ) : ?>
		<div class="footer">
			<div class="container">
				<div class="row">
					<?php
					$wd_column = array( "4", "4", "4", "4" );
					if ( isset( $deskpress_data['width_columns'] ) ) {
						$width     = preg_replace( '/\s+/', '', $deskpress_data['width_columns'] );
						$wd_column = explode( '+', $width );
					}
					if ( isset( $deskpress_data['footer_widgets_columns'] ) ) {
						$column_footer = $deskpress_data['footer_widgets_columns'];
					} else {
						$column_footer = '4';
					}
					?>
					<?php
					for ( $i = 1; $i <= $column_footer; $i ++ ) {
						$widget_footer = "footer_" . $i;
						?>
						<?php if ( is_active_sidebar( $widget_footer ) ) : ?>
							<div class="col-sm-<?php echo $wd_column[$i - 1]; ?>">
								<?php dynamic_sidebar( $widget_footer ); ?>
							</div><!-- col-sm-6 -->
						<?php endif; ?>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<!-- Copyright -->
	<div class="copyright_area">
		<div class="container">
			<?php if ( isset( $deskpress_data['footer_text'] ) ) {
				echo '<div class="copyright"><p>' . $deskpress_data['footer_text'] . '</p></div>';
			} ?>
			<?php if ( is_active_sidebar( 'copyright' ) ) : ?>
				<div class="column_right">
					<?php dynamic_sidebar( 'copyright' ); ?>
				</div><!-- col-sm-6 -->
			<?php endif; ?>
		</div>
	</div>
</footer><!-- #colophon -->

</div><!-- #page -->
<?php if ( isset( $deskpress_data['show_drawer_right'] ) && $deskpress_data['show_drawer_right'] == '1' && is_active_sidebar( 'drawer_right' ) ) { ?>
	<div class="slider_sidebar">
		<?php dynamic_sidebar( 'drawer_right' ); ?>
	</div>  <!--slider_sidebar-->
<?php } ?>
<!-- .box-area -->
<?php if ( isset( $deskpress_data['totop_show'] ) && $deskpress_data['totop_show'] == 1 ) { ?>
	<div id="topcontrol" class="icon-up-open" title="<?php _e( 'Scroll To Top', 'deskpress' ); ?>">
		<i class="fa fa-angle-up"></i></div>
<?php } ?>

<?php echo $deskpress_data['google_analytics']; ?>

REMOVE WP FOOTER END */  ?>
<?php wp_footer(); ?>

<?php echo Mage::getStoreConfig('design/footer/absolute_footer'); ?>
</body>
</html>

