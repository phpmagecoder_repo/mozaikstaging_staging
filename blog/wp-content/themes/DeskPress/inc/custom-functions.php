<?php

/**
 * Enqueue the Open Sans font.
 */
global $deskpress_data;
if ( isset( $deskpress_data['google_body_font'] ) && isset( $deskpress_data['google_headings'] ) ) {
	$customfont  = '';
	$default     = array(
		'Select Font',
	);
	$googlefonts = array(
		$deskpress_data['google_body_font'],
		$deskpress_data['google_headings'],
	);
	foreach ( $googlefonts as $googlefont ) {
		if ( ! in_array( $googlefont, $default ) ) {
			$customfont = str_replace( ' ', '+', $googlefont ) . ':300,300italic,400,400italic,500,600,700,800|' . $customfont;
		}
	}
	if ( $customfont != "" ) {

		function google_fonts() {
			global $customfont;
			$protocol = is_ssl() ? 'https' : 'http';
			wp_enqueue_style( 'deskpress-googlefonts', "$protocol://fonts.googleapis.com/css?family=" . substr_replace( $customfont, "", - 1 ) );
		}

		add_action( 'wp_enqueue_scripts', 'google_fonts' );
	}
}

/**
 * Enqueue scripts and styles.
 */
function deskpress_scripts() {
	global $deskpress_is_dev, $deskpress_data;
	wp_enqueue_script( 'deskpress-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'deskpress-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	if ( isset( $deskpress_data['bootstrap_css'] ) && $deskpress_data['bootstrap_css'] == "css_cdn" ) {
		wp_enqueue_style( 'deskpress-bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css', array(), null );
	} else {
		wp_enqueue_style( 'deskpress-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), null );
	}

	if ( isset( $deskpress_data['font_awesome'] ) && $deskpress_data['font_awesome'] == "font_awesome_cdn" ) {
		wp_enqueue_style( 'deskpress-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css', array(), null );
	} else {
		wp_enqueue_style( 'deskpress-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), null );
	}

	if ( ! $deskpress_is_dev ) {
		if ( is_multisite() ) {
			wp_enqueue_style( 'deskpress-style', get_template_directory_uri() . '/style-' . get_current_blog_id() . '.css', array(), null );
		} else {
			wp_enqueue_style( 'deskpress-style', get_stylesheet_uri(), array(), null );
		}
	}

	if ( isset( $deskpress_data['bootstrap_js'] ) && $deskpress_data['bootstrap_js'] == "js_cdn" ) {
		wp_enqueue_script( 'deskpress-script', '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js', array( 'jquery' ), null, true );
	} else {
		wp_enqueue_script( 'deskpress-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), null, true );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'deskpress-custom-script', get_template_directory_uri() . '/js/custom-script.js', array( 'jquery' ), null, true );
}

add_action( 'wp_enqueue_scripts', 'deskpress_scripts' );

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function deskpress_meta( $key, $args = array(), $post_id = null ) {
	if ( ! function_exists( 'rwmb_meta' ) ) {
		return false;
	}

	return rwmb_meta( $key, $args, $post_id );
}

function deskpress_hex2rgb( $hex ) {
	$hex = str_replace( "#", "", $hex );
	if ( strlen( $hex ) == 3 ) {
		$r = hexdec( substr( $hex, 0, 1 ) . substr( $hex, 0, 1 ) );
		$g = hexdec( substr( $hex, 1, 1 ) . substr( $hex, 1, 1 ) );
		$b = hexdec( substr( $hex, 2, 1 ) . substr( $hex, 2, 1 ) );
	} else {
		$r = hexdec( substr( $hex, 0, 2 ) );
		$g = hexdec( substr( $hex, 2, 2 ) );
		$b = hexdec( substr( $hex, 4, 2 ) );
	}
	$rgb = array( $r, $g, $b );

	return $rgb; // returns an array with the rgb values
}

function excerpt( $limit ) {
	$content = get_the_excerpt();
	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );
	$content = explode( ' ', $content, $limit );
	array_pop( $content );
	$content = implode( " ", $content );
	$content = '<div class="excerpt">' . $content . '</div>';

	return $content;
}


/* * ******************************************************************
 * Breadcrumbs
 * ****************************************************************** */

function deskpress_breadcrumbs() {
	global $wp_query, $post;

	// Start the UL
	echo '<div class="woocommerce-breadcrumb breadcrumb" itemprop="breadcrumb">';
	echo '<a href="' . home_url() . '" class="home">' . __( "Home", 'deskpress' ) . '</a>';
	if ( is_category() ) {
		$catTitle = single_cat_title( "", false );
		$cat      = get_cat_ID( $catTitle );
		echo '<i class="separator"></i>' . get_category_parents( $cat, true, "" );
	}  elseif ( is_archive() && ! is_category() ) {
		if (get_post_type() == "download") {
			if ( is_tax( 'download_category' ) ) {
				$current_term = $wp_query->get_queried_object();
				$ancestors = array_reverse( get_ancestors( $current_term->term_id, 'download_category' ) );
				foreach ( $ancestors as $ancestor ) {
					$ancestor = get_term( $ancestor, 'download_category' );
					echo  '<a href="' . get_term_link( $ancestor ) . '">' . esc_html( $ancestor->name ) . '</a>' ;
				}
				echo  '<i class="separator"></i> '. esc_html( $current_term->name );
			}else{
				echo _e(' <i class="separator"></i> Download ', 'deskpress');
			}
		}else {
			echo _e( '<i class="separator"></i> Archive', 'deskpress' );
 		}
 	} elseif ( is_search() ) {
		echo _e( '<i class="separator"></i> Search Result', 'deskpress' );
	} elseif ( is_404() ) {
		echo _e( '<i class="separator"></i> 404 Not Found', 'deskpress' );
	} elseif ( is_single( $post ) ) {
		if ( get_post_type() == 'post' ) {
			$category    = get_the_category();
			$category_id = get_cat_ID( $category[0]->cat_name );
			echo '<i class="separator"></i>' . get_category_parents( $category_id, true, "<i class='separator'></i>" );
			echo the_title( '', '', false );
		} else {
			echo '<i class="separator"></i>' . get_post_type();
			echo '<i class="separator"></i>' . get_the_title();
		}
	} elseif ( is_page() ) {
		$post = $wp_query->get_queried_object();

		if ( $post->post_parent == 0 ) {

			echo "<a href='#'><i class='separator'></i>" . the_title( '', '', false ) . "</a>";
		} else {
			$ancestors = array_reverse( get_post_ancestors( $post->ID ) );
			array_push( $ancestors, $post->ID );

			foreach ( $ancestors as $ancestor ) {
				if ( $ancestor != end( $ancestors ) ) {
					echo '<a href="' . get_permalink( $ancestor ) . '"><i class="separator"></i>' . strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) . '</a>';
				} else {
					echo '<i class="separator"></i>' . strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) );
				}
			}
		}
	} elseif ( is_attachment() ) {
		$parent = get_post( $post->post_parent );
		if ( $parent->post_type == 'page' || $parent->post_type == 'post' ) {
			$cat = get_the_category( $parent->ID );
			$cat = $cat[0];
			echo get_category_parents( $cat, true, ' ' );
		}

		echo '<a href="' . get_permalink( $parent ) . '">' . $parent->post_title . '</a> ';
		echo get_the_title();
	}
	// End the UL
	echo "</div>";
}


function deskpress_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	extract( $args, EXTR_SKIP );

	if ( 'div' == $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}
	?>
	<<?php echo $tag ?> <?php comment_class( 'description_comment' ) ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>">
	<?php endif; ?>
	<div class="des_blog">
		<div class="avatar"><?php
			if ( $args['avatar_size'] != 0 ) {
				echo get_avatar( $comment, $args['avatar_size'] );
			}
			?></div>
		<div class="comment_content">
			<div class="comment-author meta">
				<?php printf( __( '<strong>%s</strong>', 'deskpress' ), get_comment_author_link() ) ?>
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'deskpress' ) ?></em>
				<?php endif; ?>
				<?php
				printf( get_comment_date(), get_comment_time() )
				?>
				<?php comment_reply_link( array_merge( $args, array(
					'add_below' => $add_below,
					'depth'     => $depth,
					'max_depth' => $args['max_depth']
				) ) ) ?>
				<?php edit_comment_link( __( 'Edit', 'deskpress' ), '', '' ); ?>
			</div>
			<div class="comment_text"><?php comment_text() ?></div>
		</div>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
		</div>
	<?php endif; ?>
<?php
}
