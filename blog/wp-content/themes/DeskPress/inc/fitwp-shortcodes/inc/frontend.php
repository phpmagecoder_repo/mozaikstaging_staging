<?php

class FITSC_Frontend {
	/**
	 * Store the active status of current tab
	 *
	 * @var bool
	 */
	static $tab_active;
	static $intID;
	static $shortcodes_clone;
	/**
	 * Hold all custom js code
	 *
	 * @var array
	 */
	public $js = array();

	/**
	 * Constructor
	 *
	 * @return FITSC_Frontend
	 */
	function __construct() {
		self::$intID = 0;


		// Enqueue shortcodes scripts and styles
		// High priority = enqueue before theme styles = theme can overwrite styles
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ), 1 );
		//add_action( 'wp_footer', array( $this, 'footer' ), 1000 );

		// Register shortcodes
		$shortcodes             = array(
			'video',
			'icon_box',
			'more',
			'row',
			'row_inner',
			'column',
			'column_inner',
			'text_block',
			'statistics',
			'text_rotator',
			'quotes',
			'button',
			'toggles',
			'toggle',
			'accordions',
			'accordion',
			'tabs',
			'tab',
			'promo_box',
			'testimonial',
			'recent_topics',
			'kb_articles',
			'kb_category',
			'map'

		);
		self::$shortcodes_clone = $shortcodes;
		foreach ( $shortcodes as $shortcode ) {
			add_shortcode( $shortcode, array( $this, $shortcode ) );
		}
		add_shortcode( 'list', array( $this, 'custom_list' ) );

		add_filter( 'the_content', array( $this, 'shortcodes_formatter' ) );
		add_filter( 'widget_text', array( $this, 'shortcodes_formatter' ) );

	}

	function shortcodes_formatter( $content ) {
		$block = join( "|", self::$shortcodes_clone );

		// opening tag
		$rep = preg_replace( "/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/", "[$2$3]", $content );

		// closing tag
		$rep = preg_replace( "/(<p>)?\[\/($block)](<\/p>|<br \/>)/", "[/$2]", $rep );

		return $rep;
	}

	/**
	 * Enqueue scripts and styles
	 *
	 * Allow themes to overwrite shortcode script/style
	 * - false: use plugin (default) script/style
	 * - true: no js/css file is enqueued
	 * - string: URL of custom js/css file, which will be enqueued
	 *
	 * @return void
	 */
	function enqueue() {
		$script = apply_filters( 'fitsc_custom_script', false );
		if ( false === $script ) {
			$script = FITSC_URL . 'js/frontend.js';
		}
		if ( is_string( $script ) ) {
			wp_enqueue_script( 'fitsc', $script, array( 'jquery' ), '', true );
		}

		$style = apply_filters( 'fitsc_custom_style', false );
		if ( false === $style ) {
//			$style = FITSC_URL . 'css/frontend.css';
		}
//		if ( is_string( $style ) ) {
//			wp_enqueue_style( 'fitsc', $style );
//		}
	}

	/**
	 * Remove empty <br>, <p> tags
	 *
	 * @param  string $text
	 *
	 * @return string
	 */
	function cleanup( $text ) {
		return str_replace( array( '<br>', '<br />', '<p></p>' ), '', $text );
	}

	/**
	 * Show dropcap shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function dropcap( $atts, $content ) {
		extract( shortcode_atts( array(
			'type' => '',
		), $atts ) );

		return '<span class="fitsc-dropcap' . ( $type ? " fitsc-$type" : '' ) . '">' . $content . '</span>';
	}

	/**
	 * Show button shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function button( $atts, $content ) {
		extract( shortcode_atts( array(
			'link'          => '#',
			'color'         => '',
			'opacity'       => '',
			'size'          => '',
			'icon'          => '',
			'icon_position' => '',
			'id'            => '',
			'nofollow'      => '',
			'background'    => '',
			'text_color'    => '',
			'target'        => '',
			'align'         => '',
			'position'      => '',
			//'position_unit'         => '',

			'full'          => '',
			'class'         => '',
		), $atts ) );

		$classes = array( 'fitsc-button' );
		if ( $full ) {
			$classes[] = 'fitsc-full';
		}
		if ( $class ) {
			$classes[] = $class;
		}
		if ( 'right' == $icon_position ) {
			$classes[] = 'fitsc-icon-right';
		}
		if ( $color ) {
			$classes[] = "fitsc-background-$color";
		} else {
			$classes[] = "fitsc-btn-defaults";
		}
		if ( $align ) {
			$classes[] = "fitsc-align-$align";
		}
		if ( $size ) {
			$classes[] = "fitsc-$size";
		}
		$classes = implode( ' ', $classes );
		$style   = '';
		if ( $background ) {
			$style .= "background:$background;";
		}
		if ( $text_color ) {
			$style .= "color:$text_color;";
		}
		if ( $opacity ) {
			$style .= "opacity:$opacity;";
		}

		$html    = "<a href='" . esc_url( $link ) . "' class='$classes'" .
		           ( $id ? " id='$id'" : '' ) .
		           ( $nofollow ? " rel='nofollow'" : '' ) .
		           ( $target ? " target='$target'" : '' ) .
		           ( $style ? " style='$style'" : '' ) .
		           '>';
		$content = apply_filters( 'fitsc', $content );
		if ( $icon ) {
			$icon    = '<i class="' . $icon . '"></i>';
			$content = $icon_position == 'right' ? ( $content . $icon ) : ( $icon . $content );
		}
		$html .= $content . '</a>';
		if ( $align == 'center' ) {
			$position_temp = "";
			if ( $position != "" ) {
				$position = explode( " ", $position );
				if ( $position[0] != "" && $position[0] != 'none' ) {
					if ( is_numeric( $position[0] ) ) {
						$position_temp .= "top: " . $position[0] . "px;";
					} else {
						$position_temp .= "top: " . $position[0] . ";";
					}
				} else {
					$position_temp .= "";
				}

				if ( $position[1] != "" && $position[1] != 'none' ) {
					if ( is_numeric( $position[1] ) ) {
						$position_temp .= " right:" . $position[1] . "px;";
					} else {
						$position_temp .= " right:" . $position[1] . ";";
					}
				} else {
					$position_temp .= "";
				}

				if ( $position[2] != "" && $position[2] != 'none' ) {
					if ( is_numeric( $position[2] ) ) {
						$position_temp .= " bottom: " . $position[2] . "px;";
					} else {
						$position_temp .= " bottom: " . $position[2] . ";";
					}
				} else {
					$position_temp .= "";
				}

				if ( $position[3] != "" && $position[3] != 'none' ) {
					if ( is_numeric( $position[3] ) ) {
						$position_temp .= " left: " . $position[3] . "px;";
					} else {
						$position_temp .= " left: " . $position[3] . ";";
					}
				} else {
					$position_temp .= "";
				}
				$position_temp .= "position:relative;";
			}
			$html = '<div style="text-align:center;' . $position_temp . '">' . $html . '</div>';
		}

		return $html;
	}


	/**
	 * Show toggles shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function toggles( $atts, $content ) {
		// Get all toggle titles
		preg_match_all( '#\[toggle [^\]]*?title=[\'"]?(.*?)[\'"]#', $content, $matches );

		if ( empty( $matches[1] ) ) {
			return '';
		}

		return sprintf(
			'<div class="fitsc-toggles">%s</div>',
			do_shortcode( $content )
		);
	}

	/**
	 * Show toggle shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function toggle( $atts, $content ) {
		extract( shortcode_atts( array(
			'title' => '',
		), $atts ) );
		if ( ! $title || ! $content ) {
			return '';
		}

		return sprintf( '
			<div class="fitsc-toggle">
				<div class="fitsc-title">%s</div>
				<div class="fitsc-content">%s</div>
			</div>',
			$title,
			apply_filters( 'fitsc_content', $content )
		);
	}

	/**
	 * Show tabs shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function tabs( $atts, $content ) {
		extract( shortcode_atts( array(
			'type' => '',
		), $atts ) );

		// Get all tab titles
		preg_match_all( '#\[tab [^\]]*?\]#', $content, $matches );

		if ( empty( $matches ) ) {
			return '';
		}

		$tpl = '<li%s><a href="#">%s%s</a></li>';
		$lis = '';
		foreach ( $matches[0] as $k => $match ) {
			$tab_atts = shortcode_parse_atts( substr( $match, 1, - 1 ) );
			$tab_atts = shortcode_atts( array(
				'title' => '',
				'icon'  => '',
			), $tab_atts );
			$lis .= sprintf(
				$tpl,
				$k ? '' : ' class="fitsc-active"',
				$tab_atts['icon'] ? '<i class="' . $tab_atts['icon'] . '"></i>' : '',
				$tab_atts['title']
			);
		}

		self::$tab_active = true;

		return sprintf(
			'<div class="fitsc-tabs%s">
				<ul class="fitsc-nav">%s</ul>
				<div class="fitsc-content">%s</div>
			</div>',
			$type ? " fitsc-$type" : '',
			$lis,
			do_shortcode( $content )
		);
	}

	/**
	 * Show tab shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function tab( $atts, $content ) {
		$class            = 'fitsc-tab' . ( self::$tab_active ? ' fitsc-active' : '' );
		self::$tab_active = false;

		return sprintf(
			'<div class="%s">%s</div>',
			$class,
			apply_filters( 'fitsc_content', $content )
		);
	}

	/**
	 * Show accordions shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function accordions( $atts, $content ) {
		// Get all toggle titles
		preg_match_all( '#\[accordion [^\]]*?title=[\'"]?(.*?)[\'"]#', $content, $matches );

		if ( empty( $matches[1] ) ) {
			return '';
		}

		return sprintf(
			'<div class="fitsc-accordions">%s</div>',
			do_shortcode( $content )
		);
	}

	/**
	 * Show accordion shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function accordion( $atts, $content ) {
		extract( shortcode_atts( array(
			'title' => '',
		), $atts ) );
		if ( ! $title || ! $content ) {
			return '';
		}

		return sprintf( '
			<div class="fitsc-accordion">
				<div class="fitsc-title">%s</div>
				<div class="fitsc-content">%s</div>
			</div>',
			$title,
			apply_filters( 'fitsc_content', $content )
		);
	}

	/**
	 * Show tooltip shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function tooltip( $atts, $content ) {
		$atts = shortcode_atts( array(
			'content' => '',
			'link'    => '#',
		), $atts );

		return sprintf( '<a class="fitsc-tooltip" href="%s" title="%s">%s</a>', $atts['link'], $atts['content'], apply_filters( 'fitsc_content', $content ) );
	}

	/**
	 * Show progress bar shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function statistics( $atts, $content ) {
		extract( shortcode_atts( array(
			'text'    => '',
			'percent' => 100,
			'type'    => '',
			'style'   => '',
		), $atts ) );

		if ( $style == 1 ) { // Horizontal
			return sprintf( '
				<div class="fitsc-progress-bar%s">
					<div class="fitsc-title">%s</div>
					<div class="fitsc-percent-wrapper"><div class="fitsc-percent fitsc-percent-%s" data-percentage="%s"></div></div>
				</div>',
				$type ? " fitsc-$type" : '',
				$text,
				$percent,
				$percent
			);
		} else {
			if ( $style == "" ) { // Cycle
				$temp = '<div class="ichart">';
				$temp .= '<div data-percent="' . $percent . '" class="chart-draw">';
				$temp .= '<em>' . $percent . '%</em>';
				$temp .= '<span class="sub">' . $text . '</span>';
				$temp .= '</div>';
				$temp .= '</div>';

				$post_id = get_the_ID();
				$post    = get_post( $post_id );
				$slug    = $post->post_name;
				if ( ! isset( $GLOBALS[ $slug ] ) ) {
					$GLOBALS[ $slug ] = $slug;
					$temp .= '<script type="text/javascript">
						jQuery(document).ready(function(){
							$(".' . $slug . '").waypoint({
								handler    : function () {
									// Update chart
									$(".chart-draw").each(function () {
										var s = $(this);
										// Addition callback function for step added customarily in easypiechart
										s.data("easyPieChart").update(s.attr("data-percent"), function (percent) {
											s.find("em").text(Math.round(percent) + "%");
										});
									});
								},
								triggerOnce: true,
								offset     : "90%"
							});
						});
					</script>';
				}

				return $temp;
			} else { // number
				$temp = '<div class="statistic_number">';
				$temp .= '<span class="num">' . $percent . '</span>';
				$temp .= '<span>' . $text . '</span>';
				$temp .= '</div>';

				$post_id = get_the_ID();
				$post    = get_post( $post_id );
				$slug    = $post->post_name;
				if ( ! isset( $GLOBALS[ $slug ] ) ) {
					$GLOBALS[ $slug ] = $slug;
					$temp .= '<script type="text/javascript">
						jQuery(document).ready(function(){
							function animateStats() {
							var $stats = $(".statistic_number").find(".num");
							var arr = [];
							$stats.each(function (i) {
								arr[i] = $(this).text();
							});
							$(".statistic_number").waypoint({
								handler    : function () {
									$stats.each(function (i) {
										var $s = $(this);
										$({tmp: 0}).animate({tmp: arr[i]}, {
											duration: 1200,
											easing  : "swing",
											step    : function () {
												$s.text(Math.ceil(this.tmp));
											}
										});
									});
								},
								triggerOnce: true,
								offset     : "90%",
							});
						}

						animateStats();
					});
					</script>';
				}

				return $temp;
			}
		}


	}

	/**
	 * Show promo box shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function promo_box( $atts, $content ) {
		extract( shortcode_atts( array(
			'type'             => '',
			'heading'          => '',
			'text'             => '',
			'button1_text'     => '',
			'button1_link'     => '',
			'button1_color'    => '',
			'button1_target'   => '',
			'button1_nofollow' => '',
			'button2_text'     => '',
			'button2_link'     => '',
			'button2_color'    => '',
			'button2_target'   => '',
			'button2_nofollow' => '',
		), $atts ) );

		$button1 = "<a href='" . esc_url( $button1_link ) . "'" .
		           ( $button1_color ? " class='fitsc-button fitsc-large fitsc-background-$button1_color'" : '' ) .
		           ( $button1_nofollow ? " rel='nofollow'" : '' ) .
		           ( $button1_target ? " target='$button1_target'" : '' ) .
		           ">$button1_text</a>";

		$button2 = '';
		if ( $type == 'two-buttons' ) {
			$button2 = "<a href='" . esc_url( $button2_link ) . "'" .
			           ( $button2_color ? " class='fitsc-button fitsc-large fitsc-background-$button2_color'" : '' ) .
			           ( $button2_nofollow ? " rel='nofollow'" : '' ) .
			           ( $button2_target ? " target='$button2_target'" : '' ) .
			           ">$button2_text</a>";
		}

		$content = sprintf( '
			<div class="fitsc-content">
				<h3 class="fitsc-heading">%s</h3>
				<p class="fitsc-text">%s</p>
			</div>',
			$heading,
			$text
		);
		$buttons = sprintf( '
			<div class="fitsc-buttons">%s %s</div>',
			$button1,
			$button2
		);

		$html = sprintf( '
			<div class="fitsc-promo-box-wrap">
				<div class="fitsc-promo-box%s">%s</div>
			</div>',
			$type ? " fitsc-$type" : '',
			$type ? $content . $buttons : $buttons . $content
		);

		return $html;
	}

	/**
	 * Show socials shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function socials( $atts, $content ) {
		$html = '<ul class="fitsc-socials">';
		foreach ( $atts as $k => $v ) {
			$class = str_replace( '_', '-', $k );
			$html .= sprintf(
				'<li>
					<a href="%1$s" class="fitsc-%2$s"><i class="%2$s"></i></a>
				</li>',
				$v,
				$class
			);
		}
		$html .= '</ul>';

		return $html;
	}

	/**
	 * Show person shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function person( $atts, $content ) {
		$atts = array_merge( array(
			'name'     => '',
			'position' => '',
			'photo'    => '',
		), $atts );
		$meta = sprintf( '
			<div class="fitsc-meta">
				<div class="fitsc-name">%s</div>
				<div class="fitsc-position">%s</div>
			</div>',
			$atts['name'],
			$atts['position']
		);
		unset( $atts['name'], $atts['position'] );

		$html = '<div class="fitsc-person">';
		$html .= '<div class="fitsc-photo">';
		$html .= '<img src="' . $atts['photo'] . '">';
		unset( $atts['photo'] );
		$html .= '<ul class="fitsc-socials">';
		foreach ( $atts as $k => $v ) {
			$class = str_replace( '_', '-', $k );
			$html .= sprintf(
				'<li>
					<a href="%1$s" class="fitsc-%2$s"><i class="%2$s"></i></a>
				</li>',
				$v,
				$class
			);
		}
		$html .= '</ul>';
		$html .= '</div>';
		$html .= $meta;
		$html .= '</div>';

		return $html;
	}

	/**
	 * Show testimonial shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function testimonial( $atts, $content ) {
		extract( shortcode_atts( array(
			'name'  => '',
			'info'  => '',
			'photo' => '',
		), $atts ) );

		$html = sprintf( '
			<div class="fitsc-testimonial">
				<img src="%s" class="fitsc-photo">
				<div class="fitsc-text">
					%s
					<div class="fitsc-name">%s</div>
					<div class="fitsc-info">%s</div>
				</div>
			</div>',
			$photo,
			apply_filters( 'fitsc_content', $content ),
			$name,
			$info
		);

		return $html;
	}


	function more( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'text' => '',
		), $atts ) );

		$temp = '<div class="learn-more"><a href="#about"><i class="fa fa-angle-double-down"></i>&nbsp; ' . $text . '</a></div>';

		return $temp;
	}

	function row( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'width'            => '',
			'border'           => '',
			'border_color'     => '',
			'radius'           => '',
			'padding'          => '',
			'margin'           => '',
			'extra_class'      => '',
			'background_color' => '',
			'background_url'   => ''
		), $atts ) );

		$html       = "";
		$background = "";

		$border_temp       = "";
		$border_color_temp = "";
		if ( $border ) {
			$border = explode( " ", $border );
			if ( $border_color != "" ) {
				$border_color_temp .= " " . $border_color . " solid;";
			} else {
				$border_color_temp .= " solid;";
			}

			if ( $border[0] != "" && $border[0] != 'n' ) {
				if ( is_numeric( $border[0] ) ) { // default px unit
					$border_temp .= "border-top: " . $border[0] . "px" . $border_color_temp;
				} else { // other unit
					$border_temp .= "border-top: " . $border[0] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[1] != "" && $border[1] != 'n' ) {
				if ( is_numeric( $border[1] ) ) {
					$border_temp .= " border-right:" . $border[1] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-right:" . $border[1] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[2] != "" && $border[2] != 'n' ) {
				if ( is_numeric( $border[2] ) ) {
					$border_temp .= " border-bottom: " . $border[2] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-bottom: " . $border[2] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[3] != "" && $border[3] != 'n' ) {
				if ( is_numeric( $border[3] ) ) {
					$border_temp .= " border-left: " . $border[3] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-left: " . $border[3] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

		}

		$radius_temp = "";
		if ( $radius != "" ) {
			$radius = explode( " ", $radius );
			if ( $radius[0] != "" && $radius[0] != 'n' ) {
				if ( is_numeric( $radius[0] ) ) {
					$radius_temp .= "border-top-left-radius: " . $radius[0] . "px;";
				} else {
					$radius_temp .= "border-top-left-radius: " . $radius[0] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[1] != "" && $radius[1] != 'n' ) {
				if ( is_numeric( $radius[1] ) ) {
					$radius_temp .= " border-top-right-radius: " . $radius[1] . "px;";
				} else {
					$radius_temp .= " border-top-right-radius: " . $radius[1] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[2] != "" && $radius[2] != 'n' ) {
				if ( is_numeric( $radius[2] ) ) {
					$radius_temp .= " border-bottom-right-radius: " . $radius[2] . "px;";
				} else {
					$radius_temp .= " border-bottom-right-radius: " . $radius[2] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[3] != "" && $radius[3] != 'n' ) {
				if ( is_numeric( $radius[3] ) ) {
					$radius_temp .= " border-bottom-left-radius: " . $radius[3] . "px;";
				} else {
					$radius_temp .= " border-bottom-left-radius: " . $radius[3] . ";";
				}
			} else {
				$radius_temp .= "";
			}
		}

		$padding_temp = "";
		if ( $padding != "" ) {
			$padding = explode( " ", $padding );
			if ( $padding[0] != "" && $padding[0] != 'n' ) {
				if ( is_numeric( $padding[0] ) ) {
					$padding_temp .= "padding-top: " . $padding[0] . "px;";
				} else {
					$padding_temp .= "padding-top: " . $padding[0] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[1] != "" && $padding[1] != 'n' ) {
				if ( is_numeric( $padding[1] ) ) {
					$padding_temp .= " padding-right: " . $padding[1] . "px;";
				} else {
					$padding_temp .= " padding-right: " . $padding[1] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[2] != "" && $padding[2] != 'n' ) {
				if ( is_numeric( $padding[2] ) ) {
					$padding_temp .= " padding-bottom: " . $padding[2] . "px;";
				} else {
					$padding_temp .= " padding-bottom: " . $padding[2] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[3] != "" && $padding[3] != 'n' ) {
				if ( is_numeric( $padding[3] ) ) {
					$padding_temp .= " padding-left: " . $padding[3] . "px;";
				} else {
					$padding_temp .= " padding-left: " . $padding[3] . ";";
				}
			} else {
				$padding_temp .= "";
			}
		}

		$margin_temp = "";
		if ( $margin != "" ) {
			$margin = explode( " ", $margin );
			if ( $margin[0] != "" && $margin[0] != 'n' ) {
				if ( is_numeric( $margin[0] ) ) {
					$margin_temp .= "margin-top: " . $margin[0] . "px;";
				} else {
					$margin_temp .= "margin-top: " . $margin[0] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[1] != "" && $margin[1] != 'n' ) {
				if ( is_numeric( $margin[1] ) ) {
					$margin_temp .= " margin-right: " . $margin[1] . "px;";
				} else {
					$margin_temp .= " margin-right: " . $margin[1] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[2] != "" && $margin[2] != 'n' ) {
				if ( is_numeric( $margin[2] ) ) {
					$margin_temp .= " margin-bottom: " . $margin[2] . "px;";
				} else {
					$margin_temp .= " margin-bottom: " . $margin[2] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[3] != "" && $margin[3] != 'n' ) {
				if ( is_numeric( $margin[3] ) ) {
					$margin_temp .= " margin-left: " . $margin[3] . "px;";
				} else {
					$margin_temp .= " margin-left: " . $margin[3] . ";";
				}
			} else {
				$margin_temp .= "";
			}
		}

		if ( $background_color != "" ) {
			$background_color = 'background:' . $background_color . ';';
		}

		if ( $background_url != "" ) {
			$background_url = 'background:url(' . $background_url . ');';
		}
		/* add*/
		if ( $background_color != "" || $background_url != "" || $border_temp != "" || $radius_temp != "" || $padding_temp != "" || $margin_temp != "" ) {
			$background = ' style="' . $background_color . $background_url . $border_temp . $radius_temp . $padding_temp . $margin_temp . '"';
		}
		if ( $extra_class ) {
			$extra_class = " " . $extra_class;
		}
		if ( $width == "boxed" ) {
			$html .= '<div class="container' . $extra_class . '"' . $background . '>';
		} else {
			$html .= '<div class="row-fullwidth' . $extra_class . '"' . $background . '>';
		}
		$html .= do_shortcode( $content ) . '</div>';

		return $html;
	}

	/* Row shortcode */

	function row_inner( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'width'            => '',
			'border'           => '',
			'border_color'     => '',
			'radius'           => '',
			'padding'          => '',
			'extra_class'      => '',
			'margin'           => '',
			'background_color' => '',
			'background_url'   => ''
		), $atts ) );

		$html       = "";
		$background = "";

		$border_temp       = "";
		$border_color_temp = "";
		if ( $border ) {
			$border = explode( " ", $border );
			if ( $border_color != "" ) {
				$border_color_temp .= " " . $border_color . " solid;";
			} else {
				$border_color_temp .= " solid;";
			}

			if ( $border[0] != "" && $border[0] != 'n' ) {
				if ( is_numeric( $border[0] ) ) { // default px unit
					$border_temp .= "border-top: " . $border[0] . "px" . $border_color_temp;
				} else { // other unit
					$border_temp .= "border-top: " . $border[0] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[1] != "" && $border[1] != 'n' ) {
				if ( is_numeric( $border[1] ) ) {
					$border_temp .= " border-right:" . $border[1] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-right:" . $border[1] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[2] != "" && $border[2] != 'n' ) {
				if ( is_numeric( $border[2] ) ) {
					$border_temp .= " border-bottom: " . $border[2] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-bottom: " . $border[2] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[3] != "" && $border[3] != 'n' ) {
				if ( is_numeric( $border[3] ) ) {
					$border_temp .= " border-left: " . $border[3] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-left: " . $border[3] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

		}

		// if ($border_color != "") {
		// 	$border_temp .= "border-color:".$border_color.";border-style:solid;";
		// }

		$radius_temp = "";
		if ( $radius != "" ) {
			$radius = explode( " ", $radius );
			if ( $radius[0] != "" && $radius[0] != 'n' ) {
				if ( is_numeric( $radius[0] ) ) {
					$radius_temp .= "border-top-left-radius: " . $radius[0] . "px;";
				} else {
					$radius_temp .= "border-top-left-radius: " . $radius[0] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[1] != "" && $radius[1] != 'n' ) {
				if ( is_numeric( $radius[1] ) ) {
					$radius_temp .= " border-top-right-radius: " . $radius[1] . "px;";
				} else {
					$radius_temp .= " border-top-right-radius: " . $radius[1] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[2] != "" && $radius[2] != 'n' ) {
				if ( is_numeric( $radius[2] ) ) {
					$radius_temp .= " border-bottom-right-radius: " . $radius[2] . "px;";
				} else {
					$radius_temp .= " border-bottom-right-radius: " . $radius[2] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[3] != "" && $radius[3] != 'n' ) {
				if ( is_numeric( $radius[3] ) ) {
					$radius_temp .= " border-bottom-left-radius: " . $radius[3] . "px;";
				} else {
					$radius_temp .= " border-bottom-left-radius: " . $radius[3] . ";";
				}
			} else {
				$radius_temp .= "";
			}
		}

		$padding_temp = "";
		if ( $padding != "" ) {
			$padding = explode( " ", $padding );
			if ( $padding[0] != "" && $padding[0] != 'n' ) {
				if ( is_numeric( $padding[0] ) ) {
					$padding_temp .= "padding-top: " . $padding[0] . "px;";
				} else {
					$padding_temp .= "padding-top: " . $padding[0] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[1] != "" && $padding[1] != 'n' ) {
				if ( is_numeric( $padding[1] ) ) {
					$padding_temp .= " padding-right: " . $padding[1] . "px;";
				} else {
					$padding_temp .= " padding-right: " . $padding[1] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[2] != "" && $padding[2] != 'n' ) {
				if ( is_numeric( $padding[2] ) ) {
					$padding_temp .= " padding-bottom: " . $padding[2] . "px;";
				} else {
					$padding_temp .= " padding-bottom: " . $padding[2] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[3] != "" && $padding[3] != 'n' ) {
				if ( is_numeric( $padding[3] ) ) {
					$padding_temp .= " padding-left: " . $padding[3] . "px;";
				} else {
					$padding_temp .= " padding-left: " . $padding[3] . ";";
				}
			} else {
				$padding_temp .= "";
			}
		}

		$margin_temp = "";
		if ( $margin != "" ) {
			$margin = explode( " ", $margin );
			if ( $margin[0] != "" && $margin[0] != 'n' ) {
				if ( is_numeric( $margin[0] ) ) {
					$margin_temp .= "margin-top: " . $margin[0] . "px;";
				} else {
					$margin_temp .= "margin-top: " . $margin[0] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[1] != "" && $margin[1] != 'n' ) {
				if ( is_numeric( $margin[1] ) ) {
					$margin_temp .= " margin-right: " . $margin[1] . "px;";
				} else {
					$margin_temp .= " margin-right: " . $margin[1] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[2] != "" && $margin[2] != 'n' ) {
				if ( is_numeric( $margin[2] ) ) {
					$margin_temp .= " margin-bottom: " . $margin[2] . "px;";
				} else {
					$margin_temp .= " margin-bottom: " . $margin[2] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[3] != "" && $margin[3] != 'n' ) {
				if ( is_numeric( $margin[3] ) ) {
					$margin_temp .= " margin-left: " . $margin[3] . "px;";
				} else {
					$margin_temp .= " margin-left: " . $margin[3] . ";";
				}
			} else {
				$margin_temp .= "";
			}
		}

		if ( $background_color != "" ) {
			$background_color = 'background:' . $background_color . ';';
		}

		if ( $background_url != "" ) {
			$background_url = 'background:url(' . $background_heading . ');';
		}
		/* add*/
		if ( $background_color != "" || $background_url != "" || $border_temp != "" || $radius_temp != "" || $padding_temp != "" || $margin_temp != "" ) {
			$background = ' style="' . $background_color . $background_url . $border_temp . $radius_temp . $padding_temp . $margin_temp . '"';
		}

		if ( $extra_class ) {
			$extra_class = " " . $extra_class;
		}
		if ( $width == "boxed" ) {
			$html .= '<div class="container' . $extra_class . '"' . $background . '>';
		} else {
			$html .= '<div class="row-fullwidth' . $extra_class . '"' . $background . '>';
		}

		$html .= do_shortcode( $content ) . '</div>';

		return $html;
	}

	/* Row shortcode */

	function column( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'width'            => '',
			'border'           => '',
			'border_color'     => '',
			'radius'           => '',
			'padding'          => '',
			'margin'           => '',
			'background_color' => '',
			'background_url'   => '',
			'extra_class'      => ''
		), $atts ) );

		if ( $extra_class != '' ) {
			$extra_class = ' ' . $extra_class;
		}

		$border_temp       = "";
		$border_color_temp = "";
		if ( $border ) {
			$border = explode( " ", $border );
			if ( $border_color != "" ) {
				$border_color_temp .= " " . $border_color . " solid;";
			} else {
				$border_color_temp .= " solid;";
			}

			if ( $border[0] != "" && $border[0] != 'n' ) {
				if ( is_numeric( $border[0] ) ) { // default px unit
					$border_temp .= "border-top: " . $border[0] . "px" . $border_color_temp;
				} else { // other unit
					$border_temp .= "border-top: " . $border[0] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[1] != "" && $border[1] != 'n' ) {
				if ( is_numeric( $border[1] ) ) {
					$border_temp .= " border-right:" . $border[1] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-right:" . $border[1] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[2] != "" && $border[2] != 'n' ) {
				if ( is_numeric( $border[2] ) ) {
					$border_temp .= " border-bottom: " . $border[2] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-bottom: " . $border[2] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[3] != "" && $border[3] != 'n' ) {
				if ( is_numeric( $border[3] ) ) {
					$border_temp .= " border-left: " . $border[3] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-left: " . $border[3] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

		}

		// if ($border_color != "") {
		// 	$border_temp .= "border-color:".$border_color.";border-style:solid;";
		// }

		$radius_temp = "";
		if ( $radius != "" ) {
			$radius = explode( " ", $radius );
			if ( $radius[0] != "" && $radius[0] != 'n' ) {
				if ( is_numeric( $radius[0] ) ) {
					$radius_temp .= "border-top-left-radius: " . $radius[0] . "px;";
				} else {
					$radius_temp .= "border-top-left-radius: " . $radius[0] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[1] != "" && $radius[1] != 'n' ) {
				if ( is_numeric( $radius[1] ) ) {
					$radius_temp .= " border-top-right-radius: " . $radius[1] . "px;";
				} else {
					$radius_temp .= " border-top-right-radius: " . $radius[1] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[2] != "" && $radius[2] != 'n' ) {
				if ( is_numeric( $radius[2] ) ) {
					$radius_temp .= " border-bottom-right-radius: " . $radius[2] . "px;";
				} else {
					$radius_temp .= " border-bottom-right-radius: " . $radius[2] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[3] != "" && $radius[3] != 'n' ) {
				if ( is_numeric( $radius[3] ) ) {
					$radius_temp .= " border-bottom-left-radius: " . $radius[3] . "px;";
				} else {
					$radius_temp .= " border-bottom-left-radius: " . $radius[3] . ";";
				}
			} else {
				$radius_temp .= "";
			}
		}

		$padding_temp = "";
		if ( $padding != "" ) {
			$padding = explode( " ", $padding );
			if ( $padding[0] != "" && $padding[0] != 'n' ) {
				if ( is_numeric( $padding[0] ) ) {
					$padding_temp .= "padding-top: " . $padding[0] . "px;";
				} else {
					$padding_temp .= "padding-top: " . $padding[0] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[1] != "" && $padding[1] != 'n' ) {
				if ( is_numeric( $padding[1] ) ) {
					$padding_temp .= " padding-right: " . $padding[1] . "px;";
				} else {
					$padding_temp .= " padding-right: " . $padding[1] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[2] != "" && $padding[2] != 'n' ) {
				if ( is_numeric( $padding[2] ) ) {
					$padding_temp .= " padding-bottom: " . $padding[2] . "px;";
				} else {
					$padding_temp .= " padding-bottom: " . $padding[2] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[3] != "" && $padding[3] != 'n' ) {
				if ( is_numeric( $padding[3] ) ) {
					$padding_temp .= " padding-left: " . $padding[3] . "px;";
				} else {
					$padding_temp .= " padding-left: " . $padding[3] . ";";
				}
			} else {
				$padding_temp .= "";
			}
		}

		$margin_temp = "";
		if ( $margin != "" ) {
			$margin = explode( " ", $margin );
			if ( $margin[0] != "" && $margin[0] != 'n' ) {
				if ( is_numeric( $margin[0] ) ) {
					$margin_temp .= "margin-top: " . $margin[0] . "px;";
				} else {
					$margin_temp .= "margin-top: " . $margin[0] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[1] != "" && $margin[1] != 'n' ) {
				if ( is_numeric( $margin[1] ) ) {
					$margin_temp .= " margin-right: " . $margin[1] . "px;";
				} else {
					$margin_temp .= " margin-right: " . $margin[1] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[2] != "" && $margin[2] != 'n' ) {
				if ( is_numeric( $margin[2] ) ) {
					$margin_temp .= " margin-bottom: " . $margin[2] . "px;";
				} else {
					$margin_temp .= " margin-bottom: " . $margin[2] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[3] != "" && $margin[3] != 'n' ) {
				if ( is_numeric( $margin[3] ) ) {
					$margin_temp .= " margin-left: " . $margin[3] . "px;";
				} else {
					$margin_temp .= " margin-left: " . $margin[3] . ";";
				}
			} else {
				$margin_temp .= "";
			}
		}

		if ( $background_color != "" ) {
			$background_color = 'background:' . $background_color . ';';
		}

		if ( $background_url != "" ) {
			$background_url = 'background:url(' . $background_heading . ');';
		}
		/* add*/
		$background = "";
		if ( $background_color != "" || $background_url != "" || $border_temp != "" || $radius_temp != "" || $padding_temp != "" || $margin_temp != "" ) {
			$background = ' style="' . $background_color . $background_url . $border_temp . $radius_temp . $padding_temp . $margin_temp . '"';
		}

		$fraction      = $width;
		$fractionParts = explode( '/', $fraction );
		$numerator     = $fractionParts[0];
		$denominator   = $fractionParts[1];

		$arr         = simplify( $numerator, $denominator ); // Array(2,5)
		$numerator   = $arr[0];
		$denominator = $arr[1];

		if ( 12 % $denominator == 0 ) {
			$number = ( 12 / $denominator ) * $numerator;
			$temp   = '<div class="col-md-' . $number . ' col-sm-' . $number . '' . $extra_class . '"' . $background . '>' . do_shortcode( $content ) . '</div>';
		} else {
			$temp = '<div class="col-md-3 col-sm-3 col-xs-12"' . $background . '>' . do_shortcode( $content ) . '</div>';
		}

		return $temp;
	}

	function column_inner( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'width'            => '',
			'border'           => '',
			'border_color'     => '',
			'radius'           => '',
			'padding'          => '',
			'margin'           => '',
			'background_color' => '',
			'background_url'   => '',
			'extra_class'      => ''
		), $atts ) );

		$border_temp       = "";
		$border_color_temp = "";
		if ( $border ) {
			$border = explode( " ", $border );
			if ( $border_color != "" ) {
				$border_color_temp .= " " . $border_color . " solid;";
			} else {
				$border_color_temp .= " solid;";
			}

			if ( $border[0] != "" && $border[0] != 'n' ) {
				if ( is_numeric( $border[0] ) ) { // default px unit
					$border_temp .= "border-top: " . $border[0] . "px" . $border_color_temp;
				} else { // other unit
					$border_temp .= "border-top: " . $border[0] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[1] != "" && $border[1] != 'n' ) {
				if ( is_numeric( $border[1] ) ) {
					$border_temp .= " border-right:" . $border[1] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-right:" . $border[1] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[2] != "" && $border[2] != 'n' ) {
				if ( is_numeric( $border[2] ) ) {
					$border_temp .= " border-bottom: " . $border[2] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-bottom: " . $border[2] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

			if ( $border[3] != "" && $border[3] != 'n' ) {
				if ( is_numeric( $border[3] ) ) {
					$border_temp .= " border-left: " . $border[3] . "px" . $border_color_temp;
				} else {
					$border_temp .= " border-left: " . $border[3] . $border_color_temp;
				}
			} else {
				$border_temp .= "";
			}

		}

		// if ($border_color != "") {
		// 	$border_temp .= "border-color:".$border_color.";border-style:solid;";
		// }

		$radius_temp = "";
		if ( $radius != "" ) {
			$radius = explode( " ", $radius );
			if ( $radius[0] != "" && $radius[0] != 'n' ) {
				if ( is_numeric( $radius[0] ) ) {
					$radius_temp .= "border-top-left-radius: " . $radius[0] . "px;";
				} else {
					$radius_temp .= "border-top-left-radius: " . $radius[0] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[1] != "" && $radius[1] != 'n' ) {
				if ( is_numeric( $radius[1] ) ) {
					$radius_temp .= " border-top-right-radius: " . $radius[1] . "px;";
				} else {
					$radius_temp .= " border-top-right-radius: " . $radius[1] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[2] != "" && $radius[2] != 'n' ) {
				if ( is_numeric( $radius[2] ) ) {
					$radius_temp .= " border-bottom-right-radius: " . $radius[2] . "px;";
				} else {
					$radius_temp .= " border-bottom-right-radius: " . $radius[2] . ";";
				}
			} else {
				$radius_temp .= "";
			}

			if ( $radius[3] != "" && $radius[3] != 'n' ) {
				if ( is_numeric( $radius[3] ) ) {
					$radius_temp .= " border-bottom-left-radius: " . $radius[3] . "px;";
				} else {
					$radius_temp .= " border-bottom-left-radius: " . $radius[3] . ";";
				}
			} else {
				$radius_temp .= "";
			}
		}

		$padding_temp = "";
		if ( $padding != "" ) {
			$padding = explode( " ", $padding );
			if ( $padding[0] != "" && $padding[0] != 'n' ) {
				if ( is_numeric( $padding[0] ) ) {
					$padding_temp .= "padding-top: " . $padding[0] . "px;";
				} else {
					$padding_temp .= "padding-top: " . $padding[0] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[1] != "" && $padding[1] != 'n' ) {
				if ( is_numeric( $padding[1] ) ) {
					$padding_temp .= " padding-right: " . $padding[1] . "px;";
				} else {
					$padding_temp .= " padding-right: " . $padding[1] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[2] != "" && $padding[2] != 'n' ) {
				if ( is_numeric( $padding[2] ) ) {
					$padding_temp .= " padding-bottom: " . $padding[2] . "px;";
				} else {
					$padding_temp .= " padding-bottom: " . $padding[2] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[3] != "" && $padding[3] != 'n' ) {
				if ( is_numeric( $padding[3] ) ) {
					$padding_temp .= " padding-left: " . $padding[3] . "px;";
				} else {
					$padding_temp .= " padding-left: " . $padding[3] . ";";
				}
			} else {
				$padding_temp .= "";
			}
		}

		$margin_temp = "";
		if ( $margin != "" ) {
			$margin = explode( " ", $margin );
			if ( $margin[0] != "" && $margin[0] != 'n' ) {
				if ( is_numeric( $margin[0] ) ) {
					$margin_temp .= "margin-top: " . $margin[0] . "px;";
				} else {
					$margin_temp .= "margin-top: " . $margin[0] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[1] != "" && $margin[1] != 'n' ) {
				if ( is_numeric( $margin[1] ) ) {
					$margin_temp .= " margin-right: " . $margin[1] . "px;";
				} else {
					$margin_temp .= " margin-right: " . $margin[1] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[2] != "" && $margin[2] != 'n' ) {
				if ( is_numeric( $margin[2] ) ) {
					$margin_temp .= " margin-bottom: " . $margin[2] . "px;";
				} else {
					$margin_temp .= " margin-bottom: " . $margin[2] . ";";
				}
			} else {
				$margin_temp .= "";
			}

			if ( $margin[3] != "" && $margin[3] != 'n' ) {
				if ( is_numeric( $margin[3] ) ) {
					$margin_temp .= " margin-left: " . $margin[3] . "px;";
				} else {
					$margin_temp .= " margin-left: " . $margin[3] . ";";
				}
			} else {
				$margin_temp .= "";
			}
		}

		if ( $background_color != "" ) {
			$background_color = 'background:' . $background_color . ';';
		}

		if ( $background_url != "" ) {
			$background_url = 'background:url(' . $background_heading . ');';
		}
		/* add*/
		$background = "";
		if ( $background_color != "" || $background_url != "" || $border_temp != "" || $radius_temp != "" || $padding_temp != "" || $margin_temp != "" ) {
			$background = ' style="' . $background_color . $background_url . $border_temp . $radius_temp . $padding_temp . $margin_temp . '"';
		}

		$fraction      = $width;
		$fractionParts = explode( '/', $fraction );
		$numerator     = $fractionParts[0];
		$denominator   = $fractionParts[1];

		$arr         = simplify( $numerator, $denominator ); // Array(2,5)
		$numerator   = $arr[0];
		$denominator = $arr[1];

		if ( 12 % $denominator == 0 ) {
			$number = ( 12 / $denominator ) * $numerator;
			$temp   = '<div class="col-md-' . $number . ' col-sm-' . $number . ' ' . $extra_class . '"' . $background . '>' . do_shortcode( $content ) . '</div>';
		} else {
			$temp = '<div class="col-md-3 col-sm-3 col-xs-12"' . $background . '>' . do_shortcode( $content ) . '</div>';
		}

		return $temp;
	}

	/* Fantastic header */

	function text_rotator( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'effect'     => '',
			'background' => '',
			'color'      => '',
			'padding'    => ''
		), $atts ) );


		if ( $effect <> '' ) {
			$effect = 'text_rotator_' . $effect;
		} else {
			$effect = 'text_rotator_fade';
		}

		$padding_temp = "";
		if ( $padding != "" ) {
			$padding = explode( " ", $padding );
			if ( $padding[0] != "" && $padding[0] != 'n' ) {
				if ( is_numeric( $padding[0] ) ) {
					$padding_temp .= "padding-top: " . $padding[0] . "px;";
				} else {
					$padding_temp .= "padding-top: " . $padding[0] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[1] != "" && $padding[1] != 'n' ) {
				if ( is_numeric( $padding[1] ) ) {
					$padding_temp .= " padding-right: " . $padding[1] . "px;";
				} else {
					$padding_temp .= " padding-right: " . $padding[1] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[2] != "" && $padding[2] != 'n' ) {
				if ( is_numeric( $padding[2] ) ) {
					$padding_temp .= " padding-bottom: " . $padding[2] . "px;";
				} else {
					$padding_temp .= " padding-bottom: " . $padding[2] . ";";
				}
			} else {
				$padding_temp .= "";
			}

			if ( $padding[3] != "" && $padding[3] != 'n' ) {
				if ( is_numeric( $padding[3] ) ) {
					$padding_temp .= " padding-left: " . $padding[3] . "px;";
				} else {
					$padding_temp .= " padding-left: " . $padding[3] . ";";
				}
			} else {
				$padding_temp .= "";
			}
		}

		if ( $color != "" ) {
			$color = "color:" . $color . ";";
		}
		if ( $background != "" ) {
			$background = "background:" . $background . ";";
		}

		$style = "";
		if ( $background != "" || $color != "" || $padding_temp != "" ) {
			$style .= ' style="' . $padding_temp . $background . $color . '"';
		}

		$html = '<span class="' . $effect . '"' . $style . '>' . $content . '</span>';
		wp_enqueue_script( 'simple-text-rotator', get_template_directory_uri() . '/js/jquery.simple-text-rotator.js', array( 'jquery' ), '', false );

		return $html;
	}

	/** QUOTES **/
	function quotes( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'author' => ''
		), $atts ) );
		$center_block = '<div class="sr_quote"><p><i class="fa fa-quote-left"></i>';
		$center_block .= do_shortcode( $content );
		$center_block .= '<i class="fa fa-quote-right"></i></p>';
		$center_block .= '<div class="sr_quote-author">- ' . $author . ' -</div>';
		$center_block .= '</div>';

		return $center_block;
	}

	function icon_box( $atts, $content = null ) {
		extract( shortcode_atts( array(
			"style"             => '',
			//"box_padding" => '',
			"title"             => '',
			"title_size"        => '',
			"title_color"       => '',
			"title_bg_color"    => '',
			"title_padding"     => '',
			"desc_padding"      => '',
			"link"              => '',
			"icon_type"         => '',
			"icon"              => '',
			"icon_url"          => '',
			"icon_width"        => '',
			"icon_height"       => '',
			"margin_top"        => '',
			"icon_margin"       => '',
			"icon_padding"      => '',
			"icon_size"         => '',
			"icon_color"        => '',
			"icon_bg_color"     => '',
			"icon_border"       => '',
			"icon_border_color" => '',
			"icon_radius"       => '',
			"icon_position"     => '',
			"icon_effect"       => '',
			"icon_eff_color"    => '',
		), $atts ) );

		/*
		* support effect: icon-effect-1, icon-effect-2, icon-effect-3
		*/
		$array = array(
			'icon-effect-1' => 'first',
			'icon-effect-2' => 'second',
			'icon-effect-3' => 'third',
			'icon-effect-4' => 'fourth',
		);

		$post_id = get_the_ID();
		$post    = get_post( $post_id );

		self::$intID ++;
		$tp   = str_replace( "-", "_", $post->post_name );
		$slug = "ie_" . $tp . "_" . self::$intID;


		if ( $icon_effect && $icon_effect != "random" ) {
			$effect      = " " . array_search( $icon_effect, $array ) . " " . $slug;
			$effect_name = "." . array_search( $icon_effect, $array ) . "." . $slug;
		} else {
			if ( $icon_effect && $icon_effect == "random" ) {
				$rand_keys   = array_rand( $array, 1 );
				$effect      = " " . $rand_keys . " " . $slug;
				$effect_name = "." . $rand_keys . "." . $slug;
			} else {
				$effect = "";
			}
		}

		if ( $icon_position != "top" ) {
			if ( ! $icon_type ) { // font awesome
				if ( $icon_size || $icon_border || $icon_width ) {
					if ( $icon_width ) {
						$width = intval( $icon_width );
					} else {
						$width = 0;
					}
					$icon_size_temp = $width + ( $icon_size ? intval( $icon_size ) : 20 ) + intval( $icon_border ) + 20;
					$width_content  = ' style="width: calc(100% - ' . $icon_size_temp . 'px);"';
				} else {
					$width_content = ' style="width: calc(100% - 40px);"';
				}
			} else { // custom image
				if ( $icon_url || $icon_border ) {
					if ( $icon_url ) {
						if ( $icon_width ) {
							$width = intval( $icon_width );
						} else {
							$data  = getimagesize( $icon_url );
							$width = intval( $data[0] );
						}
					} else {
						$width = 0;
					}
					$icon_size_temp = $width + intval( $icon_border ) + 20;
					$width_content  = ' style="width: calc(100% - ' . $icon_size_temp . 'px);"';
				} else {
					$width_content = ' style="width: calc(100% - 40px);"';
				}

			}
		} else {
			$width_content = "";
		}

		$icon_position = " icon_" . $icon_position;


		$temp = '<div class="icon_box' . $icon_position . $effect . '">';

		if ( $icon || $icon_url ) {
			if ( $icon_size ) {
				$icon_size = "font-size:" . $icon_size . "px;";
			}
			if ( $icon_color ) {
				$icon_color = "color:" . $icon_color . ";";
			}
			if ( $icon_bg_color ) {
				$icon_bg_color = "background:" . $icon_bg_color . ";";
			}
			if ( $icon_radius ) {
				$radius = explode( " ", $icon_radius );
				if ( is_numeric( $radius[0] ) ) {
					$icon_radius = "border-radius:" . $icon_radius . "px;";
				} else {
					$icon_radius = "border-radius:" . $icon_radius . ";";
				}
			}
			if ( $icon_border ) {
				$icon_border = "border:" . $icon_border . "px;border-style: solid;";
			}
			if ( $icon_border_color ) {
				$icon_border_color = "border-color:" . $icon_border_color . ";";
			}

			$padding_temp = "";
			$css_p        = "";

			$icon_wh = $icon_margin_top = "";
			// if(!$icon_type) {
			if ( $icon_width ) {
				$icon_width = " width:" . $icon_width . "px;";
			}
			if ( $icon_height ) {
				$icon_height = " height:" . $icon_height . "px;";
			}
			if ( $icon_width || $icon_height ) {
				$icon_wh = $icon_width . $icon_height;
			}
			if ( $margin_top ) {
				$icon_margin_top = "margin-top:" . $margin_top . "px;";
			}
			// }

			if ( $icon_size || $icon_color || $icon_bg_color || $icon_radius || $icon_border || $icon_border_color || $padding_temp || $icon_wh ) {
				$css = ' style="' . $icon_size . $icon_color . $icon_bg_color . $icon_radius . $icon_border . $icon_border_color . $padding_temp . $icon_wh . '"';
			} else {
				$css = "";
			}
			if ( $icon_margin_top ) {
				$css_p = ' style="' . $icon_margin_top . '"';
			}
			$temp .= '<div class="ib_icon"' . $css_p . '>';
			if ( $icon_type ) {
				$temp .= '<img src="' . $icon_url . '"' . $css . '>';
			} else {
				$temp .= '<i class="' . $icon . '"' . $css . '></i>';
			}
			$temp .= '</div>';
		}

		if ( $title || $content ) {
			$temp .= '<div class="ib_content"' . $width_content . '>';
			if ( $title ) {
				if ( $title_color ) {
					$title_color = "color:" . $title_color . ";";
				}
				if ( $title_bg_color ) {
					$title_bg_color = "background:" . $title_bg_color . ";";
				}
				$padding_temp = "";
				if ( $title_padding != "" ) {
					$padding = explode( " ", $title_padding );
					if ( $padding[0] != "" && $padding[0] != 0 ) {
						if ( is_numeric( $padding[0] ) ) {
							$padding_temp .= $padding[0] . "px";
						} else {
							$padding_temp .= $padding[0];
						}
					}

					$padding_temp = "padding: " . $padding_temp . ";";
				}
				if ( $title_color || $title_bg_color || $padding_temp ) {
					$css = ' style="' . $title_color . $title_bg_color . $padding_temp . '"';
				} else {
					$css = "";
				}

				if ( $title_size != "" ) {
					$temp .= '<h' . $title_size . ' class="ib_title"' . $css . '>';
					if ( $link ) {
						$temp .= '<a href="' . $link . '">';
					}
					$temp .= $title;
					if ( $link ) {
						$temp .= '</a>';
					}
					$temp .= '</h' . $title_size . '>';

				} else {
					$temp .= '<h2 class="ib_title"' . $css . '>';
					if ( $link ) {
						$temp .= '<a href="' . $link . '">';
					}
					$temp .= $title;
					if ( $link ) {
						$temp .= '</a>';
					}
					$temp .= '</h2>';
				}
			}

			if ( $content ) {
				$padding_temp = "";
				if ( $desc_padding != "" ) {
					$padding = explode( " ", $desc_padding );
					if ( $padding[0] != "" && $padding[0] != 0 ) {
						if ( is_numeric( $padding[0] ) ) {
							$padding_temp .= $padding[0] . "px";
						} else {
							$padding_temp .= $padding[0];
						}
					}

					$padding_temp = "padding: " . $padding_temp . ";";
				}
				if ( $title_color || $title_bg_color || $padding_temp ) {
					$css = ' style="' . $padding_temp . '"';
				} else {
					$css = "";
				}
				$temp .= '<div class="ib_description"' . $css . '>' . do_shortcode( $content ) . '</div>';
			}
			$temp .= '</div>';
		}
		$temp .= '</div>';

		if ( $effect ) {
			if ( ! $icon_radius ) {
				$icon_radius = 'border-radius: inherit;';
			}
			if ( $icon_eff_color ) {
				$main_color = $icon_eff_color;
			} else {
				$main_color = "#000";
			}
			//test fix
			//	$style_icon = '<style>' . $effect_name . ' i {background: ' . $main_color . ';}' . $effect_name . ' i:after {' . $icon_radius . 'box-shadow: 0 0 0 4px ' . $main_color . ';}</style>';
		} else {
			//$style_icon = '';
		}

		//$temp .= $style_icon;

		return $temp;
	}

	/* Contact Box */

	function video( $atts ) {
		extract( shortcode_atts( array(
			'type'     => '',
			'id'       => '',
			'autoplay' => '',
			'width'    => '',
			'height'   => ''
		), $atts ) );

		if ( $height && ! $width ) {
			$width = intval( $height * 16 / 9 );
		}
		if ( ! $height && $width ) {
			$height = intval( $width * 9 / 16 );
		}
		if ( ! $height && ! $width ) {
			$height = 315;
			$width  = 560;
		}

		$autoplay = ( $autoplay == 'yes' ? '1' : false );

		if ( $type == "vimeo" ) {
			$rnr_video = "<div class='video-embed'><iframe src='http://player.vimeo.com/video/$id?autoplay=$autoplay&amp;title=0&amp;byline=0&amp;portrait=0' width='$width' height='$height' class='iframe'></iframe></div>";
		} else {
			$rnr_video = "<div class='video-embed'><iframe src='http://www.youtube.com/embed/$id?autoplay=$autoplay;HD=1;rel=0;showinfo=0' width='$width' height='$height' class='iframe'></iframe></div>";
		}

		if ( ! empty( $id ) ) {
			return $rnr_video;
		}
	}

	function text_block( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'css_animation' => '',
			'css_class'     => ''
		), $atts ) );

		if ( $css_animation || $css_class ) {
			$css_animation = ' class= "' . $css_class . ' ' . $this->getCSSAnimation( $css_animation ) . '"';
		}

		return '<div' . $css_animation . '>' . do_shortcode( $content ) . '</div>';
	}

	public function getCSSAnimation( $css_animation ) {
		$output = '';
		if ( $css_animation != '' ) {
			$output = ' am_animate_when_almost_visible am_' . $css_animation;
		}

		return $output;
	}

	function recent_topics( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'title'           => '',
			'number'          => '',
			'parent_forum_id' => '',
			'show_post_date'  => '',
			'show_author'     => '',
			'order_by'        => '',
		), $atts ) );

		switch ( $order_by ) {
			// Order by most recent replies
			case 'freshness' :
				$topics_query = array(
					'post_type'           => bbp_get_topic_post_type(),
					'post_parent'         => $parent_forum_id,
					'posts_per_page'      => $number,
					'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
					'ignore_sticky_posts' => true,
					'no_found_rows'       => true,
					'meta_key'            => '_bbp_last_active_time',
					'orderby'             => 'meta_value',
					'order'               => 'DESC',
				);
				break;

			// Order by total number of replies
			case 'popular' :
				$topics_query = array(
					'post_type'           => bbp_get_topic_post_type(),
					'post_parent'         => $parent_forum_id,
					'posts_per_page'      => $number,
					'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
					'ignore_sticky_posts' => true,
					'no_found_rows'       => true,
					'meta_key'            => '_bbp_reply_count',
					'orderby'             => 'meta_value',
					'order'               => 'DESC'
				);
				break;

			// Order by which topic was created most recently
			case 'newness' :
			default :
				$topics_query = array(
					'post_type'           => bbp_get_topic_post_type(),
					'post_parent'         => $parent_forum_id,
					'posts_per_page'      => $number,
					'post_status'         => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
					'ignore_sticky_posts' => true,
					'no_found_rows'       => true,
					'order'               => 'DESC'
				);
				break;
		}

		// Note: private and hidden forums will be excluded via the
		// bbp_pre_get_posts_normalize_forum_visibility action and function.
		$widget_query = new WP_Query( $topics_query );
		$temp         = '';
		// Bail if no topics are found
		if ( ! $widget_query->have_posts() ) {
			return;
		}
		$temp .= '<div class="recent-topics">';
		if ( ! empty( $title ) ) {
			$temp .= '<h3>' . $title . '</h3>';
		}

		$temp .= '<ul class="sc_recent_topics">';
		while ( $widget_query->have_posts() ) :
			$widget_query->the_post();
			$topic_id    = bbp_get_topic_id( $widget_query->post->ID );
			$author_link = '';
			// Maybe get the topic author
			if ( ! empty( $show_author ) ) :
				$author_link = bbp_get_topic_author_link( array(
					'post_id' => $topic_id,
					'type'    => 'both',
					'size'    => 16
				) );
			endif;
			$temp .= '<li><a class="bbp-forum-title" href="' . bbp_get_topic_permalink( $topic_id ) . '">' . bbp_get_topic_title( $topic_id ) . '</a>';
			if ( ! empty( $author_link ) ) :
				$temp .= '<small class="topic-author">' . sprintf( __( 'Asked by %s', 'deskpress' ), $author_link ) . '</small>';
			endif;
			if ( ! empty( $show_post_date ) ) :
				$temp .= '<div> ' . bbp_get_topic_last_active_time( $topic_id ) . '</div>';
			endif;
			$temp .= '</li>';
		endwhile;
		$temp .= '</ul>';
		$temp .= '</div>';
		// Reset the $post global
		wp_reset_postdata();

		return $temp;
	}

	function kb_articles( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'title'    => '',
			'number'   => '',
			'order_by' => '',
		), $atts ) );

		switch ( $order_by ) {
			// Order by most recent replies
			case 'freshness' :
				$topics_query = array(
					'post_type'      => 'tps',
					'posts_per_page' => $number,
					'post_status'    => 'publish',
					'order'          => 'DESC',
				);
				break;
			// Order by total number of replies
			case 'popular' :
				$topics_query = array(
					'post_type'      => 'tps',
					'posts_per_page' => $number,
					'post_status'    => 'publish',
					'orderby'        => 'meta_value',
					'order'          => 'DESC'
				);
				break;
			// Order by which topic was created most recently
			case 'newness' :
			default :
				$topics_query = array(
					'post_type'      => 'tps',
					'posts_per_page' => $number,
					'post_status'    => 'publish',
					'order'          => 'DESC'
				);
				break;
		}
		$widget_query = new WP_Query( $topics_query );
		$temp         = '';
		// Bail if no topics are found
		if ( ! $widget_query->have_posts() ) {
			return;
		}
		$temp .= '<div class="articles box-content">';
		if ( ! empty( $title ) ) {
			$temp .= '<h3>' . $title . '</h3>';
		}

		$temp .= '<ul class="kb-articles">';
		while ( $widget_query->have_posts() ) :
			$widget_query->the_post();


			// Maybe get the topic author
			$temp .= '<li> <a class="kb-articles-title" href="' . get_permalink( get_the_ID() ) . '">' . get_the_title( get_the_ID() ) . '</a>';
			$suburbs = wp_get_post_terms( get_the_ID(), 'tps_category', 'hide-empty=0&orderby=id' );
			if ( count( $suburbs ) ) :
				$temp .= '<small class="kbcategory"> ' . __( 'Posted in: ', 'deskpress' ) . '';
				shuffle( $suburbs );
				$count = 0;
				$sep   = '';
				foreach ( $suburbs as $suburb ) {
					if ( $count ++ > 10 ) {
						break;
					}
					$temp .= $sep . '<a href="' . get_term_link( $suburb ) . '">' . $suburb->name . '</a>';
					$sep = ', '; // Put your separator here.
				}
				$temp .= '</small>';
			endif;
			$temp .= '</li>';
		endwhile;
		$temp .= '</ul>';
		$temp .= '</div>';
		// Reset the $post global
		wp_reset_postdata();

		return $temp;
	}

	function kb_category( $atts, $content = null ) {
		$title = $number = $order_by = $show_count = '';
		extract( shortcode_atts( array(
			'title'    => '',
			'number'   => '',
			'order_by' => 'name',
		), $atts ) );

		$args = array(
			'orderby'    => $order_by,
			'order'      => 'ASC',
			'hide_empty' => true,
			'number'     => $number,
		);
		$html = '<div class="sc-category box-content">';
		if ( ! empty( $title ) ) {
			$html .= '<h3>' . $title . '</h3>';
		}
		$html .= '<ul class="kb_category">';
		$terms = get_terms( 'tps_category', $args );
		foreach ( $terms as $term ) {
			// The $term is an object, so we don't need to specify the $taxonomy.
			$term_link = get_term_link( $term );
			// If there was an error, continue to the next term.
			if ( is_wp_error( $term_link ) ) {
				continue;
			}
			// We successfully got a link. Print it out.
			$html .= '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '
						<span class="count">' . $term->count . '</span>
					</a></li>';
		}
		$html .= '</ul>';
		$html .= '</div>';

		return $html;
	}

	function map( $atts, $content = null ) {
		$rnr_map_lat = $rnr_map_long = $rnr_map_zoom = '';
		extract( shortcode_atts( array(
			'rnr_map_lat'  => '',
			'rnr_map_long' => '',
			'rnr_map_zoom' => "",
		), $atts ) );

		wp_enqueue_script( 'mapapi', 'https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&ver=2.1', array(), '', true );
		wp_enqueue_script( 'infobox', 'http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js?ver=2.1', array(), '', true );

		$html = '
			<div class="row contact-map">
					  <script type="text/javascript">
                        jQuery(document).ready(function() {
                     		 function initialize() {
                              var secheltLoc = new google.maps.LatLng(' . $rnr_map_lat . ',' . $rnr_map_long . ');
                              var myMapOptions = {
                                   center: secheltLoc
                                  ,mapTypeId: google.maps.MapTypeId.ROADMAP
                                  ,zoom: ' . $rnr_map_zoom . ', scrollwheel: false,mapTypeControl: false, draggable: false
                              };
                              var theMap = new google.maps.Map(document.getElementById("google-map"), myMapOptions);
                              var image = new google.maps.MarkerImage(
                                  "' . get_template_directory_uri() . '/images/pinMap.png",
									new google.maps.Size(17,26),
									new google.maps.Point(0,0),
									new google.maps.Point(8,26)
									);
									var shadow = new google.maps.MarkerImage(
									"' . get_template_directory_uri() . '/images/pinMap-shadow.png",
									new google.maps.Size(33,26),
									new google.maps.Point(0,0),
									new google.maps.Point(9,26)
									);
									var marker = new google.maps.Marker({
									map: theMap,
									icon: image,
									shadow: shadow,
									draggable: false,
									animation: google.maps.Animation.DROP,
									position: secheltLoc,
									visible: true
									});

  									var myOptions = {
 									disableAutoPan: false,maxWidth: 0
									,pixelOffset: new google.maps.Size(-140, 0)
									,zIndex: null
									,boxStyle: {
									width: "280px"
									}
									,closeBoxURL: ""
									,infoBoxClearance: new google.maps.Size(1, 1)
									,isHidden: false
									,pane: "floatPane"
									,enableEventPropagation: false
									};

									google.maps.event.addListener(theMap, "click", function (e) {
									ib.open(theMap, this);
									});

									var ib = new InfoBox(myOptions);
									ib.open(theMap, marker);
									}
									google.maps.event.addDomListener(window, \'load\', initialize);

									});
									</script>
									<div id="google-map" class="embed clearfix">
										<div class="mapPreLoading">
										<span><h4>Loading</h4></span>
										<span class="l-1"></span>
										<span class="l-2"></span>
										<span class="l-3"></span>
										<span class="l-4"></span>
										<span class="l-5"></span>
										<span class="l-6"></span>
									</div>
									</div>
									</div>
								';

		return $html;
	}

}

new FITSC_Frontend;

/**** ****/
function gcd( $a, $b ) {
	$a = abs( $a );
	$b = abs( $b );
	if ( $a < $b ) {
		list( $b, $a ) = Array( $a, $b );
	}
	if ( $b == 0 ) {
		return $a;
	}
	$r = $a % $b;
	while ( $r > 0 ) {
		$a = $b;
		$b = $r;
		$r = $a % $b;
	}

	return $b;
}

function simplify( $num, $den ) {
	$g = gcd( $num, $den );

	return Array( $num / $g, $den / $g );
}