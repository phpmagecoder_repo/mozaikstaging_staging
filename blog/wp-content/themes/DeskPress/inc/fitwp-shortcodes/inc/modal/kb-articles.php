 	<div class="fitsc-config">
  			<div class="fitsc-field">
				<label class="fitsc-label"><?php _e( 'Title', 'fitsc' ); ?></label>
 				<div class="fitsc-controls">
					<input ng-model="title" type="text">
				</div>
			</div>
			<div class="fitsc-field">
				<label class="fitsc-label"><?php _e( 'Maximum topics to show: ', 'fitsc' ); ?></label>
				<div class="fitsc-controls">
					<input ng-model="number" type="number" min="0">
 				</div>
			</div>
 			<div class="fitsc-field">
				<label class="fitsc-label"><?php _e( 'Order By: ', 'fitsc' ); ?></label>
				<div class="fitsc-controls">
					<select ng-model="order_by">
						<?php
							FITSC_Helper::options( array(
								'newness' => __( 'Newest Topics', 'fitsc' ),
								'popular'      => __( 'Popular Topics', 'fitsc' ),
								'freshness'      => __( 'Topics With Recent Replies', 'fitsc' ),
							) );
						?>
					</select>
				</div>
 			</div>

   	</div>

	<div class="fitsc-preview">
		<div class="fitsc-preview-shortcode">
			<h4 class="fitsc-heading"><?php _e( 'Shortcode Preview', 'fitsc' ); ?></h4>
			<pre class="fitsc-preview-content fitsc-shortcode"><?php
				$text = "[$shortcode";
				$text .= FITSC_Helper::shortcode_atts(
			 		'title',
					'number',
 					'order_by'
				);
				$text .= "][/$shortcode]";
				echo $text;
				?>
			</pre>
		</div>
	</div>

