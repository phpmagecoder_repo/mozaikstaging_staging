<div ng-controller="Map">

	<div class="fitsc-config">
 		<div class="fitsc-field">
			<label class="fitsc-label"><?php _e( 'Latitude', 'fitsc' ); ?></label>
			<div class="fitsc-controls">
				<input ng-model="rnr_map_lat" type="text">
 			<div class="desc"><?php _e( 'Find your latitude position at <a href="http://itouchmap.com/latlong.html" target="_blank">http://itouchmap.com/latlong.html</a>', 'fitsc' ); ?></div>
			</div>
		</div>

		<div class="fitsc-field">
			<label class="fitsc-label"><?php _e( 'Longtitude', 'fitsc' ); ?></label>
 			<div class="fitsc-controls">
				<input ng-model="rnr_map_long" type="text">
				<div class="desc"><?php _e( 'Find your longitude position at <a href="http://itouchmap.com/latlong.html" target="_blank">http://itouchmap.com/latlong.html</a>', 'fitsc' ); ?></div>
			</div>
		</div>

		<div class="fitsc-field">
			<label class="fitsc-label"><?php _e( 'Map Zoom Value', 'fitsc' ); ?></label>
			<div class="fitsc-controls">
				<input ng-model="rnr_map_zoom" type="text">
				<div class="desc"><?php _e( 'Give Map Zoom value.', 'fitsc' ); ?></div>
			</div>
		</div>

 	</div>

	<div class="fitsc-preview">
		<div class="fitsc-preview-shortcode">
			<h4 class="fitsc-heading"><?php _e( 'Shortcode Preview', 'fitsc' ); ?></h4>
			<pre class="fitsc-preview-content fitsc-shortcode"><?php
				$text = "[$shortcode";
				$text .= FITSC_Helper::shortcode_atts(
 					'rnr_map_lat',
					'rnr_map_long',
					'rnr_map_zoom'
 				);
				$text .= "]{{text}}[/$shortcode]";
 				echo $text;
				?>
			</pre>
		</div>
	</div>
</div>