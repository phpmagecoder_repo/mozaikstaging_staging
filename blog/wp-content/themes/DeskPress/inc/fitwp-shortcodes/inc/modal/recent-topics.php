 	<div class="fitsc-config">
  			<div class="fitsc-field">
				<label class="fitsc-label"><?php _e( 'Title', 'fitsc' ); ?></label>
 				<div class="fitsc-controls">
					<input ng-model="title" type="text">
				</div>
			</div>
			<div class="fitsc-field">
				<label class="fitsc-label"><?php _e( 'Maximum topics to show: ', 'fitsc' ); ?></label>
				<div class="fitsc-controls">
					<input ng-model="number" type="number" min="0">
 				</div>
			</div>
			<div class="fitsc-field">
				<label class="fitsc-label"><?php _e( 'Parent Forum ID: ', 'fitsc' ); ?></label>
				<div class="fitsc-controls">
					<input ng-model="parent_forum_id" type="text"><br/>
					<small><?php _e( '"0" to show only root - "any" to show all', 'fitsc' ); ?></small>
				</div>
 			</div>

			<div class="fitsc-field">
 				<div class="fitsc-controls">
					<?php _e( 'Show post date: ', 'fitsc' ); ?> <input ng-model="show_post_date" type="checkbox">
				</div>
 			</div>
			<div class="fitsc-field">
 				<div class="fitsc-controls">
					<?php _e( 'Show topic author: ', 'fitsc' ); ?> <input ng-model="show_author" type="checkbox">
				</div>
 			</div>
			<div class="fitsc-field">
				<label class="fitsc-label"><?php _e( 'Order By: ', 'fitsc' ); ?></label>
				<div class="fitsc-controls">
					<select ng-model="order_by">
						<?php
						FITSC_Helper::options( array(
							'newness' => __( 'Newest Topics', 'fitsc' ),
							'popular'      => __( 'Popular Topics', 'fitsc' ),
							'freshness'      => __( 'Topics With Recent Replies', 'fitsc' ),
 						) );
						?>
					</select>
				</div>
 			</div>

   	</div>

	<div class="fitsc-preview">
		<div class="fitsc-preview-shortcode">
			<h4 class="fitsc-heading"><?php _e( 'Shortcode Preview', 'fitsc' ); ?></h4>
			<pre class="fitsc-preview-content fitsc-shortcode"><?php
				$text = "[$shortcode";
				$text .= FITSC_Helper::shortcode_atts(
			 		'title',
					'number',
					'parent_forum_id',
					'show_post_date',
					'show_author',
					'order_by'
				);
				$text .= "][/$shortcode]";
				echo $text;
				?>
			</pre>
		</div>
	</div>

