<?php
if ( ! class_exists( 'thimpress_megamenu' ) ) {

	/**
	 * The thimpress megamenu class contains various methods necessary to create mega menus out of the admin backend
	 * @package    ThimPressFramework
	 */
	class thimpress_megamenu {

		/**
		 * thimpress_megamenu constructor
		 * The constructor uses wordpress hooks and filters provided and
		 * replaces the default menu with custom functions and classes within this file
		 * @package    ThimPressFramework
		 */
		function wp_enqueue_media() {
			if ( is_admin() ) {
				wp_enqueue_media();
			}
		}

		function thimpress_megamenu() {
			//adds stylesheet and javascript to the menu page
			add_action( 'admin_menu', array( &$this, 'thimpress_menu_header' ) );

			add_action( 'admin_enqueue_scripts', array( $this, 'wp_enqueue_media' ) );


			//exchange arguments and tell menu to use the thimpress walker for front end rendering
			add_filter( 'wp_nav_menu_args', array( &$this, 'modify_arguments' ), 100 );

			//exchange argument for backend menu walker
			add_filter( 'wp_edit_nav_menu_walker', array( &$this, 'modify_backend_walker' ), 100 );

			//save thimpress options:
			add_action( 'wp_update_nav_menu_item', array( &$this, 'update_menu' ), 100, 3 );
		}

		/**
		 * If we are on the nav menu page add javascript and css for the page
		 */
		function thimpress_menu_header() {
			if ( basename( $_SERVER['PHP_SELF'] ) == "nav-menus.php" ) {
				wp_enqueue_style( 'thimpress_mega_menu_backend', trailingslashit( get_template_directory_uri() ) . 'inc/megamenu/css/backend.css' );
				wp_enqueue_style( 'thimpress_font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' );
				wp_enqueue_script( 'thimpress_mega_menu_js', trailingslashit( get_template_directory_uri() ) . 'inc/megamenu/js/thimpress_mega_menu.js', array( 'jquery', 'jquery-ui-sortable' ), false, true );
			}
			add_thickbox();
		}

		/**
		 * Replaces the default arguments for the front end menu creation with new ones
		 */
		function modify_arguments( $arguments ) {

			//$walker = apply_filters("thimpress_mega_menu_walker", "thimpress_walker");
//			if($walker)
//			{
			$arguments['walker']          = new thimpress_walker();
			$arguments['container_class'] = ($arguments['container_class']=='') ? 'megaWrapper' : ' megaWrapper';
			$arguments['menu_class']      = 'thimpress_mega_menu';

//			}

			return $arguments;
		}

		/**
		 * Tells wordpress to use our backend walker instead of the default one
		 */
		function modify_backend_walker( $name ) {
			return 'thimpress_backend_walker';
		}

		/*
		 * Save and Update the Custom Navigation Menu Item Properties by checking all $_POST vars with the name of $check
		 * @param int $menu_id
		 * @param int $menu_item_db
		 */

		function update_menu( $menu_id, $menu_item_db ) {
			$check = apply_filters( 'avf_mega_menu_post_meta_fields', array( 'megamenu', 'item-style', 'menu-icon', 'bg-image', 'hide-text', 'disable-link', 'full-width-dropdown', 'submenu-columns', 'side-dropdown-elements', 'submenu-type', 'widget-area', 'bg-image-repeat', 'bg-image-attachment', 'bg-image-position', 'bg-image-size' ), $menu_id, $menu_item_db );

			foreach ( $check as $key ) {
				if ( ! isset( $_POST['menu-item-thimpress-' . $key][$menu_item_db] ) ) {
					$_POST['menu-item-thimpress-' . $key][$menu_item_db] = "";
				}

				$value = $_POST['menu-item-thimpress-' . $key][$menu_item_db];
				update_post_meta( $menu_item_db, '_menu-item-thimpress-' . $key, $value );
			}
		}

	}

}


if ( ! class_exists( 'thimpress_walker' ) ) {

	/**
	 * The thimpress walker is the frontend walker and necessary to display the menu, this is a advanced version of the wordpress menu walker
	 * @package WordPress
	 * @since   1.0.0
	 * @uses    Walker
	 */
	class thimpress_walker extends Walker {

		/**
		 * @see Walker::$tree_type
		 * @var string
		 */
		var $tree_type = array( 'post_type', 'taxonomy', 'custom' );

		/**
		 * @see  Walker::$db_fields
		 * @todo Decouple this.
		 * @var array
		 */
		var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

		/**
		 * @var int $columns
		 */
		var $columns = 0;

		/**
		 * @var int $max_columns maximum number of columns within one mega menu
		 */
		var $max_columns = 0;

		/**
		 * @var int $rows holds the number of rows within the mega menu
		 */
		var $rows = 1;

		/**
		 * @var array $rowsCounter holds the number of columns for each row within a multidimensional array
		 */
		var $rowsCounter = array();

		/**
		 * @var string $mega_active hold information whetever we are currently rendering a mega menu or not
		 */
		var $mega_active = 0;

		/**
		 * Traverse elements to create list from elements.
		 *
		 * Display one element if the element doesn't have any children otherwise,
		 * display the element and its children. Will only traverse up to the max
		 * depth and no ignore elements under that depth. It is possible to set the
		 * max depth to include all depths, see walk() method.
		 *
		 * This method should not be called directly, use the walk() method instead.
		 *
		 * @since 2.5.0
		 *
		 * @param object $element           Data object.
		 * @param array  $children_elements List of elements to continue traversing.
		 * @param int    $max_depth         Max depth to traverse.
		 * @param int    $depth             Depth of current element.
		 * @param array  $args              An array of arguments.
		 * @param string $output            Passed by reference. Used to append additional content.
		 *
		 * @return null Null on failure with no changes to parameters.
		 */
		function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {

			if ( ! $element ) {
				return;
			}

			$id_field = $this->db_fields['id'];

			//display this element
			if ( isset( $args[0] ) && is_array( $args[0] ) ) {
				$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
			}
			$cb_args = array_merge( array( &$output, $element, $depth ), $args );
			call_user_func_array( array( $this, 'start_el' ), $cb_args );

			$id = $element->$id_field;

			// descend only when the depth is right and there are childrens for this element
			if ( ( $max_depth == 0 || $max_depth > $depth + 1 ) && isset( $children_elements[$id] ) ) {
				$b          = $args[0];
				$b->element = $element;
				$args[0]    = $b;
				foreach ( $children_elements[$id] as $child ) {

					if ( ! isset( $newlevel ) ) {
						$newlevel = true;
						//start the child delimiter
						$cb_args = array_merge( array( &$output, $depth ), $args );
						call_user_func_array( array( $this, 'start_lvl' ), $cb_args );
					}
					$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
				}
				unset( $children_elements[$id] );
			}

			if ( isset( $newlevel ) && $newlevel ) {
				//end the child delimiter
				$cb_args = array_merge( array( &$output, $depth ), $args );
				call_user_func_array( array( $this, 'end_lvl' ), $cb_args );
			}

			//end this element
			$cb_args = array_merge( array( &$output, $element, $depth ), $args );
			call_user_func_array( array( $this, 'end_el' ), $cb_args );
		}

		/**
		 * @see Walker::start_lvl()
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int    $depth  Depth of page. Used for padding.
		 */
		function start_lvl( &$output, $depth = 0, $args = array() ) {
			$item_id         = isset( $args->element->ID ) ? $args->element->ID : '';
			$bg_image        = get_post_meta( $item_id, '_menu-item-thimpress-bg-image', true );
			$submenu_columns = get_post_meta( $item_id, '_menu-item-thimpress-submenu-columns', true );
			$dropdown_stype  = get_post_meta( $item_id, '_menu-item-thimpress-submenu-type', true );

			$style = '';
			if ( $bg_image ) {
				$bg_image_repeat     = get_post_meta( $item_id, '_menu-item-thimpress-bg-image-repeat', true );
				$bg_image_attachment = get_post_meta( $item_id, '_menu-item-thimpress-bg-image-attachment', true );
				$bg_image_position   = get_post_meta( $item_id, '_menu-item-thimpress-bg-image-position', true );
				$bg_image_size       = get_post_meta( $item_id, '_menu-item-thimpress-bg-image-size', true );
				$style               = ' style="background-image:url(' . $bg_image . ');background-repeat:' . $bg_image_repeat . ';background-attachment:' . $bg_image_attachment . ';background-position:' . $bg_image_position . ';background-size:' . $bg_image_size . ';" ';
			}

			$class = '';
			if ( $dropdown_stype != "standard" ) {
				if ( $submenu_columns ) {
					$class .= ' megacol submenu_columns_' . $submenu_columns;
				}
			}


			$indent = str_repeat( "\t", $depth );

			$output .= "\n$indent<ul class=\"sub-menu$class\" " . $style . ">\n";
		}

		/**
		 * @see Walker::end_lvl()
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int    $depth  Depth of page. Used for padding.
		 */
		function end_lvl( &$output, $depth = 0, $args = array() ) {
			$indent = str_repeat( "\t", $depth );
			$output .= "$indent</ul>\n";
		}

		/**
		 * @see Walker::start_el()
		 *
		 * @param string $output       Passed by reference. Used to append additional content.
		 * @param object $item         Menu item data object.
		 * @param int    $depth        Depth of menu item. Used for padding.
		 * @param int    $current_page Menu item ID.
		 * @param object $args
		 */
		function start_el( &$output, $item, $depth = 0, $args = array(), $current_object_id = 0 ) {
			global $wp_query;

			//set maxcolumns
			//if(!isset($args->max_columns)) $args->max_columns = 5;
			$hide_text    = get_post_meta( $item->ID, '_menu-item-thimpress-hide-text', true );
			$disable_link = get_post_meta( $item->ID, '_menu-item-thimpress-disable-link', true );
			$item_output  = $li_text_block_class = $column_class = "";
//var_dump($args);
			$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
			$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
			$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
			$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';

			$menu_icon   = get_post_meta( $item->ID, '_menu-item-thimpress-menu-icon', true );
			$description = ( ! empty( $item->description ) and 0 == $depth ) ? '<small class="nav_desc">' . esc_attr( $item->description ) . '</small>' : '';

			if ( isset( $args->before ) ) {
				$item_output .= $args->before;
			}
			if ( $hide_text <> '' && $disable_link <> '' ) {
				$item_output .= '';
			} elseif ( $disable_link <> '' ) {
				$item_output .= '<span class="disable_link">';
			} else {
				$item_output .= '<a' . $attributes . '>';
			}
			$item_output .= $description;
			//$item_output .= ( $disable_link ) ? '<span>' : '<a' . $attributes . '>';
			//	$item_output .= '<a' . $attributes . '>';

			if ( $menu_icon ) {
				$item_output .= '<i class="fa fa-fw fa-' . $menu_icon . '"></i> ';
			}

			$link_before = isset( $args->link_before ) ? $args->link_before : '';
			$link_after  = isset( $args->link_after ) ? $args->link_after : '';
			$title       = isset( $item->title ) ? $item->title : $item->post_title;
			if ( $hide_text == '' ) {
				$item_output .= $link_before . apply_filters( 'the_title', $title, $item->ID ) . $link_after;
			}

			//$item_output .= '</a>';
			if ( $hide_text <> '' && $disable_link <> '' ) {
				$item_output .= '';
			} elseif ( $disable_link <> '' ) {
				$item_output .= '</span>';
			} else {
				$item_output .= '</a>';
			}

			if ( isset( $args->link_after ) ) {
				$item_output .= $args->after;
			}


			$indent      = ( $depth ) ? str_repeat( "\t", $depth ) : '';
			$class_names = $value = $class_full_width = '';

			$classes         = empty( $item->classes ) ? array() : (array) $item->classes;
			$menu_full_width = get_post_meta( $item->ID, '_menu-item-thimpress-full-width-dropdown', true );
			$menu_dropdown   = get_post_meta( $item->ID, '_menu-item-thimpress-side-dropdown-elements', true );
			$dropdown_stype  = get_post_meta( $item->ID, '_menu-item-thimpress-submenu-type', true );
			$class_full_width .= ( $menu_full_width ) ? ' dropdown_full_width ' : '';
			$class_full_width .= ' ' . $menu_dropdown . ' ' . $dropdown_stype;
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
			$class_names = ' class="' . $li_text_block_class . esc_attr( $class_names ) . $column_class . $class_full_width . '"';

			if ( $depth == 1 ) {
				$columns        = 'menu_item_parent';
				$parent_item_id = $item->menu_item_parent;
			}
			//$output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';
			$output .= $indent . '<li ' . $value . $class_names . '>';
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}

		/**
		 * @see Walker::end_el()
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param object $item   Page data object. Not used.
		 * @param int    $depth  Depth of page. Not Used.
		 */
		function end_el( &$output, $item, $depth = 0, $args = array() ) {

			$submenu_type = get_post_meta( $item->ID, '_menu-item-thimpress-submenu-type', true );
			if ( $submenu_type && $submenu_type == 'widget_area' ) {
				ob_start();
				$widget_area = get_post_meta( $item->ID, '_menu-item-thimpress-widget-area', true );
				dynamic_sidebar( substr( $widget_area, 12 ) );
				$content         = ob_get_clean();
				$submenu_columns = get_post_meta( $item->ID, '_menu-item-thimpress-submenu-columns', true );
				if ( $content ) {
					if ( $submenu_columns ) {
						$output .= '<ul class="sub-menu submenu_columns_' . $submenu_columns . ' submenu-widget">' . "\n" . $content . '</ul>';
					}
					//$output .= '<ul class="mega_dropdown">' . "\n" . $content . '</ul>';
				}
			}
			$output .= "</li>\n";
		}

	}

}


if ( ! class_exists( 'thimpress_backend_walker' ) ) {

	/**
	 * Create HTML list of nav menu input items.
	 * This walker is a clone of the wordpress edit menu walker with some options appended, so the user can choose to create mega menus
	 *
	 * @package ThimPressFramework
	 * @since   1.0
	 * @uses    Walker_Nav_Menu
	 */
	class thimpress_backend_walker extends Walker_Nav_Menu {

		/**
		 * @see   Walker_Nav_Menu::start_lvl()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference.
		 * @param int    $depth  Depth of page.
		 */
		function start_lvl( &$output, $depth = 0, $args = array() ) {

		}

		/**
		 * @see   Walker_Nav_Menu::end_lvl()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference.
		 * @param int    $depth  Depth of page.
		 */
		function end_lvl( &$output, $depth = 0, $args = array() ) {

		}

		/**
		 * @see   Walker::start_el()
		 * @since 3.0.0
		 *
		 * @param string $output       Passed by reference. Used to append additional content.
		 * @param object $item         Menu item data object.
		 * @param int    $depth        Depth of menu item. Used for padding.
		 * @param int    $current_page Menu item ID.
		 * @param object $args
		 */
		function start_el( &$output, $item, $depth = 0, $args = array(), $current_object_id = 0 ) {
			global $_wp_nav_menu_max_depth;
			$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;
			$indent                 = ( $depth ) ? str_repeat( "\t", $depth ) : '';
			ob_start();

			$item_id      = esc_attr( $item->ID );
			$removed_args = array(
				'action',
				'customlink-tab',
				'edit-menu-item',
				'menu-item',
				'page-tab',
				'_wpnonce',
			);

			$original_title = '';
			if ( 'taxonomy' == $item->type ) {
				$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
			} elseif ( 'post_type' == $item->type ) {
				$original_object = get_post( $item->object_id );
				$original_title  = $original_object->post_title;
			}

			$classes = array(
				'menu-item menu-item-depth-' . $depth,
				'menu-item-' . esc_attr( $item->object ),
				'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive' ),
			);

			$title = $item->title;

			if ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
				$classes[] = 'pending';
				/* translators: %s: title of menu item in draft status */
				$title = sprintf( __( '%s (Pending)', 'deskpress' ), $item->title );
			}

			$title = empty( $item->label ) ? $title : $item->label;

			$itemValue = "";
			if ( $depth == 0 ) {
				$itemValue = get_post_meta( $item->ID, '_menu-item-thimpress-megamenu', true );
				if ( $itemValue != "" ) {
					$itemValue = 'thimpress_mega_active ';
				}
			}
			?>

		<li id="menu-item-<?php echo $item_id; ?>" class="<?php
		echo $itemValue;
		echo implode( ' ', $classes );
		?>">
		<dl class="menu-item-bar">
			<dt class="menu-item-handle">
				<span class="item-title"><?php echo esc_html( $title ); ?></span>
                    <span class="item-controls">

                        <span class="item-type item-type-default"><?php echo esc_html( $item->type_label ); ?></span>
                        <span class="item-type item-type-thimpress"><?php _e( 'Column', 'deskpress' ); ?></span>
                        <span class="item-type item-type-megafied"><?php _e( '(Mega Menu)', 'deskpress' ); ?></span>
                        <span class="item-order">
                            <a href="<?php
							echo wp_nonce_url(
								add_query_arg(
									array(
										'action'    => 'move-up-menu-item',
										'menu-item' => $item_id,
									), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) )
								), 'move-menu_item'
							);
							?>" class="item-move-up"><abbr title="<?php esc_attr_e( 'Move up', 'deskpress' ); ?>">&#8593;</abbr></a>
                            |
                            <a href="<?php
							echo wp_nonce_url(
								add_query_arg(
									array(
										'action'    => 'move-down-menu-item',
										'menu-item' => $item_id,
									), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) )
								), 'move-menu_item'
							);
							?>" class="item-move-down"><abbr title="<?php esc_attr_e( 'Move down', 'deskpress' ); ?>">&#8595;</abbr></a>
                        </span>
                        <a class="item-edit" id="edit-<?php echo $item_id; ?>" title="<?php _e( 'Edit Menu Item', 'deskpress' ); ?>" href="<?php
						echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) );
						?>"><?php _e( 'Edit Menu Item', 'deskpress' ); ?></a>
                    </span>
			</dt>
		</dl>

		<div class="menu-item-settings" id="menu-item-settings-<?php echo $item_id; ?>">
		<?php if ( 'custom' == $item->type ) : ?>
			<p class="field-url description description-wide">
				<label for="edit-menu-item-url-<?php echo $item_id; ?>">
					<?php _e( 'URL', 'deskpress' ); ?><br />
					<input type="text" id="edit-menu-item-url-<?php echo $item_id; ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
				</label>
			</p>
		<?php endif; ?>

		<p class="description description-thin description-label thimpress_label_desc_on_active">
			<label for="edit-menu-item-title-<?php echo $item_id; ?>">
				<span class='thimpress_default_label'><?php _e( 'Navigation Label', 'deskpress' ); ?></span>
				<!-- <span class='thimpress_mega_label'><?php _e('Mega Menu Column Title <span class="thimpress_supersmall">(if you dont want to display a title just enter a single dash: "-" )</span>', 'deskpress'); ?></span> -->
				<br />
				<input type="text" id="edit-menu-item-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
			</label>
		</p>

		<p class="description description-thin description-title">
			<label for="edit-menu-item-attr-title-<?php echo $item_id; ?>">
				<?php _e( 'Title Attribute', 'deskpress' ); ?><br />
				<input type="text" id="edit-menu-item-attr-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
			</label>
		</p>

		<p class="field-link-target description description-thin">
			<label for="edit-menu-item-target-<?php echo $item_id; ?>">
				<?php _e( 'link Target', 'deskpress' ); ?><br />
				<select id="edit-menu-item-target-<?php echo $item_id; ?>" class="widefat edit-menu-item-target" name="menu-item-target[<?php echo $item_id; ?>]">
					<option value="" <?php selected( $item->target, '' ); ?>><?php _e( 'Same window or tab', 'deskpress' ); ?></option>
					<option value="_blank" <?php selected( $item->target, '_blank' ); ?>><?php _e( 'New window or tab', 'deskpress' ); ?></option>
				</select>
			</label>
		</p>
		<p class="field-css-classes description description-thin">
			<label for="edit-menu-item-classes-<?php echo $item_id; ?>">
				<?php _e( 'CSS Classes (optional)', 'deskpress' ); ?><br />
				<input type="text" id="edit-menu-item-classes-<?php echo $item_id; ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo $item_id; ?>]" value="<?php echo esc_attr( implode( ' ', $item->classes ) ); ?>" />
			</label>
		</p>

		<p class="field-xfn description description-thin">
			<label for="edit-menu-item-xfn-<?php echo $item_id; ?>">
				<?php _e( 'link Relationship (XFN)', 'deskpress' ); ?><br />
				<input type="text" id="edit-menu-item-xfn-<?php echo $item_id; ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
			</label>
		</p>

		<p class="field-description description description-wide">
			<label for="edit-menu-item-description-<?php echo $item_id; ?>">
				<?php _e( 'Description', 'deskpress' ); ?><br />
				<textarea id="edit-menu-item-description-<?php echo $item_id; ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo $item_id; ?>]"><?php echo esc_html( $item->post_content ); ?></textarea>
			</label>
		</p>

		<div class='thimpress_mega_menu_options'>
		<?php
		$title = 'Menu Icon';
		$key = "menu-item-thimpress-menu-icon";
		$value = get_post_meta( $item->ID, '_' . $key, true );
		?>
		<div id="<?php echo $key . '-' . $item_id . '-popup'; ?>" style="display:none;">
			<?php
			$icons = array( 'adjust', 'adn', 'align-center', 'align-justify', 'align-left', 'align-right', 'ambulance', 'anchor', 'android', 'angle-double-down', 'angle-double-left', 'angle-double-right', 'angle-double-up', 'angle-down', 'angle-left', 'angle-right', 'angle-up', 'apple', 'archive', 'arrow-circle-down', 'arrow-circle-left', 'arrow-circle-o-down', 'arrow-circle-o-left', 'arrow-circle-o-right', 'arrow-circle-o-up', 'arrow-circle-right', 'arrow-circle-up', 'arrow-down', 'arrow-left', 'arrow-right', 'arrow-up', 'arrows', 'arrows-alt', 'arrows-h', 'arrows-v', 'asterisk', 'automobile', 'backward', 'ban', 'bank', 'bar-chart-o', 'barcode', 'bars', 'beer', 'behance', 'behance-square', 'bell', 'bell-o', 'bitbucket', 'bitbucket-square', 'bitcoin', 'bold', 'bolt', 'bomb', 'book', 'bookmark', 'bookmark-o', 'briefcase', 'btc', 'bug', 'building', 'building-o', 'bullhorn', 'bullseye', 'cab', 'calendar', 'calendar-o', 'camera', 'camera-retro', 'car', 'caret-down', 'caret-left', 'caret-right', 'caret-square-o-down', 'caret-square-o-left', 'caret-square-o-right', 'caret-square-o-up', 'caret-up', 'certificate', 'chain', 'chain-broken', 'check', 'check-circle', 'check-circle-o', 'check-square', 'check-square-o', 'chevron-circle-down', 'chevron-circle-left', 'chevron-circle-right', 'chevron-circle-up', 'chevron-down', 'chevron-left', 'chevron-right', 'chevron-up', 'child', 'circle', 'circle-o', 'circle-o-notch', 'circle-thin', 'clipboard', 'clock-o', 'cloud', 'cloud-download', 'cloud-upload', 'cny', 'code', 'code-fork', 'codepen', 'coffee', 'cog', 'cogs', 'columns', 'comment', 'comment-o', 'comments', 'comments-o', 'compass', 'compress', 'copy', 'credit-card', 'crop', 'crosshairs', 'css3', 'cube', 'cubes', 'cut', 'cutlery', 'dashboard', 'database', 'dedent', 'delicious', 'desktop', 'deviantart', 'digg', 'dollar', 'dot-circle-o', 'download', 'dribbble', 'dropbox', 'drupal', 'edit', 'eject', 'ellipsis-h', 'ellipsis-v', 'empire', 'envelope', 'envelope-o', 'envelope-square', 'eraser', 'eur', 'euro', 'exchange', 'exclamation', 'exclamation-circle', 'exclamation-triangle', 'expand', 'external-link', 'external-link-square', 'eye', 'eye-slash', 'facebook', 'facebook-square', 'fast-backward', 'fast-forward', 'fax', 'female', 'fighter-jet', 'file', 'file-archive-o', 'file-audio-o', 'file-code-o', 'file-excel-o', 'file-image-o', 'file-movie-o', 'file-o', 'file-pdf-o', 'file-photo-o', 'file-picture-o', 'file-powerpoint-o', 'file-sound-o', 'file-text', 'file-text-o', 'file-video-o', 'file-word-o', 'file-zip-o', 'files-o', 'film', 'filter', 'fire', 'fire-extinguisher', 'flag', 'flag-checkered', 'flag-o', 'flash', 'flask', 'flickr', 'floppy-o', 'folder', 'folder-o', 'folder-open', 'folder-open-o', 'font', 'forward', 'foursquare', 'frown-o', 'gamepad', 'gavel', 'gbp', 'ge', 'gear', 'gears', 'gift', 'git', 'git-square', 'github', 'github-alt', 'github-square', 'gittip', 'glass', 'globe', 'google', 'google-plus', 'google-plus-square', 'graduation-cap', 'group', 'h-square', 'hacker-news', 'hand-o-down', 'hand-o-left', 'hand-o-right', 'hand-o-up', 'hdd-o', 'header', 'headphones', 'heart', 'heart-o', 'history', 'home', 'hospital-o', 'html5', 'image', 'inbox', 'indent', 'info', 'info-circle', 'inr', 'instagram', 'institution', 'italic', 'joomla', 'jpy', 'jsfiddle', 'key', 'keyboard-o', 'krw', 'language', 'laptop', 'leaf', 'legal', 'lemon-o', 'level-down', 'level-up', 'life-bouy', 'life-ring', 'life-saver', 'lightbulb-o', 'link', 'linkedin', 'linkedin-square', 'linux', 'list', 'list-alt', 'list-ol', 'list-ul', 'location-arrow', 'lock', 'long-arrow-down', 'long-arrow-left', 'long-arrow-right', 'long-arrow-up', 'magic', 'magnet', 'mail-forward', 'mail-reply', 'mail-reply-all', 'male', 'map-marker', 'maxcdn', 'medkit', 'meh-o', 'microphone', 'microphone-slash', 'minus', 'minus-circle', 'minus-square', 'minus-square-o', 'mobile', 'mobile-phone', 'money', 'moon-o', 'mortar-board', 'music', 'navicon', 'openid', 'outdent', 'pagelines', 'paper-plane', 'paper-plane-o', 'paperclip', 'paragraph', 'paste', 'pause', 'paw', 'pencil', 'pencil-square', 'pencil-square-o', 'phone', 'phone-square', 'photo', 'picture-o', 'pied-piper', 'pied-piper-alt', 'pied-piper-square', 'pinterest', 'pinterest-square', 'plane', 'play', 'play-circle', 'play-circle-o', 'plus', 'plus-circle', 'plus-square', 'plus-square-o', 'power-off', 'print', 'puzzle-piece', 'qq', 'qrcode', 'question', 'question-circle', 'quote-left', 'quote-right', 'ra', 'random', 'rebel', 'recycle', 'reddit', 'reddit-square', 'refresh', 'renren', 'reorder', 'repeat', 'reply', 'reply-all', 'retweet', 'rmb', 'road', 'rocket', 'rotate-left', 'rotate-right', 'rouble', 'rss', 'rss-square', 'rub', 'ruble', 'rupee', 'save', 'scissors', 'search', 'search-minus', 'search-plus', 'send', 'send-o', 'share', 'share-alt', 'share-alt-square', 'share-square', 'share-square-o', 'shield', 'shopping-cart', 'sign-in', 'sign-out', 'signal', 'sitemap', 'skype', 'slack', 'sliders', 'smile-o', 'sort', 'sort-alpha-asc', 'sort-alpha-desc', 'sort-amount-asc', 'sort-amount-desc', 'sort-asc', 'sort-desc', 'sort-down', 'sort-numeric-asc', 'sort-numeric-desc', 'sort-up', 'soundcloud', 'space-shuttle', 'spinner', 'spoon', 'spotify', 'square', 'square-o', 'stack-exchange', 'stack-overflow', 'star', 'star-half', 'star-half-empty', 'star-half-full', 'star-half-o', 'star-o', 'steam', 'steam-square', 'step-backward', 'step-forward', 'stethoscope', 'stop', 'strikethrough', 'stumbleupon', 'stumbleupon-circle', 'subscript', 'suitcase', 'sun-o', 'superscript', 'support', 'table', 'tablet', 'tachometer', 'tag', 'tags', 'tasks', 'taxi', 'tencent-weibo', 'terminal', 'text-height', 'text-width', 'th', 'th-large', 'th-list', 'thumb-tack', 'thumbs-down', 'thumbs-o-down', 'thumbs-o-up', 'thumbs-up', 'ticket', 'times', 'times-circle', 'times-circle-o', 'tint', 'toggle-down', 'toggle-left', 'toggle-right', 'toggle-up', 'trash-o', 'tree', 'trello', 'trophy', 'truck', 'try', 'tumblr', 'tumblr-square', 'turkish-lira', 'twitter', 'twitter-square', 'umbrella', 'underline', 'undo', 'university', 'unlink', 'unlock', 'unlock-alt', 'unsorted', 'upload', 'usd', 'user', 'user-md', 'users', 'video-camera', 'vimeo-square', 'vine', 'vk', 'volume-down', 'volume-off', 'volume-up', 'warning', 'wechat', 'weibo', 'weixin', 'wheelchair', 'windows', 'won', 'wordpress', 'wrench', 'xing', 'xing-square', 'yahoo', 'yen', 'youtube', 'youtube-play', 'youtube-square' );
			$html = '<input type="hidden" name="" class="wpb_vc_param_value" value="' . $value . '" id="trace"/> ';
			$html .= '<input class="search icon-search" type="text" placeholder="Search" /><div class="icon-preview icon-preview-' . $item_id . '"><i class=" fa fa-' . $value . '"></i></div>';
			$html .= '<div id="' . $key . '-' . $item_id . '-icon-dropdown" >';
			$html .= '<ul class="icon-list">';
			$n = 1;
			foreach ( $icons as $icon ) {
				$selected = ( $icon == $value ) ? 'class="selected"' : '';
				$id       = 'icon-' . $n;
				$html .= '<li ' . $selected . ' data-icon="' . $icon . '"><i class="icon fa fa-' . $icon . '"></i></li>';
				$n ++;
			}
			$html .= '</ul>';
			$html .= '</div>';
			$html .= '<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery(".search").keyup(function(){
													// Retrieve the input field text and reset the count to zero
													var filter = jQuery(this).val(), count = 0;
													// Loop through the icon list
													jQuery("#' . $key . '-' . $item_id . '-icon-dropdown .icon-list li").each(function(){
														// If the list item does not contain the text phrase fade it out
														if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
															jQuery(this).fadeOut();
														} else {
															jQuery(this).show();
															count++;
														}
													});
												});
											});

											jQuery("#' . $key . '-' . $item_id . '-icon-dropdown li").click(function() {
												jQuery(this).attr("class","selected").siblings().removeAttr("class");
												var icon = jQuery(this).attr("data-icon");
												console.log("edit-' . $key . '-' . $item_id . '");
												jQuery("#edit-' . $key . '-' . $item_id . '").val(icon);
												jQuery(".icon-preview-' . $item_id . '").html("<i class=\'icon fa fa-"+icon+"\'></i>");
											});
									</script>';
			echo $html;
			?>
		</div>


		<p class="description description-wide thimpress_checkbox thimpress_mega_menu thimpress_mega_menu_d1">

			<label for="edit-<?php echo $key . '-' . $item_id; ?>">
				<?php _e( $title, 'deskpress' ); ?><br />
				<input type="text" value="<?php echo $value; ?>" id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>" />
				<input alt="#TB_inline?height=400&width=500&inlineId=<?php echo $key . '-' . $item_id . '-popup'; ?>" title="<?php _e( 'Click to browse icon', 'deskpress' ) ?>" class="thickbox button-secondary submit-add-to-menu" type="button" value="<?php _e( 'Browse Icon', 'deskpress' ) ?>" />
				<span class="icon-preview  icon-preview<?php echo '-' . $item_id; ?>"><i class=" fa fa-<?php echo $value; ?>"></i></span>
			</label>
		</p>
		<!-- ***************  end item *************** -->
		<?php
		$title = 'Hide text of This Item';
		$key = "menu-item-thimpress-hide-text";
		$value = get_post_meta( $item->ID, '_' . $key, true );
		$value = ( $value == 'active' ) ? ' checked="checked" ' : '';
		?>
		<p class="description description-wide">
			<label for="edit-<?php echo $key . '-' . $item_id; ?>">
				<input type="checkbox" value="active" id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>" <?php echo $value; ?> /><?php _e( $title ); ?>
			</label>
		</p>


		<?php
		$title = 'Disable Link';
		$key = "menu-item-thimpress-disable-link";
		$value = get_post_meta( $item->ID, '_' . $key, true );
		$value = ( $value == 'active' ) ? ' checked="checked" ' : '';
		?>
		<p class="description description-wide">
			<label for="edit-<?php echo $key . '-' . $item_id; ?>">
				<input type="checkbox" value="active" id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>" <?php echo $value; ?> /><?php _e( $title, 'deskpress' ); ?>
			</label>
		</p>

		<?php
		if ( ! $depth ) {
			?>
			<?php
			$title              = 'Submenu Type';
			$key                = "menu-item-thimpress-submenu-type";
			$submenu_type_value = $value = get_post_meta( $item->ID, '_' . $key, true );
			//$sidebars = $GLOBALS['wp_registered_sidebars'];
			?>
			<p class="description description-wide description_width_100">
				<?php _e( $title, 'deskpress' ); ?><br />
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<select id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>" onchange="onChangeSubmenuType(this);">
						<option value="standard" <?php echo ( $value == 'standard' ) ? ' selected="selected" ' : ''; ?>><?php _e( 'Standard Dropdown', 'deskpress' ); ?></option>
						<option value="multicolumn" <?php echo ( $value == 'multicolumn' ) ? ' selected="selected" ' : ''; ?>><?php _e( 'Multicolumn Dropdown', 'deskpress' ); ?></option>
						<option value="widget_area" <?php echo ( $value == 'widget_area' ) ? ' selected="selected" ' : ''; ?>><?php _e( 'Widget area', 'deskpress' ); ?></option>
					</select>
				</label>
			</p>

			<?php
			$title = 'Widget Area';
			$key = "menu-item-thimpress-widget-area";
			$value = get_post_meta( $item->ID, '_' . $key, true );
			$sidebars = $GLOBALS['wp_registered_sidebars'];
			$style = ( $submenu_type_value == 'widget_area' ) ? '' : ' style="display:none;" ';
			?>
			<p class="description description-wide description_width_100 el_widget_area"<?php echo $style; ?>>
				<?php _e( $title, 'deskpress' ); ?><br />
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<select id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>">
						<option value="" <?php echo ( $value == '' ) ? ' selected="selected" ' : ''; ?>><?php _e( 'Select Widget Area', 'deskpress' ); ?></option>
						<?php
						foreach ( $sidebars as $sidebar ) {
							echo '<option value="widget_area_' . $sidebar['id'] . '" ' . ( ( $value == "widget_area_" . $sidebar['id'] ) ? ' selected="selected" ' : '' ) . '>[' . $sidebar['id'] . '] - ' . $sidebar['name'] . '</option>';
						}
						?>
					</select>
				</label>
			</p>


			<?php
			$title = 'Submenu Columns (Not For Standard Drops)';
			$key = "menu-item-thimpress-submenu-columns";
			$value = get_post_meta( $item->ID, '_' . $key, true );
			$style = ( $submenu_type_value == 'standard' ) ? ' style="display:none;" ' : '';
			?>
			<p class="description description-wide description_width_100 el_multicolumn"<?php echo $style; ?>>
				<?php _e( $title, 'deskpress' ); ?><br />
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<select id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>">
						<?php
						for ( $i = 1; $i <= 5; $i ++ ) {
							?>
							<option value="<?php echo $i; ?>" <?php echo ( $value == $i ) ? ' selected="selected" ' : ''; ?>><?php echo $i; ?></option>
						<?php
						}
						?>
					</select>
				</label>
			</p>

			<?php
			$title = 'Side of Dropdown Elements';
			$key = "menu-item-thimpress-side-dropdown-elements";
			$value = get_post_meta( $item->ID, '_' . $key, true );
			?>
			<p class="description description-wide description_width_100">
				<?php _e( $title, 'deskpress' ); ?><br />
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<select id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>">
						<option value="drop_to_left" <?php echo ( $value == 'drop_to_left' ) ? ' selected="selected" ' : ''; ?>><?php _e( 'Drop To Left Side', 'deskpress' ); ?></option>
						<option value="drop_to_right" <?php echo ( $value == 'drop_to_right' ) ? ' selected="selected" ' : ''; ?>><?php _e( 'Drop To Right Side', 'deskpress' ); ?></option>
						<option value="drop_to_center" <?php echo ( $value == 'drop_to_center' ) ? ' selected="selected" ' : ''; ?>><?php _e( 'Drop To Center', 'deskpress' ); ?></option>
					</select>
				</label>
			</p>

			<?php
			$title = 'Enable Full Width Dropdown ()';
			$key = "menu-item-thimpress-full-width-dropdown";
			$value = get_post_meta( $item->ID, '_' . $key, true );
			$value = ( $value == 'active' ) ? ' checked="checked" ' : '';
			$style = ( $submenu_type_value == 'standard' ) ? ' style="display:none;" ' : '';
			?>
			<p class="description description-wide el_fullwidth"<?php echo $style; ?>>
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<input type="checkbox" value="active" id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>" <?php echo $value; ?> /><?php _e( $title, 'deskpress' ); ?>
				</label>
			</p>

			<?php
			$title = 'DropDown Background Image';
			$key = "menu-item-thimpress-bg-image";
			$value = get_post_meta( $item->ID, '_' . $key, true );
			$style = '';
			?>

			<p class="description description-wide thimpress_checkbox thimpress_mega_menu thimpress_mega_menu_d2"<?php echo $style; ?>>
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<span class='thimpress_long_desc'><?php _e( $title, 'deskpress' ); ?></span><br />
					<input type="text" value="<?php echo $value; ?>" id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>" />
					<button id="browse-edit-<?php echo $key . '-' . $item_id; ?>" class="set_custom_images button button-secondary submit-add-to-menu"><?php _e( 'Browse Image', 'deskpress' ); ?></button>
				</label>
			</p>
			<script type="text/javascript">
				jQuery(document).ready(function ($) {
					if ($('.set_custom_images').length > 0) {
						if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
							$('.wrap').on('click', '.set_custom_images', function (e) {
								e.preventDefault();
								var input_text = $('#' + (this.id).substring(7));
								wp.media.editor.send.attachment = function (props, attachment) {
									input_text.val(attachment.url);
								};
								wp.media.editor.open(input_text);
								return false;
							});
						}
					}
					;
				});
			</script>
			<p class="description description-wide description_width_25">
				<?php
				$key = "menu-item-thimpress-bg-image-repeat";
				$value = get_post_meta( $item->ID, '_' . $key, true );
				$options = array( 'repeat', 'no-repeat', 'repeat-x', 'repeat-y' );
				?>
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<select id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>">
						<?php
						foreach ( $options as $option ) {
							?>
							<option value="<?php echo $option; ?>" <?php echo ( $value == $option ) ? ' selected="selected" ' : ''; ?>><?php echo $option; ?></option>
						<?php
						}
						?>
					</select>
				</label>
				<?php
				$key = "menu-item-thimpress-bg-image-attachment";
				$value = get_post_meta( $item->ID, '_' . $key, true );
				$options = array( 'scroll', 'fixed' );
				?>
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<select id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>">
						<?php
						foreach ( $options as $option ) {
							?>
							<option value="<?php echo $option; ?>" <?php echo ( $value == $option ) ? ' selected="selected" ' : ''; ?>><?php echo $option; ?></option>
						<?php
						}
						?>
					</select>
				</label>

				<?php
				$key = "menu-item-thimpress-bg-image-position";
				$value = get_post_meta( $item->ID, '_' . $key, true );
				$options = array( 'center', 'center left', 'center right', 'top left', 'top center', 'top right', 'bottom left', 'bottom center', 'bottom right' );
				?>
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<select id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>">
						<?php
						foreach ( $options as $option ) {
							?>
							<option value="<?php echo $option; ?>" <?php echo ( $value == $option ) ? ' selected="selected" ' : ''; ?>><?php echo $option; ?></option>
						<?php
						}
						?>
					</select>
				</label>

				<?php
				$key = "menu-item-thimpress-bg-image-size";
				$value = get_post_meta( $item->ID, '_' . $key, true );
				$options = array( "auto"      => "Keep original",
								  "100% auto" => "Stretch to width",
								  "auto 100%" => "Stretch to height",
								  "cover"     => "cover",
								  "contain"   => "contain" );
				?>
				<label for="edit-<?php echo $key . '-' . $item_id; ?>">
					<select id="edit-<?php echo $key . '-' . $item_id; ?>" class=" <?php echo $key; ?>" name="<?php echo $key . "[" . $item_id . "]"; ?>">
						<?php
						foreach ( $options as $op_value => $op_text ) {
							?>
							<option value="<?php echo $op_value; ?>" <?php echo ( $value == $op_value ) ? ' selected="selected" ' : ''; ?>><?php echo $op_text; ?></option>
						<?php
						}
						?>
					</select>
				</label>
			</p>
			<!-- ***************  end item *************** -->
		<?php
		}
		?>
		</div>


		<?php do_action( 'thimpress_mega_menu_option_fields', $output, $item, $depth, $args ); ?>

		<!-- ################# end thimpress custom code here ################# -->

		<div class="menu-item-actions description-wide submitbox">
			<?php if ( 'custom' != $item->type ) : ?>
				<p class="link-to-original">
					<?php printf( __( 'Original: %s', 'deskpress' ), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
				</p>
			<?php endif; ?>
			<a class="item-delete submitdelete deletion" id="delete-<?php echo $item_id; ?>" href="<?php
			echo wp_nonce_url(
				add_query_arg(
					array(
						'action'    => 'delete-menu-item',
						'menu-item' => $item_id,
					), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) )
				), 'delete-menu_item_' . $item_id
			);
			?>"><?php _e( 'Remove', 'deskpress' ); ?></a> <span class="meta-sep"> | </span>
			<a class="item-cancel submitcancel" id="cancel-<?php echo $item_id; ?>" href="<?php echo add_query_arg( array( 'edit-menu-item' => $item_id, 'cancel' => time() ), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) ) );
			?>#menu-item-settings-<?php echo $item_id; ?>">Cancel</a>
		</div>

		<input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo $item_id; ?>]" value="<?php echo $item_id; ?>" />
		<input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
		<input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
		<input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
		<input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
		<input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
		</div>
		<!-- .menu-item-settings-->
		<ul class="menu-item-transport"></ul>
			<?php
			$output .= ob_get_clean();
		}

	}

}


if ( ! function_exists( 'thimpress_fallback_menu' ) ) {

	/**
	 * Create a navigation out of pages if the user didnt create a menu in the backend
	 *
	 */
	function thimpress_fallback_menu() {
		$current = "";
		$exclude = thimpress_get_option( 'frontpage' );
		if ( is_front_page() ) {
			$current = "class='current-menu-item'";
		}
		if ( $exclude ) {
			$exclude = "&exclude=" . $exclude;
		}

		echo "<div class='fallback_menu'>";
		echo "<ul class='thimpress_mega menu'>";
		echo "<li $current><a href='" . home_url() . "'>" . __( 'Home', 'thimpress_framework' ) . "</a></li>";
		wp_list_pages( 'title_li=&sort_column=menu_order' . $exclude );
		echo apply_filters( 'avf_fallback_menu_items', "", 'fallback_menu' );
		echo "</ul></div>";
	}

}
new thimpress_megamenu();
    