<?php

/**
 * This class generates custom CSS into static CSS file in uploads folder
 * and enqueue it in the frontend
 *
 * CSS is generated only when theme options is saved (changed)
 * Works with LESS (for unlimited color schemes)
 */
require "lessc.inc.php";
require( THEME_DIR . "css/custom.php" );

function generate($folder, $fileout, $type, $deskpress_data) {
    $files = scandir($folder);
    $css = "";
    $regex = array(
        "`^([\t\s]+)`ism" => '',
        "`^\/\*(.+?)\*\/`ism" => "",
        "`([\n\A;]+)\/\*(.+?)\*\/`ism" => "$1",
        "`([\n\A;\s]+)//(.+?)[\n\r]`ism" => "$1\n",
        "`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism" => "\n"
    );
    $content_file_options ="";
    $themeoptions_variation = themeoptions_variation($deskpress_data);

    foreach ($files as $file) {
        if (strpos($file, 'less') !== false) {
            $content_file_options = $themeoptions_variation;
            $content_file_options .= file_get_contents(THEME_DIR . "css/less/" . $file);
            $compiler = new lessc;
            $compiler->setFormatter('compressed');
            $css .= $compiler->compile($content_file_options);
            $content_file_options = "";
        }
    }

    $css .= customcss($deskpress_data);

    $css = preg_replace(array_keys($regex), $regex, $css);
    $style = file_get_contents(THEME_DIR . "inc/theme-info.txt");

    // Determine whether Multisite support is enabled
    if (is_multisite()) {
        // Write Theme Info into style.css
        if (!file_put_contents($fileout . $type, $style, LOCK_EX)) {
            echo __LINE__ . _e("Can't write the file \"", 'deskpress') . $fileout . $type . "\"";
        }

        // Write the rest to specific site style_ID.css
        $fileout = $fileout . '-' . get_current_blog_id();
        if (!file_put_contents($fileout . $type, $css, LOCK_EX)) {
            echo __("Can't write the file \"", 'deskpress') . $fileout . $type . "\"";
        }
    } else {
        // If this is not multisite, we write them all in style.css file
        $style .= $css;
        if (!file_put_contents($fileout . $type, $style, LOCK_EX)) {
            echo __LINE__ . _e("Can't write the file \"", 'deskpress') . $fileout . $type . "\"";
        }
    }
}
