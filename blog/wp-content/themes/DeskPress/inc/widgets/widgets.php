<?php

/********************************************************************
 * deskpress_social_links
 ********************************************************************/
class deskpress_social_links extends WP_Widget {
	function deskpress_social_links() {
		$widget_ops = array( 'classname' => 'deskpress_social_links', 'description' => 'Social Links' );
		$this->WP_Widget( 'widget_deskpress_social_links', 'deskpress: Social Links', $widget_ops );
	}

	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		$title          = empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );
		$link_face      = empty( $instance['link_face'] ) ? '' : apply_filters( 'widget_link_face', $instance['link_face'] );
		$link_twitter   = empty( $instance['link_twitter'] ) ? '' : apply_filters( 'widget_link_twitter', $instance['link_twitter'] );
		$link_google    = empty( $instance['link_google'] ) ? '' : apply_filters( 'widget_link_google', $instance['link_google'] );
		$link_dribble   = empty( $instance['link_dribble'] ) ? '' : apply_filters( 'widget_link_dribble', $instance['link_dribble'] );
		$link_linkedin  = empty( $instance['link_linkedin'] ) ? '' : apply_filters( 'widget_link_linkedin', $instance['link_linkedin'] );
		$link_pinterest = empty( $instance['link_pinterest'] ) ? '' : apply_filters( 'widget_link_pinterest', $instance['link_pinterest'] );
		$link_digg      = empty( $instance['link_digg'] ) ? '' : apply_filters( 'widget_link_digg', $instance['link_digg'] );
		$link_youtube   = empty( $instance['link_youtube'] ) ? '' : apply_filters( 'widget_link_youtube', $instance['link_youtube'] );
		$class_custom   = empty( $instance['class_custom'] ) ? '' : apply_filters( 'widget_class_custom', $instance['class_custom'] );
		echo $before_widget;
		?>
		<?php if ( $class_custom <> '' ) {
			echo '<div class="' . $class_custom . '">';
		}
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}?>
		<ul class="deskpress_social_link">
			<?php
			if ( $link_face != '' ) {
				echo '<li><a class="face hasTooltip" href="' . $link_face . '" ><i class="fa fa-facebook"></i></a></li>';
			}
			if ( $link_twitter != '' ) {
				echo '<li><a class="twitter hasTooltip" href="' . $link_twitter . '"  ><i class="fa fa-twitter"></i></a></li>';
			}
			if ( $link_google != '' ) {
				echo '<li><a class="google hasTooltip" href="' . $link_google . '"  ><i class="fa fa-google-plus"></i></a></li>';
			}
			if ( $link_dribble != '' ) {
				echo '<li><a class="dribble hasTooltip" href="' . $link_dribble . '"  ><i class="fa fa-dribbble"></i></a></li>';
			}
			if ( $link_linkedin != '' ) {
				echo '<li><a class="linkedin hasTooltip" href="' . $link_linkedin . '"  ><i class="fa fa-linkedin"></i></a></li>';
			}

			if ( $link_pinterest != '' ) {
				echo '<li><a class="pinterest hasTooltip" href="' . $link_pinterest . '"  ><i class="fa fa-pinterest"></i></a></li>';
			}
			if ( $link_digg != '' ) {
				echo '<li><a class="digg hasTooltip" href="' . $link_digg . '"  ><i class="fa fa-digg"></i></a></li>';
			}
			if ( $link_youtube != '' ) {
				echo '<li><a class="youtube hasTooltip" href="' . $link_youtube . '"  ><i class="fa fa-youtube"></i></a></li>';
			}
			?>
		</ul>

		<?php if ( $class_custom <> '' ) {
			echo '</div>';
		} ?>

		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance                   = $old_instance;
		$instance['title']          = strip_tags( $new_instance['title'] );
		$instance['link_face']      = strip_tags( $new_instance['link_face'] );
		$instance['link_twitter']   = strip_tags( $new_instance['link_twitter'] );
		$instance['link_google']    = strip_tags( $new_instance['link_google'] );
		$instance['link_dribble']   = strip_tags( $new_instance['link_dribble'] );
		$instance['link_linkedin']  = strip_tags( $new_instance['link_linkedin'] );
		$instance['link_pinterest'] = strip_tags( $new_instance['link_pinterest'] );
		$instance['link_digg']      = strip_tags( $new_instance['link_digg'] );
		$instance['link_youtube']   = strip_tags( $new_instance['link_youtube'] );
		$instance['class_custom']   = strip_tags( $new_instance['class_custom'] );

		return $instance;
	}

	function form( $instance ) {
		$instance       = wp_parse_args( (array) $instance, array(
			'title'          => '',
			'link_face'      => '',
			'link_twitter'   => '',
			'link_google'    => '',
			'link_dribble'   => '',
			'link_linkedin'  => '',
			'link_pinterest' => '',
			'link_digg'      => '',
			'link_youtube'   => '',
			'class_custom'   => ''
		) );
		$title          = strip_tags( $instance['title'] );
		$link_face      = strip_tags( $instance['link_face'] );
		$link_twitter   = strip_tags( $instance['link_twitter'] );
		$link_google    = strip_tags( $instance['link_google'] );
		$link_dribble   = strip_tags( $instance['link_dribble'] );
		$link_linkedin  = strip_tags( $instance['link_linkedin'] );
		$link_pinterest = strip_tags( $instance['link_pinterest'] );
		$link_digg      = strip_tags( $instance['link_digg'] );
		$link_youtube   = strip_tags( $instance['link_youtube'] );
		$class_custom   = strip_tags( $instance['class_custom'] );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo _e( 'Title: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link_face' ); ?>"><?php echo _e( 'Facebook Url: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'link_face' ); ?>" name="<?php echo $this->get_field_name( 'link_face' ); ?>" value="<?php echo esc_attr( $link_face ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link_twitter' ); ?>"><?php echo _e( 'Twitter Url: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'link_twitter' ); ?>" name="<?php echo $this->get_field_name( 'link_twitter' ); ?>" value="<?php echo esc_attr( $link_twitter ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link_google' ); ?>"><?php echo _e( 'Google Url: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'link_google' ); ?>" name="<?php echo $this->get_field_name( 'link_google' ); ?>" value="<?php echo esc_attr( $link_google ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link_dribble' ); ?>"><?php echo _e( 'Dribble Url: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'link_dribble' ); ?>" name="<?php echo $this->get_field_name( 'link_dribble' ); ?>" value="<?php echo esc_attr( $link_dribble ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link_linkedin' ); ?>"><?php echo _e( 'Linkedin Url: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'link_linkedin' ); ?>" name="<?php echo $this->get_field_name( 'link_linkedin' ); ?>" value="<?php echo esc_attr( $link_linkedin ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'link_pinterest' ); ?>"><?php echo _e( 'pinterest Url: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'link_pinterest' ); ?>" name="<?php echo $this->get_field_name( 'link_pinterest' ); ?>" value="<?php echo esc_attr( $link_pinterest ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'link_digg' ); ?>"><?php echo _e( 'Digg Url: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'link_digg' ); ?>" name="<?php echo $this->get_field_name( 'link_digg' ); ?>" value="<?php echo esc_attr( $link_digg ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link_youtube' ); ?>"><?php echo _e( 'Youtube Url: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'link_youtube' ); ?>" name="<?php echo $this->get_field_name( 'link_youtube' ); ?>" value="<?php echo esc_attr( $link_youtube ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'class_custom' ); ?>"><?php echo _e( 'Css Class: ', 'deskpress' ) ?> </label>
			<input type="text" class="widefat" size="3" id="<?php echo $this->get_field_id( 'class_custom' ); ?>" name="<?php echo $this->get_field_name( 'class_custom' ); ?>" value="<?php echo esc_attr( $class_custom ); ?>">
		</p>

	<?php
	}
}

register_widget( 'deskpress_social_links' );

/**
 * DeskPress Login Widget
 *
 * Adds a widget which displays the login form
 *
 * @since DeskPress (v1.0)
 *
 * @uses  BBP_Login_Widget
 */
if ( class_exists( 'BBP_Login_Widget' ) ) {

	class deskpress_Login_Widget extends BBP_Login_Widget {
		public function __construct() {
			$widget_ops = apply_filters( 'deskpress_login_widget_options', array(
				'classname'   => 'deskpress_widget_login',
				'description' => __( 'A fancy login form', 'deskpress' )
			) );

			$this->WP_Widget( "deskpress_Login_Widget", __( '(DeskPress) Login Widget', 'deskpress' ), $widget_ops );
		}

		public static function register_widget() {
			register_widget( 'deskpress_Login_Widget' );
		}

		public function widget( $args = array(), $instance = array() ) {

			// Get widget settings
			$settings = $this->parse_settings( $instance );

			// Typical WordPress filter
			$settings['title'] = apply_filters( 'widget_title', $settings['title'], $instance, $this->id_base );

			// bbPress filters
			$settings['title']    = apply_filters( 'bbp_login_widget_title', $settings['title'], $instance, $this->id_base );
			$settings['register'] = apply_filters( 'bbp_login_widget_register', $settings['register'], $instance, $this->id_base );
			$settings['lostpass'] = apply_filters( 'bbp_login_widget_lostpass', $settings['lostpass'], $instance, $this->id_base );

			echo $args['before_widget'];

			if ( ! $settings['register'] ) {
				$settings['register'] = get_site_url() . '/wp-login.php?action=register';
			}
			if ( get_option( 'users_can_register', '' ) ) {
				if ( ! is_user_logged_in() ) {
					$settings['title'] = sprintf( 'Login <small>or</small> <a href="%s">Register</a>', $settings['register'] );

					echo '<div id="login_form">' . $args['before_title'] . $settings['title'] . $args['after_title'];
				}
			} else {
				if ( ! is_user_logged_in() ) {
					$settings['title'] = __( 'Login', 'deskpress' );

					echo '<div id="login_form">' . $args['before_title'] . $settings['title'] . $args['after_title'];
				}
			}

//		if ( ! empty( $settings['title'] ) ) {
//			echo $args['before_title'] . $settings['title'] . $args['after_title'];
//		}

			if ( ! $settings['lostpass'] ) {
				$settings['lostpass'] = get_site_url() . '/wp-login.php?action=lostpassword';
			}

			if ( ! is_user_logged_in() ) : ?>

				<form method="post" action="<?php bbp_wp_login_action( array( 'context' => 'login_post' ) ); ?>" class="bbp-login-form" role="form">

					<div class="bbp-username form-group">
						<label for="user_login"><?php _e( 'Username', 'bbpress' ); ?>: </label>
						<input type="text" class="form-control" name="log" value="<?php bbp_sanitize_val( 'user_login', 'text' ); ?>" size="20" id="user_login" tabindex="<?php bbp_tab_index(); ?>" />
					</div>

					<div class="bbp-password form-group">
						<label for="user_pass">
							<?php _e( 'Password', 'bbpress' ); ?>:
							<?php if ( get_option( 'users_can_register', '' ) ) { ?>
								&nbsp;(
								<a href="<?php echo esc_url( $settings['lostpass'] ); ?>" title="<?php esc_attr_e( 'Lost Password?', 'bbpress' ); ?>" class="bbp-lostpass-link"><?php _e( 'Lost Password?', 'bbpress' ); ?></a>)
							<?php } ?>
						</label>
						<input type="password" class="form-control" name="pwd" value="<?php bbp_sanitize_val( 'user_pass', 'password' ); ?>" size="20" id="user_pass" tabindex="<?php bbp_tab_index(); ?>" />
					</div>

					<div class="bbp-remember-me checkbox">
						<label for="rememberme">
							<input type="checkbox" name="rememberme" value="forever" <?php checked( bbp_get_sanitize_val( 'rememberme', 'checkbox' ), true, true ); ?> id="rememberme" tabindex="<?php bbp_tab_index(); ?>" />
							<?php _e( 'Remember Me', 'bbpress' ); ?>
						</label>
						<button type="submit" name="user-submit" id="user-submit" tabindex="<?php bbp_tab_index(); ?>" class="button submit user-submit pull-right"><?php _e( 'Log In', 'bbpress' ); ?></button>
						<?php do_action( 'login_form' ); ?>
						<?php bbp_user_login_fields(); ?>
					</div>
				</form>
				<?php echo "</div>"; ?>    <!--end login_form-->
			<?php else : ?>

				<div class="bbp-logged-in">
					<a href="<?php bbp_user_profile_url( bbp_get_current_user_id() ); ?>" class="submit user-submit"><?php echo get_avatar( bbp_get_current_user_id(), '40' ); ?></a>

					<p><?php echo sprintf( __( 'Hi %s', 'deskpress' ), bbp_get_user_nicename( bbp_get_current_user_id() ) ); ?>, <?php bbp_logout_link(); ?></p>
				</div>

			<?php endif;

			echo $args['after_widget'];
		}
	}

	register_widget( 'deskpress_Login_Widget' );
}