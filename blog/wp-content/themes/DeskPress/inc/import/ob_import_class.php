<?php

class ob_wp_import extends WP_Import {
	var $preStringOption;
	var $results;
	var $getOptions;
	var $saveOptions;
	var $termNames;


	function import_woosetting( $file ) {
		if ( ! is_file( $file ) ) {
			return false;
		}
		$woo_datas = file_get_contents( $file );
		$woo_datas = json_decode( $woo_datas, true );
		if ( count( array_filter( $woo_datas ) ) < 1 ) {
			return false;
		}
		$keys = array_keys( $woo_datas );

		foreach ( $keys as $key ) {
			update_option( $key, unserialize( $woo_datas[$key] ) );
		}

		return true;
	}

	/**
	 * @param $option_file File path of restore setting file
	 */
	function saveOptions( $option_file ) {
		if ( ! is_file( $option_file ) ) {
			return false;
		}
		$setting_data = unserialize( base64_decode( file_get_contents( $option_file ) ) );

		$setting_data['page_id'] = get_page_by_title( $setting_data['page_id_show_top_logo'] )->ID;

		update_option( 'theme_mods_deskpress', $setting_data );

		return true;
	}


	/**
	 * @return bool
	 * Function reset menu for theme
	 */
	function set_menus() {
		$deskpress_menus = wp_get_nav_menus();
		global $deskpress_data;
		if ( isset( $deskpress_data['page_on_front'] ) && trim( $deskpress_data['show_on_front'] ) == 'page' ) {
			update_option( 'show_on_front', $deskpress_data['show_on_front'] );
			if ( count( $deskpress_data['page_on_front'] ) > 0 ) {
				$pages = explode( ',', $deskpress_data['page_on_front'] );
				$page  = get_page_by_title( $pages[1] );
				update_option( 'page_on_front', $page->ID );
			}
		}

		//get all registered menu locations
		@$locations = get_theme_mod( 'nav_menu_locations' );
		@$menu_nav_settings = $deskpress_data['reg_nav'];
		if ( $menu_nav_settings ) {
			$menu_nav_settings = explode( "\n", $menu_nav_settings );
			$menu_nav_settings = array_filter( $menu_nav_settings );
			$menu_nav          = array();
			for ( $i = 0; $i < count( $menu_nav_settings ); $i ++ ) {
				$key_nav = explode( ',', $menu_nav_settings[$i] );
				if ( count( $key_nav ) > 1 ) {
					@$menu_nav[trim( strtolower( str_replace( ' ', '_', trim( $key_nav[0] ) ) ) )] = trim( $key_nav[1] );
				}
			}
		}
		//get all created menus


		if ( ! empty( $deskpress_menus ) && ! empty( $menu_nav ) ) {

			foreach ( $deskpress_menus as $deskpress_menu ) {
				//check if we got a menu that corresponds to the Menu name array ($menu_nav) we have set in functions.php
				if ( is_object( $deskpress_menu ) && in_array( $deskpress_menu->name, $menu_nav ) ) {
					$key = array_search( $deskpress_menu->name, $menu_nav );
					//update the theme
					if ( $key ) {
						//if we have found a menu with the correct menu name apply the id to the menu location
						$locations[$key] = $deskpress_menu->term_id;
					}
				}
			}
		} else {
			return false;
		}
		@set_theme_mod( 'nav_menu_locations', $locations );
		if ( class_exists( 'Woocommerce' ) ) {
			// Set pages
			$woopages = array(
				'woocommerce_shop_page_id'            => 'Shop',
				'woocommerce_cart_page_id'            => 'Cart',
				'woocommerce_checkout_page_id'        => 'Checkout',
				'woocommerce_pay_page_id'             => 'Checkout &#8594; Pay',
				'woocommerce_thanks_page_id'          => 'Order Received',
				'woocommerce_myaccount_page_id'       => 'My Account',
				'woocommerce_edit_address_page_id'    => 'Edit My Address',
				'woocommerce_view_order_page_id'      => 'View Order',
				'woocommerce_change_password_page_id' => 'Change Password',
				'woocommerce_logout_page_id'          => 'Logout',
				'woocommerce_lost_password_page_id'   => 'Lost Password'
			);
			foreach ( $woopages as $woo_page_name => $woo_page_title ) {
				$woopage = get_page_by_title( $woo_page_title );
				if ( $woopage->ID ) {
					update_option( $woo_page_name, $woopage->ID ); // Front Page
				}
			}
		}

		return true;
	}

	/**
	 *
	 */
	function import_revslider() {
		@$data_rev = file_get_contents( REV_IMPORT );

		if ( $handle = opendir( OB_INC . "data" . DIRECTORY_SEPARATOR . "revslider" ) ) {
			$check = 0;
			while ( false !== ( $entry = readdir( $handle ) ) ) {
				if ( $entry != "." && $entry != ".." ) {
					$_FILES['import_file']['tmp_name'] = OB_INC . "data" . DIRECTORY_SEPARATOR . "revslider" . DIRECTORY_SEPARATOR . $entry;
					if ( $data_rev == $entry ) {
						$check = 1;
						continue;
					}
					if ( ! $data_rev ) {
						$check = 1;
					}
					if ( class_exists( 'RevSlider' ) ) {
						if ( $check ) {
							$slider   = new RevSlider();
							$response = $slider->importSliderFromPost( true, true );
							file_put_contents( REV_IMPORT, $entry );

							return 1;
						}
					} else {
						return false;
					}
				}
			}
			closedir( $handle );
		} else {
			return false;
		}
		unlink( REV_IMPORT );

		return 2;
	}

	/**
	 * @param $import_array Widget Json
	 *
	 * @return bool
	 */
	function import_widgets( $import_array ) {
		global $wp_registered_sidebars;
		$json_data     = $import_array;
		$json_data     = json_decode( $json_data, true );
		$sidebars_data = $json_data[0];
		$widget_data   = $json_data[1];
		$new_widgets   = array();

		foreach ( $sidebars_data as $import_sidebar => $import_widgets ) :

			foreach ( $import_widgets as $import_widget ) :
				//if the sidebar exists
				if ( isset( $wp_registered_sidebars[$import_sidebar] ) ) :
					$title               = trim( substr( $import_widget, 0, strrpos( $import_widget, '-' ) ) );
					$index               = trim( substr( $import_widget, strrpos( $import_widget, '-' ) + 1 ) );
					$current_widget_data = get_option( 'widget_' . $title );
					$new_widget_name     = $this->new_widget_name( $title, $index );
					$new_index           = trim( substr( $new_widget_name, strrpos( $new_widget_name, '-' ) + 1 ) );

					if ( ! empty( $new_widgets[$title] ) && is_array( $new_widgets[$title] ) ) {
						while ( array_key_exists( $new_index, $new_widgets[$title] ) ) {
							$new_index ++;
						}
					}
					$current_sidebars[$import_sidebar][] = $title . '-' . $new_index;
					if ( array_key_exists( $title, $new_widgets ) ) {
						$new_widgets[$title][$new_index] = $widget_data[$title][$index];
						$multiwidget                     = $new_widgets[$title]['_multiwidget'];
						unset( $new_widgets[$title]['_multiwidget'] );
						$new_widgets[$title]['_multiwidget'] = $multiwidget;
					} else {
						$current_widget_data[$new_index] = $widget_data[$title][$index];
						$current_multiwidget             = $current_widget_data['_multiwidget'];
						$new_multiwidget                 = isset( $widget_data[$title]['_multiwidget'] ) ? $widget_data[$title]['_multiwidget'] : false;
						$multiwidget                     = ( $current_multiwidget != $new_multiwidget ) ? $current_multiwidget : 1;
						unset( $current_widget_data['_multiwidget'] );
						$current_widget_data['_multiwidget'] = $multiwidget;
						$new_widgets[$title]                 = $current_widget_data;
					}

				endif;
			endforeach;
		endforeach;
		if ( isset( $new_widgets ) && isset( $current_sidebars ) ) {
			update_option( 'sidebars_widgets', $current_sidebars );

			foreach ( $new_widgets as $title => $content ) {
				update_option( 'widget_' . $title, $content );
			}

			return true;
		}

		return false;
	}

	/**
	 * @param $widget_name  Name of widget
	 * @param $widget_index Number order in withget
	 *
	 * @return string
	 */
	function new_widget_name( $widget_name, $widget_index ) {
		$current_sidebars = get_option( 'sidebars_widgets' );
		$all_widget_array = array();
		foreach ( $current_sidebars as $sidebar => $widgets ) {
			if ( ! empty( $widgets ) && is_array( $widgets ) && $sidebar != 'wp_inactive_widgets' ) {
				foreach ( $widgets as $widget ) {
					$all_widget_array[] = $widget;
				}
			}
		}
		while ( in_array( $widget_name . '-' . $widget_index, $all_widget_array ) ) {
			$widget_index ++;
		}
		$new_widget_name = $widget_name . '-' . $widget_index;

		return $new_widget_name;
	}
}