<?php
/**
 * Created by PhpStorm.
 * User: Anh Tuan
 * Date: 7/29/14
 * Time: 10:06 AM
 */
global $deskpress_data;
?>
<!-- Brand and toggle get grouped for better mobile display -->

<div class="navbar-header">

	<button type="button" class="navbar-toggle" data-toggle="offcanvas">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>

	<div class="mobile_logo">
		 <h2 class="header_logo">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" rel="home">
			<?php
			if ( $deskpress_data['site_logo'] ) {
				$size      = @getimagesize( $deskpress_data['site_logo'] );
				$logo_size = '';
				if ( !empty( $size ) && isset( $size[3] ) ) {
					$logo_size = $size[3];
				}
				$site_title = esc_attr( get_bloginfo( 'name', 'display' ) );
				echo '<img src="' . $deskpress_data['site_logo'] . '" alt="' . $site_title . '"  ' . $logo_size . '/>';
			} else {
				bloginfo( 'name' );
			}?>
		</a>
		 </h2>
	</div>

	<?php if ( isset( $deskpress_data['show_drawer_right'] ) && $deskpress_data['show_drawer_right'] == '1' && is_active_sidebar( 'drawer_right' ) ) { ?>
		<div class="sliderbar-menu-controller visible-xs menu-controller-mobile">
			<?php
			$icon = '';
			if ( isset( $deskpress_data['icon_drawer_right'] ) ) {
				$icon = 'fa ' . $deskpress_data['icon_drawer_right'];
			}
			?>
			<div>
				<i class="<?php echo $icon; ?>"></i>
			</div>
		</div>
	<?php } ?>

</div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="main_menu">
	<div class="main_menu_container desktop_menu">
		<ul class="nav navbar-nav table_cell">
			<?php
			if ( has_nav_menu( 'primary' ) ) {
				wp_nav_menu( array(
					'theme_location' => 'primary',
					'container'      => false,
					'items_wrap'     => '%3$s',
 				) );
			} else {
				wp_nav_menu( array(
					'theme_location' => '',
					'container'      => false,
					'items_wrap'     => '%3$s',
 				) );
			}

			?>
		</ul>
		<?php if ( is_active_sidebar( 'menu_right' ) || ( isset( $deskpress_data['show_drawer_right'] ) && $deskpress_data['show_drawer_right'] == '1' && is_active_sidebar( 'drawer_right' ) ) ) {
			echo '<ul class="navbar_right table_cell">';
		}

		if ( is_active_sidebar( 'menu_right' ) ) {
			dynamic_sidebar( 'menu_right' );
		}

		if ( isset( $deskpress_data['show_drawer_right'] ) && $deskpress_data['show_drawer_right'] == '1' && is_active_sidebar( 'drawer_right' ) ) {
			?>
			<li class="sliderbar-menu-controller">
				<?php
				$icon = '';
				if ( isset( $deskpress_data['icon_drawer_right'] ) ) {
					$icon = 'fa ' . $deskpress_data['icon_drawer_right'];
				}
				?>
				<div>
					<?php if ( isset( $deskpress_data['icon_drawer_right'] ) && $deskpress_data['icon_drawer_right'] <> '' ) {
						echo '<i class="' . $icon . '"></i> ';
					}?>
					<?php
					if ( !is_user_logged_in() ) {
						if ( isset( $deskpress_data['text_drawer_right'] ) && $deskpress_data['text_drawer_right'] <> '' ) {
							echo '<span>' . $deskpress_data['text_drawer_right'] . '</span>';
						}
					} else {
						if ( isset( $deskpress_data['second_text_drawer_right'] ) && $deskpress_data['second_text_drawer_right'] <> '' ) {
							echo '<span>' . $deskpress_data['second_text_drawer_right'] . '</span>';
						}
					}?>

				</div>
			</li>
		<?php
		}
		if ( is_active_sidebar( 'menu_right' ) || ( isset( $deskpress_data['show_drawer_right'] ) && $deskpress_data['show_drawer_right'] == '1' && is_active_sidebar( 'drawer_right' ) ) ) {
			echo '</ul>';
		}
		?>
	</div>
</div>
