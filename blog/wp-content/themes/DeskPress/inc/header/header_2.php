<?php
/**
 * Created by PhpStorm.
 * User: Tuan
 * Date: 5/12/14
 * Time: 3:19 PM
 */
global $deskpress_data;
?>
<header id="masthead" class="site-header" role="banner">
	<?php
	// widdth top left sidebar
	$width_topsidebar_left = 5;
	if ( isset( $deskpress_data['width_left_top_sidebar'] ) ) {
		$width_topsidebar_left = $deskpress_data['width_left_top_sidebar'];
	}
	$width_topsidebar_right = 12 - $width_topsidebar_left;
	if ( isset( $deskpress_data['topbar_show'] ) && $deskpress_data['topbar_show'] == '1' ) {
		?>
		<?php if ( ( is_active_sidebar( 'top_right_sidebar' ) ) || ( is_active_sidebar( 'top_left_sidebar' ) ) ) : ?>
			<div class="top-header">
				<div class="container">
					<div class="row">
						<?php if ( is_active_sidebar( 'top_left_sidebar' ) ) : ?>
							<div class="col-sm-<?php echo $width_topsidebar_left; ?> top_left">
								<ul class="top-left-menu">
									<?php dynamic_sidebar( 'top_left_sidebar' ); ?>
								</ul>
							</div><!-- col-sm-6 -->
						<?php endif; ?>
						<?php if ( is_active_sidebar( 'top_right_sidebar' ) ) : ?>
							<div class="col-sm-<?php echo $width_topsidebar_right; ?> top_right">
								<ul class="top-right-menu">
									<?php dynamic_sidebar( 'top_right_sidebar' ); ?>
								</ul>
							</div><!-- col-sm-6 -->
						<?php endif; ?>
					</div>
				</div>
			</div><!--End/div.top-->
		<?php
		endif;
	}
	?>
	<div class="navigation affix-top" <?php if ( isset( $deskpress_data['header_sticky'] ) && $deskpress_data['header_sticky'] == 1 ) {
		echo 'data-spy="affix" data-offset-top="' . $deskpress_data['header_height_sticky'] . '" ';
	} ?>>
		<div class="container tm-table">
			<div class="table_cell sm-logo">
				<h2 class="header_logo">
					<a class="no-sticky_logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" rel="home">
						<?php
						if ( $deskpress_data['site_logo'] ) {
							$size      = @getimagesize( $deskpress_data['site_logo'] );
							$logo_size = '';
							if ( !empty( $size ) && isset( $size[3] ) ) {
								$logo_size = $size[3];
							}
							$site_title = esc_attr( get_bloginfo( 'name', 'display' ) );
							echo '<img src="' . $deskpress_data['site_logo'] . '" alt="' . $site_title . '" ' . $logo_size . ' />';
						} else {
							bloginfo( 'name' );
						}?>
					</a>
					<a class="sticky_logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" rel="home">
						<?php
						if ( $deskpress_data['sticky_logo'] ) {
							$size      = @getimagesize( $deskpress_data['sticky_logo'] );
							$logo_size = '';
							if ( !empty( $size ) && isset( $size[3] ) ) {
								$logo_size = $size[3];
							}
							$site_title = esc_attr( get_bloginfo( 'name', 'display' ) );
							echo '<img src="' . $deskpress_data['sticky_logo'] . '" alt="' . $site_title . '" ' . $logo_size . '  />';
						} else {
							$size      = @getimagesize( $deskpress_data['site_logo'] );
							$logo_size = '';
							if ( !empty( $size ) && isset( $size[3] ) ) {
								$logo_size = $size[3];
							}
							$site_title = esc_attr( get_bloginfo( 'name', 'display' ) );
							echo '<img src="' . $deskpress_data['site_logo'] . '" alt="' . $site_title . '" ' . $logo_size . '/>';
						}
						?>
					</a>
				</h2>
			</div>
			<nav class="table_cell table_right" role="navigation">
				<?php get_template_part( 'inc/header/main_menu' ); ?>
			</nav>
		</div>
	</div>
	<?php if ( use_bbpress() || is_home() || is_front_page() || is_post_type_archive( 'tps' ) || ( get_post_type() == "tps" ) ) {
		$class = '';
		if ( use_bbpress() ) {
			$class = " deskpress_forums";
		}
		?>

		<?php if ( is_active_sidebar( 'header' ) ) : ?>
			<div class="header<?php echo $class; ?>">
				<div class="container">
					<?php dynamic_sidebar( 'header' ); ?>
				</div>
			</div><!-- col-sm-6 -->
		<?php endif; ?>
	<?php } ?>

</header>
