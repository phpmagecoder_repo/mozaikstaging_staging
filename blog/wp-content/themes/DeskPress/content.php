<?php
/**
 * @package deskpress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="content-inner">
		<?php
		//global $sidebar_thumb_size;
		//do_action( 'deskpress_entry_top', $sidebar_thumb_size );

		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			?>
			<div class="post-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
					<?php  the_post_thumbnail('large'); ?>
				</a>
			</div>
		<?php
		}

		?>
		<div class="entry-content">
			<header class="entry-header">

				<h4>
					<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>" itemprop="name"><?php the_title(); ?></a>
				</h4>
				<?php deskpress_posted_on(false); ?>
 			</header>
			<!-- .entry-header -->
			<div class="entry-summary" itemprop="description">
				<?php
				global $deskpress_data;
				if ( isset( $deskpress_data['excerpt_length_blog'] ) ) {
					$length = $deskpress_data['excerpt_length_blog'];
				} else {
					$length = '50';
				}
				echo excerpt( $length );
				?>
 			</div>
			<!-- .entry-summary -->
		</div>
	</div>
</article><!-- #post-## -->
