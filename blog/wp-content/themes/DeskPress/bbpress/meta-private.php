<?php
global $post;


?>
<div class="bbp-template-notice not-logged-in">
	<?php if ( is_active_sidebar( 'drawer_right' ) ) { ?>
		<p><?php echo __( 'You must be logged in to reply to this topic. <a href="#login_form" class="form_register">Click here</a> to login or register', 'tps' ) ?></p>
	<?php } else { ?>
		<p><?php echo __( 'You must be logged in to reply to this topic. You can click <a target="_blank" href="' . wp_login_url( get_the_permalink( $post->ID ) ) . '">here</a>', 'tps' ) ?></p>
	<?php } ?>
</div>