<?php

/**
 * Single Forum Content Part
 *
 * @package    bbPress
 * @subpackage Theme
 */

?>
<?php
$check_pur    = false;
if ( is_user_logged_in() ) {
	global $current_user;
	$forum_id     = bbp_get_forum_id();
	$forum_params = get_post_meta( $forum_id, '_tps_params', true );
	$user_metas   = get_user_meta( $current_user->ID, '_purchase_code', true );

	if ( $forum_params ) {
		if ( in_array( 99, $forum_params['envato_items'] ) || current_user_can( 'moderate' ) ) {
			$check_pur = true;
		} else {
			if ( $user_metas ) {
				foreach ( $user_metas as $user_meta ) {
					if ( in_array( $user_meta['item_id'], $forum_params['envato_items'] ) ) {
						$check_pur = true;
						break;
					}
				}
			}
		}
	} else {
		$check_pur = true;
	}
}
?>

<div id="bbpress-forums">
	<?php
	if ( ( $check_pur || bbp_allow_anonymous() ) && ( !bbp_is_forum_category() && bbp_has_topics() ) ) { ?>
		<a class="btn btn-primary btn-create-topic" href="#new-post"><?php _e( 'Create topic', 'tps' ) ?></a>
	<?php } ?>
	<?php bbp_forum_subscription_link(); ?>


	<?php do_action( 'bbp_template_before_single_forum' ); ?>

	<?php if ( post_password_required() ) : ?>

		<?php bbp_get_template_part( 'form', 'protected' ); ?>

	<?php else : ?>

		<?php // bbp_single_forum_description(); ?>

		<?php if ( bbp_has_forums() ) : ?>

			<?php bbp_get_template_part( 'loop', 'forums' ); ?>

		<?php endif; ?>

		<?php if ( !bbp_is_forum_category() && bbp_has_topics() ) : ?>

			<?php //bbp_get_template_part( 'pagination', 'topics' ); ?>

			<?php bbp_get_template_part( 'loop', 'topics' ); ?>

			<?php bbp_get_template_part( 'pagination', 'topics' ); ?>

			<?php

			if ( $check_pur || bbp_allow_anonymous() ) {
				bbp_get_template_part( 'form', 'topic' );
			} else {
				bbp_get_template_part( 'meta', 'purchasedcode' );
			}

			?>

		<?php elseif ( !bbp_is_forum_category() ) : ?>

			<?php bbp_get_template_part( 'feedback', 'no-topics' ); ?>

			<?php
			if ( is_user_logged_in() ) {

				if ( $check_pur || bbp_allow_anonymous() ) {
					bbp_get_template_part( 'form', 'topic' );
				} else {
					bbp_get_template_part( 'meta', 'purchasedcode' );
				}
			}
			?>

		<?php endif; ?>

	<?php endif; ?>

	<?php do_action( 'bbp_template_after_single_forum' ); ?>

</div>
