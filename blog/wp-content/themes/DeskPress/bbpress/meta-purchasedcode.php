<?php
global $post;
$params = get_option( '_tps_params', array() );

$pur_page = get_the_permalink( $params['page'] );

$return_page = get_the_permalink( $post->ID );
?>
<div class="bbp-template-notice not-logged-in">
	<p><?php echo __( 'This forum is for logged in users only. Please go to the ', 'tps' ) ?>
		<a href="<?php echo wp_login_url(); ?>"><?php echo __( 'Login/Register Page.','tps' ) ?></a>
	</p>
</div>