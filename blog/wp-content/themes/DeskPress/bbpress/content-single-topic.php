<?php

/**
 * Single Topic Content Part
 *
 * @package    bbPress
 * @subpackage Theme
 */

?>

<div id="bbpress-forums">

	<?php do_action( 'bbp_template_before_single_topic' ); ?>

	<?php if ( post_password_required() ) : ?>

		<?php bbp_get_template_part( 'form', 'protected' ); ?>

	<?php else : ?>

		<?php bbp_topic_tag_list(); ?>

		<?php // bbp_single_topic_description(); ?>

		<?php if ( bbp_show_lead_topic() ) : ?>

			<?php bbp_get_template_part( 'content', 'single-topic-lead' ); ?>

		<?php endif; ?>

		<?php if ( bbp_has_replies() ) :


			if ( is_user_logged_in() ) {
				global $current_user;
				$forum_id = bbp_get_forum_id();

				$forum_params = get_post_meta( $forum_id, '_tps_params', true );
				$user_metas   = get_user_meta( $current_user->ID, '_purchase_code', true );
				$show_replies = isset( $forum_params['show_replies'] ) ? $forum_params['show_replies'] : 0;
				$check_pur    = false;
				if ( $forum_params ) {
					if ( in_array( 99, $forum_params['envato_items'] ) || current_user_can( 'moderate' ) ) {
						$check_pur = true;
					} else {
						if ( $user_metas ) {
							foreach ( $user_metas as $user_meta ) {
								if ( in_array( $user_meta['item_id'], $forum_params['envato_items'] ) ) {
									$check_pur = true;
									break;
								}
							}
						}
					}
				} else {
					$check_pur = true;
				}
				if ( $check_pur || $show_replies ) {
					bbp_get_template_part( 'pagination', 'replies' );
				}
			} elseif ( bbp_allow_anonymous() ) {
				bbp_get_template_part( 'pagination', 'replies' );
			}
			bbp_get_template_part( 'loop', 'replies' );
			if ( is_user_logged_in() ) {
				if ( $check_pur || $show_replies ) {
					bbp_get_template_part( 'pagination', 'replies' );
				}
			} elseif ( bbp_allow_anonymous() ) {
				bbp_get_template_part( 'pagination', 'replies' );
			} ?>

		<?php endif; ?>

		<?php bbp_get_template_part( 'form', 'reply' ); ?>

	<?php endif; ?>

	<?php do_action( 'bbp_template_after_single_topic' ); ?>

</div>
