<?php

/**
 * Replies Loop
 *
 * @package    bbPress
 * @subpackage Theme
 */

?>

<?php do_action( 'bbp_template_before_replies_loop' ); ?>

<ul id="topic-<?php bbp_topic_id(); ?>-replies" class="forums bbp-replies" itemscope itemtype="http://schema.org/ItemList">

	<li class="bbp-header">

		<div class="bbp-reply-author"><?php _e( 'Author', 'bbpress' ); ?></div>
		<!-- .bbp-reply-author -->

		<div class="bbp-reply-content">

			<?php if ( !bbp_show_lead_topic() ) : ?>

				<?php _e( 'Posts', 'bbpress' ); ?>

				<?php bbp_topic_subscription_link(); ?>

				<?php bbp_user_favorites_link(); ?>

			<?php else : ?>

				<?php _e( 'Replies', 'bbpress' ); ?>

			<?php endif; ?>

		</div>
		<!-- .bbp-reply-content -->

	</li>
	<!-- .bbp-header -->

	<li class="bbp-body">

		<?php if ( bbp_thread_replies() ) : ?>

			<?php bbp_list_replies(); ?>

		<?php else : ?>

			<?php while ( bbp_replies() ) : bbp_the_reply(); ?>

				<?php

				global $current_user;
				$forum_id     = bbp_get_forum_id();
				$topic_id     = bbp_get_topic_id();
				$reply_id     = bbp_get_reply_id();
				$forum_params = get_post_meta( $forum_id, '_tps_params', true );
				$show_replies = isset( $forum_params['show_replies'] ) ? $forum_params['show_replies'] : 0;
				$user_metas   = get_user_meta( $current_user->ID, '_purchase_code', true );
				$check_pur    = false;
				if ( $forum_params ) {
					if ( in_array( 99, $forum_params['envato_items'] ) || current_user_can( 'moderate' ) ) {
						$check_pur = true;
					} else {
						if ( $user_metas ) {
							foreach ( $user_metas as $user_meta ) {
								if ( in_array( $user_meta['item_id'], $forum_params['envato_items'] ) ) {
									$check_pur = true;
									break;
								}
							}
						}
					}
				} else {
					$check_pur = true;
				}

				if ( !is_user_logged_in() ) {

					if ( $reply_id == $topic_id ) {
						bbp_get_template_part( 'loop', 'single-reply' );
					}

					if ( $show_replies ) {
						if ( $reply_id != $topic_id ) {
							bbp_get_template_part( 'loop', 'single-reply' );
						}
					} else {
						break;
					}

					//bbp_get_template_part( 'meta', 'private' );


				} elseif ( !$check_pur ) {

					if ( $reply_id == $topic_id ) {
						bbp_get_template_part( 'loop', 'single-reply' );
					}
					if ( $show_replies ) {
						if ( $reply_id != $topic_id ) {
							bbp_get_template_part( 'loop', 'single-reply' );
						}
					} else {
						break;
					}

					//bp_get_template_part( 'meta', 'purchasedcode' );
//					break;
				} else {
					bbp_get_template_part( 'loop', 'single-reply' );
				}

			endwhile; ?>

		<?php endif; ?>

	</li>
	<!-- .bbp-body -->

	<li class="bbp-footer">

		<div class="bbp-reply-author"><?php _e( 'Author', 'bbpress' ); ?></div>

		<div class="bbp-reply-content">

			<?php if ( !bbp_show_lead_topic() ) : ?>

				<?php _e( 'Posts', 'bbpress' ); ?>

			<?php else : ?>

				<?php _e( 'Replies', 'bbpress' ); ?>

			<?php endif; ?>

		</div>
		<!-- .bbp-reply-content -->

	</li>
	<!-- .bbp-footer -->

</ul><!-- #topic-<?php bbp_topic_id(); ?>-replies -->

<?php do_action( 'bbp_template_after_replies_loop' ); ?>
