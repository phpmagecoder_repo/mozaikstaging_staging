<?php

/**
 * deskpress functions and definitions
 *
 * @package deskpress
 */
define( 'HOME_URL', trailingslashit( home_url() ) );
define( 'THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'THEME_URL', trailingslashit( get_template_directory_uri() ) );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

// Define flag development status
global $deskpress_is_dev;
$deskpress_is_dev = false; // Turn on/off flag status

if ( ! function_exists( 'deskpress_setup' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function deskpress_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on deskpress, use a find and replace
		 * to change 'deskpress' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'deskpress', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Declare WooCommerce support
		add_theme_support( 'woocommerce' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'edd-con', 480, 320, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'deskpress' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		) );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
//		add_theme_support( 'post-formats', array(
//			'aside', 'image', 'video', 'quote', 'link'
//		) );

//		// Setup the WordPress core custom background feature.
//		add_theme_support( 'custom-background', apply_filters( 'deskpress_custom_background_args', array(
//			'default-color' => 'ffffff',
//			'default-image' => '',
//		) ) );
	}

endif; // deskpress_setup
add_action( 'after_setup_theme', 'deskpress_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function deskpress_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar 1', 'deskpress' ),
		'id'            => 'sidebar-1',
		'description'   => 'Left Sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Sidebar 2', 'deskpress' ),
		'id'            => 'sidebar-2',
		'description'   => 'Right Sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => 'Top Drawer',
		'id'            => 'drawer_top',
		'description'   => __( 'Drawer Top', 'deskpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => 'Top Left',
		'id'            => 'top_left_sidebar',
		'description'   => __( 'Top Left', 'fedora' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Top Right',
		'id'            => 'top_right_sidebar',
		'description'   => __( 'Top Right Sidebar', 'fedora' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<div class="title-widget">',
		'after_title'   => '</div>',
	) );

	register_sidebar( array(
		'name'          => 'Header Right',
		'id'            => 'header_right',
		'description'   => __( 'Header Right', 'deskpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => 'Menu Right',
		'id'            => 'menu_right',
		'description'   => __( 'Menu Right', 'deskpress' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Offcanvas Sidebar', 'deskpress' ),
		'id'            => 'drawer_right',
		'description'   => 'Drawer Right',
		'before_widget' => '<aside id="%1$s" class="clearfix widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => 'Jumbotron',
		'id'            => 'header',
		'description'   => __( 'Jumbotron', 'deskpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => 'Knowledge Base',
		'id'            => 'knowledge_base',
		'description'   => __( 'Knowledge Base', 'deskpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s kb_widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => 'Footer 1',
		'id'            => 'footer_1',
		'description'   => __( 'Footer 1 Sidebar', 'deskpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s footer_widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title"><span>',
		'after_title'   => '</span></h4>',
	) );
	register_sidebar( array(
		'name'          => 'Footer 2',
		'id'            => 'footer_2',
		'description'   => __( 'Footer 2 Sidebar', 'deskpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s footer_widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title"><span>',
		'after_title'   => '</span></h4>',
	) );
	register_sidebar( array(
		'name'          => 'Footer 3',
		'id'            => 'footer_3',
		'description'   => __( 'Footer 3 Sidebar', 'deskpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s footer_widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title"><span>',
		'after_title'   => '</span></h4>',
	) );
	register_sidebar( array(
		'name'          => 'Footer 4',
		'id'            => 'footer_4',
		'description'   => __( 'Footer 4 Sidebar', 'deskpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s footer_widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title"><span>',
		'after_title'   => '</span></h4>',
	) );
	register_sidebar( array(
		'name'          => 'Copyright',
		'id'            => 'copyright',
		'description'   => __( 'Copyright Right', 'deskpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s copyright_widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}

add_action( 'widgets_init', 'deskpress_widgets_init' );

/**
 * Add less for developer
 */
function deskpress_add_less_for_dev() {
	global $deskpress_is_dev;
	if ( $deskpress_is_dev ) {
		require_once( THEME_DIR . "css/custom.php" );
		$regex = array(
			"`^([\t\s]+)`ism"                       => '',
			"`^\/\*(.+?)\*\/`ism"                   => "",
			"`([\n\A;]+)\/\*(.+?)\*\/`ism"          => "$1",
			"`([\n\A;\s]+)//(.+?)[\n\r]`ism"        => "$1\n",
			"`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism" => "\n"
		);
		$css   = customcss();
		$css   = preg_replace( array_keys( $regex ), $regex, $css );
		echo '<link rel="stylesheet/less" type="text/css" href="' . get_template_directory_uri() . '/style.less" />';
		echo '<style type="text/less">' . $css . '</style>';
		echo '<script src="' . get_template_directory_uri() . '/js/less-1.7.5.min.js"></script>';
	}
}

add_action( 'wp_head', 'deskpress_add_less_for_dev' );

/**
 * Function add Theme Options
 */
function deskpress_admin_bar_render() {
	global $wp_admin_bar;
	$wp_admin_bar->add_menu( array(
		'parent' => 'site-name', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'smof_options', // link ID, defaults to a sanitized title value
		'title'  => __( 'Theme Options', 'deskpress' ), // link title
		'href'   => admin_url( 'themes.php?page=optionsframework' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );
}

add_action( 'wp_before_admin_bar_render', 'deskpress_admin_bar_render' );

/*     * * Theme options. ** */
include THEME_DIR . 'admin/index.php';

/* * * Custom functions. ** */
require THEME_DIR . 'inc/custom-functions.php';

/* * * Tax meta class. ** */
/* * * Theme wrapper. ** */
include THEME_DIR . 'inc/theme-wrapper.php';

/* * * Add Shortcodes ** */
require_once( get_template_directory() . '/inc/fitwp-shortcodes/fitwp-shortcodes.php' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Plugin.
 */
require get_template_directory() . '/inc/class-tgm-plugin/class-tgm-plugin-config.php';

// Shortcode
//require get_template_directory() . '/inc/shortcodes/shortcodes.php';

// megamenu
require get_template_directory() . '/inc/megamenu/class-megamenu.php';

// widgets
require get_template_directory() . '/inc/widgets/widgets.php';

require get_template_directory() . '/woocommerce/woocommerce.php';
/************override_woocommerce_widgets*************** */

add_action( 'widgets_init', 'override_woocommerce_widgets', 15 );

function override_woocommerce_widgets() {
	if ( class_exists( 'WC_Widget_Cart' ) ) {
		unregister_widget( 'WC_Widget_Cart' );
		include_once( 'woocommerce/widgets/class-wc-widget-cart.php' );
		register_widget( 'Custom_WC_Widget_Cart' );
	}
}

function use_bbpress() {
	if ( in_array( 'bbpress/bbpress.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		return is_bbpress();
	} else {
		return false;
	}
}

function deskpress_edd_cart_image( $item, $id ) {
	add_image_size( 'thumbnail_cart', 90, 90, true );
	$image = get_the_post_thumbnail( $id, 'thumbnail_cart' );
	$link  = get_the_permalink( $id );
	$item  = str_replace( '{item_link}', $link, $item );
	$item  = str_replace( '{item_image}', $image, $item );

	return $item;
}

add_filter( 'edd_cart_item', 'deskpress_edd_cart_image', 10, 2 );