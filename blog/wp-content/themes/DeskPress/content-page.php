<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package deskpress
 */
?>
<?php

	if(use_bbpress()){
		bbp_breadcrumb();
		echo '<div class="clear"></div>';
	}else{
		$hide_breadcrumbs = '';
		if ( isset( $deskpress_data['opt_hide_breadcrumbs'] ) && $deskpress_data['opt_hide_breadcrumbs'] != '0' ) {
			$hide_breadcrumbs = $deskpress_data['opt_hide_breadcrumbs'];
		}

		if ( $hide_breadcrumbs != '1' ) {
			deskpress_breadcrumbs();
		}
	}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

 	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header>
	<!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'deskpress' ),
			'after'  => '</div>',
		) );
		?>
	</div>
	<!-- .entry-content -->
	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'deskpress' ), '<span class="edit-link">', '</span>' ); ?>
	</footer>
	<!-- .entry-footer -->
</article><!-- #post-## -->
