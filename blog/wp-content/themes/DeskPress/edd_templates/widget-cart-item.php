<li class="edd-cart-item">

	<a href="{item_link}">
		{item_image}{item_title}
	</a>
	<span class="quantity"><span class="amount">{item_amount}</span></span>
	<a href="{remove_url}" data-cart-item="{cart_item_id}" data-download-id="{item_id}" data-action="edd_remove_from_cart" class="edd-remove-from-cart">
		<i class="fa fa-times-circle"></i>
	</a>
</li>

