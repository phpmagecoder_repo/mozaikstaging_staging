<?php
/**
 * @package deskpress
 */
?>
	<ul class="edd_download">
		<?php
		while ( have_posts() ) : the_post(); ?>
			<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="item-download">
					<?php

					?>
					<div class="post-thumbnail">
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
							<?php
							if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
								the_post_thumbnail( array( 260, 300 ) );
							} else {
								?>
								<img src="<?php echo get_template_directory_uri(); ?>/images/default_image.jpg" width="260" height="300" />
							<?php
							}
							?>
						</a>
					</div>
					<div class="edd-buttons">
						<?php if ( edd_has_variable_prices( $post->ID ) ) { ?>
							<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>" class="read_more"><i class="fa fa-search"></i> <?php echo _e( 'View', 'deskpress' ); ?>
							</a>
						<?php
						} else {
							edd_get_template_part( 'form', 'add-to-cart' );
						} ?>
					</div>
				</div>
				<h3 class="edd-name">
					<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>" itemprop="name"><?php the_title(); ?></a>
				</h3>
				<?php
				$id = $post->ID;
				if ( edd_has_variable_prices( $post->ID ) ) {

					$lowest_price  = edd_get_lowest_price_option( $id );
					$highest_price = edd_get_highest_price_option( $id );
					$price         = edd_currency_filter( edd_format_amount( $lowest_price ) ) . " - " . edd_currency_filter( edd_format_amount( $highest_price ) );

				} else {
					$price = edd_get_cart_item_price( $post->ID, array() );
					$price = edd_currency_filter( edd_format_amount( $price ) );

				}
				echo '<p class="price">' . $price . '</p>';
				?>
			</li>
		<?php
		endwhile;
		?>
	</ul>
<?php
deskpress_paging_nav();
//echo '<div class="blog_btn_load_more" style="text-align:center;"><a href="#" data-size="one_sidebar" data-type="masonry" data-cat="'.$cat.'" data-offset="'.get_option( 'posts_per_page' ).'" class="sc-btn big light">Load More</a></div>';
?>