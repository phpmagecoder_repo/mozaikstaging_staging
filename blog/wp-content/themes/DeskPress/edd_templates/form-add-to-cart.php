<?php
$id = $post->ID;
?>
<form method="post" class="edd_download_purchase_form" id="edd_purchase_<?php echo $id ?>">
	<div class="edd_purchase_submit_wrapper">
		<a data-price-mode="single" data-variable-price="no" data-download-id="<?php echo $id ?>" data-action="edd_add_to_cart" class="edd-add-to-cart button blue edd-submit edd-has-js" href="#"><span class="edd-add-to-cart-label"><?php echo __( 'Purchase', 'deskpress' ) ?></span>
			<span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
		<input type="submit" data-price-mode="single" data-variable-price="no" data-download-id="<?php echo $id ?>" data-action="edd_add_to_cart" value="Purchase" name="edd_purchase_download" class="edd-add-to-cart edd-no-js button blue edd-submit" style="display: none;">
		<a style="display:none;" class="edd_go_to_checkout button blue edd-submit" href="<?php echo edd_get_checkout_uri() ?>"><?php echo __( 'Checkout', 'deskpress' ) ?></a>
		<span class="edd-cart-ajax-alert">
			<span style="display: none;" class="edd-cart-added-alert">
				<i class="edd-icon-ok"></i> <?php echo __( 'Added to cart', 'deskpress' ) ?>
			</span>
		</span>
	</div>
	<!--end .edd_purchase_submit_wrapper-->

	<input type="hidden" value="<?php echo $id ?>" name="download_id">
	<input type="hidden" value="add_to_cart" class="edd_action_input" name="edd_action">
</form>