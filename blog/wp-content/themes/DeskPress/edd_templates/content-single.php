<?php
/**
 * @package deskpress
 */
?>
<div class="page-content">
	<?php
	while ( have_posts() ) : the_post(); ?>
		<?php
		$hide_breadcrumbs = '';
		if ( isset( $deskpress_data['opt_hide_breadcrumbs'] ) && $deskpress_data['opt_hide_breadcrumbs'] != '0' ) {
			$hide_breadcrumbs = $deskpress_data['opt_hide_breadcrumbs'];
		}

		if ( $hide_breadcrumbs != '1' ) {
			deskpress_breadcrumbs();
		}
		?>
		<div class="edd_content woocommerce">
 		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="page-content-inner product" itemscope itemtype="http://schema.org/Article">
				<div class="images">
					<?php
					if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						the_post_thumbnail( array( 460, 460 ) );
					}else{?>
						<img src="<?php echo get_template_directory_uri();?>/images/default_image.jpg" width="460" height="460"/>
					<?php }
					?>
 				</div>
				<div class="summary entry-summary">
 					<h1 itemprop="name" class="product_title entry-title"><?php the_title();?></h1>
  					<?php the_content(); ?>
 					<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'deskpress' ),
						'after'  => '</div>',
					) );
					?>
					<?php
					/* translators: used between list items, there is a space after the comma */
					$tag_list = get_the_tag_list( '<footer class="entry-footer"><i class="fa fa-tags"></i>', __( ', ', 'deskpress' ), '</footer>' );
					echo $tag_list;
					?>
				</div>
 			</div>
		</article><!-- #post-## -->
		</div>
	<?php endwhile; // end of the loop. ?>
</div>