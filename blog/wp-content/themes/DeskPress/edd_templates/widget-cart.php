<?php
$cart_items = edd_get_cart_contents();
$cart_quantity = edd_get_cart_quantity();
$display = $cart_quantity > 0 ? '' : 'style="display:none;"';
?>
<div class="widget_download_cart">
	<div id="header-mini-cart" class="minicart_hover">
		<div class="main-header-cart">
			<a href="#">
				<span class="icon_cart"></span>
				<!--<span id="cart-items-number">--><?php //echo $cart_quantity; ?><!--</span>-->
			</a>
		</div>
		<div class="widget_download_cart_content">
			<h4 class="recenty_item"><?php echo __( 'RECENTLY ADDED ITEM(S)', 'deskpress' ) ?></h4>
			<ul class="edd-cart">
				<?php if ( $cart_items ) : ?>

					<?php foreach ( $cart_items as $key => $item ) : ?>

						<?php echo edd_get_cart_item_template( $key, $item, false ); ?>

					<?php endforeach; ?>

				<?php else : ?>

					<?php edd_get_template_part( 'widget', 'cart-empty' ); ?>

				<?php endif; ?>
			</ul>
			<?php if ( $cart_items ) { ?>
				<?php edd_get_template_part( 'widget', 'cart-checkout' ); ?>
			<?php } ?>
		</div>
	</div>
</div>