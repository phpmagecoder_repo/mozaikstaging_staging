/* exec function when document ready */

/* utility functions*/
(function ($) {
	"use strict";
 	$.avia_utilities = $.avia_utilities || {};
	$.avia_utilities.supported = {};
	$.avia_utilities.supports = (function () {
		var div = document.createElement('div'),
			vendors = ['Khtml', 'Ms', 'Moz', 'Webkit', 'O'];  // vendors   = ['Khtml', 'Ms','Moz','Webkit','O'];  exclude opera for the moment. stil to buggy
		return function (prop, vendor_overwrite) {
			if (div.style.prop !== undefined) {
				return "";
			}
			if (vendor_overwrite !== undefined) {
				vendors = vendor_overwrite;
			}
			prop = prop.replace(/^[a-z]/, function (val) {
				return val.toUpperCase();
			});

			var len = vendors.length;
			while (len--) {
				if (div.style[vendors[len] + prop] !== undefined) {
					return "-" + vendors[len].toLowerCase() + "-";
				}
			}
			return false;
		};
	}());
	/* ****** jp-jplayer  ******/
	jQuery(function ($) {
		$('.jp-jplayer').each(function () {
			var $this = $(this),
				url = $this.data('audio'),
				type = url.substr(url.lastIndexOf('.') + 1),
				player = '#' + $this.data('player'),
				audio = {};
			audio[type] = url;

			$this.jPlayer({
				ready              : function () {
					$this.jPlayer('setMedia', audio);
				},
				swfPath            : 'jplayer/',
				cssSelectorAncestor: player
			});
		});
	});
	jQuery(document).ready(function () {
		scrollToTop();
		miniCartHover();
		//checkMenuBottom();
		deskpressLoginSocialPopup();
		deskpressLoginWidget();

		/*Collapse*/
		jQuery('.drawer_link').click(function () {
			jQuery('#collapseDrawer').on('hide.bs.collapse', function () {
				setCookie('topdrawer', 1, 1);
			});
			jQuery('#collapseDrawer').on('show.bs.collapse', function () {
				setCookie('topdrawer', 2, 1);
			});
		})
	});
	jQuery(window).load(function () {
		var check_topDrawer = getCookie('topdrawer');
		if (check_topDrawer == 1) {
			jQuery('#collapseDrawer').height(0);
		} else if (check_topDrawer == 2) {
			jQuery('#collapseDrawer').collapse('show');
		}

	});

	function setCookie(c_name, value, exdays) {
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value = escape(value) + "; path=/" + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
		document.cookie = c_name + "=" + c_value;
	}

	function getCookie(c_name) {
		var c_value = document.cookie;
		var c_start = c_value.indexOf(" " + c_name + "=");
		if (c_start == -1) {
			c_start = c_value.indexOf(c_name + "=");
		}
		if (c_start == -1) {
			c_value = null;
		}
		else {
			c_start = c_value.indexOf("=", c_start) + 1;
			var c_end = c_value.indexOf(";", c_start);
			if (c_end == -1) {
				c_end = c_value.length;
			}
			c_value = unescape(c_value.substring(c_start, c_end));
		}
		return c_value;
	}

	/* ****** hasTooltip  ******/
	jQuery(function ($) {
		$('.hasTooltip').tooltip('hide');
	});

	/* ****** PRODUCT QUICK VIEW  ******/
	jQuery(function ($) {
 		$(window).scroll(function () {
			if ($(window).scrollTop() != 0) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});
		$('#back-top').click(function () {
			$('html,body').animate({scrollTop: 0}, 500);
		});
	});

	var generateCarousel = function () {
		if (jQuery().carouFredSel) {
			jQuery(function ($) {
				jQuery('.products-slider').each(function () {
					var carousel = jQuery(this).find('ul');
					carousel.carouFredSel({
						auto      : false,
						prev      : jQuery(this).find('.es-nav-prev'),
						next      : jQuery(this).find('.es-nav-next'),
						align     : "left",
						left      : 0,
						width     : '100%',
						height    : 'variable',
						responsive: true,
						scroll    : {
							items: 1
						},
						items     : {
							width  : '245',
							height : 'variable',
							visible: {
								min: 1,
								max: 30
							}
						}
					});
				});
			});


		}
	};

	jQuery(window).load(function () {
		generateCarousel();
	});

	jQuery(function ($) {
		if (jQuery().flexslider) {
			$('.post-formats-wrapper .flexslider').flexslider({
				animation : "slide",
				prevText  : "<i class='fa fa-angle-left'></i>",
				nextText  : "<i class='fa fa-angle-right'></i>",
				controlNav: false
			});
		}
	});

//Scroll To top
	var scrollToTop = function () {
		jQuery(window).scroll(function () {
			if (jQuery(this).scrollTop() > 100) {
				jQuery('#topcontrol').css({bottom: "15px"});
			} else {
				jQuery('#topcontrol').css({bottom: "-100px"});
			}
		});
		jQuery('#topcontrol').click(function () {
			jQuery('html, body').animate({scrollTop: '0px'}, 800);
			return false;
		});
	}

	/* Process show popup cart when hover cart info */
	var miniCartHover = function () {
		jQuery(document).on('mouseover', '.minicart_hover', function () {
			jQuery(this).children('.widget_shopping_cart_content,.widget_download_cart_content').slideDown();
		}).on('mouseleave', '.minicart_hover', function () {
			jQuery(this).children('.widget_shopping_cart_content,.widget_download_cart_content').delay(100).slideUp();
		});

		jQuery(document)
			.on('mouseenter', '.widget_shopping_cart_content,.widget_download_cart_content', function () {
				jQuery(this).stop(true, true).show()
			})
			.on('mouseleave', '.widget_shopping_cart_content,.widget_download_cart_content', function () {
				jQuery(this).delay(100).slideUp()
			});
	}

// Parallax background
	jQuery(function ($) {
		$('.parallax_effect').each(function () {
			var $bgobj = $(this); // assigning the object

			$(window).scroll(function () {
				var yPos = -($(window).scrollTop() / 4);
				var coords = '50%' + (yPos + 0) + 'px';
				$bgobj.css({backgroundPosition: coords});
			}); // window scroll Ends
		});
	});

	jQuery('#page').click(function () {
		jQuery('.slider_sidebar').removeClass('opened');
		jQuery('.menu-mobile').removeClass('opened');
		jQuery('html,body').removeClass('menu-opened');
		jQuery('html,body').removeClass('slider-bar-opened');
	});
	jQuery(document).keyup(function (e) {
		if (e.keyCode === 27) {
			jQuery('.slider_sidebar').removeClass('opened');
			jQuery('html,body').removeClass('menu-opened');
			jQuery('html,body').removeClass('slider-bar-opened');
		}
		;
	});

	jQuery('[data-toggle=offcanvas]').click(function (e) {
		e.stopPropagation();
		jQuery('.menu-mobile').toggleClass('opened');
		jQuery('html,body').toggleClass('menu-opened');
	});

	/********************************
	 Menu Sidebar
	 ********************************/
	jQuery('.sliderbar-menu-controller').click(function (e) {
		e.stopPropagation();
		jQuery('.slider_sidebar').toggleClass('opened');
		jQuery('html,body').toggleClass('slider-bar-opened');
	});
 	/* Social login popup */
	var deskpressLoginSocialPopup = function () {
		jQuery('.deskpress-link-login a').click(function (event) {
			var popupWrapper = '#deskpress-popup-login-wrapper';
			jQuery.ajax({
				type   : 'POST',
				data   : 'action=deskpress_social_login',
				url    : ob_ajax_url,
				success: function (html) {
					if (jQuery(popupWrapper).length) {
						jQuery(popupWrapper).remove();
					}
					jQuery('body').append(html);
					jQuery('ul.the_champ_login_ul li i', popupWrapper).show();
					jQuery('.deskpress-popup-login-close', popupWrapper).click(function () {
						jQuery(this).parent().parent().parent().parent().remove();
					});

					jQuery('#deskpress-popup-login-form').submit(function (event) {
						var input_data = jQuery('#deskpress-popup-login-form').serialize();

						jQuery.ajax({
							type   : 'POST',
							data   : input_data,
							url    : ob_ajax_url,
							success: function (html) {
								var response_data = jQuery.parseJSON(html);
								jQuery('.login-message', '#deskpress-popup-login-form').html(response_data.message);
							},
							error  : function (html) {
							}
						});
						event.preventDefault();
						return false;
					});
				},
				error  : function (html) {
				}
			});
			event.preventDefault();
		});
	}

	/* DeskPress Login Widget*/
	var deskpressLoginWidget = function () {
		jQuery('.deskpress-login-widget-form').each(function () {
			jQuery(this).submit(function (event) {
				if (this.checkValidity()) {
					var $form = jQuery(this);
					var input_data = jQuery($form).serialize();
					jQuery.ajax({
						type   : 'POST',
						data   : input_data,
						url    : ob_ajax_url,
						success: function (html) {
							var response_data = jQuery.parseJSON(html);
							jQuery('.deskpress-login-widget-message', $form).html(response_data.message);
						},
						error  : function (html) {
						}
					});
				}
				event.preventDefault();
				return false;
			});
		});
	}
})(jQuery);