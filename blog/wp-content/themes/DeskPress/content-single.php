<?php
/**
 * @package deskpress
 */
?>
<?php
$hide_breadcrumbs = '';
if ( isset( $deskpress_data['opt_hide_breadcrumbs'] ) && $deskpress_data['opt_hide_breadcrumbs'] != '0' ) {
	$hide_breadcrumbs = $deskpress_data['opt_hide_breadcrumbs'];
}

if ( $hide_breadcrumbs != '1' ) {
	deskpress_breadcrumbs();
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="page-content-inner" itemscope itemtype="http://schema.org/Article">
		<header class="entry-header">

			<?php the_title( '<h1 class="entry-title" itemprop="name">', '</h1>' ); ?>
			<?php deskpress_posted_on( true ); ?>
		</header>
		<?php if ( has_post_thumbnail() ) {
			// the_post_thumbnail();
		}
		?>
		<!-- .entry-header -->
		<div class="entry-content" itemprop="articleBody">
			<?php the_content(); ?>
			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'deskpress' ),
				'after'  => '</div>',
			) );
			?>
		</div>
		<?php
		/* translators: used between list items, there is a space after the comma */
		$tag_list = get_the_tag_list( '<footer class="entry-footer"><i class="fa fa-tags"></i>', __( ', ', 'deskpress' ), '</footer>' );
		echo $tag_list;
		?>
		<?php /* <section class="about-author">
			<h3 class="tm-title"><?php echo _e( 'about the author: ', 'deskpress' ) ?>
				<?php the_author_posts_link(); ?>
			</h3>

			<div class="about-author-conteainer">
				<div class="avatar">
					<?php echo get_avatar( get_the_author_meta( 'user_email' ), '90', '' ); ?>
				</div>
				<div class="description">
					<p>
						<?php the_author_meta( 'description' ); ?>
					</p>
				</div>
			</div>
		</section> */ ?>
	</div>
</article><!-- #post-## -->
