<div class="wrap" id="of_container">

    <div id="of-popup-save" class="of-save-popup">
        <div class="of-save-save"><?php echo _e( 'Options Updated', 'deskpress' ) ?></div>
    </div>

    <div id="of-popup-generate-less" class="of-generate-less-popup">
        <div class="of-generate-less"><?php echo _e( 'CSS Generated', 'deskpress' ) ?></div>
    </div>

    <div id="of-popup-reset" class="of-save-popup">
        <div class="of-save-reset"><?php echo _e( 'Options Reset', 'deskpress' ) ?></div>
    </div>

    <div id="of-popup-fail" class="of-save-popup">
        <div class="of-save-fail"><?php echo _e( 'Error', 'deskpress' ) ?>!</div>
    </div>

    <span style="display: none;" id="hooks"><?php echo json_encode( of_get_header_classes_array() ); ?></span>
    <input type="hidden" id="reset" value="<?php
    if ( isset( $_REQUEST['reset'] ) ) {
        echo $_REQUEST['reset'];
    }
    ?>" />
    <input type="hidden" id="security" name="security" value="<?php echo wp_create_nonce( 'of_ajax_nonce' ); ?>" />
    <input type="hidden" id="generatelesscss" value="<?php
    if ( isset( $_REQUEST['generatelesscss'] ) ) {
        echo $_REQUEST['generatelesscss'];
    }
    ?>" />

    <form id="of_form" method="post" action="<?php echo esc_attr( $_SERVER['REQUEST_URI'] ) ?>" enctype="multipart/form-data">
        <h2>
            <?php echo THEMENAME; ?>
            <small>
                <small><?php echo( 'v' . THEMEVERSION ); ?></small>
            </small>
        </h2>

        <div id="js-warning">Warning- This options panel will not work properly without javascript!</div>
        <div class="icon-option"></div>
        <div class="clear"></div>
        <div id="info_bar">
            <div class="info_bar_links">
                <a href="http://obtheme.com/docs/" target="_blank"><?php echo _e( 'Documentation', 'deskpress' ); ?></a>&nbsp;|&nbsp;
                <a href="http://obtheme.com/envato/support/" target="_blank"><?php echo _e( 'Support', 'deskpress' ); ?></a>
            </div>
            <div class="info_bar_buttons">
                
                <img style="display:none" src="<?php echo ADMIN_DIR; ?>assets/images/loading-bottom.gif" class="ajax-loading-img ajax-loading-img-bottom" alt="Working..." />
                <!--<button id="of_generate_less" type="button" class="button-primary submit-button "><?php _e( 'Generate Less to CSS', 'deskpress' ); ?></button>
                &nbsp;
                -->
                <button id="of_save" type="button" class="button-primary">
                    <?php _e( 'Save All Changes', 'deskpress' ); ?>
                </button>
            </div>
        </div>
        <!--.info_bar-->

        <div id="main">

            <div id="of-nav">
                <ul>
                    <?php echo $options_machine->Menu ?>
                </ul>
            </div>

            <div id="content">
                <?php echo $options_machine->Inputs /* Settings */ ?>
            </div>

            <div class="clear"></div>

        </div>

        <div class="save_bar">
            <div class="info_bar_links">
                <button id="of_reset" type="button" class="button submit-button reset-button"><?php _e( 'Options Reset', 'deskpress' ); ?></button>
                <img style="display:none" src="<?php echo ADMIN_DIR; ?>assets/images/loading-bottom.gif" class="ajax-reset-loading-img ajax-loading-img-bottom" alt="Working..." />
            </div>
            <div class="info_bar_buttons">
                <!--
                <img style="display:none" src="<?php echo ADMIN_DIR; ?>assets/images/loading-bottom.gif" class="ajax-generate-less-loading-img ajax-loading-img-bottom" alt="Working..." />
                <button id="of_generate_less" type="button" class="button-primary submit-button "><?php _e( 'Generate Less to CSS', 'deskpress' ); ?></button>
                -->
                <img style="display:none" src="<?php echo ADMIN_DIR; ?>assets/images/loading-bottom.gif" class="ajax-loading-img ajax-loading-img-bottom" alt="Working..." />
                <button id="of_save" type="button" class="button-primary"><?php _e( 'Save All Changes', 'deskpress' ); ?></button>
            </div>
        </div>
        <!--.save_bar-->

    </form>

    <div style="clear:both;"></div>

</div><!--wrap-->