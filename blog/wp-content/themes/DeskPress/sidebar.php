<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package deskpress
 */
?>
<div id="secondary" class="widget-sidebar-area col-sm-3" role="complementary">
	<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) :
		dynamic_sidebar( 'sidebar-1' );
	endif; // end sidebar widget area
	?>
</div><!-- #secondary -->
