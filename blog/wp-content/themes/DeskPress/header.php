<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package deskpress
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php
/**
 * @author      MagePsycho <info@magepsycho.com>
 * @website     http://www.magepsycho.com
 * @category    using Header / Footer outside of Magento
 */

$mageFilename = "/var/www/vhosts/creativeecommerce.com/mozaikstaging/app/Mage.php";
require_once $mageFilename;

umask(0);
Mage::init();
Mage::getSingleton('core/session', array('name'=>'frontend'));
 
$block = Mage::getSingleton('core/layout');
 
# HEAD BLOCK
$headBlock  = $block->createBlock('page/html_head')->setTemplate('page/html/head_wp.phtml');// this wont give you the css/js inclusion

# add infortis js
$headBlock->addJs('prototype/prototype.js');
$headBlock->addJs('lib/ccard.js');
$headBlock->addJs('prototype/validation.js');
$headBlock->addJs('scriptaculous/builder.js');
$headBlock->addJs('scriptaculous/effects.js');
$headBlock->addJs('scriptaculous/dragdrop.js');
$headBlock->addJs('scriptaculous/controls.js');
$headBlock->addJs('scriptaculous/slider.js');
$headBlock->addJs('varien/js.js');
$headBlock->addJs('varien/form.js');
$headBlock->addJs('varien/menu.js');
$headBlock->addJs('mage/translate.js');
$headBlock->addJs('mage/cookies.js');

# add general js
//$headBlock->addJs('infortis/jquery/jquery-1.7.2.min.js');
//$headBlock->addJs('infortis/jquery/jquery-noconflict.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.easing.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.tabs.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.accordion.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.ba-throttle-debounce.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.owlcarousel.min.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.cookie.js');
//$headBlock->addJs('infortis/jquery/plugins/jquery.reveal.js');
                        
# add css
$headBlock->addCss('css/styles.css');
$headBlock->addCss('css/styles-infortis.css');  
$headBlock->addCss('css/infortis/_shared/generic-cck.css');
$headBlock->addCss('css/infortis/_shared/accordion.css');
$headBlock->addCss('css/infortis/_shared/dropdown.css');
$headBlock->addCss('css/infortis/_shared/itemslider.css');
$headBlock->addCss('css/infortis/_shared/itemslider-old.css');
$headBlock->addCss('css/infortis/_shared/generic-nav.css');
$headBlock->addCss('css/infortis/_shared/icons.css');
$headBlock->addCss('css/infortis/_shared/itemgrid.css');
$headBlock->addCss('css/infortis/_shared/tabs.css');

$headBlock->addCss('css/infortis/ultra-megamenu/ultra-megamenu.css'); 
$headBlock->addCss('css/infortis/ultra-megamenu/wide.css');



$headBlock->addCss('css/icons-theme.css');
$headBlock->addCss('css/icons-social.css');
$headBlock->addCss('css/common.css');
$headBlock->addCss('css/_config/design_default.css');
$headBlock->addCss('css/override-components.css');
$headBlock->addCss('css/override-modules.css');
$headBlock->addCss('css/override-theme.css');
$headBlock->addCss('css/infortis/_shared/grid12.css');
$headBlock->addCss('css/_config/grid_default.css');
$headBlock->addCss('css/_config/layout_default.css');
$headBlock->addCss('css/reveal.css');
$headBlock->addCss('css/custom1.css');
      
$headBlock->getCssJsHtml();
$headBlock->getIncludes();
 
# HEADER BLOCK
 $themename =  Mage::getSingleton('core/design_package')->getTheme('frontend');
	if($themename =='child'){
		$headerBlock = $block->createBlock('page/html_header')->setTemplate('page/html/cusheader.phtml');
	}
	else{
		$headerBlock = $block->createBlock('page/html_header')->setTemplate('page/html/header.phtml');
	}

$linksBlock = $block->createBlock('cms/block')->setBlockId('block_header_top_left');
$headerBlock->setChild('block_header_top_left',$linksBlock);

$linksBlock1 = $block->createBlock('cms/block')->setBlockId('block_header_top_right');
$headerBlock->setChild('block_header_top_right',$linksBlock1);

$currencyBlock = $block->createBlock('directory/currency')->setTemplate('directory/currency.phtml');
$headerBlock->setChild('currency',$currencyBlock);


$cartBlock = $block->createBlock('cms/block')->setBlockId('external_cart_block');   // My custom cart Block
$headerBlock->setChild('cart_sidebar',$cartBlock);   

$searchBlock =  $block->createBlock('core/template')->setTemplate('catalogsearch/form.mini.phtml');
$headerBlock->setChild('topSearch',$searchBlock);

$navBlock = $block->createBlock('ultramegamenu/navigation')->setTemplate('infortis/ultramegamenu/mainmenu.phtml');
$headerBlock->setChild('topMenu',$navBlock);

$navBlockChild1 = $block->createBlock('cms/block')->setBlockId('block_header_nav_dropdown');
$navBlock->setChild('block_header_nav_dropdown',$navBlockChild1);

$navBlockChild2 = $block->createBlock('cms/block')->setBlockId('block_nav_links');
$navBlock->setChild('block_nav_links',$navBlockChild2);

$headerBlock = $headerBlock->toHtml();   
 
?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php    global $deskpress_data;
	$class_header = '';
	if ( isset( $deskpress_data['header_style'] ) ) {
		$class_header = $deskpress_data['header_style'];
	}
	?>
	<link rel="shortcut icon" href="<?php
	if ( isset( $deskpress_data['favicon'] ) ) {
		echo $deskpress_data['favicon'];
	} else {
		echo get_template_directory_uri() . "/images/favicon.ico";
	}
	?>" type="image/x-icon" />

	 <!--------------------------- MAGENTO HEAD START -------------------------------->
	<?php echo $headBlock->toHtml(); ?>
	<!--------------------------- MAGENTO HEAD END -------------------------------->
	<!--------------------------- WP HEAD START -------------------------------->
	<?php wp_head(); ?>
	<!--------------------------- WP HEAD END -------------------------------->

	 <script type="text/javascript" src="/js/mirasvit/core/jquery.min.js"></script>
	 <script type="text/javascript" src="/js/mirasvit/core/underscore.js"></script>
	 <script type="text/javascript" src="/js/mirasvit/core/backbone.js"></script>
	 <script type="text/javascript" src="/js/mirasvit/code/searchautocomplete/form.js"></script>
	 <script type="text/javascript" src="/js/mirasvit/code/searchautocomplete/autocomplete.js"></script>
	 
	 <script type="text/javascript" src="http://www.housingcamera.com/js/infortis/jquery/plugins/jquery.cookie.js"></script>
<script type="text/javascript" src="http://www.housingcamera.com/js/infortis/jquery/plugins/jquery.easing.min.js"></script>

	<!--------------------------- START GANALYTICS -------------------------------->
        <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-2273859-1', 'housingcamera.com');
	  ga('send', 'pageview');

	</script>
        <!--------------------------- END GANALYTICS -------------------------------->
        <!--------------------------- START METRILO -------------------------------->
<script type="text/javascript">
window.metrilo||(window.metrilo=[]),window.metrilo.queue=[],window.metrilo.methods=["identify","track","event","pageview","purchase","debug","atr"],
window.metrilo.skelet=function(e){return function(){a=Array.prototype.slice.call(arguments);a.unshift(e);window.metrilo.queue.push(a)}};
for(var i=0;window.metrilo.methods.length>i;i++){var mthd=window.metrilo.methods[i];window.metrilo[mthd]=window.metrilo.skelet(mthd)}
window.metrilo.load=function(e){var t=document,n=t.getElementsByTagName("script")[0],r=t.createElement("script");
r.type="text/javascript";r.async=true;r.src="//t.metrilo.com/j/"+e+".js";n.parentNode.insertBefore(r,n)};
metrilo.load("14824cb0bb7dc07f");
                        </script>
        <!--------------------------- END METRILO -------------------------------->
<script>
	jQuery(document).ready(function(){
		jQuery('.opener').toggle(function(e) {
			
		    var $this = jQuery(this);
		  	$this.next().addClass('show');
        	$this.next().slideDown(350);
		    
		},function(){
			
			var $this = jQuery(this);
		  	$this.next().removeClass('show');
        	$this.next().slideUp(350);
		});
	});

</script>
<style>
ul.level0 {
    display: none;
}
ul.level1 {
    display: none;
}
</style>
</head>

<body <?php body_class( $class_header ); ?> itemscope itemtype="http://schema.org/WebPage">
<!-- Magento header -->
<div class="page magento-css">
        <?php echo $headerBlock; ?>
</div>
<!-- END Magento header -->
<!--------------------------------------------->
<!--------------------------------------------->

 <div id="wrapper" <?php if ( isset( $deskpress_data['box_layout'] ) && $deskpress_data['box_layout'] == "boxed" ) {
	echo 'class="boxed_area"';
} ?>>
 	<div class="menu-mobile">
		<div class="main_menu_container menu-canvas visible-xs" id="main_menu_mobile">
			<ul class="nav navbar-nav">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'primary',
					'container'      => false,
					'items_wrap'     => '%3$s'
				) );
				?>
			</ul>
		</div>
	</div>
	<div id="page" class="hfeed site menu-mobile-left">
		<?php
		// drawer
		if ( isset( $deskpress_data['show_drawer'] ) && $deskpress_data['show_drawer'] == '1' && is_active_sidebar( 'drawer_top' ) ) {
			//get_template_part( 'inc/header/drawer' );
		}
		//header
		if ( isset( $deskpress_data['header_style'] ) && $deskpress_data['header_style'] == 'header_v1' ) {
			get_template_part( 'inc/header/header_1' );
		} else {
			get_template_part( 'inc/header/header_3' );
		}
		?>
		<!-- #masthead -->
		<div id="content" class="site-content">