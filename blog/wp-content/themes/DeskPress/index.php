<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package deskpress
 */
?>
<?php


global $deskpress_data;
$class      = 'col-sm-9 alignright';
$sidebar_cl = " sidebar-left";
if ( $deskpress_data['layout'] == '2c-r-fixed' ) {
	$class      = "col-sm-9 alignleft";
	$sidebar_cl = " sidebar-right";
}
if ( $deskpress_data['layout'] == '1col-fixed' ) {
	$class      = "col-sm-12 fullwith";
	$sidebar_cl = "";
}
if (is_front_page() ){
	$class      = "col-sm-9 alignleft";
	$sidebar_cl = " sidebar-right";
}
?>
<section id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		//get_template_part( 'templates/archive', 'top' );
		?>
		<div class="container">
			<div class="row">
				<div class="content-post <?php echo $sidebar_cl; ?>">
					<div class="<?php echo $class; ?>">
						<?php if ( have_posts() ) : ?>
							<div class="blog-basic blog-content-home">
								<?php while ( have_posts() ) : the_post(); ?>
									<?php
									/* Include the Post-Format-specific template for the content.
									 * If you want to override this in a child theme, then include a file
									 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
									 */
									get_template_part( 'content', get_post_format() );
									?>

								<?php endwhile; ?>
							</div>
							<?php deskpress_paging_nav(); ?>

						<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>
						<?php endif; ?>

					</div>
					<?php
					if ( $class == "col-sm-9 alignleft" ) {
						get_sidebar( '2' );
					} elseif ( $class == "col-sm-9 alignright" ) {
						get_sidebar();
					}
					?>
				</div>
				<!--end content-post-->
			</div>
			<!-- row-->
		</div>
		<!-- container-->
	</main>
	<!-- #main -->
</section><!-- #primary -->
