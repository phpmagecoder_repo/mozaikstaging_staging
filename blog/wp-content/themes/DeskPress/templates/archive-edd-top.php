<?php
/**
 * Created by PhpStorm.
 * User: Anh Tuan
 * Date: 5/5/14
 * Time: 2:27 PM
 */

global $deskpress_data;

/***********custom Top Images*************/
$height = 100;
$text_color = $bg_color = $bg_header = $class_full = $text_color_header = '';
$hide_breadcrumbs = $hide_title = 0;
// color theme options

	if ( isset( $deskpress_data['text_color_product'] ) && $deskpress_data['text_color_product'] <> '' ) {
		$text_color_header = 'style="color: ' . $deskpress_data['text_color_product'] . '"';
	}
	if ( isset( $deskpress_data['bg_color_product'] ) && $deskpress_data['bg_color_product'] <> '' ) {
		$bg_color = 'background: ' . $deskpress_data['bg_color_product'] . ';';
	}

	if ( isset( $deskpress_data['height_custom_heading_product'] ) && $deskpress_data['height_custom_heading_product'] <> '' ) {
		$height = $deskpress_data['height_custom_heading_product'];
	}
	if ( isset( $deskpress_data['opt_hide_title_product'] ) && $deskpress_data['opt_hide_title_product'] != '0' ) {
		$hide_title = $deskpress_data['opt_hide_title_product'];
	}


$c_css = '';
if ( $height <> '' ) {
	$height = 'height: ' . $height . 'px;';
}
if ( $height || $bg_color ) {
	$c_css = ' style="' . $bg_color . $height . '"';
}


?>
<?php
if ( $hide_title != '1' ) {
	?>
	<div class="top_site_main" <?php echo $c_css; ?>>
		<div class="container page-title-wrapper" <?php echo $text_color_header; ?>>
			<div class="page-title-captions width100">
				<header class="entry-header">
					<h2 class="page-title">
						<?php _e( 'Downloads', 'deskpress' ); ?>
					</h2>
				</header>
				<!-- .page-header -->
			</div>
		</div>
	</div>
<?php } ?>

<?php
//if ( $hide_breadcrumbs != '1' ) {
// 	if ( get_post_type() == 'product' ) {
//		echo '<div class="breadcrumbs">';
//		woocommerce_breadcrumb();
//		echo '</div>';
//	} else {
//		echo '<div class="breadcrumbs">';
//		deskpress_breadcrumbs();
//		echo '</div>';
//	}
//}
?>