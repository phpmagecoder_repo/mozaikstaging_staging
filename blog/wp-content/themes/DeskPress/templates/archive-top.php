<?php
/**
 * Created by PhpStorm.
 * User: Anh Tuan
 * Date: 5/5/14
 * Time: 2:27 PM
 */

global $deskpress_data;

/***********custom Top Images*************/
$height = 100;
$text_color =  $bg_color = $bg_header = $class_full = $text_color_header = '';
$hide_breadcrumbs = $hide_title = 0;
// color theme options
if ( get_post_type() == "product" ) {
	if ( isset( $deskpress_data['text_color_product'] ) && $deskpress_data['text_color_product'] <> '' ) {
		$text_color_header = 'style="color: ' . $deskpress_data['text_color_product'] . '"';
	}
	if ( isset( $deskpress_data['bg_color_product'] ) && $deskpress_data['bg_color_product'] <> '' ) {
		$bg_color =  'background: ' . $deskpress_data['bg_color_product']. ';';
	}

	if ( isset( $deskpress_data['height_custom_heading_product'] ) && $deskpress_data['height_custom_heading_product'] <> '' ) {
		$height = $deskpress_data['height_custom_heading_product'];
	}
	if ( isset( $deskpress_data['opt_hide_title_product'] ) && $deskpress_data['opt_hide_title_product'] != '0' ) {
		$hide_title = $deskpress_data['opt_hide_title_product'];
	}

}else{
	if ( isset( $deskpress_data['text_color_archive'] ) && $deskpress_data['text_color_archive'] <> '' ) {
		$text_color_header = 'style="color: ' . $deskpress_data['text_color_archive'] . '"';
	}
	if ( isset( $deskpress_data['bg_color_archive'] ) && $deskpress_data['bg_color_archive'] <> '' ) {
 		$bg_color =  'background: ' . $deskpress_data['bg_color_archive']. ';';
	}
	if ( isset( $deskpress_data['height_custom_heading_archive'] ) && $deskpress_data['height_custom_heading_archive'] <> '' ) {
		$height = $deskpress_data['height_custom_heading_archive'];
	}
	if ( isset( $deskpress_data['opt_hide_title_archive'] ) && $deskpress_data['opt_hide_title_archive'] != '0' ) {
		$hide_title = $deskpress_data['opt_hide_title_archive'];
	}

}
$c_css = '';
if($height <>''){
	$height = 'height: ' . $height . 'px;';
}
if ($height || $bg_color) {
	$c_css = ' style="'.$bg_color.$height.'"';
}


?>
<?php
if ( $hide_title != '1' ) {
	?>
	<div class="top_site_main" <?php echo $c_css; ?>>
		<div class="container page-title-wrapper" <?php echo $text_color_header; ?>>
			<div class="page-title-captions width100">
 					<header class="entry-header" >
						<h2 class="page-title">
							<?php
							if ( get_post_type() == "product" ):
								woocommerce_page_title();
							elseif ( is_category() ) :
								single_cat_title();
							elseif ( is_tag() ) :
								single_tag_title();

							elseif ( is_author() ) :
								printf( __( 'Author: %s', 'deskpress' ), '<span class="vcard">' . get_the_author() . '</span>' );

							elseif ( is_day() ) :
								printf( __( 'Day: %s', 'deskpress' ), '<span>' . get_the_date() . '</span>' );

							elseif ( is_month() ) :
								printf( __( 'Month: %s', 'deskpress' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'deskpress' ) ) . '</span>' );

							elseif ( is_year() ) :
								printf( __( 'Year: %s', 'deskpress' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'deskpress' ) ) . '</span>' );
							elseif ( is_search() ) :
								printf( __( 'Search Results for: %s', 'deskpress' ), '<span>' . get_search_query() . '</span>' );

							elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
								_e( 'Asides', 'deskpress' );

							elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
								_e( 'Galleries', 'deskpress' );

							elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
								_e( 'Images', 'deskpress' );

							elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
								_e( 'Videos', 'deskpress' );

							elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
								_e( 'Quotes', 'deskpress' );

							elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
								_e( 'Links', 'deskpress' );

							elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
								_e( 'Statuses', 'deskpress' );

							elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
								_e( 'Audios', 'deskpress' );

							elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
								_e( 'Chats', 'deskpress' );

							elseif ( get_post_type() == "portfolio" ) :
								_e( 'Portfolio', 'deskpress' );
							else :
								_e( 'Archives', 'deskpress' );
 							endif;
							?>
						</h2>
					</header>
 				<!-- .page-header -->
			</div>
 		</div>
	</div>
<?php } ?>

<?php
//if ( $hide_breadcrumbs != '1' ) {
// 	if ( get_post_type() == 'product' ) {
//		echo '<div class="breadcrumbs">';
//		woocommerce_breadcrumb();
//		echo '</div>';
//	} else {
//		echo '<div class="breadcrumbs">';
//		deskpress_breadcrumbs();
//		echo '</div>';
//	}
//}
?>