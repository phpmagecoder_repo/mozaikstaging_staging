<?php
/**
 * Created by PhpStorm.
 * User: Anh Tuan
 * Date: 5/5/14
 * Time: 2:27 PM
 */

global $deskpress_data;
$height = 100;
$postid = get_the_ID();

/***********custom title*************/
$text_color = $bg_color = "";
/******************hide_title_and_subtitle *********************/
$hide_title = $hide_breadcrumbs = $hide_title_and_subtitle = 0;

	if ( isset( $deskpress_data['text_color_header'] ) && $deskpress_data['text_color_header'] <> '' ) {
		$text_color = 'style="color: ' . $deskpress_data['text_color_header'] . '"';
	}
	if ( isset( $deskpress_data['bg_color'] ) && $deskpress_data['bg_color'] <> '' ) {
		$bg_color = 'background: ' . $deskpress_data['bg_color'] . ';';
	}
	// height custom heading
	if ( isset( $deskpress_data['height_custom_heading'] ) && $deskpress_data['height_custom_heading'] != '0' ) {
		$height = $deskpress_data['height_custom_heading'];
	}
	if ( isset( $deskpress_data['opt_hide_title'] ) && $deskpress_data['opt_hide_title'] != '0' ) {
		$hide_title = $deskpress_data['opt_hide_title'];
	}

if ( $height <> '' ) {
	$height = 'height: ' . $height . 'px;';
}

$c_css = '';
if ($height || $bg_color) {
	$c_css = ' style="'.$bg_color.$height.'"';
}
if ( $hide_title != '1' ) {
 ?>
<div class="top_site_main" <?php echo $c_css; ?>>
	<div class="container page-title-wrapper" <?php echo $text_color; ?>>
		<div class="page-title-captions width100">
			<?php
			echo '<header class="entry-header" >';
				echo '<h2 class="entry-title">' . get_the_title( $postid ) . '</h2>';
			echo '</header>';
			?>
		</div>
 	</div>
</div>
<?php }?>