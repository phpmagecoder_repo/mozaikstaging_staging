<?php
/**
 * Created by PhpStorm.
 * User: Anh Tuan
 * Date: 5/5/14
 * Time: 2:27 PM
 */

global $deskpress_data,$post;
$height = 100;
$postid = get_the_ID();

/***********custom title*************/
$text_color = $bg_color = "";
/******************hide_title_and_subtitle *********************/
$hide_title = $hide_breadcrumbs = $hide_title_and_subtitle = 0;
if ( get_post_type() == "product" ) {
	if ( isset( $deskpress_data['opt_hide_title_product'] ) && $deskpress_data['opt_hide_title_product'] != '0' ) {
		$hide_title = $deskpress_data['opt_hide_title_product'];
	}
//	if ( isset( $deskpress_data['opt_hide_breadcrumbs_product'] ) && $deskpress_data['opt_hide_breadcrumbs_product'] != '0' ) {
//		$hide_breadcrumbs = $deskpress_data['opt_hide_breadcrumbs_product'];
//	}
	if ( isset( $deskpress_data['text_color_product'] ) && $deskpress_data['text_color_product'] <> '' ) {
		$text_color = 'style="color: ' . $deskpress_data['text_color_product'] . '"';
	}
	if ( isset( $deskpress_data['bg_color_product'] ) && $deskpress_data['bg_color_product'] <> '' ) {
		$bg_color = 'background: ' . $deskpress_data['bg_color_product'] . ';';
	}
	// height custom heading
	if ( isset( $deskpress_data['height_custom_heading'] ) && $deskpress_data['height_custom_heading'] != '0' ) {
		$height = $deskpress_data['height_custom_heading'];
	}
} else {
	if ( isset( $deskpress_data['opt_hide_title'] ) && $deskpress_data['opt_hide_title'] != '0' ) {
		$hide_title = $deskpress_data['opt_hide_title'];
	}

	if ( isset( $deskpress_data['text_color_header'] ) && $deskpress_data['text_color_header'] <> '' ) {
		$text_color = 'style="color: ' . $deskpress_data['text_color_header'] . '"';
	}
	if ( isset( $deskpress_data['bg_color'] ) && $deskpress_data['bg_color'] <> '' ) {
		$bg_color = 'background: ' . $deskpress_data['bg_color'] . ';';
	}
	// height custom heading
	if ( isset( $deskpress_data['height_custom_heading'] ) && $deskpress_data['height_custom_heading'] != '0' ) {
		$height = $deskpress_data['height_custom_heading'];
	}

}

if ( $height <> '' ) {
	$height = 'height: ' . $height . 'px;';
}

$c_css = '';
if ($height || $bg_color) {
	$c_css = ' style="'.$bg_color.$height.'"';
}

if ( $hide_title != '1' ) {
?>
 		<div class="top_site_main" <?php echo $c_css; ?>>
			<div class="container page-title-wrapper" <?php echo $text_color; ?>>
 					<div class="page-title-captions width100">
						<?php
							if ( get_post_type() == 'product' ) {
								echo '<header class="entry-header" ><h2 class="entry-title">';
								echo  _e('Shop','deskpress');
								echo '</h2></header>';
 							}elseif(is_single($post)){
								if ( get_post_type() == 'post' ) {
								echo '<header class="entry-header" ><h2 class="entry-title">';
								$category    = get_the_category();
								$category_id = get_cat_ID( $category[0]->cat_name );
								echo  get_category_parents( $category_id, FALSE, "" );
 								echo '</h2></header>';
 								}elseif ( is_attachment() ) {
									echo '<header class="entry-header" ><h2 class="entry-title">';
									echo _e('Attachment','deskpress');
									echo '</h2></header>';
								} else {
									echo '<header class="entry-header" ><h2 class="entry-title">';
 									echo  get_the_title();
									echo '</h2></header>';
								}

							}
 						?>
					</div>
  			</div>
		</div>
		<?php //}
 }
?>