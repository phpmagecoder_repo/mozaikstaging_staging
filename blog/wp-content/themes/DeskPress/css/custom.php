<?php

function themeoptions_variation($deskpress_data) {
    //global $deskpress_data;
     $content_file = "
		@primary: {$deskpress_data['body_color_primary']};
		@secondary: {$deskpress_data['body_color_secondary']};
		@anchor_text_color: {$deskpress_data['anchor_text_color']};
		@anchor_text_hover_color: {$deskpress_data['anchor_text_hover_color']};
		@headings_color: {$deskpress_data['headings_color']};
		@body_color: {$deskpress_data['body_color']};
		@body_background: {$deskpress_data['body_background']};
		@font_body_size:{$deskpress_data['body_font_size']};
		@text_footer_color: {$deskpress_data['text_footer_color']};
		@text_footer_color_hover: {$deskpress_data['link_color_footer_hover']};
		@color_border_bottom: {$deskpress_data['color_border_bottom']};
		@top_site_main: {$deskpress_data['bg_color_archive']};
		@jumbotron_color_one:{$deskpress_data['jumbotron_color_one']};
		@jumbotron_color_second:{$deskpress_data['jumbotron_color_second']};
		@btn_search: #363635;
		@btn_search_hover: #6F6F6E;
 		@border_color : #ddd;
		@white: #fff;
		@black: #000;
	";
    return $content_file;
}

/**
 * Generate custom.css file from Theme Options
 * @return mixed|string
 */
function customcss($deskpress_data) {
    //global $deskpress_data;
    // var_dump($deskpress_data);exit();
    $font = $font_heading = "'Helvetica Neue',Helvetica,Arial,sans-serif";
    $linkcolor = $deskpress_data['anchor_text_color'];
    if ($deskpress_data['google_body_font'] != 'Select Font') {
        $font = '"' . $deskpress_data['google_body_font'] . '", Arial, Helvetica, sans-serif';
    } elseif ($deskpress_data['standard_body'] != 'Select Font') {
        $font = $deskpress_data['standard_body'];
    }
    if ($deskpress_data['google_headings'] != 'Select Font') {
        $font_heading = '"' . $deskpress_data['google_headings'] . '", Arial, Helvetica, sans-serif';
    } elseif ($deskpress_data['standard_heading'] != 'Select Font') {
        $font_heading = $deskpress_data['standard_heading'];
    }

    $drawer_link = deskpress_hex2rgb($deskpress_data['bg_top_color']);
    $drawer_link_1 = 'rgba(' . $drawer_link[0] . ',' . $drawer_link[1] . ',' . $drawer_link[2] . ',0.5)';
    if (!$deskpress_data['drawer_columns'])
        $deskpress_data['drawer_columns'] = 2;
    $width_drawer = 100 / $deskpress_data['drawer_columns'];

    $bg_jombotron_opacity = deskpress_hex2rgb($deskpress_data['body_color_primary']);
    $bg_opacity = $deskpress_data['bg_jombotron_opacity'] / 100;
	$bg_jombotron = 'rgba(' . $bg_jombotron_opacity[0] . ',' . $bg_jombotron_opacity[1] . ',' . $bg_jombotron_opacity[2] . ',' . $bg_opacity . ')';
	//
	$bg_header_opacity = deskpress_hex2rgb($deskpress_data['bg_header_color']);
	$opacity = $deskpress_data['bg_opacity'] / 100;
	$bg_header = 'rgba(' . $bg_header_opacity[0] . ',' . $bg_header_opacity[1] . ',' . $bg_header_opacity[2] . ',' . $opacity . ')';

    // Typography
    $custom_css = '
		body{background-color: ' . $deskpress_data['body_background'] . '; color: ' . $deskpress_data['body_color'] . '; font:' . $deskpress_data['font_weight_body'] . ' ' . $deskpress_data['body_font_size'] . 'px/1.8 ' . $font . '}
 		.boxed_area{background-color: ' . $deskpress_data['body_background'] . '}
 		h1{font-size: ' . $deskpress_data['font_size_h1'] . 'px; font-weight:' . $deskpress_data['font_weight_h1'] . '; font-style:' . $deskpress_data['font_style_h1'] . '; text-transform:' . $deskpress_data['text_transform_h1'] . ' }
		h2{font-size: ' . $deskpress_data['font_size_h2'] . 'px; font-weight:' . $deskpress_data['font_weight_h2'] . '; font-style:' . $deskpress_data['font_style_h2'] . '; text-transform:' . $deskpress_data['text_transform_h2'] . '}

		h1.entry-title,.kb-content-title h1{ font-size: ' . $deskpress_data['font_size_h2'] . 'px;}

		h3{font-size: ' . $deskpress_data['font_size_h3'] . 'px; font-weight:' . $deskpress_data['font_weight_h3'] . '; font-style:' . $deskpress_data['font_style_h3'] . '; text-transform:' . $deskpress_data['text_transform_h3'] . '}
		.product-name{font-size: ' . ( $deskpress_data['font_size_h3'] - 2 ) . 'px; font-weight: normal}
		h4{font-size: ' . $deskpress_data['font_size_h4'] . 'px; font-weight:' . $deskpress_data['font_weight_h4'] . '; font-style:' . $deskpress_data['font_style_h4'] . '; text-transform:' . $deskpress_data['text_transform_h4'] . '}
		h5{font-size: ' . $deskpress_data['font_size_h5'] . 'px; font-weight:' . $deskpress_data['font_weight_h5'] . '; font-style:' . $deskpress_data['font_style_h5'] . '; text-transform:' . $deskpress_data['text_transform_h5'] . '}
		h6{font-size: ' . $deskpress_data['font_size_h6'] . 'px; font-weight:' . $deskpress_data['font_weight_h6'] . '; font-style:' . $deskpress_data['font_style_h6'] . '; text-transform:' . $deskpress_data['text_transform_h6'] . '}
		h1,h2,h3,h4,h5,h6,h1 a, h2 a, h3 a, h4 a, h5 a, h6 a{color: ' . $deskpress_data['headings_color'] . '}
		h1,h2,h3,h4,h5,h6{font-family: ' . $font_heading . ';}
		a {color: ' . $deskpress_data['anchor_text_color'] . ';}
		a:hover {color: ' . $deskpress_data['anchor_text_hover_color'] . ';}

		#main_menu li .sub-menu a:hover,.page-title-wrapper .breadcrumbs .woocommerce-breadcrumb a:hover{color:  ' . $linkcolor . '}
		.navbar-header:hover .icon-bar{background: ' . $linkcolor . ' }
		.top-right-menu >li,.top-left-menu >li{ border-right: 1px solid ' . $deskpress_data['border_top_color'] . '}
 		#rt-drawer .drawer_link a,#rt-drawer .widget-title,#rt-drawer .drawer_link a:hover{color: ' . $deskpress_data['drawer_text_color'] . '}


		#masthead .top-header{border-bottom: 1px solid ' . $deskpress_data['border_top_color'] . '; background:' . $deskpress_data['bg_top_color'] . '; color: ' . $deskpress_data['top_header_text_color'] . ' }
 		#masthead .top-header i,#masthead .top-header a{color: ' . $deskpress_data['top_header_link_color'] . '!important }
 		#masthead .top-header h1 {color: ' . $deskpress_data['top_header_text_color'] . '!important}
		#masthead .top-header li a:hover{color: ' . $deskpress_data['top_header_text_color'] . '!important}
		#masthead .top-header{font-size:' . $deskpress_data['font_size_top_header'] . 'px}
		#masthead .top-header .deskpress_social_link a{font-size:' . ( $deskpress_data['font_size_top_header'] + 3 ) . 'px}

		.kb-article-list .entry-title a {font-size: ' . ( $deskpress_data['body_font_size'] + 2 ) . 'px;}

		#rt-drawer{background: ' . $deskpress_data['bg_drawer_color'] . '; color: ' . $deskpress_data['drawer_text_color'] . ' }
 		#rt-drawer .drawer_link{background: ' . $drawer_link_1 . ';}
		#rt-drawer.style2 .drawer_link{ border-color: transparent ' . $deskpress_data['bg_drawer_color'] . ' transparent transparent !important;}
		#rt-drawer #collapseDrawer .widget{ width: ' . $width_drawer . '%;}

		.navigation {background:' . $deskpress_data['bg_main_menu_color'] . '}
		.main_menu_container .nav>li>a,.main_menu_container .nav>li>span.disable_link,.main_menu_container .navbar_right li.sliderbar-menu-controller >div{color: ' . $deskpress_data['main_menu_text_color'] . '; font-weight: ' . $deskpress_data['font_weight_main_menu'] . '; font-size: ' . $deskpress_data['font_size_main_menu'] . 'px}
		#masthead .wapper_logo{ background: ' . $bg_header . ' }
		#masthead .wapper_logo,#masthead .wapper_logo a{ color: ' . $deskpress_data['header_text_color'] . '}
		.main_menu_container .nav>li.current-menu-item>a{color: ' . $linkcolor . '}

		.main_bottom_area{color: ' . $deskpress_data['text_main_bottom_color'] . '; background:  ' . $deskpress_data['bg_main_bottom_color'] . '}
		.main_bottom_area .widget-title{color: ' . $deskpress_data['header_main_bottom_color'] . ';}

 		.site-footer .footer .widget-title{color: ' . $deskpress_data['title_footer_color'] . '; font-size: ' . $deskpress_data['ft_title_font_size'] . 'px; font-weight: ' . $deskpress_data['font_weight_footer'] . '}
  		.site-footer .footer {background:  ' . $deskpress_data['bg_footer_color'] . '; color: ' . $deskpress_data['text_footer_color'] . ';font-size:' . $deskpress_data['ft_footer_font_size'] . 'px;}
 		.site-footer .footer a{color: ' . $deskpress_data['text_footer_color'] . '}
		.site-footer .copyright_area {background:  ' . $deskpress_data['bg_copyright_color'] . '; color: ' . $deskpress_data['text_copyright_color'] . '; font-size:' . $deskpress_data['ft_footer_font_size'] . 'px;}
		.site-footer .copyright_area a{color: ' . $deskpress_data['text_copyright_color'] . '}

  	';
    if (isset($deskpress_data['user_bg_pattern']) && $deskpress_data['user_bg_pattern'] == '1') {
        $custom_css .= ' body{background-image: url("' . $deskpress_data['bg_pattern'] . '"); }
 		';
    }
    if (isset($deskpress_data['bg_pattern_upload']) && $deskpress_data['bg_pattern_upload'] <> '') {
        $custom_css .= ' body{background-image: url("' . $deskpress_data['bg_pattern_upload'] . '"); }
						body{
							 background-repeat: ' . $deskpress_data['bg_repeat'] . ';
							 background-position: ' . $deskpress_data['bg_position'] . ';
							 background-attachment: ' . $deskpress_data['bg_attachment'] . ';
						}
 		';
    }
	if (isset($deskpress_data['bg_jombotron']) && $deskpress_data['bg_jombotron'] <> '') {
		$custom_css .= '
			#masthead .header{background-image: url("' . $deskpress_data['bg_jombotron'] . '"); position: relative }
			#masthead .header:before{
				content:"";
				position: absolute;
				top:0 ;
				left: 0;
				bottom: 0;
				right: 0;
				background: '.$bg_jombotron.';
			}
		';

	}else{
		$custom_css .= '#masthead .header{background-color: '.$bg_jombotron.' } ';
	}


    $custom_css .= $deskpress_data['css_custom'];
    // Remove all newline & tab characters
    $custom_css = str_replace("\n", '', $custom_css);
    $custom_css = str_replace("\t", '', $custom_css);

    return $custom_css;
}
