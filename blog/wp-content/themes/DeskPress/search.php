<?php
/**
 * The template for displaying search results pages.
 *
 * @package deskpress
 */
?>

<div class="blog-mas" >
<?php if ( have_posts() ) : ?>

	<?php /* <header class="page-header">
		<h2 class="page-title"><?php printf( __( 'Search Results for: %s', 'deskpress' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
	</header><!-- .page-header --> */ ?>
        
	<?php /* Start the Loop */ 
        wp_enqueue_script( 'deskpress-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array( 'jquery' ), '', false );
            wp_enqueue_script( 'deskpress-isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array(), '', true );
            ?>
	<div class="blog-basic">
 	<?php while ( have_posts() ) : the_post(); ?>
            <div class="item_grid">
		<?php
		/**
		 * Run the loop for the search to output the results.
		 * If you want to overload this in a child theme then include a file
		 * called content-search.php and that will be used instead.
		 */
		get_template_part( 'content');

		?>
            </div>
 	<?php endwhile; ?>
	</div>

	<?php deskpress_paging_nav(); ?>

<?php else : ?>

	<?php get_template_part( 'content', 'none' ); ?>

<?php endif; ?>
</div>
  