<?php
global $tps_faq_category;
// @todo: get correct category_id
$category_id = $tps_faq_category->term_id;
$args        = array(
	'orderby'     => 'title',
	'order'       => 'ASC',
	'post_type'   => 'tps-faq',
	'post_status' => 'publish',
	'tax_query'   => array(
		array(
			'taxonomy' => 'tps_faq_category',
			'field'    => 'id',
			'terms'    => array( $tps_faq_category->term_id )
		)
	)
);
?>

<div class="panel-group" id="accordion-<?php echo $category_id; ?>">
	<?php
	$the_query = new WP_Query( $args );
	$i         = 1;
	// The Loop
	if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion-<?php echo $category_id . get_the_ID(); ?>" href="#faq-<?php echo $category_id . get_the_ID();; ?>">
							<?php echo the_title() ?>
						</a>
					</h4>
				</div>
				<div id="faq-<?php echo $category_id . get_the_ID(); ?>" class="panel-collapse collapse<?php echo $i == 1 ? ' in' : ''; ?>">
					<div class="panel-body">
						<?php echo the_content() ?>
					</div>
				</div>
			</div>
			<?php
			$i ++;
		endwhile;
	endif;

	// Reset Post Data
	wp_reset_postdata();
	?>

</div>
