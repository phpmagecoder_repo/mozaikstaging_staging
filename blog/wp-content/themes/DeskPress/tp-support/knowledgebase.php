<?php get_header(); ?>

<?php
$args  = array(
	'orderby'           => 'name',
	'order'             => 'ASC',
	'hide_empty'        => true,
	'exclude'           => array(),
	'exclude_tree'      => array(),
	'include'           => array(),
	'number'            => '',
	'fields'            => 'all',
	'slug'              => '',
	'parent'            => '',
	'hierarchical'      => true,
	'child_of'          => 0,
	'get'               => '',
	'name__like'        => '',
	'description__like' => '',
	'pad_counts'        => false,
	'offset'            => '',
	'search'            => '',
	'cache_domain'      => 'core'
);
$cates = get_terms( 'tps_category', $args );
global $tps_category;
?>
	<div id="knowledgebase-wrapper" class="container">
          
		<div class="row">
                      <?php get_sidebar(); ?>
			<div class="col-sm-9">
				<?php tps_breadcrumb(); ?>
				<h2>
					<span class="fa-stack pull-left">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-file-text-o fa-stack-1x fa-inverse"></i>
					</span>
					<?php echo __( 'Knowledge Base', 'tps' ); ?>
					<small class="des_kb"><?php echo __( 'Resolve all of your questions in no time', 'tps' ); ?> </small>
				</h2>
                            <div class="row">

                                <?php
                                $i = 1;

                                foreach ( $cates as $cate ) {
                                        $tps_category = $cate;
                                        if ( $i % 2 ) {
                                                echo '</div><div class="row">';
                                        }
                                        $i ++;
                                        ?>
                                        <?php tps_get_template_part( 'loop', 'kb-category' ) ?>
                                <?php } ?>
                               
                                <div class="contact-box column col-md-12">
                                    <hr />
                                     <h2>
					<span class="fa-stack pull-left">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-stack-1x fa-inverse"></i>
					</span>
					<?php echo __( 'Have a Question?', 'tps' ); ?>
					<small class="des_kb"><?php echo __( 'Fill the form below and we will reply and update the KB shortly.', 'tps' ); ?> </small>
                                    </h2>
                                    <?php
                                        echo do_shortcode('[contact-form-7 id="6012" title="Contact Form KB"]');
                                    ?>
                                </div>
                            </div>
                         </div>
		</div>
	</div>
       	

<?php get_footer(); ?>