<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package deskpress
 */
?>
<div id="widget_knowledge_base" class="widget-sidebar-area col-sm-3" role="complementary">
	<?php if ( ! dynamic_sidebar( 'knowledge_base' ) ) :
		dynamic_sidebar( 'knowledge_base' );
	endif; // end sidebar widget area
	?>
</div><!-- #secondary -->
