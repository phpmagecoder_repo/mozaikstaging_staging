<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package deskpress
 */
?>
<?php  echo "<div style=\"color:black;\">";
echo do_shortcode('[wp_biographia]'); 
echo "</div>"; ?>
<h2>Blog posts by the author:  </h2>
<div class="blog-mas" itemscope itemtype="http://schema.org/ItemList">
   
	<?php
	if ( have_posts() ) : ?>
		<?php
		wp_enqueue_script( 'deskpress-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array( 'jquery' ), '', false );
		wp_enqueue_script( 'deskpress-isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array(), '', true );
		echo '<script>
				jQuery(function ($) {
					// init Isotope
					var $container = $(\'.blog-mas\');
					window.addEventListener(\'load\', function () {
						setTimeout(function () {
							$container.isotope({
								itemSelector: \'.item_grid\'
							});
						}, false);
					})
				 })
			</script>';
		?>

		<?php
 		while ( have_posts() ) : the_post(); ?>
			<div class="item_grid">
				<?php
				/* Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content' );
				?>
			</div>
			<?php
 		endwhile;
		?>
	<?php
	else : ?>
		<?php get_template_part( 'content', 'none' ); ?>
	<?php
	endif;
	?>
</div>
<?php
	deskpress_paging_nav();
 	//echo '<div class="blog_btn_load_more" style="text-align:center;"><a href="#" data-size="one_sidebar" data-type="masonry" data-cat="'.$cat.'" data-offset="'.get_option( 'posts_per_page' ).'" class="sc-btn big light">Load More</a></div>';
?>
