<?php get_header(); ?>

	<div id="content"> 
 
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="post" id="post-<?php the_ID(); ?>">

		<div class="posthead">
		<p class="postdate">
		<small class="day"><?php the_time('j') ?></small>
		<small class="month"><?php the_time('M') ?></small>
		<small class="year"><? // php the_time('Y') ?></small>
		</p>
		<h2><a title="Permanent Link to <?php the_title(); ?>" href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		<small class="postauthor">Posted on <?php the_time('Y') ?> under <?php the_category(', ') ?> | <?php comments_popup_link(__('No Comment'), __('1 Comment'), __('% Comments'), 'commentslink', __('Comments are off')); ?> <?php edit_post_link(__('Edit'), ' &#183; ', ''); ?></small>
		</div> <!-- end posthead -->
		
			<?php the_content('Read more &raquo;'); ?>

			<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

		</div> <!-- end post -->


<?php
$this_post = $post;
$category = get_the_category(); $category = $category[0]; $category = $category->cat_ID;
$posts = get_posts('numberposts=6&offset=0&orderby=post_date&order=DESC&category='.$category);
$count = 0;
foreach ( $posts as $post ) {
if ( $post->ID == $this_post->ID || $count == 5) {
unset($posts[$count]);
}else{
$count ++;
}
}
?>

<?php if ( $posts ) : ?>
		<div class="relatedposts">
<h3>Related Posts</h3>
<ul>
<?php foreach ( $posts as $post ) : ?>
<li><a href="<?php the_permalink() ?>" title="<?php echo trim(str_replace("\n"," ",preg_replace('#<[^>]*?>#si','',get_the_excerpt()))) ?>"><?php if ( get_the_title() ){ the_title(); }else{ echo "Untitled"; } ?></a> (<?php the_time('M d, Y') ?>)</li>
<?php endforeach // $posts as $post ?>
</ul>
		</div> <!-- end relatedposts -->
<?php endif // $posts ?>
<?php
$post = $this_post;
unset($this_post);
?>



	<div class="commentsbox"><?php comments_template(); ?></div>

	<?php endwhile; else: ?>

		<p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>

	</div> <!-- end content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
