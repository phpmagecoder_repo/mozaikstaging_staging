<?php get_header(); ?>
	<div id="content">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">

		  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permalink to'); ?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>

			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

			<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

		</div> <!-- end post -->

		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

	</div> <!-- end content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>