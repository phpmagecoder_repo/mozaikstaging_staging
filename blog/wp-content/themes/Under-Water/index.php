<?php get_header(); ?>
	<div id="content">

	<?php /* If this is a category archive */  if (is_category()) { ?>
	<div class="notes"><p>Archives for <?php single_cat_title(''); ?> category</p></div>

	<?php /* If this is a yearly archive */ } elseif (is_day()) { ?>
	<div class="notes"><p>Archives for the day <?php the_time('l, F jS, Y'); ?></p></div>

	<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
	<div class="notes"><p>Archives for <?php the_time('F, Y'); ?></p></div>

	<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
	<div class="notes"><p>Archives for the year <?php the_time('Y'); ?></p></div>

	<?php /* If this is a monthly archive */ } elseif (is_search()) { ?>
	<div class="notes"><p>You searched for <strong><?php the_search_query(); ?></strong>, here are the results: </p></div>

	<?php } ?> 

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

		<div class="post" id="post-<?php the_ID(); ?>">

		<div class="posthead">
		<p class="postdate">
		<small class="day"><?php the_time('j') ?></small>
		<small class="month"><?php the_time('M') ?></small>
		<small class="year"><? // php the_time('Y') ?></small>
		</p>
		<h2><a title="Permanent Link to <?php the_title(); ?>" href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		<small class="postauthor">Posted on <?php the_time('Y') ?> under <?php the_category(', ') ?> | <?php comments_popup_link(__('No Comment'), __('1 Comment'), __('% Comments'), 'commentslink', __('Comments are off')); ?> <?php edit_post_link(__('Edit'), ' &#183; ', ''); ?></small>
		</div> <!-- end posthead -->

			<?php the_content('Read more... &raquo;'); ?>

		</div> <!-- end post -->

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Previous Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Next Entries &raquo;') ?></div>
		</div> <!-- end navigation -->

	<?php else : ?>

		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>


	<?php endif; ?>

	</div> <!-- end content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
