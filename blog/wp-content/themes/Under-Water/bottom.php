<div id="bottom-sep">&nbsp;</div>
<div id="bottom">

<div class="bottom-divs">


	<!-- Bottom Left -->
	<div id="bottom-left">
		<h2><?php _e("About Author"); ?></h2>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vestibulum at arcu. Integer et risus. Morbi id tellus. Integer felis. Mauris malesuada, turpis vitae facilisis euismod, dui arcu adipiscing sem, eu vulputate leo ante in lacus. Sed porta accumsan lectus. Aenean ac sem. In consequat tempus velit. Phasellus leo enim, adipiscing a, egestas nec, pretium ut, pede. Mauris sollicitudin diam et mauris. Sed quis enim vel augue egestas lobortis. Etiam tempus ipsum vel neque.
		</p>
	</div> <!-- end Bottom Left -->
	

	<!-- Bottom Mid -->
	<div id="bottom-mid">
		<h2><?php _e("Resources"); ?></h2>
		<ul>
			<li><a href="" title="">Resources 1</a></li>
			<li><a href="" title="">Resources 2</a></li>
			<li><a href="" title="">Resources 3</a></li>
			<li><a href="" title="">Resources 4</a></li>
			<li><a href="" title="">Resources 5</a></li>
		</ul>
	</div> <!-- end Bottom Midi -->


	<!-- Bottom Right -->
	<div id="bottom-right">
		<h2><?php _e("Meta"); ?></h2>
		<ul>
			<?php wp_register(); ?>
			<li><?php wp_loginout(); ?></li>
			<li><a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional">Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a></li>
			<li><a href="http://gmpg.org/xfn/"><abbr title="XHTML Friends Network">XFN</abbr></a></li>
			<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
			<?php wp_meta(); ?>
		</ul>
	</div> <!-- end Bottom Right -->


</div> <!-- end bottom-divs -->

</div> <!-- end bottom -->