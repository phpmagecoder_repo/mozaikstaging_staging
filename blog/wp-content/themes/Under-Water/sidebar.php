<div id="sidebar">

<div class="lsidebar">
<ul>
	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>



			<?php wp_list_pages('title_li=<h2>Pages</h2>' ); ?>

	<?php if (function_exists('wp_theme_switcher')) { ?>
	<li><h2>Themes</h2>
		<?php wp_theme_switcher('dropdown'); ?></li>
	<?php } ?>

			<?php wp_list_categories('show_count=1&title_li=<h2>Categories</h2>'); ?>

			<li><h2>Archives</h2>
				<ul>
				<?php wp_get_archives('type=monthly&limit=12&show_post_count=1'); ?>
				</ul>
			</li>

			<?php /* If this is the frontpage */ if ( is_home() || is_page() ) { ?>
				<?php wp_list_bookmarks(); ?>

			<?php } ?>
	<?php endif; ?>
	</ul>
</div> <!-- end sidebar-left -->


<div class="rsidebar">
<ul>
	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(2) ) : else : ?>	

    <li><h2>Recent Comments</h2>
		
				     <?php
    global $wpdb;

    $sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID,
    comment_post_ID, comment_author, comment_date_gmt, comment_approved,
    comment_type,comment_author_url,
    SUBSTRING(comment_content,1,30) AS com_excerpt
    FROM $wpdb->comments
    LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID =
    $wpdb->posts.ID)
    WHERE comment_approved = '1' AND comment_type = '' AND
    post_password = ''
    ORDER BY comment_date_gmt DESC
    LIMIT 10";
    $comments = $wpdb->get_results($sql);

    $output = $pre_HTML;
    $output .= "\n<ul>";
    foreach ($comments as $comment) {

    $output .= "\n<li>".strip_tags($comment->comment_author)
    .": " . "<a href=\"" . get_permalink($comment->ID) .
    "#comment-" . $comment->comment_ID . "\" title=\"on " .
    $comment->post_title . "\">" . strip_tags($comment->com_excerpt)
    ."</a></li>";

    }
    $output .= "\n</ul>";
    $output .= $post_HTML;

    echo $output;?>

	
			</li>
    <li><h2>Recent Post</h2>
		<ul>

<?php
$myposts = get_posts('numberposts=10&offset=1');
foreach($myposts as $post) :
?>
<li><a href="<?php the_permalink(); ?>"><?php the_title();
?></a></li>
<?php endforeach; ?>
	
		</ul>
		</li>
    
		<?php endif; ?>
		</ul>
</div> <!-- end sidebar-right -->

</div> <!-- end sidebar -->