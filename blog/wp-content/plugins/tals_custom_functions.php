<?php
/**
 * Plugin Name: Tals Custom Functions
 * Plugin URI: http://yoursite.com
 * Description: This is an awesome custom plugin with functionality that I'd like to keep when switching things.
 * Author: Your Name
 * Author URI: http://yoursite.com
 * Version: 0.1.0
 */

/* Place custom code below this line. */
register_sidebar(
    array(
        'name' => 'Bottom of Post',
'id' => 'bottom-post',
        'description' => 'My new widget area to appear at the bottom of a post',
        'before_widget' => '<div id="bottom_post">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>'
    )
);

function new_belowpost_widget($content){
	if (is_single()){
$content .= '<div class="header-widget">';
 	ob_start(); // Creates an output buffer 
	if ( function_exists(dynamic_sidebar('bottom-post')) ) {
          dynamic_sidebar('bottom-post'); 
	}
	$contents = ob_get_clean();
	$content .= $contents;   
    $content .=  '</div>';
}
	return $content;
}
add_filter('the_content', 'new_belowpost_widget',2);

/**
 * Plugin Name: Custom Press-This Post Type
 * Plugin URI:  http://wordpress.stackexchange.com/a/192065/26350
 */
add_action( 'wp_ajax_press-this-save-post', function()        
{ 
    add_filter( 'wp_insert_post_data', function( $data )
    {
        if( isset( $data['post_type'] ) && 'post' === $data['post_type'] )
            $data['post_type'] = 'tps'; // <-- Edit this to your needs!

        return $data;
    }, PHP_INT_MAX );

}, 0 );


add_filter('pre_get_posts','searchfilter');

add_filter('relevanssi_modify_wp_query', 'rlv_asc_relevance');
function rlv_asc_relevance($query) {
    $query->set('orderby', 'relevance');
    $query->set('order', 'DESC');
    return $query;
}
/* Place custom code above this line. */
?>