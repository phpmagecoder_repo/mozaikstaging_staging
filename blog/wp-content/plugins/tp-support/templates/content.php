<?php get_header();
global $post, $current_user;
$helpful_text = '';
if ( is_user_logged_in() ) {
	$helpful_count = get_post_meta( $post->ID, '_tps_helpful', true );
	if ( $helpful_count ) {
		if ( in_array( $current_user->ID, $helpful_count ) ) {
			$helpful_text = __( ' <span class="pull-left"> <i class="fa fa-smile-o fa-fw"></i> You too </span>', 'deskpress' );
		}
	}
}
//$unhelpful = get_post_meta( $post->ID, '_tps_unhelpful', true );
$helpful = get_post_meta( $post->ID, '_tps_helpful', true );
//@$vote = count( array_filter( $unhelpful ) ) + count( array_filter( $helpful ) );
//@$percent = intval( $helpful ) / intval( $vote );
if ( ! is_array( $helpful ) ) {
	$helpful = array();
}
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		?>
		<div id="knowledgebase-wrapper" class="container post-<?php echo $post->ID ?>">
			<div class="row">
				<div class="col-sm-9">
					<div class="content-kb" itemscope itemtype="http://schema.org/CreativeWork">
						<?php tps_breadcrumb(); ?>
						<div class="kb-content-title">
							<meta content="<?php echo the_title(); ?>" itemprop="name" />
							<h1>
								<span class="fa-stack pull-left">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-file-text-o fa-stack-1x fa-inverse"></i>
								</span>
								<?php echo the_title(); ?>
							</h1>

							<div class="kb-meta">
								<time datetime="<?php echo $post->post_date ?>" itemprop="dateCreated"><?php echo date( "M d, Y", strtotime( $post->post_date ) ) ?></time>
								<?php
								$suburbs = wp_get_post_terms( $post->ID, 'tps_category', 'hide-empty=0&orderby=id' );
								if ( count( $suburbs ) ) :
									?>
									<span class="kbcategory">
									<span><?php echo _e( 'Posted in: ', 'deskpress' ) ?></span>
										<?php
										$count = 0;
										$sep = '';
										shuffle( $suburbs );
										$count = 0;
										$sep = '';
										foreach ( $suburbs as $suburb ) {
											if ( $count ++ > 10 ) {
												break;
											}
											echo $sep . '<a href="' . get_term_link( $suburb ) . '">' . $suburb->name . '</a>';
											$sep = ', '; // Put your separator here.
										}
										?>
 								</span>
								<?php endif; ?>
								<?php
								$hits = get_post_meta( $post->ID, 'tps_hits', true );
								$hits ? $hits : 0;
								?>
								<span class="kbcategory">
									<?php echo '<span>' . __( 'Hits: ', 'tps' ) . '</span>' . $hits ?>
								</span>
								<?php
								$suburbs = wp_get_post_terms( $post->ID, 'tps_tag', 'hide-empty=0&orderby=id' );
								if ( count( $suburbs ) ) :
									?>
									<span class="kbtags">
 										<?php
										$count = 0;
										$sep = '';
										shuffle( $suburbs );
										$count = 0;
										$sep = '';
										foreach ( $suburbs as $suburb ) {
											if ( $count ++ > 10 ) {
												break;
											}
											echo $sep . '
									<i class="fa fa-tag"></i>&nbsp;
									<a href="' . get_term_link( $suburb ) . '">' . $suburb->name . ' <span class="tag_count">(' . $suburb->count . ')</span></a>';
											$sep = '&nbsp;'; // Put your separator here.
										}
										?>
 								</span>
								<?php endif; ?>

							</div>
						</div>
						<!--end kb-content-title-->


						<article itemprop="text">
							<?php the_content(); ?>
						</article>
						<div class="kbrating">
							<div class="row">
								<div class="kbratingstars col-md-6" itemscope itemtype="http://schema.org/AggregateRating">
									<?php if ( count( array_filter( $helpful ) ) ) : ?>
										<meta itemprop="ratingValue" content="5" />
										<meta itemprop="reviewCount" content="<?php echo count( $helpful ); ?>" />
									<?php endif; ?>
									<span class="pull-left"><?php echo sprintf( __( '%s people found this useful.', 'tps' ), count( $helpful ) ); ?></span>
									<?php if ( $helpful_text ) {
										echo $helpful_text;
									} else {
										?>
										<a href="javascript:void();" onclick="ArticleHelpful(<?php echo $post->ID ?>);" class="kbratinghelpful pull-left">
											&nbsp;<span><i class="fa fa-smile-o"></i> <?php echo __( 'Me too', 'tps' ) ?></span>
										</a>
									<?php } ?>
									<div onclick="ArticleNotHelpful(<?php echo $post->ID ?>);" class="hide kbratingnothelpful">
										<span><i class="fa fa-thumbs-o-down"></i> <?php echo __( 'Not Helpful', 'tps' ) ?></span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="kb-share">
										<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>" title="<?php echo _e( 'Share on Facebooks', 'deskpress' ) ?>" target="_blank"><i class="fa fa-facebook"></i></a>
										<a href="http://twitter.com/home?status=<?php the_title(); ?> <?php the_permalink(); ?>" title="<?php echo _e( 'Share on Twitter', 'deskpress' ) ?>" target="_blank"><i class="fa fa-twitter"></i></a>
										<a href="http://linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>" title="<?php echo _e( 'Share on Linkedin', 'deskpress' ) ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
										<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode( get_permalink() ); ?>&amp;name=<?php echo the_title(); ?>&amp;description=<?php echo urlencode( get_the_excerpt() ); ?>" title="<?php echo _e( 'Share on Tumblr', 'deskpress' ) ?>" target="_blank"><i class="fa fa-tumblr"></i></a>
										<a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="<?php echo _e( 'Share on Google +1', 'deskpress' ) ?>"><i class="fa fa-google-plus"></i></a>
									</div>
									<!-- Social Sharing -->
								</div>
							</div>
						</div>
					</div>
					<!--end content-kb-->
				</div>
				<?php get_sidebar( 'kb' ); ?>
			</div>

		</div>
	<?php
	}
}
do_action( 'tps_content_footer', $post->ID );
get_footer(); ?>
