<?php
$user_data = get_userdata( $post->post_author );
?>
<li class="post-<?php echo $post->ID ?>" id="post-<?php echo $post->ID ?>">
	<span class="entry-title">
		<a href="<?php echo get_post_permalink( $post->ID ) ?>" rel="bookmark" itemprop="name">
			<i class="fa fa-file-text-o"></i> <?php echo $post->post_title ?>
		</a>
	</span>

	<time class="pull-right" datetime="<?php echo $post->post_date ?>" itemprop="dateCreated"><?php echo date( "M d, Y", strtotime( $post->post_date ) ) ?></time>

	<?php if ( isset( $post->post_excerpt ) ) { ?>
		<p class="post-excerpt-<?php $post->ID ?>" itemprop="description">
			<?php echo $post->post_excerpt ?>
		</p>
	<?php } ?>
</li>