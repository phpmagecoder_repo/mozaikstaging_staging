<?php get_header();
global $wp_query, $post;

$params = get_option( '_tps_params', array() );
?>
	<div id="knowledgebase-wrapper" class="container">

		<div class="row">
			<div class="col-sm-9">
				<?php tps_breadcrumb(); ?>
				<div class="content-kb">
					<h2>
						<?php echo $wp_query->queried_object->name; ?>
						<small><?php //echo $wp_query->queried_object->description; ?>

						</small>
					</h2>
					<ul itemscope itemtype="http://schema.org/ItemList" class="kb-article-list">
						<?php
						$args = array(
							'post_type'      => 'tps',
							'post_status'    => 'publish',
							'orderby'        => isset( $params['kb']['order_by'] ) ? $params['kb']['order_by'] ? 'date' : 'title' : 'title',
							'posts_per_page' => $wp_query->queried_object->count,
							'order'          => isset( $params['kb']['order'] ) ? $params['kb']['order'] ? 'DESC' : 'ASC' : 'ASC',
							'tax_query'      => array(
								array(
									'taxonomy' => 'tps_category',
									'field'    => 'id',
									'terms'    => array( $wp_query->queried_object->term_id )
								)
							)
						);
						$the_query = new WP_Query( $args );

						while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<?php tps_get_template_part( 'loop', 'content' ); ?>
						<?php endwhile; ?>
					</ul>
				</div>

			</div>

		</div>
	</div>
<?php get_footer(); ?>