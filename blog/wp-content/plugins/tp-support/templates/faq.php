<?php get_header();

$args = array(
	'orderby'           => 'name',
	'order'             => 'ASC',
	'hide_empty'        => true,
	'exclude'           => array(),
	'exclude_tree'      => array(),
	'include'           => array(),
	'number'            => '',
	'fields'            => 'all',
	'slug'              => '',
	'parent'            => '',
	'hierarchical'      => true,
	'child_of'          => 0,
	'get'               => '',
	'name__like'        => '',
	'description__like' => '',
	'pad_counts'        => false,
	'offset'            => '',
	'search'            => '',
	'cache_domain'      => 'core'
);
$cates = get_terms( 'tps_faq_category', $args );
global $tps_faq_category;
?>
	<div class="top_site_main faqs">
		<div class="container page-title-wrapper">
			<div class="page-title-captions width100">
				<header class="entry-header">
					<h2 class="page-title">
						<?php _e('FAQs','deskpress')?></h2>
				</header>
				<!-- .page-header -->
			</div>
		</div>
	</div>
	<div id="knowledgebase-wrapper" class="container faqs">
		<div class="row">
			<div class="col-sm-9">
				<?php tps_breadcrumb(); ?>
				<?php
				foreach ( $cates as $cate ) {
					$tps_faq_category = $cate;
					?>
					<h2><?php echo $cate->name ?></h2>
					<?php
					tps_get_template_part( 'loop', 'faq' );
				}
				?>
			</div>
			<?php get_sidebar('kb');?>
		</div>
	</div>
<?php
do_action( 'tps_content_footer', $post->ID );
get_footer(); ?>