<?php get_header(); ?>

<?php
$args  = array(
	'orderby'           => 'name',
	'order'             => 'ASC',
	'hide_empty'        => true,
	'exclude'           => array(),
	'exclude_tree'      => array(),
	'include'           => array(),
	'number'            => '',
	'fields'            => 'all',
	'slug'              => '',
	'parent'            => '',
	'hierarchical'      => true,
	'child_of'          => 0,
	'get'               => '',
	'name__like'        => '',
	'description__like' => '',
	'pad_counts'        => false,
	'offset'            => '',
	'search'            => '',
	'cache_domain'      => 'core'
);
$cates = get_terms( 'tps_category', $args );
global $tps_category;
?>
	<div id="knowledgebase-wrapper" class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php tps_breadcrumb(); ?>
				<h2>
					<span class="fa-stack pull-left">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-file-text-o fa-stack-1x fa-inverse"></i>
					</span>
					<?php echo __( 'Knowledge Base', 'tps' ); ?>
					<small class="des_kb"><?php echo __( 'Resolve all of your questions in no time', 'tps' ); ?> </small>
				</h2>
			</div>
		</div>
		<div class="clear"></div>
		<div class="row">
			<?php
			$i = 1;
			foreach ( $cates as $cate ) {
				$tps_category = $cate;
				if ( $i % 2 ) {
					echo '</div><div class="row">';
				}
				$i ++;
				?>
				<?php tps_get_template_part( 'loop', 'kb-category' ) ?>
			<?php } ?>
		</div>
	</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>