<?php
/* Template Name: Custom Search */
get_header();
?>
	<div class="contentarea">
		<div id="content" class="content_right">
			<h3><?php echo __( 'Search Result for :', 'tps' ) . '"' . $s . '"'; ?> </h3>
			<?php if ( have_posts() ) :
				while ( have_posts() ) :
					the_post(); ?>
					<div id="post-<?php the_ID(); ?>" class="posts">
						<article>
							<h4>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
							</h4>

							<p><?php the_exerpt(); ?></p>

							<p align="right">
								<a href="<?php the_permalink(); ?>"><?php echo __( 'Read More', 'tps' ) ?></a>
							</p>
							<span class="post-meta">
								<?php echo __( 'Post By ', 'tps' );
								the_author();
								echo __( '| Date :', 'tps' );
								echo date( 'j F Y' ); ?>
							</span>

						</article>
						<!-- #post -->
					</div>
				<?php endwhile;
			endif;
			?>
		</div>

		<!-- content -->
	</div><!-- contentarea -->
<?php get_sidebar();
get_footer(); ?>