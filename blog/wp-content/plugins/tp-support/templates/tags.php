<?php get_header();
?>
	<div id="knowledgebase-wrapper" class="container">

		<div class="row">
			<div class="col-sm-9">
				<?php tps_breadcrumb(); ?>
				<div class="content-kb">
					<h2>
						<?php echo $wp_query->queried_object->name; ?>
					</h2>
					<ul itemscope itemtype="http://schema.org/ItemList" class="kb-article-list">
						<?php


						while (have_posts() ) :the_post(); ?>
							<?php tps_get_template_part( 'loop', 'content' ); ?>
						<?php endwhile; ?>
					</ul>
				</div>

			</div>

		</div>
	</div>
<?php get_footer(); ?>