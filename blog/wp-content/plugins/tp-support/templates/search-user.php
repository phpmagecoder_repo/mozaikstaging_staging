<?php get_header(); ?>

<?php

?>
	<div id="knowledgebase-wrapper" class="container">
		<h2>
			<?php echo __( 'Search Envato\'s User', 'tps' ); ?>

		</h2>

		<div class="clear"></div>
		<div class="row">
				<div class="deskpress-search-user">
					<input type="text" placeholder="<?php echo __( 'Please enter your Envato\'s username', 'tps' ) ?>" class="form-control" id="data-search-user" value="" />
					<button class="btn btn-primary btn-search-users" data-loading-text="<?php echo __( 'Finding...', 'tps' ) ?>"><?php echo __( 'Find Now', 'tps' ) ?></button>
					<ul class="ob-list-users list-unstyled clear"></ul>
				</div>

		</div>
	</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>