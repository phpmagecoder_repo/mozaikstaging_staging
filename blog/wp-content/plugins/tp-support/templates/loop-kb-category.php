<?php global $tps_category; ?>
<div class="column col-md-6">
	<h3>
		<a title="<?php echo __( 'View all posts in ', 'tps' ) . $tps_category->name ?>" href="<?php echo get_term_link( $tps_category ) ?>"> <?php echo $tps_category->name ?></a>
		<small>(<?php echo $tps_category->count ?>)</small>
	</h3>
	<ul class="kb-article-list">
		<?php
		$params = get_option( '_tps_params', array() );
		$args = array(
			'post_type'      => 'tps',
			'post_status'    => 'publish',
			'orderby'       => isset( $params['kb']['order_by'] ) ? $params['kb']['order_by'] ? 'date' : 'title' : 'title',
			'posts_per_page' => isset( $params['kb']['limit'] ) ? $params['kb']['limit'] ? $params['kb']['limit'] : 6 : 6,
			'order'          => isset( $params['kb']['order'] ) ? $params['kb']['order'] ? 'DESC' : 'ASC' : 'ASC',
			'tax_query'      => array(
				array(
					'taxonomy' => 'tps_category',
					'field'    => 'id',
					'terms'    => array( $tps_category->term_id )
				)
			)
		);

		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();
				tps_get_template_part( 'loop', 'kb' );
			endwhile;
		endif;

		// Reset Post Data
		wp_reset_postdata(); ?>
	</ul>
	<a href="<?php echo get_term_link( $tps_category ) ?>" class="view-all"><i class="fa fa-angle-double-right"></i> <?php echo sprintf( __( "View all %s items", 'tps' ), $tps_category->count ); ?>
	</a>
</div>