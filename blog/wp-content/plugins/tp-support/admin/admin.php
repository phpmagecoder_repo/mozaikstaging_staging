<?php

/*
Class Name: ThimPress Support
Author: Andy Ha (tu@wpbriz.com)
Author URI: http://wpbriz.com
Copyright 2007-2014 wpBriz.com. All rights reserved.
*/

class TPSAdmin {

	function __construct() {
		$initsearch = new TPSSearchLog();

		add_action( 'init', array( $this, 'init' ) );
		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'scriptInitAdmin' ) );
		add_action( 'add_meta_boxes', array( $this, 'adding_meta_box' ) );
		add_action( 'save_post', array( $this, 'save_meta_box_data' ) );
		add_action( 'admin_menu', array( $this, 'register_tps_submenu_page' ) );
		add_action( 'wp_loaded', array( $this, 'update_settings_callback' ) );
		add_action( 'admin_print_styles', array( $this, 'add_notices' ) );

		add_filter( 'plugin_action_links_tp-support/tps.php', array( $this, 'settings_link' ) );
		add_filter( 'avatar_defaults', array( &$this, 'avatar_defaults' ) );
		add_filter( 'get_avatar', array( &$this, 'get_avatar' ), 10, 5 );

	}

	function admin_init() {
		register_setting( 'discussion', 'add_new_default_avatar', array( $this, 'validate' ) );
		add_settings_field( 'add_new_default_avatar', __( 'Add New Default Avatar', 'tps' ), array( $this, 'field_html' ), 'discussion', 'avatars', $args = array() );
	}

	/**
	 *
	 */
	function field_html() {
		$value = get_option( 'add_new_default_avatar', array( array( 'name' => 'Custom Avatar', 'url' => 'http://thimpress.com/wp-content/uploads/2014/09/logo.png' ) ) );

		foreach ( $value as $k => $pair ) {
			extract( $pair );
			echo "<p><input type='text' name='add_new_default_avatar[$k][name]' value='$name' size='15' />";
			echo "<input type='text' name='add_new_default_avatar[$k][url]' value='$url' size='35' /></p>";
		}

		$uid = uniqid();
		echo "<input type='hidden' id='add_new_default_avatar_garbage' value='$uid' />";
		echo "<p id='add_new_default_avatar'><input type='text' name='add_new_default_avatar[$uid][name]' value='' size='15' />";
		echo "<input type='text' name='add_new_default_avatar[$uid][url]' value='' size='35' /></p>";

	}

	/**
	 * @param $input
	 *
	 * @return mixed
	 */
	function validate( $input ) {
		foreach ( $input as $k => $pair ) {
			$input[$k]['name'] = esc_attr( $pair['name'] );
			$input[$k]['url']  = esc_url( $pair['url'] );
			if ( empty( $pair['name'] ) && empty( $pair['url'] ) ) {
				unset( $input[$k] );
			}
		}

		return $input;
	}

	/**
	 *
	 */
	function avatar_defaults( $avatar_defaults ) {
		$opts = get_option( 'add_new_default_avatar', false );
		if ( $opts ) {
			foreach ( $opts as $k => $pair ) {
				// ensures matching so correct option will be selected in admin
				$av                   = html_entity_decode( $pair['url'] );
				$avatar_defaults[$av] = $pair['name'];
			}
		}

		return $avatar_defaults;
	}

	/**
	 * @param        $avatar
	 * @param        $id_or_email
	 * @param        $size
	 * @param string $default
	 * @param        $alt
	 *
	 * @return mixed
	 */
	function get_avatar( $avatar, $id_or_email, $size, $default = '', $alt ) {

		if ( is_numeric( $id_or_email ) ) {
			$email   = get_userdata( $id_or_email )->user_email;
			$user_id = (int) $id_or_email;
		} elseif ( is_object( $id_or_email ) ) {
			$email   = $id_or_email->comment_author_email;
			$user_id = (int) $id_or_email->user_id;
		} elseif ( is_string( $id_or_email ) && ( $user = get_user_by( 'email', $id_or_email ) ) ) {
			$email   = $id_or_email;
			$user_id = $user->ID;
		} else {
			return $avatar;
		}


		// special exception for our 10up friends
		// http://wordpress.org/extend/plugins/simple-local-avatars/
		// (and check hook suffix while we're at it, if the current user has a simple_local_avatar, it'll throw the list off)
		$local_avatars = get_user_meta( $user_id, 'simple_local_avatar', true );
		if ( !empty( $local_avatars ) && ( isset( $GLOBALS['hook_suffix'] ) && $GLOBALS['hook_suffix'] != 'options-discussion.php' ) ) {
			remove_filter( 'get_avatar', array( &$this, 'get_avatar' ), 88, 5 );

			return $avatar;
		}

		// since we're hooking directly into get_avatar,
		// we need to make sure another avatar hasn't been selected
		/* Once upon a time was needed for Mr WordPress. Do we care?
		$direct = get_option('avatar_default');
		if ( strpos( $default, $direct ) !== false ) {
			$email = empty( $email ) ? 'nobody' : md5( $email );

			// in rare cases were there is no email associated with the comment (like Mr WordPress)
			// we have to work around a bit to insert the custom avatar
			// 'www' version for WP2.9 and older
			if ( strpos( $default, 'http://0.gravatar.com/avatar/') === 0 || strpos( $default, 'http://www.gravatar.com/avatar/') === 0 )
				$avatar = str_replace( $default, $direct, $avatar );

		}
		*/
		// hack the correct size parameter back in, if necessary
		$avatar = str_replace( '%size%', $size, $avatar );
		$avatar = str_replace( urlencode( '%size%' ), $size, $avatar );

		return $avatar;
	}

	/**
	 *
	 */
	static public function attachment_plugin_notice() {
		?>
		<div class="error">
			<p><?php echo __( 'You are using <b>GD bbPress Attachments plugin </b>. Please <b>deactive</b> this plugin to use Thim Support plugin better.', 'tps' ); ?></p>
		</div>
	<?php
	}

	/**
	 *
	 */
	public function add_notices() {
		$intall = get_option( 'tps_install', '' );
		$id     = get_page_by_title( 'Check Purchased Code' );
		if ( $id ) {
			return;
		}
		if ( !$intall ) {
			add_action( 'admin_notices', array( $this, 'install_notice' ) );
		}
	}

	/**
	 * Warring note on WP Backend
	 */
	static function admin_notice() {
		//Wrap notices with link to options page
		$url = admin_url( 'options-general.php?page=tps-settings-page' );

		//Dont display if user cant manage options
		if ( current_user_can( 'manage_options' ) ) {

			$envato = get_option( '_tps_params' );

			if ( !isset( $envato['e_username'] ) ) {

				echo '<div class="error"><strong><a href="' . $url . '"><p>' . __( 'Please enter your Envato username', 'tps' ) . '</p></a></strong></div>';

			}

			if ( !isset( $envato['e_api_key'] ) ) {

				echo '<div class="error"><strong><a href="' . $url . '"><p>' . __( 'Please enter your Envato API Key', 'tps' ) . '</p></a></strong></div>';

			}

		}
	}

	function install_notice() {
		?>
		<div class="tps-install-page updated">
			<p><?php _e( '<strong>Welcome to ThimPress Support</strong>', 'tps' ); ?></p>

			<p class="submit">
				<a href="<?php echo add_query_arg( 'tps_intall', 'true', admin_url( 'options-general.php?page=tps-settings-page' ) ); ?>" class="button-primary"><?php _e( 'Install Check Purchased Code Page', 'tps' ); ?></a>
				<a class="btn button-primary" href="<?php echo add_query_arg( 'tps_skip_intall', 'true', admin_url( 'options-general.php?page=tps-settings-page' ) ); ?>"><?php _e( 'Skip setup', 'tps' ); ?></a>
			</p>
		</div>
	<?php
	}

	/**
	 * Create pages that the plugin relies on, storing page id's in variables.
	 *
	 * @access public
	 * @return void
	 */
	protected function create_pages() {
		$pages = array(
			'purchasecode' => array(
				'name'    => _x( 'check-purchased-code', 'Page slug', 'tps' ),
				'title'   => _x( 'Check Purchased Code', 'Page title', 'tps' ),
				'content' => '[check_purchased_code]'
			) );

		foreach ( $pages as $key => $page ) {
			$this->create_page( esc_sql( $page['name'] ), '_tps_params', $page['title'], $page['content'] );
		}

		return true;
	}

	/**
	 * Create a page the ID in an option.
	 *
	 * @access public
	 *
	 * @param mixed  $slug         Slug for the new page
	 * @param mixed  $option       Option name to store the page's ID
	 * @param string $page_title   (default: '') Title for the new page
	 * @param string $page_content (default: '') Content for the new page
	 * @param int    $post_parent  (default: 0) Parent for the new page
	 *
	 * @return int page ID
	 */
	protected function create_page( $slug, $option = '', $page_title = '', $page_content = '', $post_parent = 0 ) {
		global $wpdb;

		$option_value = get_option( $option );

		$page_found = null;

		if ( strlen( $page_content ) > 0 ) {
			// Search for an existing page with the specified page content (typically a shortcode)
			$page_found = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM " . $wpdb->posts . " WHERE post_type='page' AND post_content LIKE %s LIMIT 1;", "%{$page_content}%" ) );
		} else {
			// Search for an existing page with the specified page slug
			$page_found = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM " . $wpdb->posts . " WHERE post_type='page' AND post_name = %s LIMIT 1;", $slug ) );
		}


		if ( $page_found ) {
			if ( !$option_value ) {
				$option_value['page'] = $page_found;
				update_option( $option, $option_value );
			}

			return $page_found;
		}

		$page_data = array(
			'post_status'    => 'publish',
			'post_type'      => 'page',
			'post_author'    => 1,
			'post_name'      => $slug,
			'post_title'     => $page_title,
			'post_content'   => $page_content,
			'post_parent'    => $post_parent,
			'comment_status' => 'closed'
		);

		$page_id = wp_insert_post( $page_data );

		if ( $option ) {
			$option_value['page'] = $page_id;
			update_option( $option, $option_value );
		}

		return $page_id;
	}


	/**
	 * Function init when run plugin+
	 */
	function init() {
		$global_params = get_option( '_tps_params', array() );
		$size          = '';
		if ( isset( $global_params['icon_size'] ) ) {
			preg_match( '/x/i', $global_params['icon_size'], $result_size );
			if ( count( array_filter( $result_size ) ) > 0 ) {
				$size = explode( 'x', $global_params['icon_size'] );
			} else {
				$size = array( 16, 16 );
			}
		}
		if ( $size ) {
			add_image_size( 'icon', $size[0], $size[1], true );
		}
		load_plugin_textdomain( 'tps' );
		$this->load_plugin_textdomain();
		$this->register_post_type();
		$this->create_taxonomies();

	}

	/**
	 * Function create tag and category
	 */
	function create_taxonomies() {
		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => _x( 'Categories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Categories', 'tps' ),
			'all_items'         => __( 'All Categories', 'tps' ),
			'parent_item'       => __( 'Parent Category', 'tps' ),
			'parent_item_colon' => __( 'Parent Category:', 'tps' ),
			'edit_item'         => __( 'Edit Category', 'tps' ),
			'update_item'       => __( 'Update Category', 'tps' ),
			'add_new_item'      => __( 'Add New Category', 'tps' ),
			'new_item_name'     => __( 'New Category Name', 'tps' ),
			'menu_name'         => __( 'KB Categories', 'tps' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'knowledge-base-category' ),
		);

		register_taxonomy( 'tps_category', array( 'tps' ), $args );

		// Add new taxonomy, NOT hierarchical (like tags)
		$labels = array(
			'name'                       => _x( 'Tags', 'taxonomy general name' ),
			'singular_name'              => _x( 'Tag', 'taxonomy singular name' ),
			'search_items'               => __( 'Search Tags', 'tps' ),
			'popular_items'              => __( 'Popular Tags', 'tps' ),
			'all_items'                  => __( 'All Tags', 'tps' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Tag', 'tps' ),
			'update_item'                => __( 'Update Tag', 'tps' ),
			'add_new_item'               => __( 'Add New Tag', 'tps' ),
			'new_item_name'              => __( 'New Tag Name', 'tps' ),
			'separate_items_with_commas' => __( 'Separate Tags with commas', 'tps' ),
			'add_or_remove_items'        => __( 'Add or remove Tags', 'tps' ),
			'choose_from_most_used'      => __( 'Choose from the most used Tags', 'tps' ),
			'not_found'                  => __( 'No Tags found.', 'tps' ),
			'menu_name'                  => __( 'KB Tags', 'tps' ),
		);

		$args = array(
			'hierarchical'          => false,
			'labels'                => $labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			'rewrite'               => array( 'slug' => 'knowledge-base-tag' ),
		);

		register_taxonomy( 'tps_tag', 'tps', $args );
	}

	/**
	 * load Language translate
	 */
	function load_plugin_textdomain() {
		$locale = apply_filters( 'plugin_locale', get_locale(), 'tps' );
		// Admin Locale
		if ( is_admin() ) {
			load_textdomain( 'tps', TPS_LANGUAGES . "tps-$locale.mo" );
		}

		// Global + Frontend Locale
		load_textdomain( 'tps', TPS_LANGUAGES . "tps-$locale.mo" );
		load_plugin_textdomain( 'tps', false, TPS_LANGUAGES );
	}

	/**
	 * Init Script in Admin
	 */
	function scriptInitAdmin() {

		$post_type = filter_input( INPUT_GET, 'post_type', FILTER_SANITIZE_STRING );
		$page      = filter_input( INPUT_GET, 'page', FILTER_SANITIZE_STRING );

		//if ( get_post_type() == 'forum' || get_post_type() == 'tps' || $post_type == 'tps' || $page == 'tps-settings-page' ) {

		wp_enqueue_script( 'jquery-chosen', TPS_JS . 'chosen.jquery.min.js', array(), false, false );
		wp_enqueue_script( 'tps', TPS_JS . 'admin-tps.js', array(), false, false );

		wp_enqueue_style( 'jquery-chosen', TPS_CSS . 'chosen.min.css' );
		wp_enqueue_style( 'tps', TPS_CSS . 'admin-tps.css' );
		//}

	}

	/**
	 * Register post type
	 */
	function register_post_type() {

		if ( post_type_exists( 'tps' ) ) {
			return;
		}
		$labels = array(
			'name'               => _x( 'Knowledge Base', 'tps' ),
			'singular_name'      => _x( 'Knowledge Base', 'tps' ),
			'menu_name'          => _x( 'Knowledge Base', 'Admin menu', 'tps' ),
			'name_admin_bar'     => _x( 'Knowledge Base', 'Add new on Admin bar', 'tps' ),
			'add_new'            => _x( 'Add New', 'role', 'tps' ),
			'add_new_item'       => __( 'Add New Knowledge Base', 'tps' ),
			'new_item'           => __( 'New Knowledge Base', 'tps' ),
			'edit_item'          => __( 'Edit Knowledge Base', 'tps' ),
			'view_item'          => __( 'View Knowledge Base', 'tps' ),
			'all_items'          => __( 'All Knowledge Base', 'tps' ),
			'search_items'       => __( 'Search Knowledge Base', 'tps' ),
			'parent_item_colon'  => __( 'Parent Knowledge Base:', 'tps' ),
			'not_found'          => __( 'No Knowledge Base found.', 'tps' ),
			'not_found_in_trash' => __( 'No Knowledge Base found in Trash.', 'tps' )

		);
		$args   = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'knowledge-base' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'excerpt', 'post-formats', 'comments' ),
			'menu_icon'          => "dashicons-book-alt"
		);
		register_post_type( 'tps', $args );
	}


	/**
	 * Add Custome Script
	 */
	function style_load() {

	}

	/**
	 * Add meta box
	 */
	function adding_meta_box() {
		add_meta_box(
			'tps_sectionid',
			__( 'TP Settings', 'tps' ),
			array( $this, 'meta_box_callback' ), 'forum', 'side', 'high' );
		add_meta_box(
			'tps_sectionid',
			__( 'Site information', 'tps' ),
			array( $this, 'meta_box_topic_callback' ), 'topic' );
		add_meta_box(
			'tps_sectionid',
			__( 'From Topic', 'tps' ),
			array( $this, 'meta_box_tps_callback' ), 'tps', 'side', 'high' );
		add_meta_box(
			'tps_sectionid',
			__( 'Product Icon', 'tps' ),
			array( $this, 'meta_box_product_callback' ), 'product', 'side' );
	}

	function meta_box_topic_callback( $post ) {
		$params = get_post_meta( $post->ID, '_tps_params', true );
		?>
		<div class="inside">
			<p>
				<label for="bbp-topictitle"><?php echo __( 'Domain', 'tps' ); ?></label><br />
				<input type="text" class="form-control" value="<?php echo isset( $params['domain'] ) ? $params['domain'] : '' ?>" size="40" name="tps[params][domain]" />
			</p>

			<p>
				<label for="bbp-topic-title"><?php echo __( 'Site\'s access information:', 'tps' ); ?></label><br />
				<textarea class="form-control" name="tps[params][site_access]"><?php echo isset( $params['site_access'] ) ? $params['site_access'] : '' ?></textarea>
			</p>

			<p><?php echo __( 'Note! All information will be protected.', 'tps' ) ?></p>
		</div>
	<?php
	}

	function meta_box_product_callback( $post ) {
		$params = get_post_meta( $post->ID, '_tps_params', true );
		$icon   = isset( $params['icon'] ) ? $params['icon'] : '';
		?>
		<div id="product_icons_container">
			<ul class="product_icons">
				<?php
				if ( $icon ) {
					echo '<li class="image" data-attachment_id="' . esc_attr( $icon ) . '">'
						. wp_get_attachment_image( $icon, 'icon' ) . '
							<ul class="actions">
								<li><a href="#" class="delete tips" data-tip="' . __( 'Delete image', 'tps' ) . '">' . __( 'Delete', 'tps' ) . '</a></li>
							</ul>
						</li>';
				}
				?>
			</ul>
			<input type="hidden" id="product-icon" name="tps[params][icon]" value="<?php echo $icon; ?>" />

		</div>
		<p class="add_product_icon hide-if-no-js">
			<a href="#" data-choose="<?php _e( 'Add Product Icon', 'tps' ); ?>" data-update="<?php _e( 'Add Icon', 'tps' ); ?>" data-delete="<?php _e( 'Delete icon', 'tps' ); ?>" data-text="<?php _e( 'Delete', 'tps' ); ?>"><?php _e( 'Add product icon', 'tps' ); ?></a>
		</p>
	<?php
	}

	/**
	 * Function meta box in tps
	 *
	 * @param $post
	 */
	function meta_box_tps_callback( $post ) {
		$params    = get_post_meta( $post->ID, '_tps_params', true );
		$args      = array(
			'post_type'   => 'topic',
			'post_status' => 'publish',
			'orderby'     => 'title'
		);
		$the_query = new WP_Query( $args );

		$items = $the_query->posts;
		?>
		<div class="inside">
			<p>
				<label for="bbp_forum_type_select" class="screen-reader-text"><?php echo __( 'Select Items', 'tps' ); ?></label>
				<select name="tps[params][topic_id][]" multiple="true" class="chosen-select">
					<?php
					if ( is_array( $items ) ) {
						foreach ( $items as $item ) {
							$selected = '';
							if ( isset( $params['topic_id'] ) ) {
								if ( is_array( $params['topic_id'] ) ) {
									if ( in_array( $item->ID, $params['topic_id'] ) ) {
										$selected = 'selected = "selected"';
									}
								}
							}
							?>
							<option value="<?php echo $item->ID ?>" <?php echo $selected; ?>><?php echo $item->post_title ?></option>
						<?php
						}
					} ?>

				</select>

			</p>
		</div>
	<?php
	}

	/**
	 * Assign in forum
	 *
	 * @param $post
	 */
	function meta_box_callback( $post ) {
		$opt_params = get_option( '_tps_params', array() );
		$params     = get_post_meta( $post->ID, '_tps_params', true );
		$items      = array();

		if ( isset( $opt_params['envato_items'] ) ) {
			if ( is_array( $opt_params['envato_items'] ) ) {
				if ( count( array_filter( $opt_params['envato_items'] ) ) > 0 ) {
					$items = json_decode( base64_decode( $opt_params['items_info'] ) );
				}
			}
		}
		$args      = array(
			'role' => 'bbp_moderator'
		);
		$args_key  = array(
			'role' => 'bbp_keymaster'
		);
		$users_mod = get_users( $args );
		$users_key = get_users( $args_key );
		$mods      = array_merge( $users_mod, $users_key );
		?>
		<div class="inside">

			<p>

				<strong><?php echo __( 'Select Product Items', 'tps' ); ?></strong>
				<select name="tps[params][envato_items][]" multiple="true" class="chosen-select">
					<option value="99"
						<?php

						if ( isset( $params['envato_items'] ) ) {
							if ( is_array( $params['envato_items'] ) ) {
								if ( in_array( 99, $params['envato_items'] ) ) {
									echo 'selected="selected"';
								}
							}
						} else {
							echo 'selected="selected"';
						} ?>> <?php echo __( 'All Items', 'tps' ) ?>
					</option>
					<?php
					if ( is_array( $items ) ) {
						foreach ( $items as $item ) {
							$selected = '';
							if ( in_array( $item->id, $opt_params['envato_items'] ) ) {
								if ( isset( $params['envato_items'] ) ) {
									if ( is_array( $params['envato_items'] ) ) {
										if ( in_array( $item->id, $params['envato_items'] ) ) {
											$selected = 'selected = "selected"';
										}
									}
								}
								?>
								<option value="<?php echo $item->id ?>" <?php echo $selected; ?>><?php echo $item->item ?></option>
							<?php
							}
						}
					} ?>

				</select>

			</p>
			<p>
				<strong><?php echo __( 'Moderators', 'tps' ); ?></strong>
				<select name="tps[params][mod][]" multiple="true" class="chosen-select">
					<?php
					if ( count( $mods ) > 0 ) {
						foreach ( $mods as $mod ) {
							$selected = '';
							if ( isset( $params['mod'] ) ) {
								if ( in_array( $mod->ID, $params['mod'] ) ) {
									$selected = 'selected = "selected"';
								}
							}
							?>
							<option value="<?php echo $mod->ID ?>" <?php echo $selected; ?>><?php echo $mod->data->display_name ?></option>
						<?php
						}
					} ?>
				</select>
			</p>
			<p>
				<label title="<?php _e( 'Show all replies in topic detail.', 'tps' ) ?>">
					<input type="checkbox" name="tps[params][show_replies]" value="1" <?php isset( $params['show_replies'] ) ? checked( $params['show_replies'], 1 ) : '' ?>  />
					<?php echo __( 'Show replies', 'tps' ); ?></label>
			</p>
			<?php
			// Get the folum roles
			$dynamic_roles = bbp_get_dynamic_roles();
			// Only keymasters can set other keymasters
			if ( !bbp_is_user_keymaster() ) {
				unset( $dynamic_roles[bbp_get_keymaster_role()] );
			} ?>

			<p>
				<strong><?php _e( 'Forum permission', 'tps' ) ?></strong>
				<select name="tps[params][forum_permission]" class="chosen-select">
					<option <?php isset( $params['forum_permission'] ) ? selected( $params['forum_permission'], 0 ) : 'selected="selected"' ?> value="0"><?php _e( 'Public', 'tps' ) ?></option>
					<?php foreach ( $dynamic_roles as $role => $details ) : ?>
						<?php if ( $role == 'bbp_spectator' || $role == 'bbp_blocked' ) {
							continue;
						} ?>
						<option <?php isset( $params['forum_permission'] ) ? selected( $params['forum_permission'], $role ) : '' ?> value="<?php echo esc_attr( $role ); ?>"><?php echo translate_user_role( $details['name'] ); ?></option>

					<?php endforeach; ?>
				</select>

			</p>
			<p>
				<?php
				$args  = array(
					'orderby'           => 'name',
					'order'             => 'ASC',
					'hide_empty'        => true,
					'exclude'           => array(),
					'exclude_tree'      => array(),
					'include'           => array(),
					'number'            => '',
					'fields'            => 'all',
					'slug'              => '',
					'parent'            => '',
					'hierarchical'      => true,
					'child_of'          => 0,
					'get'               => '',
					'name__like'        => '',
					'description__like' => '',
					'pad_counts'        => false,
					'offset'            => '',
					'search'            => '',
					'cache_domain'      => 'core'
				);
				$cates = get_terms( 'tps_category', $args );

				if ( count( $cates ) ) {
					?>
					<strong><?php _e( 'Related Knowledge Base Category', 'tps' ) ?></strong>
					<select name="tps[params][related_kb]" class="chosen-select">
						<?php foreach ( $cates as $cate ) : ?>
							<option <?php isset( $params['related_kb'] ) ? selected( $params['related_kb'], $cate->term_id ) : '' ?> value="<?php echo $cate->term_id; ?>"><?php echo esc_html( $cate->name ) ; ?></option>

						<?php endforeach; ?>
					</select>
				<?php } ?>

			</p>

		</div>
	<?php
	}

	/**
	 * Save data in meta box
	 */
	function save_meta_box_data( $post_id ) {
		/*
		 * We need to verify this came from our screen and with proper authorization,
		 * because the save_post action can be triggered at other times.
		 */

		// Check the user's permissions.

		if ( current_user_can( 'edit_post', $post_id ) || current_user_can( 'moderate' ) ) {

		} else {
			return;
		}

		if ( get_post_type() == 'forum' || get_post_type() == 'tps' || get_post_type() == 'topic' || get_post_type() == 'product' ) {
			$params = $_POST['tps']['params'];

			update_post_meta( $post_id, '_tps_params', $params );

		}


	}

	/**
	 * Create sub page
	 */
	function register_tps_submenu_page() {
		add_submenu_page( 'options-general.php', __( 'Thim HelpDesk Settings', 'tps' ), __( 'Thim HelpDesk', 'tps' ), 'update_core', 'tps-settings-page', array( $this, 'tps_settings_callback' ) );
	}

	/**
	 * Function settings page
	 */
	function tps_settings_callback() {
		global $verifi;
		$params       = get_option( '_tps_params', array() );
		$envato_sites = array();
		$envato_sites = $verifi->user_items_by_site();
		$envato_items = array();

		if ( is_array( $envato_sites ) ) {
			if ( count( array_filter( $envato_sites ) ) > 0 ) {
				foreach ( $envato_sites as $envato_site ) {
					$envato_item_site = array();
					$envato_item_site = $verifi->new_files_from_user( '', $envato_site->site, 0, 0 );
					if ( count( array_filter( $envato_item_site ) ) > 0 ) {
						$envato_items = array_merge( $envato_items, $envato_item_site );
					}
				}
			}
		}
		$pages_args = array( 'authors'        => '',
							 'child_of'       => 0,
							 'date_format'    => get_option( 'date_format' ),
							 'depth'          => 0,
							 'echo'           => 1,
							 'exclude'        => '',
							 'include'        => '',
							 'link_after'     => '',
							 'link_before'    => '',
							 'post_type'      => 'page',
							 'post_status'    => 'publish',
							 'show_date'      => '',
							 'sort_column'    => 'menu_order, post_title',
							 'sort_order'     => '',
							 'title_li'       => __( 'Pages' ),
							 'walker'         => '',
							 'posts_per_page' => - 1
		);

		$pages = new WP_Query( $pages_args );
		if ( count( $pages->posts ) > 0 ) {
			$pages = $pages->posts;
		} else {
			$pages = array();
		}
		/*Woo get data*/
		$product_args = array( 'authors'        => '',
							   'child_of'       => 0,
							   'date_format'    => get_option( 'date_format' ),
							   'depth'          => 0,
							   'echo'           => 1,
							   'exclude'        => '',
							   'include'        => '',
							   'link_after'     => '',
							   'link_before'    => '',
							   'post_type'      => 'product',
							   'post_status'    => 'publish',
							   'show_date'      => '',
							   'sort_column'    => 'menu_order, post_title',
							   'sort_order'     => '',
							   'title_li'       => __( 'Pages' ),
							   'walker'         => '',
							   'posts_per_page' => - 1
		);

		$products = new WP_Query( $product_args );
		if ( count( $products->posts ) > 0 ) {
			$products = $products->posts;
		} else {
			$products = array();
		}
		/*EDD get data*/
		$edd_args = array( 'authors'        => '',
						   'child_of'       => 0,
						   'date_format'    => get_option( 'date_format' ),
						   'depth'          => 0,
						   'echo'           => 1,
						   'exclude'        => '',
						   'include'        => '',
						   'link_after'     => '',
						   'link_before'    => '',
						   'post_type'      => 'download',
						   'post_status'    => 'publish',
						   'show_date'      => '',
						   'sort_column'    => 'menu_order, post_title',
						   'sort_order'     => '',
						   'title_li'       => __( 'Pages' ),
						   'walker'         => '',
						   'posts_per_page' => - 1
		);

		$edds = new WP_Query( $edd_args );
		if ( count( $edds->posts ) > 0 ) {
			$edds = $edds->posts;
		} else {
			$edds = array();
		}


		if ( count( array_filter( $products ) ) > 0 ) {
			foreach ( $products as $product ) {
				$woo_item       = new stdClass();
				$woo_item->id   = 'woo_' . $product->ID;
				$woo_item->item = $product->post_title;
				$envato_items[] = $woo_item;
			}
		}
		if ( count( array_filter( $edds ) ) > 0 ) {
			foreach ( $edds as $edd ) {
				$edd_item       = new stdClass();
				$edd_item->id   = 'edd_' . $edd->ID;
				$edd_item->item = $edd->post_title;
				$envato_items[] = $edd_item;
			}
		}

		?>
		<div class="wrap">
			<h2><?php echo __( 'Thim HelpDesk Settings', 'tps' ); ?></h2>

			<form action="options-general.php?page=tps-settings-page" method="post">
				<h3><?php echo __( 'General Setting', 'tps' ); ?></h3>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">
							<label for="tps-page"><?php echo __( 'Default Purchased Code Page', 'tps' ) ?></label>
						</th>
						<td>
							<select name="tps[params][page]" id="tps-page" class="chosen-select">
								<?php foreach ( $pages as $page ) { ?>
									<option value="<?php echo $page->ID ?>" <?php selected( @$params['page'], $page->ID ) ?>> <?php echo $page->post_title ?></option>
								<?php } ?>
							</select>
							<br />
							<span>
								<?php
								echo __( 'When login bbPress will be redirect to check purchased code.', 'tps' );
								?>
							</span>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps[params][search_log]"><?php echo __( 'Write search log', 'tps' ) ?></label>
						</th>
						<td>
							<input type="radio" <?php echo isset( $params['search_log'] ) ? '' : 'checked="checked"' ?> name="tps[params][search_log]" value="0" <?php checked( @$params['search_log'], 0 ) ?> /> <?php echo __( 'No', 'tps' ) ?>
							<input type="radio" name="tps[params][search_log]" value="1" <?php checked( @$params['search_log'], 1 ) ?>/> <?php echo __( 'Yes', 'tps' ) ?>
							<br />
							<span>
								<?php
								echo __( 'When turn on, Keyword will save log what on live search.', 'tps' );
								?>
							</span>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="icon-size"><?php echo __( 'Product Icon Size', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][icon_size]" id="icon-size" class="" value="<?php echo isset( $params['icon_size'] ) ? $params['icon_size'] : '16x16' ?>">
							<br />
							<span>
								<?php
								echo __( '<br/>Your product icon on user avatar. Format: XX x YY', 'tps' );
								?>
							</span>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps-page"><?php echo __( 'Use WP register default', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][use_register_default]" id="use-register-default" value="1" type="checkbox" <?php isset( $params['use_register_default'] ) ? checked( $params['use_register_default'], 1 ) : '' ?> />
							<br />
							<br />
							<span>
								<?php
								echo __( 'Register page will be used WP register default.', 'tps' );
								?>
							</span>
						</td>
					</tr>
				</table>
				<h3><?php echo __( 'Forum Setting', 'tps' ); ?></h3>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">
							<label for="tps-page"><?php echo __( 'Turn off Access Information', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][access_information]" id="access-information" value="1" type="checkbox" <?php isset( $params['access_information'] ) ? checked( $params['access_information'], 1 ) : '' ?> />
							<br />
							<br />
							<span>
								<?php
								echo __( 'Hide access information field and access information button on forum.', 'tps' );
								?>
							</span>
						</td>
					</tr>
				</table>
				<h3><?php echo __( 'Knowledge Base Setting', 'tps' ); ?></h3>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">
							<label for="tps-kb-order-by"><?php echo __( 'Order By', 'tps' ) ?></label>
						</th>
						<td>
							<select name="tps[params][kb][order_by]" class="chosen-select">
								<option value="0" <?php echo isset( $params['kb']['order_by'] ) ? $params['kb']['order_by'] == 0 ? 'selected="selected"' : '' : 'selected="selected"' ?> ><?php echo __( 'Title', 'tps' ) ?></option>
								<option value="1" <?php echo isset( $params['kb']['order_by'] ) ? $params['kb']['order_by'] == 1 ? 'selected="selected"' : '' : '' ?>><?php echo __( 'Date created', 'tps' ) ?></option>
							</select>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps-kb-order"><?php echo __( 'Order', 'tps' ) ?></label>
						</th>
						<td>
							<select name="tps[params][kb][order]" class="chosen-select">
								<option value="0" <?php echo isset( $params['kb']['order'] ) ? $params['kb']['order'] == 0 ? 'selected="selected"' : '' : 'selected="selected"' ?>><?php echo __( 'ASC', 'tps' ) ?></option>
								<option value="1" <?php echo isset( $params['kb']['order'] ) ? $params['kb']['order'] == 1 ? 'selected="selected"' : '' : '' ?>><?php echo __( 'DESC', 'tps' ) ?></option>
							</select>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps-kb-limit"><?php echo __( 'Limit', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][kb][limit]" value="<?php echo isset( $params['kb']['limit'] ) ? $params['kb']['limit'] : '6' ?>">
							<br />
							<span><?php echo __( 'Number items on category', 'tps' ) ?></span>
						</td>
					</tr>
				</table>
				<h3><?php echo __( 'FAQs Setting', 'tps' ); ?></h3>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">
							<label for="tps-faqs-order-by"><?php echo __( 'Order By', 'tps' ) ?></label>
						</th>
						<td>
							<select name="tps[params][faqs][order_by]" class="chosen-select">
								<option value="0" <?php echo isset( $params['faqs']['order_by'] ) ? $params['faqs']['order_by'] == 0 ? 'selected="selected"' : '' : 'selected="selected"' ?> ><?php echo __( 'Title', 'tps' ) ?></option>
								<option value="1" <?php echo isset( $params['faqs']['order_by'] ) ? $params['faqs']['order_by'] == 1 ? 'selected="selected"' : '' : '' ?>><?php echo __( 'Date created', 'tps' ) ?></option>
							</select>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps-faqs-order"><?php echo __( 'Order', 'tps' ) ?></label>
						</th>
						<td>
							<select name="tps[params][faqs][order]" class="chosen-select">
								<option value="0" <?php echo isset( $params['faqs']['order'] ) ? $params['faqs']['order'] == 0 ? 'selected="selected"' : '' : 'selected="selected"' ?>><?php echo __( 'ASC', 'tps' ) ?></option>
								<option value="1" <?php echo isset( $params['faqs']['order'] ) ? $params['faqs']['order'] == 1 ? 'selected="selected"' : '' : '' ?>><?php echo __( 'DESC', 'tps' ) ?></option>
							</select>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps-faqs-limit"><?php echo __( 'Limit', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][faqs][limit]" value="<?php echo isset( $params['faqs']['limit'] ) ? $params['faqs']['limit'] : '6' ?>">
							<br />
							<span><?php echo __( 'Number items on category FAQs', 'tps' ) ?></span>
						</td>
					</tr>
				</table>
				<h3><?php echo __( 'Envato Config', 'tps' ); ?></h3>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">
							<label for="e_username"><?php echo __( 'Envato USERNAME', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][e_username]" id="e_username" class="" value="<?php echo @$params['e_username'] ?>">
							<br />
							<span>
								<?php
								echo __( '<br/>Your Envato username at Envato site(eg: themeforest.net or codecanyon.net...)<br/>Example: obTheme', 'tps' );
								?>
							</span>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="e_api_key"><?php echo __( 'Envato API Key', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][e_api_key]" id="e_api_key" class="" value="<?php echo @$params['e_api_key'] ?>">
							<br />
							<span>
								<?php
								echo __( '<br/>Your Envato Marketplace API <br/>See instruction at <a href=\"http://extras.envato.com/api/\">http://extras.envato.com/api/</a><br/>', 'tps' );
								?>
							</span>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="redirect_url"><?php echo __( 'Redirect URL', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][redirect_url]" id="redirect_url" class="" value="<?php echo @$params['redirect_url'] ?>">
							<br />
							<span>
								<?php
								echo __( 'When login successful, It will go to this URL.', 'tps' );
								?>
							</span>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps-envato-items"><?php echo __( 'Product Items', 'tps' ) ?></label>
						</th>
						<td>
							<select name="tps[params][envato_items][]" id="tps-envato-items" multiple="true" class="chosen-select">
								<?php

								foreach ( $envato_items as $envato_item ) {
									$selected = '';
									if ( in_array( $envato_item->id, $params['envato_items'] ) ) {
										$selected = 'selected="selected"';
									}
									?>
									<option value="<?php echo $envato_item->id ?>" <?php echo $selected ?> > <?php echo $envato_item->item ?> </option>
								<?php } ?>
							</select>
							<br />
							<span>
								<?php
								echo __( '<br/>Product List will be autoloaded when Username and API Key are saved. Only 25 latest products are loaded for each Envato\'s site.', 'tps' );
								?>
							</span>
						</td>
					</tr>

				</table>
				<h3><?php echo __( 'Email Template Notification', 'tps' ); ?></h3>
				<?php
				$subject = $content = '';
				if ( !@$params['subject'] ) {
					$subject = '[[site-name]] [topic-title]';
				} else {
					$subject = $params['subject'];
				}


				if ( !@$params['body'] ) {
					$content = "Hi [user],

A new topic has been posted.

Topic URL: [topic-url]
Title: [topic-title]
Author: [topic-author]

Content : [topic-content]
-----
[site-name] Support Team!";
				} else {
					$content = $params['body'];
				}
				?>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">
							<label for="tps-email-subject"><?php echo __( 'Email Subject', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][subject]" id="tps-email-subject" class="form-control" value="<?php echo $subject ?>">
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps-email-body"><?php echo __( 'Email body', 'tps' ) ?></label>
						</th>
						<td>
							<textarea rows="10" cols="100" id="tps-email-body" name="tps[params][body]" class="form-control"><?php echo htmlspecialchars( $content ) ?></textarea>
							<br />
							<span>
								<?php
								echo __( 'Email notification will be sent with content same here.<br/><br/> Available Shortcodes what are working with Email Subject and Email Content  <br/><b>[site-name]</b> This site name <br/><b>[user]</b> Moderator name <br/> <b>[topic-title]</b> Topic\'s Title<br/><b> [topic-content]</b> Topic\'s Content<br/><b> [topic-url]</b> Link to topic<br/><b>[topic-author]</b> User who has posted this topic', 'tps' );
								?>
							</span>
						</td>
					</tr>

				</table>
				<h3><?php echo __( 'Thim HelpDesk Slug', 'tps' ); ?></h3>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">
							<label for="tps-knowledge-base"><?php echo __( 'Knowledge Base', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][knowledge_base]" id="tps-knowledge-base" class="form-control" value="<?php echo isset( $params['knowledge_base'] ) ? $params['knowledge_base'] : 'knowledge-base' ?>">
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps-faq"><?php echo __( 'FAQs', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][faq]" id="tps-faq" class="form-control" value="<?php echo isset( $params['faq'] ) ? $params['faq'] : 'faq' ?>">
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="tps-faq"><?php echo __( 'Envato User Search', 'tps' ) ?></label>
						</th>
						<td>
							<input name="tps[params][envato_user]" id="tps-faq" class="form-control" value="<?php echo isset( $params['envato_user'] ) ? $params['envato_user'] : 'envato-user' ?>">
						</td>
					</tr>


				</table>
				<p class="submit">
					<input type="hidden" name="tps[params][items_info]" value="<?php echo base64_encode( json_encode( $envato_items ) ) ?>" />
					<input type="submit" value="<?php echo __( 'Save Changes', 'tps' ) ?>" class="button button-primary">
				</p>
				<?php wp_nonce_field( 'tps_update_settings', '_tps_wpnonce' ); ?>
			</form>

		</div>

	<?php
	}

	/**
	 * Save setting
	 */
	function update_settings_callback() {
		if ( isset( $_REQUEST['tps_intall'] ) ) {
			if ( !get_option( 'tps_install' ) ) {
				if ( $this->create_pages() ) {
					update_option( 'tps_install', 1 );
					ob_end_clean();
					wp_redirect( 'edit.php?post_type=page' );
				}
			}
		}
		if ( isset( $_REQUEST['tps_skip_intall'] ) ) {
			update_option( 'tps_install', 1 );
		}
		// Check if our nonce is set.
		if ( !isset( $_POST['_tps_wpnonce'] ) ) {
			return;
		}

		// Verify that the nonce is valid.
		if ( !wp_verify_nonce( $_POST['_tps_wpnonce'], 'tps_update_settings' ) ) {
			return;
		}


		$params = $_POST['tps']['params'];
		$icons  = array();

		$items_info = json_decode( base64_decode( $params['items_info'] ) );
		@$items = $params['envato_items'];
		if ( $params['icon_size'] ) {
			preg_match( '/x/i', $params['icon_size'], $result_size );
			if ( count( array_filter( $result_size ) ) > 0 ) {
				$size = explode( 'x', $params['icon_size'] );
			} else {
				$size = array( 16, 16 );
			}

		}

		if ( count( array_filter( $items_info ) ) > 0 ) {
			if ( count( $items ) ) {
				foreach ( $items as $k => $item ) {
					foreach ( $items_info as $item_info ) {
						if ( $item == $item_info->id ) {
							$item = $item_info;
							break;
						}
					}
					preg_match( '/woo_([\d]+)/i', $item->id, $result );
					if ( isset( $item->thumbnail ) ) {
						$image         = $this->fetch_remote_file( $item->thumbnail, $size[0], $size[1], true );
						$image['id']   = $item->id;
						$image['name'] = $item->item;
						$icons[]       = $image;
					} elseif ( count( array_filter( $result ) ) > 0 ) {
						$image_meta = get_post_meta( $result[1], '_tps_params', true );
						$image_meta = isset( $image_meta['icon'] ) ? $image_meta['icon'] : '';
						if ( $image_meta ) {
							$image_meta = wp_get_attachment_image_src( $image_meta, 'icon' );
							if ( count( $image_meta ) > 0 ) {
								$image         = $this->fetch_remote_file( $image_meta[0], $size[0], $size[1], true );
								$image['id']   = $item->id;
								$image['name'] = get_post_field( 'post_title', $result[1] );
								$icons[]       = $image;
							}
						}
					}
				}
			}
		}

		update_option( '_tps_icons', $icons );
		update_option( '_tps_params', $params );
	}

	/**
	 * Add Link to Plugin Setting
	 */
	function settings_link( $links ) {
		$settings_link = '<a href="options-general.php?page=tps-settings-page" title="' . __( 'Settings', 'tps' ) . '">' . __( 'Settings', 'tps' ) . '</a>';
		array_unshift( $links, $settings_link );

		return $links;
	}

	/**
	 * Attempt to download a remote file attachment
	 *
	 * @param string $url  URL of item to fetch
	 * @param array  $post Attachment details
	 *
	 * @return array|WP_Error Local file location details on success, WP_Error otherwise
	 */
	protected function fetch_remote_file( $url, $width = 24, $height = 24, $crop = false ) {
		// extract the file name and extension from the url
		$file_name = basename( $url );

		// get placeholder file in the upload dir with a unique, sanitized filename
		$upload = wp_upload_bits( $file_name, 0, '' );
		if ( $upload['error'] ) {
			return new WP_Error( 'upload_dir_error', $upload['error'] );
		}

		// fetch the remote url and write it to the placeholder file
		$headers = wp_get_http( $url, $upload['file'] );

		// request failed
		if ( !$headers ) {
			@unlink( $upload['file'] );

			return new WP_Error( 'import_file_error', __( 'Remote server did not respond', THEME_NAME ) );
		}

		// make sure the fetch was successful
		if ( $headers['response'] != '200' ) {
			@unlink( $upload['file'] );

			return new WP_Error( 'import_file_error', sprintf( __( 'Remote server returned error response %1$d %2$s', THEME_NAME ), esc_html( $headers['response'] ), get_status_header_desc( $headers['response'] ) ) );
		}

		$filesize = filesize( $upload['file'] );

		if ( isset( $headers['content-length'] ) && $filesize != $headers['content-length'] ) {
			@unlink( $upload['file'] );

			return new WP_Error( 'import_file_error', __( 'Remote file is incorrect size', THEME_NAME ) );
		}

		if ( 0 == $filesize ) {
			@unlink( $upload['file'] );

			return new WP_Error( 'import_file_error', __( 'Zero size file downloaded', THEME_NAME ) );
		}

		$max_size = 512000;
		if ( !empty( $max_size ) && $filesize > $max_size ) {
			@unlink( $upload['file'] );

			return new WP_Error( 'import_file_error', sprintf( __( 'Remote file is too large, limit is %s', THEME_NAME ), size_format( $max_size ) ) );
		}

		$thumb           = image_make_intermediate_size( $upload['file'], $width, $height, $crop );
		$upload['thumb'] = str_replace( basename( $upload['file'] ), $thumb['file'], $upload['url'] );

		return $upload;
	}
}

?>