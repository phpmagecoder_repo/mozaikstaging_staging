<?php

/*
Class Name: Widget Countdown timer

Author: Andy Ha (tu@wpbriz.com)
Author URI: http://wpbriz.com
Copyright 2007-2014 wpBriz.com. All rights reserved.
*/

class TPSTimeWork extends WP_Widget {
	function TPSTimeWork() {
		$widget_ops = array(
			'classname'   => 'TPSTimeWork_class',
			'description' => __( 'Show time work support', 'tps' )
		);
		$this->WP_Widget( 'TPSTimeWork', '(TPS) Time Work', $widget_ops );
	}

	function form( $instance ) {

		$defaults     = array(
			'title'        => 'Time Work',
			'from'         => '08:00',
			'to'           => '17:30',
			'weekday1'     => 1,
			'weekday2'     => 1,
			'weekday3'     => 1,
			'weekday4'     => 1,
			'weekday5'     => 1,
			'weekday6'     => 0,
			'weekday7'     => 0,
			'weekday_note' => '',
			'is_holiday'   => 0,
			'holiday_note' => '',
			'offline'      => ''
		);
		$instance     = wp_parse_args( (array) $instance, $defaults );
		$title        = $instance['title'];
		$form         = $instance['from'];
		$to           = $instance['to'];
		$weekday1     = $instance['weekday1'];
		$weekday2     = $instance['weekday2'];
		$weekday3     = $instance['weekday3'];
		$weekday4     = $instance['weekday4'];
		$weekday5     = $instance['weekday5'];
		$weekday6     = $instance['weekday6'];
		$weekday7     = $instance['weekday7'];
		$weekday_note = $instance['weekday_note'];
		$is_holiday   = $instance['is_holiday'];
		$holiday_note = $instance['holiday_note'];
		$offline      = $instance['offline'];
		?>

		<p>
			<label><?php echo __( 'Title', 'tps' ); ?></label>
			<input class="form-control" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<hr />
		<h3><?php echo __( 'Work Time', 'tps' ) ?></h3>
		<p>
			<label><?php echo __( 'From', 'tps' ); ?></label> <br />
			<input class="form-control" type="text" placeholder="00:00" value="<?php echo $form; ?>" name="<?php echo $this->get_field_name( 'from' ); ?>" />
		</p>
		<p>
			<label><?php echo __( 'To', 'tps' ); ?></label><br />
			<input class="form-control" type="text" placeholder="00:00" value="<?php echo $to; ?>" name="<?php echo $this->get_field_name( 'to' ); ?>" />
		</p>
		<hr />
		<h3><?php echo __( 'Weekday', 'tps' ) ?></h3>
		<p>

			<input class="form-control" type="checkbox" value="1" <?php if ( $weekday1 ) {
				echo 'checked="checked"';
			} ?> name="<?php echo $this->get_field_name( 'weekday1' ); ?>" />
			<label><?php echo __( 'Monday', 'tps' ); ?></label><br />

			<input class="form-control" type="checkbox" value="1" <?php if ( $weekday2 ) {
				echo 'checked="checked"';
			} ?> name="<?php echo $this->get_field_name( 'weekday2' ); ?>" />
			<label><?php echo __( 'Tuesday', 'tps' ); ?></label><br />

			<input class="form-control" type="checkbox" value="1" <?php if ( $weekday3 ) {
				echo 'checked="checked"';
			} ?> name="<?php echo $this->get_field_name( 'weekday3' ); ?>" />
			<label><?php echo __( 'Wednesday', 'tps' ); ?></label><br />

			<input class="form-control" type="checkbox" value="1" <?php if ( $weekday4 ) {
				echo 'checked="checked"';
			} ?> name="<?php echo $this->get_field_name( 'weekday4' ); ?>" />
			<label><?php echo __( 'Thursday', 'tps' ); ?></label><br />

			<input class="form-control" type="checkbox" value="1" <?php if ( $weekday5 ) {
				echo 'checked="checked"';
			} ?> name="<?php echo $this->get_field_name( 'weekday5' ); ?>" />
			<label><?php echo __( 'Friday', 'tps' ); ?></label><br />

			<input class="form-control" type="checkbox" value="1" <?php if ( $weekday6 ) {
				echo 'checked="checked"';
			} ?> name="<?php echo $this->get_field_name( 'weekday6' ); ?>" />
			<label><?php echo __( 'Saturday', 'tps' ); ?></label><br />

			<input class="form-control" type="checkbox" value="1" <?php if ( $weekday7 ) {
				echo 'checked="checked"';
			} ?> name="<?php echo $this->get_field_name( 'weekday7' ); ?>" />
			<label><?php echo __( 'Subday', 'tps' ); ?></label><br />

			<label><?php echo __( 'Weekday Notification', 'tps' ); ?></label><br />
			<textarea rows="10" cols="25" name="<?php echo $this->get_field_name( 'weekday_note' ); ?>"><?php echo $weekday_note ?></textarea>

			<label><?php echo __( 'Offline Notification', 'tps' ); ?></label><br />
			<textarea rows="10" cols="25" name="<?php echo $this->get_field_name( 'offline' ); ?>"><?php echo $offline ?></textarea>

		</p>
		<hr />
		<h3><?php echo __( 'Holiday', 'tps' ) ?></h3>
		<p>
			<label><?php echo __( 'Today is holiday', 'tps' ); ?></label><br />
			<select class="form-control" name="<?php echo $this->get_field_name( 'is_holiday' ); ?>">
				<option value="0" <?php selected( $is_holiday, 0 ) ?>><?php echo __( 'No', 'tps' ) ?></option>
				<option value="1" <?php selected( $is_holiday, 1 ) ?>><?php echo __( 'Yes', 'tps' ) ?></option>
			</select>
			<br />
			<label><?php echo __( 'Holiday Notification', 'tps' ); ?></label><br />
			<textarea rows="10" cols="25" name="<?php echo $this->get_field_name( 'holiday_note' ); ?>"><?php echo $holiday_note ?></textarea>
		</p>

	<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance          = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );

		$instance['from']         = $new_instance['from'];
		$instance['to']           = $new_instance['to'];
		$instance['weekday1']     = sanitize_text_field( $new_instance['weekday1'] );
		$instance['weekday2']     = sanitize_text_field( $new_instance['weekday2'] );
		$instance['weekday3']     = sanitize_text_field( $new_instance['weekday3'] );
		$instance['weekday4']     = sanitize_text_field( $new_instance['weekday4'] );
		$instance['weekday5']     = sanitize_text_field( $new_instance['weekday5'] );
		$instance['weekday6']     = sanitize_text_field( $new_instance['weekday6'] );
		$instance['weekday7']     = sanitize_text_field( $new_instance['weekday7'] );
		$instance['weekday_note'] = $new_instance['weekday_note'];
		$instance['is_holiday']   = sanitize_text_field( $new_instance['is_holiday'] );
		$instance['holiday_note'] = $new_instance['holiday_note'];
		$instance['offline']      = $new_instance['offline'];

		return $instance;
	}


	function widget( $args, $instance ) {
		echo $args['before_widget'];

		extract( $args );
		//echo $before_widget;
		wp_enqueue_script( 'tps-widget', TPS_JS . 'tps-widget.js', array(), false, false );
		$class      = $notification = '';
		$time_index = current_time( "N" );
		$instance['weekday' . $time_index];
		if ( $instance['is_holiday'] ) {
			$class        = 'holiday';
			$notification = $instance['holiday_note'];
		} else {
			if ( $instance['from'] && $instance['to'] ) {
				$notification = $instance['weekday_note'];
				$time_open    = $instance['from'];
				$time_close   = $instance['to'];

				$date_string = $time_open . " " . current_time( "m/d/Y" );
				$dateTime    = DateTime::createFromFormat( "H:i m/d/Y", $date_string );
				$time_open   = $dateTime->getTimestamp();

				$date_string = $time_close . " " . current_time( "m/d/Y" );
				$dateTime    = DateTime::createFromFormat( "H:i m/d/Y", $date_string );
				$time_close  = $dateTime->getTimestamp();

				$date_string  = current_time( "H:i m/d/Y" );
				$dateTime     = DateTime::createFromFormat( "H:i m/d/Y", $date_string );
				$current_time = $dateTime->getTimestamp();

				if ( $current_time > $time_open && $current_time < $time_close && $instance['weekday' . $time_index] ) {
					$class = 'online';
				} else {
					$class        = 'offline';
					$notification = $instance['offline'];
				}
			}
		}


		?>
		<script type="text/javascript">
			var smart_today = new Date('<?php echo current_time( "l M d Y H:i:s" ) ?>');
		</script>
		<div class="<?php echo $class ?>">
			<?php echo $notification ?>
			<div class="spt-clock cf">
				<?php echo __( 'Your time:', 'tps' ); ?>
				<span id="spt_local_time">00:00:00</span>
				<?php echo __( 'Our time:', 'tps' ); ?>
				<span id="spt_our_time">00:00:00</span>
			</div>
		</div>
		<?php
		echo $args['after_widget'];
	}

}

/**
 * Show live search
 */
class TPSSearch extends WP_Widget {
	function TPSSearch() {
		$widget_ops = array(
			'classname'   => 'TPSSearch_class',
			'description' => __( 'Search with BBPress, FAQ and Knowledge base', 'tps' )
		);
		$this->WP_Widget( 'TPSSearch', '(TPS) Search', $widget_ops );
	}

	function form( $instance ) {

		$defaults = array(
			'title'    => 'Live Search',
			'keywords' => ''
		);
		@$instance = wp_parse_args( (array) $instance, $defaults );
		$title    = $instance['title'];
		$keywords = $instance['keywords'];
		?>

		<p>
			<label><?php echo __( 'Title', 'tps' ); ?></label><br />
			<input class="form-control" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label><?php echo __( 'Keywords', 'tps' ); ?></label><br />
			<textarea class="form-control" name="<?php echo $this->get_field_name( 'keywords' ); ?>"><?php echo esc_attr( $keywords ); ?></textarea>
		</p>

	<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance             = $old_instance;
		$instance['title']    = sanitize_text_field( $new_instance['title'] );
		$instance['keywords'] = trim( $new_instance['keywords'] );

		return $instance;
	}

	function widget( $args, $instance ) {
		global $post;
		echo $args['before_widget'];

		extract( $args );
		//echo $before_widget;
		
		// Register the script
		wp_register_script( 'tps-widget-search', TPS_JS . 'tps-widget-search.js' );
		$translation_array = array(
			'focus' => __( 'Search in our knowledge base, tutorials & Forums', 'tps' ),
			'blur' => __('Search your question, issue or topic to research','tps')
		);
		wp_localize_script( 'tps-widget-search', 'data_search_placeholder', $translation_array );
		wp_enqueue_script( 'tps-widget-search' );
		
//		wp_enqueue_script( 'tps-widget-search', TPS_JS . 'tps-widget-search.js', array(), false, false );
//		wp_enqueue_style( 'tps', TPS_CSS . 'tps.css' );

		$keywords = explode( "\n", $instance['keywords'] );
		$links    = array();
		if ( !is_user_logged_in() ) {
			$title = __( 'Please login to make request support.', 'tps' );
			$class = ' notlogin';
		} else {
			$title = __( 'It\'s time to request a new support topic', 'tps' );
		}
		?>
		<script>
		function jumpToSearch(){
				location.href = '/blog/search/' + document.getElementById("data-search").value;
		}
		
		</script>
		
		<div class="deskpress-smartsearch <?php echo $class ?>">
			<i class="fa fa-search tp_livesearch"></i>
			<input type="text" id="data-search" class="form-control" placeholder="<?php echo __( 'Search your question, issue or topic to research', 'tps' ) ?>" />
			<a class="btn btn-primary btn-lg btn-request-support fadeOut" onclick="jumpToSearch()">
				<i class="fa fa-send fa-fw"></i><span><?php echo __( 'Get Answers!', 'tps' ) ?></span>
			</a>
			<?php if ( !is_user_logged_in() ) {
				?>

			<?php
			} else {
				$args_forums = array(
					'post_type'      => 'forum',
					'post_status'    => 'publish',
					'orderby'        => 'name',
					'order'          => 'ASC',
					'posts_per_page' => 999,
					'meta_query'     => array(
						array(
							'key'     => '_bbp_forum_type',
							'compare' => 'NOT EXISTS'
						)
					)
				);
				$forums      = new WP_Query( $args_forums );
				$forums      = $forums->posts;
				wp_enqueue_script( 'tps-widget-create-topic', TPS_JS . 'tps-widget-create-topic.js', array(), false, false );
				?>

				<div class="modal fade tps_modal" id="quick-create-topic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<?php
								if ( $instance['title'] ) {
									echo "<h4 class='modal-title'>" . __( 'Create a support topic', 'tps' ) . "</h4>";
								}
								?>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<p>
										<label><?php echo __( 'Title', 'tps' ); ?></label><br />
										<input class="form-control q-title" name="data[title]" type="text" value="" placeholder="<?php echo __( 'Please enter the topic title', 'tps' ) ?>" />
									<ul class="ob-list-suggestions list-unstyle"></ul>
									</p>

									<p>
										<label><?php echo __( 'Content', 'tps' ); ?></label><br />
										<textarea rows="10" class="form-control q_content" id="q_content" name="data[content]"></textarea>
									</p>

									<p>
										<label><?php echo __( 'Forum', 'tps' ); ?></label><br />
										<select name="data[category]" class="form-control">
											<?php foreach ( $forums as $forum ) { ?>
												<option value="<?php echo $forum->ID ?>"><?php echo $forum->post_title ?></option>
											<?php } ?>
										</select>
									</p>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __( 'Close', 'tps' ) ?></button>
								<button type="button" class="btn btn-primary btn_createtopic" data-loading-text="<?php echo __( 'Creating', 'tps' ) ?>..."> <?php echo __( 'Create', 'tps' ) ?></button>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<ul class="ob-list-search list-unstyled"></ul>
		</div>

		<?php if ( count( array_filter( $keywords ) ) > 0 ) { ?>
			<div class="trending-searches">
				<?php echo '<span class="popular-keyword-title">' . __( 'Trending searches:', 'tps' ) . '</span>';

				foreach ( $keywords as $keyword ) {
					$links[] = '&nbsp;<a href="#" class="popular-keyword">' . trim( $keyword ) . '</a>';
				}

				echo implode( ',', $links );
				?>
			</div>
		<?php } ?>
		<?php
		echo $args['after_widget'];
	}
}

/**
 * Show Topic Assigned
 */
class TPSTopicAssigned extends WP_Widget {
	function TPSTopicAssigned() {
		$widget_ops = array(
			'classname'   => 'TPSTopicAssigned_class',
			'description' => __( 'Show All Topic Assigned what is not resolved.', 'tps' )
		);
		$this->WP_Widget( 'TPSTopicAssigned', '(TPS) My Topic Assigned', $widget_ops );
	}

	function form( $instance ) {

		$defaults = array(
			'title' => 'My Topic Assigned',
			'limit' => '30'
		);
		@$instance = wp_parse_args( (array) $instance, $defaults );
		$title = $instance['title'];
		$limit = $instance['limit'];
		?>

		<p>
			<label><?php echo __( 'Title', 'tps' ); ?></label><br />
			<input class="form-control" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label><?php echo __( 'Limit', 'tps' ); ?></label><br />
			<input class="form-control" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>" />
		</p>

	<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance          = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['limit'] = sanitize_text_field( $new_instance['limit'] );

		return $instance;
	}

	function widget( $args, $instance ) {
		if ( !current_user_can( 'moderate' ) ) {
			return;
		}
		global $current_user;
		echo $args['before_widget'];

		extract( $args );
		$args_topic    = array(
			'post_type'      => 'topic',
			'post_status'    => 'publish',
			'orderby'        => 'date',
			'order'          => 'DESC',
			'meta_query'     => array(
				'relation' => 'AND',
				array(
					'key'     => '_tps_status',
					'value'   => '3',
					'type'    => 'CHAR',
					'compare' => '='
				),
				array(
					'key'     => '_tps_user_assign',
					'value'   => $current_user->ID,
					'type'    => 'CHAR',
					'compare' => '='
				)
			),
			'posts_per_page' => $instance['limit']
		);
		$topics_result = new WP_Query( $args_topic );
		$topics        = $topics_result->posts;
		if ( count( $topics ) > 0 ) {
			?>
			<?php
			if ( $instance['title'] ) {
				echo "<h3>{$instance['title']}</h3>";
			}
			?>

			<div class="row">
				<div class="col-md-12">
					<ul class="my_list_assigned">
						<?php foreach ( $topics as $topic ) { ?>
							<li>
								<i class="fa fa-comment fa-fw"></i>
								<a href="<?php echo get_post_permalink( $topic->ID ) ?>"><?php echo $topic->post_title ?></a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		<?php
		}
		echo $args['after_widget'];
	}
}

/**
 * Show Topic UnAssigned
 */
class TPSTopicUnassigned extends WP_Widget {
	function TPSTopicUnassigned() {
		$widget_ops = array(
			'classname'   => 'TPSTopicUnassigned_class',
			'description' => __( 'Show All Topic UnAssigned.', 'tps' )
		);
		$this->WP_Widget( 'TPSTopicUnassigned', '(TPS) Topics Unassigned', $widget_ops );
	}

	function form( $instance ) {

		$defaults = array(
			'title' => 'Topics Unassigned',
			'limit' => '30'
		);
		@$instance = wp_parse_args( (array) $instance, $defaults );
		$title = $instance['title'];
		$limit = $instance['limit'];
		?>

		<p>
			<label><?php echo __( 'Title', 'tps' ); ?></label><br />
			<input class="form-control" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label><?php echo __( 'Limit', 'tps' ); ?></label><br />
			<input class="form-control" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>" />
		</p>

	<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance          = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['limit'] = sanitize_text_field( $new_instance['limit'] );

		return $instance;
	}

	function widget( $args, $instance ) {
		if ( !current_user_can( 'moderate' ) ) {
			return;
		}
		global $current_user;
		echo $args['before_widget'];

		extract( $args );
		$args_topic = array(
			'post_type'      => 'topic',
			'post_status'    => 'publish',
			'orderby'        => 'date',
			'order'          => 'DESC',
			'meta_query'     => array(
				'relation' => 'OR',
				array(
					'key'     => '_tps_user_assign',
					'compare' => 'NOT EXISTS'
				),
				array(
					'key'     => '_tps_user_assign',
					'value'   => '',
					'type'    => 'CHAR',
					'compare' => '='
				),
				array(
					'key'     => '_tps_user_assign',
					'value'   => '-1',
					'type'    => 'CHAR',
					'compare' => '='
				)
			),
			'posts_per_page' => $instance['limit']
		);
		$topics     = get_posts( $args_topic );

		if ( count( $topics ) > 0 ) {
			?>
			<?php
			if ( $instance['title'] ) {
				echo "<h3>{$instance['title']}</h3>";
			}
			?>

			<div class="row">
				<div class="col-md-12">
					<ul class="my_list_unassigned">
						<?php foreach ( $topics as $topic ) { ?>
							<li>
								<i class="fa fa-comment fa-fw"></i>
								<a href="<?php echo get_post_permalink( $topic->ID ) ?>"><?php echo $topic->post_title ?></a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		<?php
		}
		echo $args['after_widget'];
	}
}

/**
 * Show Knowledge Base items related.
 */
class TPSKBItems extends WP_Widget {
	function TPSKBItems() {
		$widget_ops = array(
			'classname'   => 'TPSKBRelated_class',
			'description' => __( 'Show Knowledge Base items related and popular.', 'tps' )
		);
		$this->WP_Widget( 'TPSKBRelated', '(TPS) KB Items', $widget_ops );
	}

	function form( $instance ) {

		$defaults = array(
			'title' => 'Knowledge Base Related',
			'limit' => '10'
		);
		@$instance = wp_parse_args( (array) $instance, $defaults );
		$title           = $instance['title'];
		$limit           = $instance['limit'];
		$items_type      = $instance['items_type'];
		$order_by        = $instance['order_by'];
		$order           = $instance['order'];
		$cagegory_val    = $instance['category'];
		$categories_args = array(
			'orderby'           => 'name',
			'order'             => 'ASC',
			'hide_empty'        => true,
			'exclude'           => array(),
			'exclude_tree'      => array(),
			'include'           => array(),
			'number'            => '',
			'fields'            => 'all',
			'slug'              => '',
			'parent'            => '',
			'hierarchical'      => true,
			'child_of'          => 0,
			'get'               => '',
			'name__like'        => '',
			'description__like' => '',
			'pad_counts'        => false,
			'offset'            => '',
			'search'            => '',
			'cache_domain'      => 'core'
		);
		$categories      = get_terms( 'tps_category', $categories_args );

		?>

		<p>
			<label><?php echo __( 'Title', 'tps' ); ?></label><br />
			<input class="form-control" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label><?php echo __( 'Items type', 'tps' ); ?></label><br />
			<select class="form-control" name="<?php echo $this->get_field_name( 'items_type' ); ?>">
				<option value="0" <?php selected( $items_type, 0 ) ?>><?php echo __( 'In Category', 'tps' ) ?></option>
				<option value="1" <?php selected( $items_type, 1 ) ?>><?php echo __( 'Related', 'tps' ) ?></option>
			</select>
		</p>
		<p>
			<label><?php echo __( 'Category', 'tps' ); ?></label><br />
			<select class="form-control" name="<?php echo $this->get_field_name( 'category' ); ?>[]" multiple="true">
				<?php foreach ( $categories as $category ) {
					$selected = '';
					if ( in_array( $category->term_id, $cagegory_val ) ) {
						$selected = 'selected="selected"';
					}
					echo '<option value="' . $category->term_id . '" ' . $selected . '>' . $category->name . ' </option> ';
				} ?>
			</select>
		</p>
		<p>
			<label><?php echo __( 'Order by', 'tps' ); ?></label><br />
			<select class="form-control" name="<?php echo $this->get_field_name( 'order_by' ); ?>">
				<option value="0" <?php selected( $order_by, 0 ) ?>><?php echo __( 'Title', 'tps' ) ?></option>
				<option value="1" <?php selected( $order_by, 1 ) ?>><?php echo __( 'Date', 'tps' ) ?></option>
				<option value="2" <?php selected( $order_by, 2 ) ?>><?php echo __( 'Hits', 'tps' ) ?></option>
			</select>
		</p>
		<p>
			<label><?php echo __( 'Order', 'tps' ); ?></label><br />
			<select class="form-control" name="<?php echo $this->get_field_name( 'order' ); ?>">
				<option value="0" <?php selected( $order, 0 ) ?>><?php echo __( 'ASC', 'tps' ) ?></option>
				<option value="1" <?php selected( $order, 1 ) ?>><?php echo __( 'DESC', 'tps' ) ?></option>
			</select>
		</p>
		<p>
			<label><?php echo __( 'Limit', 'tps' ); ?></label><br />
			<input class="form-control" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>" />
		</p>

	<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance               = $old_instance;
		$instance['title']      = sanitize_text_field( $new_instance['title'] );
		$instance['items_type'] = sanitize_text_field( $new_instance['items_type'] );
		$instance['order_by']   = sanitize_text_field( $new_instance['order_by'] );
		$instance['order']      = sanitize_text_field( $new_instance['order'] );
		$instance['limit']      = sanitize_text_field( $new_instance['limit'] );
		$instance['category']   = esc_sql( $new_instance['category'] );

		return $instance;
	}

	function widget( $args, $instance ) {
		global $wp_query;
//		print_r( $wp_query );
		extract( $args );
		if ( ( $wp_query->query_vars['post_type'] == 'tps' && $wp_query->queried_object_id ) && $instance['items_type'] == 1 ) {
			$terms        = wp_get_post_terms( $wp_query->queried_object_id, 'tps_category', 'hide-empty=0&orderby=id' );
			$tps_category = array();
			if ( count( $terms ) ) {
				foreach ( $terms as $term ) {
					$tps_category[] = $term->term_id;
				}
			}
			if ( $instance['order_by'] == 2 ) {
				$args_topic = array(
					'post_type'      => 'tps',
					'post_status'    => 'publish',
					'post__not_in'   => array( $wp_query->queried_object_id ),
					'orderby'        => 'meta_value_num',
					'meta_key'       => 'tps_hits',
					'posts_per_page' => $instance['limit'],
					'order'          => $instance['order'] ? 'DESC' : 'ASC',
					'tax_query'      => array(
						array(
							'taxonomy' => 'tps_category',
							'field'    => 'id',
							'terms'    => $tps_category
						)
					)
				);
			} else {
				$args_topic = array(
					'post_type'      => 'tps',
					'post_status'    => 'publish',
					'post__not_in'   => array( $wp_query->queried_object_id ),
					'orderby'        => $instance['order_by'] ? 'date' : 'title',
					'posts_per_page' => $instance['limit'],
					'order'          => $instance['order'] ? 'DESC' : 'ASC',
					'tax_query'      => array(
						array(
							'taxonomy' => 'tps_category',
							'field'    => 'id',
							'terms'    => $tps_category
						)
					)
				);
			}
		} else {
			if ( $instance['order_by'] == 2 ) {
				$args_topic = array(
					'post_type'      => 'tps',
					'post_status'    => 'publish',
					'meta_key'       => 'tps_hits',
					'orderby'        => 'meta_value_num',
					'posts_per_page' => $instance['limit'],
					'order'          => $instance['order'] ? 'DESC' : 'ASC',
					'tax_query'      => array(
						array(
							'taxonomy' => 'tps_category',
							'field'    => 'id',
							'terms'    => $instance['category']
						)
					)
				);
			} else {
				$args_topic = array(
					'post_type'      => 'tps',
					'post_status'    => 'publish',
					'orderby'        => $instance['order_by'] ? 'date' : 'title',
					'posts_per_page' => $instance['limit'],
					'order'          => $instance['order'] ? 'DESC' : 'ASC',
					'tax_query'      => array(
						array(
							'taxonomy' => 'tps_category',
							'field'    => 'id',
							'terms'    => $instance['category']
						)
					)
				);
			}

		}

		echo $args['before_widget'];

		$topics = new WP_Query( $args_topic );

		$topics = $topics->posts;

		if ( count( $topics ) > 0 ) {
			?>
			<?php
			if ( $instance['title'] ) {
				echo "<h3>{$instance['title']}</h3>";
			}
			?>

			<div class="row">
				<div class="col-md-12">
					<ul class="my_list_unassigned">
						<?php foreach ( $topics as $topic ) { ?>
							<li>
								<i class="fa fa-file-text-o fa-fw"></i>
								<a href="<?php echo get_post_permalink( $topic->ID ) ?>"><?php echo $topic->post_title ?></a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		<?php
		}
		echo $args['after_widget'];
	}
}

/**
 * Show Topic Assigned
 */
class TPSRelatedKB extends WP_Widget {
	function TPSRelatedKB() {
		$widget_ops = array(
			'classname'   => 'TPSRelatedKB_class',
			'description' => __( 'Show All Related Knowledge Base for each forum.', 'tps' )
		);
		$this->WP_Widget( 'TPSRelatedKB', '(TPS) Related Knowledge Base', $widget_ops );
	}

	function form( $instance ) {

		$defaults = array(
			'title' => 'Related Knowledge Base',
			'limit' => '30'
		);
		@$instance = wp_parse_args( (array) $instance, $defaults );
		$title = $instance['title'];
		$limit = $instance['limit'];
		?>

		<p>
			<label><?php echo __( 'Title', 'tps' ); ?></label><br />
			<input class="form-control" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label><?php echo __( 'Limit', 'tps' ); ?></label><br />
			<input class="form-control" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>" />
		</p>

	<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance          = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['limit'] = sanitize_text_field( $new_instance['limit'] );

		return $instance;
	}

	function widget( $args, $instance ) {
		echo $args['before_widget'];

		extract( $args );

		if ( get_post_type() == 'forum' || get_post_type() == 'topic' ) {
			$forum_id = bbp_get_forum_id();
		} else {
			return;
		}
		$forum_params = get_post_meta( $forum_id, '_tps_params', true );
		if ( isset( $forum_params['related_kb'] ) ) {
			if ( $forum_params['related_kb'] ) {
				$kb_id = $forum_params['related_kb'];
			} else {
				return;
			}
		} else {
			return;
		}
		$args_kb   = array(
			'post_type'      => 'tps',
			'post_status'    => 'publish',
			'orderby'        => 'date',
			'order'          => 'DESC',
			'posts_per_page' => $instance['limit'],
			'tax_query'      => array(
				array(
					'taxonomy' => 'tps_category',
					'field'    => 'id',
					'terms'    => array( $kb_id )
				)
			)
		);
		$kb_result = new WP_Query( $args_kb );
		if ( $kb_result->have_posts() ) {
			$topics = $kb_result->posts;
		} else {
			$topics = array();
		}
		if ( count( $topics ) > 0 ) {
			?>
			<?php
			if ( $instance['title'] ) {
				echo "<h3>{$instance['title']}</h3>";
			}
			?>

			<div class="row">
				<div class="col-md-12">
					<ul class="related_kb">
						<?php foreach ( $topics as $topic ) { ?>
							<li>
								<i class="fa fa-fw fa-file-text-o"></i>
								<a href="<?php echo get_post_permalink( $topic->ID ) ?>"><?php echo $topic->post_title ?></a>
							</li>
						<?php } ?>
						<li>
							<i class="fa fa-fw fa-angle-double-right"></i>
							<?php $kb_cate = get_term($kb_id,'tps_category'); ?>
							<a href="<?php echo get_term_link($kb_cate) ?>"><?php _e( 'View All', 'tps' ) ?></a>
						</li>
					</ul>
				</div>
			</div>
		<?php
		}
		echo $args['after_widget'];
	}
}

?>