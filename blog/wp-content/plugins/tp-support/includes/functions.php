<?php
function is_tps() {
	global $post;
	if ( in_array( get_post_type(), array( 'tps', 'tps-faq' ) ) ) {
		return true;
	}

	return false;
}

/**
 * Get template part (for templates like the shop-loop).
 *
 * @access public
 *
 * @param mixed  $slug
 * @param string $name (default: '')
 *
 * @return void
 */
function tps_get_template_part( $slug, $name = '' ) {
	$template = '';

	// Get default slug-name.php
	if ( $name && $slug ) {
		$template = locate_template( array( "tp-support/{$slug}-{$name}.php" ), false, false );
	}

	if ( ! $template && $slug ) {
		$template = locate_template( array( "tp-support/{$slug}.php" ), false, false );
	}

	if ( ! $template && $name && $slug && file_exists( TPS_DIR . "templates/{$slug}-{$name}.php" ) ) {
		$template = TPS_DIR . "templates" . DIRECTORY_SEPARATOR . "{$slug}-{$name}.php";

	}

	if ( ! $template && $slug && file_exists( TPS_DIR . "templates" . DIRECTORY_SEPARATOR . "{$slug}.php" ) ) {

		$template = TPS_DIR . "templates" . DIRECTORY_SEPARATOR . "{$slug}.php";

	}
	if ( $template ) {
		load_template( $template, false );
	}

}

/**
 * Function replace keyword in paragraph
 *
 * @param string $keyword
 * @param string $string
 *
 * @return string
 */
function tps_search_highlight( $keyword = '', $string = '' ) {
	if ( $keyword ) {
		$keyword = strip_tags( $keyword );
		$string  = preg_replace( "/{$keyword}/iu", "<span class='tps-lighter'>$0</span>", $string );
	}

	return $string;
}

/**
 * Create breadcrum
 */
function tps_breadcrumb() {

	/* === OPTIONS === */
	$text['home']     = 'Home'; // text for the 'Home' link
	$text['category'] = '%s'; // text for a category page
	$text['search']   = 'Search Results for "%s" Query'; // text for a search results page
	$text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
	$text['author']   = 'Articles Posted by %s'; // text for an author page
	$text['404']      = 'Error 404'; // text for the 404 page

	$show_current   = 1; // 1 - show current post/page/category title in breadcrumbs, 0 - don't show
	$show_on_home   = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show
	$show_title     = 1; // 1 - show the title for the links, 0 - don't show
	$delimiter      = ' <i class="separator"></i> '; // delimiter between crumbs
	$before         = '<span class="current">'; // tag before the current crumb
	$after          = '</span>'; // tag after the current crumb
	/* === END OF OPTIONS === */

	global $post, $wp_query;
	$home_link    = home_url( '/' );
	$link_before  = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
	$link_after   = '</span>';
	$link_attr    = ' itemprop="url"';
	$link         = $link_before . '<a' . $link_attr . ' href="%1$s"><span itemprop="title">%2$s</span></a>' . $link_after;
	$parent_id    = $parent_id_2 = $post->post_parent;
	$frontpage_id = get_option( 'page_on_front' );

	if ( is_home() || is_front_page() ) {

		if ( $show_on_home == 1 ) {
			echo '<div class="breadcrumbs">' . $link_before . '<a href="' . $home_link . '"><span itemprop="title">' . $text['home'] . '</span></a>' . $link_after . '</div>';
		}

	} else {

		echo '<div class="breadcrumbs">';
		if ( $show_home_link == 1 ) {
			echo $link_before . '<a href="' . $home_link . '"> <span itemprop="title">' . $text['home'] . '</span></a>' . $link_after;
			if ( $frontpage_id == 0 || $parent_id != $frontpage_id ) {
				echo $delimiter;
			}
		}

		if ( isset( $wp_query->query_vars['tps_category'] ) || isset( $wp_query->query_vars['tps_tag'] ) ) {
			$term_id = get_queried_object()->term_id;
			if ( isset( $wp_query->query_vars['tps_category'] ) ) {
				$this_cat = get_term( $term_id, 'tps_category' );
			} else {
				$this_cat = get_term( $term_id, 'tps_tag' );
			}
			$post_type = get_post_type_object( get_post_type() );
			$slug      = $post_type->rewrite;
			printf( $link, $home_link . $slug['slug'] . '/', $post_type->labels->singular_name );
			echo $delimiter;
			if ( $this_cat->parent != 0 ) {
				$cats = get_custom_category_parents( $this_cat->parent, 'tps_category', true, $delimiter, false );
				if ( $show_current == 0 ) {
					$cats = preg_replace( "#^(.+)$delimiter$#", "$1", $cats );
				}
				$cats = str_replace( '<a', $link_before . '<a' . $link_attr, $cats );
				$cats = str_replace( '</a>', '</a>' . $link_after, $cats );
				if ( $show_title == 0 ) {
					$cats = preg_replace( '/ title="(.*?)"/', '', $cats );
				}
				echo $cats;
			}
			if ( $show_current == 1 ) {
				echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
			}

		} elseif ( is_search() ) {
			echo $before . sprintf( $text['search'], get_search_query() ) . $after;

		} elseif ( is_day() ) {
			echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
			echo sprintf( $link, get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), get_the_time( 'F' ) ) . $delimiter;
			echo $before . get_the_time( 'd' ) . $after;

		} elseif ( is_month() ) {
			echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
			echo $before . get_the_time( 'F' ) . $after;

		} elseif ( is_year() ) {
			echo $before . get_the_time( 'Y' ) . $after;

		} elseif ( is_single() && ! is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object( get_post_type() );

				$term_ids = wp_get_post_terms( $post->ID, 'tps_category' );
				$slug     = $post_type->rewrite;
				printf( $link, $home_link . $slug['slug'] . '/', $post_type->labels->singular_name );
				echo $delimiter;
				if ( count( $term_ids ) > 0 ) {
					$this_cat = get_term( $term_ids[0], 'tps_category' );

					if ( $this_cat->parent != 0 ) {
						$cats = get_custom_category_parents( $this_cat->parent, 'tps_category', true, $delimiter, false );
						if ( $show_current == 0 ) {
							$cats = preg_replace( "#^(.+)$delimiter$#", "$1", $cats );
						}
						$cats = str_replace( '<a', $link_before . '<a' . $link_attr, $cats );
						$cats = str_replace( '</a>', '</a>' . $link_after, $cats );
						if ( $show_title == 0 ) {
							$cats = preg_replace( '/ title="(.*?)"/', '', $cats );
						}
						echo $cats;
					} else {
						$cats = '';
					}
				}
				printf( $link, get_term_link( $this_cat->term_id, 'tps_category' ), $this_cat->name );
				echo $delimiter;
				if ( $show_current == 1 ) {
					echo $before . get_the_title() . $after;
				}

			} else {
				$cat  = get_the_category();
				$cat  = $cat[0];
				$cats = get_category_parents( $cat, TRUE, $delimiter );
				if ( $show_current == 0 ) {
					$cats = preg_replace( "#^(.+)$delimiter$#", "$1", $cats );
				}
				$cats = str_replace( '<a', $link_before . '<a' . $link_attr, $cats );
				$cats = str_replace( '</a>', '</a>' . $link_after, $cats );
				if ( $show_title == 0 ) {
					$cats = preg_replace( '/ title="(.*?)"/', '', $cats );
				}
				echo $cats;
				if ( $show_current == 1 ) {
					echo $before . get_the_title() . $after;
				}
			}

		} elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' && ! is_404() ) {
			$post_type = get_post_type_object( get_post_type() );
			echo $before . $post_type->labels->singular_name . $after;

		} elseif ( is_attachment() ) {
			$parent = get_post( $parent_id );
			$cat    = get_the_category( $parent->ID );
			$cat    = $cat[0];
			$cats   = get_category_parents( $cat, TRUE, $delimiter );
			$cats   = str_replace( '<a', $link_before . '<a' . $link_attr, $cats );
			$cats   = str_replace( '</a>', '</a>' . $link_after, $cats );
			if ( $show_title == 0 ) {
				$cats = preg_replace( '/ title="(.*?)"/', '', $cats );
			}
			echo $cats;
			printf( $link, get_permalink( $parent ), $parent->post_title );
			if ( $show_current == 1 ) {
				echo $delimiter . $before . get_the_title() . $after;
			}

		} elseif ( is_page() && ! $parent_id ) {
			if ( $show_current == 1 ) {
				echo $before . get_the_title() . $after;
			}

		} elseif ( is_page() && $parent_id ) {
			if ( $parent_id != $frontpage_id ) {
				$breadcrumbs = array();
				while ( $parent_id ) {
					$page = get_page( $parent_id );
					if ( $parent_id != $frontpage_id ) {
						$breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
					}
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse( $breadcrumbs );
				for ( $i = 0; $i < count( $breadcrumbs ); $i ++ ) {
					echo $breadcrumbs[$i];
					if ( $i != count( $breadcrumbs ) - 1 ) {
						echo $delimiter;
					}
				}
			}
			if ( $show_current == 1 ) {
				if ( $show_home_link == 1 || ( $parent_id_2 != 0 && $parent_id_2 != $frontpage_id ) ) {
					echo $delimiter;
				}
				echo $before . get_the_title() . $after;
			}

		} elseif ( is_tag() ) {
			echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;

		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata( $author );
			echo $before . sprintf( $text['author'], $userdata->display_name ) . $after;

		} elseif ( is_404() ) {
			echo $before . $text['404'] . $after;
		}

		if ( get_query_var( 'paged' ) ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
				echo ' (';
			}
			echo __( 'Page' ) . ' ' . get_query_var( 'paged' );
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
				echo ')';
			}
		}

		echo '</div><!-- .breadcrumbs -->';

	}
}

function get_custom_category_parents( $id, $taxonomy = false, $link = false, $separator = '/', $nicename = false, $visited = array() ) {

	if ( ! ( $taxonomy && is_taxonomy_hierarchical( $taxonomy ) ) ) {
		return '';
	}

	$chain = '';
	// $parent = get_category( $id );
	$parent = get_term( $id, $taxonomy );
	if ( is_wp_error( $parent ) ) {
		return $parent;
	}

	if ( $nicename ) {
		$name = $parent->slug;
	} else {
		$name = $parent->name;
	}

	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && ! in_array( $parent->parent, $visited ) ) {
		$visited[] = $parent->parent;
		// $chain .= get_category_parents( $parent->parent, $link, $separator, $nicename, $visited );
		$chain .= get_custom_category_parents( $parent->parent, $taxonomy, $link, $separator, $nicename, $visited );
	}

	if ( $link ) {
		// $chain .= '<a href="' . esc_url( get_category_link( $parent->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$name.'</a>' . $separator;
		$chain .= '<a href="' . esc_url( get_term_link( (int) $parent->term_id, $taxonomy ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '"><span itemprop="title">' . $name . '</span></a>' . $separator;
	} else {
		$chain .= $name . $separator;
	}

	return $chain;
}

?>