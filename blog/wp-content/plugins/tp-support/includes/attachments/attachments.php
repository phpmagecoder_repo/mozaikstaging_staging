<?php
/**
 * This feature is a fork of GD bbPress Attachments plugin by Milan Petrovic
 * @url	https://wordpress.org/plugins/gd-bbpress-attachments/
*/
if ( ! defined( 'GDBBPRESSATTACHMENTS_CAP' ) ) {
	define( 'GDBBPRESSATTACHMENTS_CAP', 'activate_plugins' );
}

require_once( dirname( __FILE__ ) . '/code/defaults.php' );
require_once( dirname( __FILE__ ) . '/code/shared.php' );
require_once( dirname( __FILE__ ) . '/code/attachments/class.php' );
require_once( dirname( __FILE__ ) . '/code/public.php' );

?>