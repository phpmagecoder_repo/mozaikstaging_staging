<?php

if (!defined('ABSPATH')) exit;

require_once(GDBBPRESSATTACHMENTS_PATH.'code/attachments/admin.php');

class gdbbA_Admin {
    private $page_ids = array();
    private $admin_plugin = false;

    function __construct() {
        add_action('after_setup_theme', array($this, 'load'));
    }

    public function admin_init() {
        if (isset($_GET['page'])) {
            $this->admin_plugin = $_GET['page'] == 'gdbbpress_attachments';
        }

        if ($this->admin_plugin) {
            wp_enqueue_style('gd-bbpress-attachments', GDBBPRESSATTACHMENTS_URL."css/gd-bbpress-attachments-admin.css", array(), GDBBPRESSATTACHMENTS_VERSION);
        }

    }

    public function load() {
        add_action('admin_init', array(&$this, 'admin_init'));
        add_action('admin_menu', array(&$this, 'admin_menu'));
    }

    public function admin_menu() {
        $this->page_ids[] = add_submenu_page('edit.php?post_type=forum', 'BBPress Attachments Setting', __("Attachments Setting", "tps"), GDBBPRESSATTACHMENTS_CAP, 'tps_attachments', array($this, 'menu_attachments'));
    }


    public function menu_attachments() {
        global $gdbbpress_attachments;

        $options = $gdbbpress_attachments->o;
        $_user_roles = d4p_bbpress_get_user_roles();

        include(GDBBPRESSATTACHMENTS_PATH.'forms/panels.php');
    }
}

global $gdbbpress_a_admin;
$gdbbpress_a_admin = new gdbbA_Admin();

?>