<?php

$tabs = array(
	'attachments' => __( "Settings", "tps" )
);

?>
<div class="wrap">
	<h2><?php echo __( 'BBPress Attachments', 'tps' ) ?></h2>
	<div id="d4p-panel" class="d4p-panel-attachments">
		<?php include( GDBBPRESSATTACHMENTS_PATH . "forms/tabs/attachments.php" ); ?>
	</div>
</div>