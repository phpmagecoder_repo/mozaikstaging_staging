<?php
/*
Plugin Name: Thim HelpDesk
Plugin URI: http://thimpress.com
Description: Turn bbPress into a HelpDesk Support System for customers from Envato site and/or WooCommerce.
Version: 1.1.7.2
Author: Andy Ha (tu@thimpress.com)
Author URI: http://thimpress.com
Copyright 2007-2014 ThimPress.com. All rights reserved.
*/
ob_start();
define( 'TPS_DIR', WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . "tp-support" . DIRECTORY_SEPARATOR );
define( 'TPS_ADMIN', TPS_DIR . "admin" . DIRECTORY_SEPARATOR );
define( 'TPS_SITE', TPS_DIR . "site" . DIRECTORY_SEPARATOR );
define( 'TPS_LIBRARIES', TPS_DIR . "libraries" . DIRECTORY_SEPARATOR );
define( 'TPS_LANGUAGES', TPS_DIR . "languages" . DIRECTORY_SEPARATOR );
define( 'TPS_TEMPLATES', TPS_DIR . "templates" . DIRECTORY_SEPARATOR );
define( 'TPS_INCLUDES', TPS_DIR . "includes" . DIRECTORY_SEPARATOR );
define( 'TPS_ATTACHMENTS', TPS_INCLUDES . "attachments" . DIRECTORY_SEPARATOR );

define( 'TPS_CSS', WP_PLUGIN_URL . "/tp-support/css/" );
define( 'TPS_JS', WP_PLUGIN_URL . "/tp-support/js/" );
define( 'TPS_IMAGES', WP_PLUGIN_URL . "/tp-support/images/" );
if ( class_exists( 'bbPress' ) ) {
	if ( !class_exists( 'TPSAdmin' ) ) {
		require_once TPS_ADMIN . "admin.php";
	}
	if ( !class_exists( 'TPSSearchLog' ) ) {
		require_once TPS_ADMIN . "search.php";
	}
	if ( !class_exists( 'TPSBBPress' ) ) {
		require_once TPS_SITE . "bbpress.php";
	}
	if ( !class_exists( 'TPSCanned' ) ) {
		require_once TPS_SITE . "canned.php";
	}
	if ( !class_exists( 'TPSFaq' ) ) {
		require_once TPS_SITE . "faq.php";
	}
	if ( !class_exists( 'TPSTimeWork' ) ) {
		require_once TPS_DIR . "widget.php";
	}
	require_once TPS_INCLUDES . "functions.php";
	if ( !class_exists( 'gdbbPressAttachments_Defaults' ) ) {
		require_once TPS_ATTACHMENTS . "attachments.php";
	} else {
		add_action( 'admin_notices', array( 'TPSAdmin', 'attachment_plugin_notice' ) );
	}
	if ( !class_exists( 'TPS_WP_EnvatoAPI' ) ) {
		require_once TPS_LIBRARIES . 'class-envato-api.php';
		global $verifi;
		$envato = get_option( '_tps_params' );
		@$verifi = new TPS_WP_EnvatoAPI( $envato['e_username'], $envato['e_api_key'] );
		if ( isset( $envato['e_username'] ) && isset( $envato['e_api_key'] ) ) {

		} else {
			add_action( 'admin_notices', array( 'TPSAdmin', 'admin_notice' ) );
		}
	}
	new TPS();
} else {
	add_action( 'admin_notices', 'tps_global_note' );
	function tps_global_note() {
		?>
		<div class="">
			<p><?php _e( 'Please install <strong>BBPress</strong> plguin.', 'tps' ); ?></p>
		</div>
	<?php
	}
}

class TPS {
	public function __construct() {

		$initadmin = new TPSAdmin();

		$initbbpress = new TPSBBPress();

		$initFaq = new TPSFaq();

		register_activation_hook( __FILE__, array( $this, 'install' ) );
		register_deactivation_hook( __FILE__, array( $this, 'uninstall' ) );
		add_action( 'init', array( $this, 'site_init' ) );
		add_action( 'login_form_register', array( $this, 'register_form' ) );

		/*Add Order In Profile WooCommerce*/
		add_action( 'woocommerce_order_items_table', array( $this, 'update_order' ) );
		/*Add Order In Profile EDD*/
		add_action( 'edd_update_payment_status', array( $this, 'edd_update_order' ), 10, 3 );


		add_action( 'tps_content_footer', array( $this, 'update_hit' ) );
		add_action( 'widgets_init', array( $this, 'register_widgets' ) );

		add_action( 'wp_ajax_kbhelpful', array( $this, 'kbhelpful_callback' ) );
		add_action( 'wp_ajax_kbunhelpful', array( $this, 'kbunhelpful_callback' ) );
		add_action( 'wp_ajax_live_search', array( $this, 'live_search_callback' ) );
		add_action( 'wp_ajax_nopriv_live_search', array( $this, 'live_search_callback' ) );


		add_action( 'template_redirect', array( $this, 'template_redirect' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'initScript' ) );

		add_shortcode( 'check_purchased_code', array( $this, 'check_purchased_code' ) );
		//add_shortcode( 'tps_register_form', array( $this, 'tps_register_form' ) );

		add_filter( 'pre_get_posts', array( $this, 'filter_search' ) );
		// hook add_query_vars function into query_vars
		add_filter( 'query_vars', array( $this, 'add_query_vars' ) );

	}

	/**
	 * Update hit for each KB, FAQ
	 *
	 * @param $post_id
	 */
	function update_hit( $post_id ) {
		$hit = get_post_meta( $post_id, 'tps_hits', true );
		$hit ++;
		update_post_meta( $post_id, 'tps_hits', $hit );
	}

	/**
	 * Add Var on wp_query
	 *
	 * @param $aVars
	 *
	 * @return array
	 */
	function add_query_vars( $aVars ) {
		$aVars[] = "envato_user"; // represents the name of the product category as shown in the URL
		return $aVars;
	}

	/**
	 * TPS Init front end
	 */
	function site_init() {
		global $wp_rewrite;
		$params = get_option( '_tps_params', array() );

		if ( !isset( $params['knowledge_base'] ) ) {
			$params['knowledge_base'] = 'knowledge-base';
		}
		if ( !isset( $params['faq'] ) ) {
			$params['faq'] = 'faq';
		}
		if ( !isset( $params['envato_user'] ) ) {
			$params['envato_user'] = 'envato-user';
		}
		add_rewrite_rule( $params['knowledge_base'] . '/?$', 'index.php?post_type=tps', 'top' );
		add_rewrite_rule( $params['faq'] . '/?$', 'index.php?post_type=tps-faq', 'top' );
		add_rewrite_rule( $params['envato_user'] . '/?$', 'index.php?post_type=tps&envato_user=search', 'top' );

		$wp_rewrite->extra_permastructs['tps']['struct']          = '/' . $params['knowledge_base'] . '/%tps%';
		$wp_rewrite->extra_permastructs['tps_category']['struct'] = '/knowledge-base-category/%tps_category%';
		$wp_rewrite->extra_permastructs['tps-faq']['struct']      = '/' . $params['faq'] . '/';
		$wp_rewrite->extra_permastructs['tps_tag']['struct']      = '/knowledge-base-tag/%tps_tag%';

	}

	/**
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	function filter_search( $query ) {
		if ( $query->query_vars['s'] ) {
			$query->set( 'post_type', array( 'post', 'tps', 'tps-faq', 'topic' ) );
			$query->set( 'orderby', 'date' );
			$query->set( 'order', 'DESC' );
			$query->set( 'posts_per_page', 15 );
		};

		return $query;
	}

	/**
	 * Ajax live search
	 */
	function live_search_callback() {
		global $wpdb;
		$keyword    = filter_input( INPUT_POST, 'keyword', FILTER_SANITIZE_STRING );
		$params     = get_option( '_tps_params', array() );
		$check_from = $_POST['from'];
		$newdata    = array();
		if ( $keyword ) {
			$my_keyword = array(
				'post_title'  => $keyword,
				'post_status' => 'publish',
				'post_type'   => 'tps-search'
			);
			if ( isset( $params['search_log'] ) ) {
				if ( $params['search_log'] && $check_from == 'search' ) {
					wp_insert_post( $my_keyword );
				}
			}
			$keyword = strtoupper( $keyword );
			$ids     = $wpdb->get_col( "SELECT ID FROM $wpdb->posts WHERE (UCASE(post_title) LIKE '%$keyword%' OR UCASE(post_content) LIKE '%$keyword%' )AND post_type IN ('post','tps','topic','tps-faq') AND post_status='publish'" );
			if ( count( $ids ) > 0 ) {
				$showmore = false;
				if ( count( $ids ) > 6 ) {
					$showmore = true;
				}
				$search_query = array(
					'post__in'       => $ids,
					'order'          => 'DESC',
					'orderby'        => 'date',
					'post_type'      => array( 'post', 'tps', 'topic', 'tps-faq' ),
					'posts_per_page' => 6
				);

				$search = new WP_Query( $search_query );
				$search = $search->posts;

				foreach ( $search as $kb ) {
					if ( $kb->post_type == 'tps-faq' ) {

						$faq_cate  = wp_get_post_terms( $kb->ID, 'tps_faq_category' );
						$cate_id   = count( $faq_cate ) ? $faq_cate[0]->term_id : '';
						$newdata[] = array(
							'id'        => $kb->ID,
							'title'     => tps_search_highlight( $keyword, $kb->post_title ),
							'guid'      => get_site_url() . '/' . $params['faq'] . '/#faq-' . $cate_id . $kb->ID,
							'date'      => mysql2date( 'M d Y', $kb->post_date ),
							'post_type' => $kb->post_type
						);
					} else {
						$newdata[] = array(
							'id'        => $kb->ID,
							'title'     => tps_search_highlight( $keyword, $kb->post_title ),
							'guid'      => get_permalink( $kb->ID ),
							'date'      => mysql2date( 'M d Y', $kb->post_date ),
							'post_type' => $kb->post_type
						);
					}
				}
				if ( $showmore ) {
					$newdata[] = array(
						'id'        => '',
						'title'     => __( 'Show more', 'tps' ),
						'guid'      => get_search_link( $keyword ),
						'date'      => '',
						'post_type' => 'showmore'
					);
				}
			} else {
				$newdata[] = array(
					'id'        => '',
					'title'     => __( 'Your search did not match any faqs, knowledge base or forum topics.', 'tps' ),
					'guid'      => '#',
					'date'      => '',
					'post_type' => ''
				);
			}
			//ob_end_clean();
			echo json_encode( $newdata );
		}
		die(); // this is required to return a proper result
	}

	/**
	 * Add helpful
	 */
	function kbhelpful_callback() {
		global $current_user;

		$id = $_POST['id'];
		if ( !isset( $id ) ) {
			$msg['check'] = 'error';
			echo json_encode( $msg );
			die;
		}

		$helpful   = get_post_meta( $id, '_tps_helpful', true );
		$helpful[] = $current_user->ID;
		@$helpful = array_unique( $helpful );
		update_post_meta( $id, '_tps_helpful', $helpful );

		$unhelpful = get_post_meta( $id, '_tps_unhelpful', true );
		if ( $unhelpful ) {
			if ( in_array( $current_user->ID, $unhelpful ) ) {
				$key = array_search( $current_user->ID, $unhelpful );
				unset( $unhelpful[$key] );
			}
		}
		update_post_meta( $id, '_tps_unhelpful', $unhelpful );
		$msg['check'] = 'done';
		echo json_encode( $msg );
		die;
	}

	/**
	 * Function KB unhelpful
	 */
	function kbunhelpful_callback() {
		global $current_user;

		$id = $_POST['id'];
		if ( !isset( $id ) ) {
			$msg['check'] = 'error';
			echo json_encode( $msg );
			die;
		}
		$unhelpful   = get_post_meta( $id, '_tps_unhelpful', true );
		$unhelpful[] = $current_user->ID;
		@$unhelpful = array_unique( $unhelpful );
		update_post_meta( $id, '_tps_unhelpful', $unhelpful );

		$helpful = get_post_meta( $id, '_tps_helpful', true );
		if ( in_array( $current_user->ID, $helpful ) ) {
			$key = array_search( $current_user->ID, $helpful );
			unset( $helpful[$key] );
		}
		update_post_meta( $id, '_tps_helpful', $helpful );
		$msg['check'] = 'done';
		echo json_encode( $msg );
		die;
	}

	/*Register Wibget*/
	function register_widgets() {
		register_widget( 'TPSTimeWork' );
		register_widget( 'TPSSearch' );
		register_widget( 'TPSTopicAssigned' );
		register_widget( 'TPSTopicUnassigned' );
		register_widget( 'TPSKBItems' );
		register_widget( 'TPSRelatedKB' );

	}

	/**
	 * Init Script
	 */
	function initScript() {
		if ( get_post_type() == 'tps' || get_post_type() == 'tps-faq' ) {
//			wp_enqueue_script( 'jquery-ui-core' );
//			wp_enqueue_script( 'jquery-ui-accordion' );
			if ( get_post_type() == 'tps-faq' ) {
				wp_enqueue_script( 'tps-faq', TPS_JS . 'tps-faq.js', array(), false, false );
				wp_enqueue_style( 'tps-faq', TPS_CSS . 'tps-faq.css' );
			} elseif ( get_post_type() == 'tps' ) {
				wp_enqueue_script( 'tps', TPS_JS . 'tps.js', array(), false, false );
			}
//			wp_enqueue_style( 'jquery-ui-core', '//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css' );
//			wp_enqueue_style( 'tps', TPS_CSS . 'tps.css' );
		}


	}

	/**
	 * Set template for each page
	 */
	function template_redirect() {
		global $wp_query;
//		print_r($wp_query);die;
		if ( !trim( $wp_query->query_vars['s'] ) ) {
			if ( 'tps' == get_post_type() && isset( $wp_query->query_vars['tps_category'] ) ) {
				tps_get_template_part( 'category' );
				exit();
			} elseif ( 'tps' == get_post_type() && isset( $wp_query->query_vars['tps'] ) ) {
				tps_get_template_part( 'content' );
				exit();
			} elseif ( isset( $wp_query->query_vars['envato_user'] ) && 'tps' == get_post_type() ) {
				if ( current_user_can( 'moderate' ) ) {
					tps_get_template_part( 'search', 'user' );
					exit();
				}
			} elseif ( isset( $wp_query->query_vars['tps_tag'] ) ) {
				tps_get_template_part( 'tags' );
				exit();
			} elseif ( 'tps' == get_post_type() ) {
				tps_get_template_part( 'knowledgebase' );
				exit();
			} elseif ( 'tps-faq' == get_post_type() ) {
				tps_get_template_part( 'faq' );
				exit();
			}
		}


		return;
	}


	/**
	 * Register form shortcode
	 * @return string
	 */
	function tps_register_form() {
		if ( is_user_logged_in() ) {
			wp_redirect( site_url() );
			die;
		}
		$short_code = '';

		return $short_code;
	}

	/**
	 *
	 * @param $order
	 */
	function edd_update_order( $payment_id, $new_status, $old_status ) {
		if ( $old_status == 'publish' || $old_status == 'complete' ) {
			return;
		} // Make sure that payments are only completed once

		// Make sure the payment completion is only processed when new status is complete
		if ( $new_status != 'publish' && $new_status != 'complete' ) {
			return;
		}

		$creation_date = get_post_field( 'post_date', $payment_id, 'raw' );

		$customer_id = edd_get_payment_customer_id( $payment_id );
		$_downloads  = edd_get_payment_meta_cart_details( $payment_id );

		$user_params = get_user_meta( $customer_id, '_purchase_code', true );

		foreach ( $_downloads as $_download ) {
			$user_params[] = array(
				'purchase_code' => $payment_id,
				'item_name'     => $_download['name'],
				'item_id'       => 'edd_' . $_download['id'],
				'created_at'    => $creation_date
			);
		}
		$user_params = $this->_purchase_unique( $user_params );

		update_user_meta( $customer_id, '_purchase_code', $user_params );
	}

	/**
	 * Function update user profile when buy item successfully.
	 *
	 * @param $order
	 */
	function update_order( $order ) {
		if ( !is_user_logged_in() ) {
			return;
		}
		global $current_user;

		$_products   = $order->get_items();
		$user_params = get_user_meta( $current_user->ID, '_purchase_code', true );
		foreach ( $_products as $_product ) {
			$item          = $order->get_product_from_item( $_product );
			$user_params[] = array(
				'purchase_code' => $order->id,
				'item_name'     => $item->post->post_title,
				'item_id'       => 'woo_' . $item->id,
				'created_at'    => $item->post->post_date
			);
		}

		$user_params = $this->_purchase_unique( $user_params );
		if ( $order->has_status( 'completed' ) ) {
			update_user_meta( $current_user->ID, '_purchase_code', $user_params );
		}
	}

	/**
	 * Function Array unique
	 *
	 * @param $params
	 *
	 * @return array
	 */
	function _purchase_unique( $params ) {
		$keys    = array();
		$newData = array();
		foreach ( $params as $param ) {
			if ( !in_array( $param['item_id'], $keys ) ) {
				$newData[] = $param;
				$keys[]    = $param['item_id'];
			}
		}

		return $newData;
	}

	/**
	 * @return string
	 */
	function check_purchased_code() {
		global $post, $current_user;
		$shortcode = '';
		$str_error = '';
		$errors    = new WP_Error();

		if ( !is_user_logged_in() ) {
			wp_redirect( wp_login_url( get_the_permalink( $post->ID ) ) );
		}

		$errors      = $this->submit_purchased_code();
		$redirect_to = filter_input( INPUT_GET, 'redirect_to', FILTER_SANITIZE_STRING );

		if ( $redirect_to ) {
			@$redirect_to['redirect_to'] = $redirect_to;
		} else {
			$redirect_to = array();
		}

		if ( $errors ) {

			if ( !is_object( $errors ) ) {
				if ( filter_input( INPUT_GET, 'redirect_to', FILTER_SANITIZE_STRING ) ) {
					$redirect_url = base64_decode( filter_input( INPUT_GET, 'redirect_to', FILTER_SANITIZE_STRING ) );
					ob_end_clean();
					wp_redirect( $redirect_url );

				} else {
					ob_end_clean();
					wp_redirect( get_site_url() );
				}
				die;
			} else {

				$codes      = array();
				$errors_arg = array();

				foreach ( $errors->get_error_codes() as $code ) {
					if ( in_array( $code, $codes ) ) {
						continue;
					}
					$message = $errors->get_error_message( $code );
					if ( $message ) {
						$errors_arg[] = $message;
					}
					$codes[] = $code;
				}
				if ( count( $errors_arg ) > 0 ) {
					$str_error = implode( '<br/>', $errors_arg );
				}
			}
		}

		$shortcode = '<p>' . $str_error . '</p><form class="form-check-purchasedcode" method="post" action="' . add_query_arg( $redirect_to, get_the_permalink( $post->ID ) ) . '">
			<p>' . __( 'Please enter your Envato Purchased Code or Download Order ID to activate your support role.', 'tps' ) . '</p>
			<p>' . __( 'You can see all your Purchase codes at  your profile page. Please click', 'tps' ) . '<a target="_blank" href="' . bbp_get_user_profile_url( $current_user->ID ) . '"> <b> here </b></a></p>
			<input type="text" name="purchase_code" class="form-control">' . wp_nonce_field( 'check_purchase_code', 'check_purchase_code_form' ) . '
			<input class="" type="submit" value=" ' . __( 'Submit', 'tps' ) . ' ">
		</form>';

		return $shortcode;
	}

	/**
	 * Update purchased code
	 */
	function submit_purchased_code() {
		global $current_user;
		$user_id      = $current_user->ID;
		$download_ids = $result = array();
		$errors       = new WP_Error();
		if ( !isset( $_POST['check_purchase_code_form'] ) ) {
			return;
		}
		// Verify that the nonce is valid.
		if ( !wp_verify_nonce( $_POST['check_purchase_code_form'], 'check_purchase_code' ) ) {
			// Check the purchase code
			return;
		}
		$purchase_code = filter_input( INPUT_POST, 'purchase_code', FILTER_SANITIZE_STRING );

		if ( $purchase_code == '' ) {

			$errors->add( 'empty_purchase_code', __( '<strong>ERROR</strong>: Please enter your purchase code', 'tps' ) );


		} else {
			preg_match( '/^(\d)*$/i', $purchase_code, $result );
			if ( count( array_filter( $result ) ) ) {
				$downloads = edd_get_payment_meta( $purchase_code );
				$status    = edd_get_payment_status( get_post( $purchase_code ), true );
				//print_r($downloads);die;
//				die;
				if ( isset( $downloads['user_info'] ) && 'complete' == strtolower( $status ) ) {

					if ( $current_user->data->user_email == $downloads['user_info']['email'] ) {
						$carts = $downloads['cart_details'];
						foreach ( $carts as $cart ) {
							$item                  = array();
							$item['purchase_code'] = $purchase_code;
							$item['item_name']     = $cart['name'];
							$item['item_id']       = 'edd_' . $cart['id'];
							$item['created_at']    = $downloads['date'];

							$download_ids[] = $item;
						}
					} else {
						$errors->add( 'invalid_purchase_code', __( '<strong>ERROR</strong>: Please enter a valid Download Order ID', 'tps' ) );
					}
				} else {
					$errors->add( 'invalid_purchase_code', __( '<strong>ERROR</strong>: Please enter a valid Download Order ID', 'tps' ) );
				}
			} else {
				if ( !$this->validate_api( $purchase_code, true ) ) {

					$errors->add( 'invalid_purchase_code', __( '<strong>ERROR</strong>: Please enter a valid purchase code', 'tps' ) );

				} elseif ( $this->purchase_exists( $purchase_code ) ) {

					$errors->add( 'used_purchase_code', __( '<strong>ERROR</strong>: Sorry this purchase code exists', 'tps' ) );

				}
			}
		}
		$errors = apply_filters( 'purchased_code_errors', $errors );
		if ( $errors->get_error_code() ) {
			return $errors;
		}


		/*Get meta and update order in user*/
		$meta = array();
		$meta = get_user_meta( $user_id, '_purchase_code', true );
		if ( count( array_filter( $result ) ) ) {
			if ( is_array( $meta ) ) {
				$meta = array_merge( $meta, $download_ids );
			} else {
				$meta = $download_ids;
			}
		} else {
			$meta[] = $this->get_purchase_data( $purchase_code );
		}
		$meta = $this->_purchase_unique( $meta );
		//Add all meta to db
		update_user_meta( $user_id, '_purchase_code', $meta );

		return true;
	}

	/**
	 * Function check 2 arrays
	 *
	 * @param array $args
	 * @param array $args2
	 *
	 * @return bool
	 */
	function checkArray( $args = array(), $args2 = array() ) {
		foreach ( $args as $arg ) {
			if ( in_array( $arg, $args2 ) ) {
				return true;
			}
		}

		return false;
	}

	function getUserPurchaseCode() {
		global $current_user;
		$purchase_ids = array();
		$user_params  = get_user_meta( $current_user->ID, '_purchase_code', true );
		if ( is_array( $user_params ) ) {
			if ( count( array_filter( $user_params ) ) > 0 ) {
				foreach ( $user_params as $user_param ) {
					$purchase_ids[] = $user_param['item_id'];
				}
			}
		}

		return array_unique( $purchase_ids );
	}

	/**
	 * This will override the default registration form
	 *
	 * Make sure all default WordPress actions and filters are intact to avoid breakage
	 *
	 * @since  0.3
	 * @access public
	 * @return mixed
	 */
	function register_form() {
		$params = get_option( '_tps_params', array() );
		if ( isset( $params['use_register_default'] ) ) {
			if ( $params['use_register_default'] ) {
				return;
			}
		}
		if ( $_REQUEST['action'] = 'register' ) {

			$http_post = ( 'POST' == $_SERVER['REQUEST_METHOD'] );

			$errors = new WP_Error();

			if ( is_multisite() ) {

				// Multisite uses wp-signup.php
				wp_redirect( apply_filters( 'wp_signup_location', network_site_url( 'wp-signup.php' ) ) );

				exit;
			}

			if ( !get_option( 'users_can_register' ) ) {

				wp_redirect( site_url( 'wp-login.php?registration=disabled' ) );

				exit();

			}

			$user_login = '';

			$user_email = '';

			$user_pass = '';

			$confirm_pass = '';

			$purchase_code = '';

			if ( $http_post ) {

				$user_login = $_POST['user_login'];

				$user_email = $_POST['user_email'];

				$user_pass = $_POST['user_pass'];

				$confirm_pass = $_POST['confirm_pass'];

				$purchase_code = $_POST['purchase_code'];

				$errors = $this->verifi_register_user( $user_login, $user_email, $user_pass, $confirm_pass, $purchase_code );


				if ( !is_wp_error( $errors ) ) {

					$params = get_option( '_tps_params' );

					$redirect_url = $params['redirect_url'];

					if ( isset( $redirect_url ) ) {
						$redirect_url = home_url( "/" );
					}

					$redirect_to = apply_filters( 'verifi_redirect', !empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : $redirect_url );

					wp_safe_redirect( $redirect_to );

					exit();

				}

			}

			$redirect_to = apply_filters( 'registration_redirect', !empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '' );

			login_header( __( 'Registration Form' ), '<p class="message register">' . __( 'Register For This Site' ) . '</p>', $errors );

			?>

			<form name="registerform" id="registerform" action="<?php echo esc_url( site_url( 'wp-login.php?action=register', 'login_post' ) ); ?>" method="post">

				<p>

					<label for="user_login"><?php _e( 'Username', 'tps' ) ?><br />

						<input type="text" name="user_login" id="user_login" class="input cw-username" value="<?php echo esc_attr( stripslashes( $user_login ) ); ?>" size="20" /></label>

				</p>

				<p>

					<label for="user_email"><?php _e( 'E-mail', 'tps' ) ?><br />

						<input type="text" name="user_email" id="user_email" class="input" value="<?php echo esc_attr( stripslashes( $user_email ) ); ?>" size="25" /></label>

				</p>

				<p>

					<label for="user_pass"><?php _e( 'Password', 'tps' ) ?><br />

						<input type="password" name="user_pass" id="user_pass" class="input cw-pass" value="<?php echo esc_attr( stripslashes( $user_pass ) ); ?>" size="25" /></label>

				</p>

				<p>

					<label for="confirm_pass"><?php _e( 'Confirm Password', 'tps' ) ?><br />

						<input type="password" name="confirm_pass" id="confirm_pass" class="input cw-confirm" value="<?php echo esc_attr( stripslashes( $confirm_pass ) ); ?>" size="25" /></label>

				</p>

				

				<?php do_action( 'register_form' ); ?>

				<p><?php _e( 'Password must be at least 7 characters', 'tps' ); ?></p>

				<br class="clear" />

				<input type="hidden" name="redirect_to" value="<?php echo esc_attr( $redirect_to ); ?>" />

				<p class="submit">
					<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e( 'Register' ); ?>" />
				</p>

			</form>

			<p id="nav">

				<a href="<?php echo esc_url( wp_login_url() ); ?>"><?php _e( 'Log in' ); ?></a>

				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" title="<?php esc_attr_e( 'Password Lost and Found' ) ?>"><?php _e( 'Lost your password?' ); ?></a>
			</p>

			<?php

			login_footer( 'user_login' );

			//This prevents the switch from running and duplicating login form
			exit;

		}

	}

	/**
	 * When active plugin Function will be call
	 */
	public function install() {
		global $wp_version;
		If ( version_compare( $wp_version, "2.9", "<" ) ) {
			deactivate_plugins( basename( __FILE__ ) ); // Deactivate our plugin
			wp_die( "This plugin requires WordPress version 2.9 or higher." );
		}
	}

	/**
	 * When deactive function will be call
	 */
	public function uninstall() {

	}

	/**
	 * Checks to see purchase already exists
	 *
	 * @since  0.1
	 * @uses   WP_User_Query
	 * @access public
	 *
	 * @param mixed $cw_purcahse_code
	 *
	 * @return bool
	 */
	function purchase_exists( $input ) {

		// Query for users based on the meta data
		$user_query = new WP_User_Query(
			array(
				'meta_query' => array(
					array(
						'key'     => '_purchase_code',
						'value'   => strval( $input ),
						'compare' => 'like',
					)
				)

			)
		);

		if ( $users = $user_query->get_results() ) {

			return true;

		} else {

			return false;

		}

	}

	/**
	 * Check if a API retruns buyer, Uses Envato Marketplace Class
	 *
	 * @since  0.1
	 * @access public
	 *
	 * @param mixed $cw_purcahse_code
	 *
	 * @return bool
	 */
	function validate_api( $purcahse_code ) {

		global $verifi;

		$verify = $verifi->verify_purchase( $purcahse_code );

		if ( isset( $verify->buyer ) ) {

			return true;

		} else {

			return false;

		}

	}

	/**
	 * Pulls all data from API and returns array.
	 *
	 * @since  0.2
	 * @access public
	 *
	 * @param mixed $purchase_code
	 *
	 * @return array
	 */
	function get_purchase_data( $purchase_code ) {

		global $verifi;

		$api_check = $verifi->verify_purchase( $purchase_code );

		$meta = array(
			"purchase_code" => $purchase_code,
			"item_name"     => $api_check->item_name,
			"item_id"       => $api_check->item_id,
			"created_at"    => $api_check->created_at,
			"buyer"         => $api_check->buyer,
			"licence"       => $api_check->licence
		);

		return $meta;

	}

	/**
	 * Custom function to register users.
	 *
	 * In prior versions this function was used in multiple classes, we're getting rid of that excess code!
	 *
	 * @since  0.4
	 * @access public
	 * @return void
	 */
	function verifi_register_user( $user_login, $user_email, $user_pass, $confirm_pass, $purchase_code ) {

		$errors = new WP_Error();

		$sanitized_user_login = sanitize_user( $user_login );

		$user_email = apply_filters( 'user_registration_email', $user_email );

		// Check the username
		if ( $sanitized_user_login == '' ) {

			$errors->add( 'empty_username', __( '<strong>ERROR</strong>: Please enter a username.', 'tps' ) );

		} elseif ( !validate_username( $user_login ) ) {

			$errors->add( 'invalid_username', __( '<strong>ERROR</strong>: This username is invalid because it uses illegal characters. Please enter a valid username.', 'tps' ) );

		} elseif ( username_exists( $sanitized_user_login ) ) {

			$errors->add( 'username_exists', __( '<strong>ERROR</strong>: This username is already registered. Please choose another one.', 'tps' ) );

		}

		if ( $sanitized_user_login === $user_pass ) {

			$errors->add( 'bad_combo', __( '<strong>ERROR</strong>: Your Username and Password cannot match.', 'tps' ) );


		}
		// Check the e-mail address
		if ( $user_email == '' ) {

			$errors->add( 'empty_email', __( '<strong>ERROR</strong>: Please type your e-mail address.', 'tps' ) );

		} elseif ( !is_email( $user_email ) ) {

			$errors->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.', 'tps' ) );

			$user_email = '';

		} elseif ( email_exists( $user_email ) ) {

			$errors->add( 'email_exists', __( '<strong>ERROR</strong>: This email is already registered, please choose another one.', 'tps' ) );
		}

		// Check the password fields

		if ( $user_pass == '' ) {

			$errors->add( 'empty_pass', __( '<strong>ERROR</strong>: Please enter a password.', 'tps' ) );

		} elseif ( $user_pass != $confirm_pass ) {

			$errors->add( 'pass_match', __( '<strong>ERROR</strong>: Your passwords dont match!', 'tps' ) );

		} elseif ( strlen( $user_pass ) < 6 ) {

			$errors->add( 'short_pass', __( '<strong>ERROR</strong>: Your Password is too short.', 'tps' ) );

		}

		if ( $confirm_pass == '' ) {

			$errors->add( 'empty_confirm', __( '<strong>ERROR</strong>: Please confirm your password', 'tps' ) );

		}
		// Check the purchase code
		$check_pur = null;
		if ( $purchase_code == '' ) {
			$check_pur = null;
		} elseif ( !$this->validate_api( $purchase_code, true ) ) {
			$check_pur = null;
		} elseif ( $this->purchase_exists( $purchase_code ) ) {
			$check_pur = null;
		}

		do_action( 'register_post', $sanitized_user_login, $user_email, $errors );
		$errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email, $user_pass, $confirm_pass );

		if ( $errors->get_error_code() ) {
			return $errors;
		}

		$user_id = wp_create_user( $sanitized_user_login, $user_pass, $user_email );

		if ( !$user_id ) {

			$errors->add( 'registerfail', sprintf( __( '<strong>ERROR</strong>: Couldn&#8217;t register you... please contact the <a href="mailto:%s">webmaster</a> !', 'tps' ), get_option( 'admin_email' ) ) );

			return $errors;

		}
//		if ( $check_pur ) {
			$meta   = array();
			$meta[] = $this->get_purchase_data( $purchase_code );

			//Add all meta to db
			update_user_meta( $user_id, '_purchase_code', $meta );
//		}
		wp_new_user_notification( $user_id );

		//Lets set a cookie for 60 minutes so we can display cool messages
		if ( !isset( $_COOKIE['verifi_new_user'] ) ) {
			ob_start();
			setcookie( 'verifi_new_user', 1, time() + ( 60 * 60 ), COOKIEPATH, COOKIE_DOMAIN, false );
			ob_end_flush();
		}

		$credentials                  = array();
		$credentials['user_login']    = $user_login;
		$credentials['user_password'] = $user_pass;
		$credentials['remember']      = true;
		wp_signon( $credentials );

		return $user_id;
	}
}


?>