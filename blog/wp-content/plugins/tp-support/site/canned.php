<?php

/*
Class Name: ThimPress Support Canned Response
Author: Andy Ha (tu@wpbriz.com)
Author URI: http://wpbriz.com
Copyright 2007-2014 wpBriz.com. All rights reserved.
*/

class TPSCanned {

	function __construct() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'scriptInitAdmin' ) );
		add_action( 'add_meta_boxes', array( $this, 'adding_meta_box' ) );
		add_action( 'save_post', array( $this, 'save_meta_box_data' ) );

		// Add our front end markup
		add_action( 'bbp_theme_before_reply_form_content', array( $this, 'reply_form' ) );

		add_filter( 'bbp_get_form_reply_content', array( $this, 'load_canned' ) );
	}

	function load_canned( $reply_content ) {
		if ( get_post_type() == 'reply' ) {
			return $reply_content;
		}
		if ( ! current_user_can( 'moderate' ) ) {
			return;
		}
		global $current_user, $post, $wpdb;
		$number_replis = $wpdb->get_var( "SELECT COUNT(ID) FROM $wpdb->posts WHERE `post_author` = {$current_user->ID} AND  `post_parent` = {$post->ID} AND post_type='reply' AND post_status='publish'", 0 );

		$args = array(
			'post_type'   => 'tps-canned',
			'author__in'  => array( $current_user->ID ),
			'post_status' => 'publish'
		);

		$canneds = get_posts( $args );
		$level   = array();
		if ( count( $canneds ) > 0 ) {
			$levels = array();

			foreach ( $canneds as $canned ) {
				$level    = get_post_meta( $canned->ID, '_tps_params', true );
				$levels[] = @$level['level'];
			}
			for ( $i = 0; $i < count( $levels ) - 1; $i ++ ) {
				for ( $j = $i + 1; $j < count( $levels ); $j ++ ) {
					if ( $levels[$j] < $levels[$i] ) {
						$tg         = $levels[$i];
						$levels[$i] = $levels[$j];
						$levels[$j] = $tg;

						$tgcanned    = $canneds[$i];
						$canneds[$i] = $canneds[$j];
						$canneds[$j] = $tgcanned;

					}
				}
			}
			$key = '';
			if ( in_array( $number_replis + 1, $levels ) ) {
				$key           = array_search( $number_replis + 1, $levels );
				$reply_content = $this->replaceReply( $canneds[$key]->post_content );
			} else {
				foreach ( $levels as $k => $level ) {
					if ( $level > $number_replis + 1 ) {
						$key = $k;
					}
				}

				if ( ! $key ) {
					$key = count( $levels ) - 1;
				}
				$reply_content = $this->replaceReply( $canneds[$key]->post_content );
			}

		} else {
			return;
		}


		return $reply_content;
	}

	/**
	 * Function init when run plugin+
	 */
	function init() {
		$this->register_post_type();
	}


	/**
	 * Init Script in Admin
	 */
	function scriptInitAdmin() {

		$post_type = filter_input( INPUT_GET, 'post_type', FILTER_SANITIZE_STRING );

		if ( get_post_type() == 'tps-canned' || $post_type == 'tps-canned' ) {

//			wp_enqueue_script( 'tps', TPS_JS . 'admin-tps.js', array(), false, false );

			wp_enqueue_style( 'tps', TPS_CSS . 'admin-tps.css' );
		}
	}

	/**
	 * Register post type
	 */
	function register_post_type() {

		if ( post_type_exists( 'tps-canned' ) ) {
			return;
		}
		if ( ! class_exists( 'tps' ) ) {
			return;
		}

		$labels = array(
			'name'               => _x( 'Canned Response', 'post type general name', 'tps' ),
			'singular_name'      => _x( 'Canned Response', 'post type singular name', 'tps' ),
			'add_new'            => __( 'Add New', 'tps' ),
			'add_new_item'       => __( 'Add New Canned Reply', 'tps' ),
			'edit_item'          => __( 'Edit Canned Reply', 'tps' ),
			'new_item'           => __( 'New Canned Reply', 'tps' ),
			'all_items'          => __( 'Canned Replies', 'tps' ),
			'view_item'          => __( 'View Canned Reply', 'tps' ),
			'search_items'       => __( 'Search Canned Replies', 'tps' ),
			'not_found'          => __( 'No Canned Replies found', 'tps' ),
			'not_found_in_trash' => __( 'No Canned Replies found in Trash', 'tps' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'Canned Replies', 'tps' )
		);

		$args = array(
			'labels'          => $labels,
			'public'          => false,
			'show_ui'         => true,
			'query_var'       => false,
			'rewrite'         => false,
			'capabilities'    => bbp_get_topic_caps(),
			'capability_type' => array( 'reply', 'replies' ),
			'supports'        => array( 'editor', 'title', 'author' ),
			'can_export'      => true,
			'menu_icon'       => 'dashicons-testimonial'
		);

		register_post_type( 'tps-canned', $args );

	}

	/**
	 * Add meta box
	 */
	function adding_meta_box() {
		add_meta_box(
			'tps_sectionid',
			__( 'Level', 'tps' ),
			array( $this, 'meta_box_callback' ), 'tps-canned', 'side', 'high' );
	}

	/**
	 * Assign in Canned
	 *
	 * @param $post
	 */
	function meta_box_callback( $post ) {

		$params = get_post_meta( $post->ID, '_tps_params', true );

		?>
		<div class="inside">

			<p>
				<label for="level"><?php echo __( 'Select Level ', 'tps' ); ?></label>

				<select name="tps[params][level]" class="chosen-select">
					<option value="0" <?php selected( @$params['level'], 0 ) ?> ><?php echo __( 'Level 0', 'tps' ) ?></option>
					<option value="1" <?php selected( @$params['level'], 1 ) ?> ><?php echo __( 'Level 1', 'tps' ) ?></option>
					<option value="2" <?php selected( @$params['level'], 2 ) ?> ><?php echo __( 'Level 2', 'tps' ) ?></option>
					<option value="3" <?php selected( @$params['level'], 3 ) ?> ><?php echo __( 'Level 3', 'tps' ) ?></option>
					<option value="4" <?php selected( @$params['level'], 4 ) ?> ><?php echo __( 'Level 4', 'tps' ) ?></option>
					<option value="5" <?php selected( @$params['level'], 5 ) ?> ><?php echo __( 'Level 5', 'tps' ) ?></option>
				</select>
			</p>
			<p>
				<label for="level"><b><?php echo __( 'List prefix in Canned Response ', 'tps' ); ?></b></label>

			<ul>
				<li><b>{user}</b> Your user name</li>
				<li><b>{customer}</b> Your customer name</li>
				<li><b>{cursor}</b> Cursor postion when you load canned response</li>
			</ul>
			</p>
		</div>
	<?php
	}

	/**
	 * Save data in meta box
	 */
	function save_meta_box_data( $post_id ) {
		/*
		 * We need to verify this came from our screen and with proper authorization,
		 * because the save_post action can be triggered at other times.
		 */

		// Check the user's permissions.

		if ( current_user_can( 'edit_post', $post_id ) || current_user_can( 'moderate' ) ) {

		} else {
			return;
		}

		if ( get_post_type() == 'tps-canned' ) {
			$params = $_POST['tps']['params'];
			if ( $params ) {
				update_post_meta( $post_id, '_tps_params', $params );
			}
		}
	}

	/**
	 * Front end output
	 *
	 * @since 1.0
	 *
	 * @return void
	 */
	public function reply_form() {

		if ( ! current_user_can( 'moderate' ) ) {
			return;
		}
		$data_canned = '';
		echo '<div class="bbp-canned-wrapper">';
		if ( $this->have_canned_replies() ) {
			?>
			<select class="chosen-select tps-canned">
				<option value=""><?php echo __( 'Please select Canned Response' ) ?></option>
				<?php
				foreach ( $this->have_canned_replies() as $reply ) {
					?>
					<option value="<?php echo $reply->ID ?>"><?php echo $reply->post_title ?></option>

				<?php
				}
				?>
			</select>
			<?php
			$data_canned = '<ul class="bbp-canned-list" style="display: none">';
			foreach ( $this->have_canned_replies() as $reply ) {
				$data_canned .= '<li class="bpp-canned-' . $reply->ID . '">';
				$data_canned .= $this->replaceReply( $reply->post_content );
				$data_canned .= '</li>';
			}
			$data_canned .= '</ul>';
		}
		$data_canned .= '</div>';
		echo $data_canned;
	}

	/**
	 * Function replace reply
	 *
	 * @param $content
	 * return Reply content
	 */
	protected function replaceReply( $content ) {
		global $current_user, $post;
		$reply_id = filter_input( INPUT_GET, 'bbp_reply_to', FILTER_SANITIZE_NUMBER_INT );
		if ( $reply_id ) {
			$post_data = get_post( $reply_id );
			$user_data = get_userdata( $post_data->post_author );
		} else {
			$user_data = get_userdata( $post->post_author );
		}
		$preg_args = array(
			'/\{customer\}/i',
			'/\{user\}/i'
		);
		if ( isset( $user_data ) ) {
			$content = preg_replace( $preg_args[0], $user_data->data->display_name, $content );
		}
		$content = preg_replace( $preg_args[1], $current_user->data->display_name, $content );

		return $content;
	}

	/**
	 * Get canned replies
	 *
	 * @since 1.0
	 *
	 * @param $category INT The category ID to get canned replies for
	 *
	 * @return array
	 */
	protected function have_canned_replies() {
		global $current_user;
		$args = array(
			'post_type'   => 'tps-canned',
			'nopaging'    => true,
			'post_status' => 'publish',
			'author__in'  => array( $current_user->ID )
		);

		return get_posts( $args );
	}
}

?>