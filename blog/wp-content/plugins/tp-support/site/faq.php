<?php

/*
Class Name: ThimPress Support Canned Response
Author: Andy Ha (tu@wpbriz.com)
Author URI: http://wpbriz.com
Copyright 2007-2014 wpBriz.com. All rights reserved.
*/

class TPSFaq {

	function __construct() {
		add_action( 'init', array( $this, 'init' ) );

	}

	/**
	 * Function init when run plugin+
	 */
	function init() {
		$this->register_post_type();
		$this->create_taxonomies();
	}

	/**
	 * Function create tax
	 */
	function create_taxonomies(){
		$labels = array(
			'name'              => _x( 'FAQ Categories', 'taxonomy general name' ),
			'singular_name'     => _x( 'FAQ Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Categories', 'tps' ),
			'all_items'         => __( 'All Categories', 'tps' ),
			'parent_item'       => __( 'Parent Category', 'tps' ),
			'parent_item_colon' => __( 'Parent Category:', 'tps' ),
			'edit_item'         => __( 'Edit Category', 'tps' ),
			'update_item'       => __( 'Update Category', 'tps' ),
			'add_new_item'      => __( 'Add New Category', 'tps' ),
			'new_item_name'     => __( 'New Category Name', 'tps' ),
			'menu_name'         => __( 'FAQ Categories', 'tps' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'faq-category' ),
		);

		register_taxonomy( 'tps_faq_category', array( 'tps-faq' ), $args );
	}


	/**
	 * Register post type
	 */
	function register_post_type() {

		if ( post_type_exists( 'tps-faq' ) ) {
			return;
		}

		$labels = array(
			'name'               => _x( 'FAQs', 'tps' ),
			'singular_name'      => _x( 'FAQ', 'tps' ),
			'menu_name'          => _x( 'FAQs', 'Admin menu', 'tps' ),
			'name_admin_bar'     => _x( 'FAQ', 'Add new on Admin bar', 'tps' ),
			'add_new'            => _x( 'Add New', 'role', 'tps' ),
			'add_new_item'       => __( 'FAQ', 'tps' ),
			'new_item'           => __( 'FAQ', 'tps' ),
			'edit_item'          => __( 'FAQ', 'tps' ),
			'view_item'          => __( 'FAQ', 'tps' ),
			'all_items'          => __( 'All FAQs', 'tps' ),
			'search_items'       => __( 'Search FAQs', 'tps' ),
			'parent_item_colon'  => __( 'Parent FAQ', 'tps' ),
			'not_found'          => __( 'No FAQ found.', 'tps' ),
			'not_found_in_trash' => __( 'No FAQ found in Trash.', 'tps' )
		);
		$args   = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'faq' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor','post-formats' ),
			'menu_icon'          => "dashicons-editor-help"
		);
		register_post_type( 'tps-faq', $args );

	}

}

?>