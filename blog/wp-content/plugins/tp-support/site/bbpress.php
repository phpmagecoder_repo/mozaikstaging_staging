<?php

/*
Class Name: ThimPress Support BBPress
Author: Andy Ha (tu@wpbriz.com)
Author URI: http://wpbriz.com
Copyright 2007-2014 wpBriz.com. All rights reserved.
*/

class TPSBBPress {
	function __construct() {
		if ( class_exists( 'TPSCanned' ) ) {
			$canned = new TPSCanned();
		}
		add_action( 'bbp_theme_after_reply_admin_links', array( $this, 'best_anwser' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'initScript' ) );
		add_action( 'bbp_template_before_replies_loop', array( $this, 'popup_form' ) );
		add_action( 'bbp_template_before_replies_loop', array( $this, 'best_answer_tick' ) );
		add_action( 'bbp_template_before_replies_loop', array( $this, 'assign_form' ) );

		add_action( 'wp_ajax_best_answer', array( $this, 'best_answer_callback' ) );
		add_action( 'wp_ajax_get_topic', array( $this, 'get_topic_callback' ) );
		add_action( 'wp_ajax_update_assign', array( $this, 'update_assign_callback' ) );
		add_action( 'wp_ajax_reply_autosearch', array( $this, 'reply_autosearch_callback' ) );
		add_action( 'wp_ajax_create_topic', array( $this, 'create_topic_callback' ) );
		add_action( 'wp_ajax_get_reply', array( $this, 'get_reply_callback' ) );
		add_action( 'wp_ajax_envato_user', array( $this, 'CheckUserEnvato' ) );

		add_action( 'wp_print_scripts', array( $this, 'ob_ajax_url' ) );

		add_action( 'bbp_theme_before_topic_title', array( $this, 'status_before_title' ) );
		add_action( 'bbp_theme_before_topic_form_title', array( $this, 'access_info_form' ) );
		add_action( 'bbp_theme_after_topic_form_title', array( $this, 'topic_suggestions' ) );
//		add_action( 'bbp_template_notices', array( $this, 'note_autocomplete' ) ); // this one is so annoying
		/*Update new topic*/
		add_action( 'bbp_new_topic', array( $this, 'update_site_access' ) );
		/*Update new reply*/
		add_action( 'bbp_new_reply', array( $this, 'update_info' ) );

		add_action( 'bbp_theme_after_reply_form_content', array( $this, 'reply_autoCompleteForm' ) );
//		add_action( 'bbp_theme_after_topic_started_by', array( $this, 'topic_show_staff' ) ); // @todo: considering to place it in effective place
		add_action( 'bbp_new_reply_pre_extras', array( $this, 'assign_user' ) );
		/*User detail*/
		add_action( 'bbp_template_after_user_profile', array( $this, 'user_list_product' ) );
		add_action( 'bbp_template_before_user_details', array( $this, 'add_icons' ), 10 );
		add_action( 'bbp_theme_after_reply_author_details', array( $this, 'add_icons' ), 10 );

		add_action( 'bbp_template_before_user_details', array( $this, 'add_geo_image' ) );
		add_action( 'bbp_theme_after_reply_author_details', array( $this, 'add_geo_image' ) );

		/*Check forum permission*/
		add_action( 'template_redirect', array( $this, 'check_forum_permission' ) );
		add_action( 'bbp_archive_template_notice', array( $this, 'forum_permission_note' ) );

		add_filter( 'bbp_get_reply_class', array( $this, 'addClassRole' ) );


	}

	/**
	 * Function note on BBPress
	 */
	function forum_permission_note() {
		if ( !isset( $_REQUEST['notice'] ) ) {
			return;
		}
		if ( $_REQUEST['notice'] ) { ?>
			<div role="alert" class="alert alert-warning">
				<strong><?php _e( 'Warning!', 'tps' ) ?></strong> <?php _e( 'You are have not permission to access this forum.', 'tps' ) ?>
			</div>
		<?php }
	}

	/**
	 * Check forum permission
	 */
	function check_forum_permission() {

		if ( is_admin() ) {
			return;
		} elseif ( get_post_type() == 'forum' || get_post_type() == 'topic' || get_post_type() == 'reply' ) {
			$this->allow_access_forum();
		} else {
			return;
		}
	}

	/**
	 * Function redirect if have not forum permission
	 */
	protected function allow_access_forum() {

		global $current_user, $post;
		$forum_permission_id = intval( $this->get_access_forum_id() );

		if ( !$forum_permission_id ) {
			return;
		} elseif ( $forum_permission_id ) {

			if ( !is_user_logged_in() ) {
				ob_start();
				ob_end_clean();
				wp_redirect( wp_login_url( get_permalink( $post->ID ) ) );
				die;

			}

			$user_role = bbp_get_user_role( $current_user->ID );
			if ( !$user_role ) {
				switch ( $user_role ) {
					case 'bbp_moderator':
						$permission = 2;
						break;
					case 'bbp_keymaster':
						$permission = 3;
						break;
					case 'bbp_participant':
						$permission = 1;
						break;
					default :
						$permission = 0;
				}
			} else {
				$permission = 0;
			}
			if ( $permission < $forum_permission_id ) {
				ob_start();
				ob_end_clean();
				wp_redirect( esc_url( add_query_arg( array( 'notice' => 1 ), bbp_get_forums_url() ) ) );
				die;
			} else {
				return;
			}
		} else {
			return;
		}
	}

	/**
	 * Check forum access permission
	 *
	 * @param null $forum_id
	 *
	 * @return 0 : Public
	 *         1 : Participant
	 *         2: Moderator
	 *         3: Keymaster
	 */
	protected function get_access_forum_id( $forum_id = null ) {
		$permission = 0;

		if ( !$forum_id ) {

			$forum_id = bbp_get_forum_id();

			if ( $forum_id ) {
				$forum_parent_id = bbp_get_forum_parent_id( $forum_id );

				$params           = get_post_meta( $forum_id, '_tps_params', true );
				$forum_permission = isset( $params['forum_permission'] ) ? $params['forum_permission'] : 0;
				if ( $forum_permission ) {
					switch ( $forum_permission ) {
						case 'bbp_moderator':
							$permission = 2;
							break;
						case 'bbp_keymaster':
							$permission = 3;
							break;
						case 'bbp_participant':
							$permission = 1;
							break;
						default :
							$permission = 0;
					}
				} else {
					$permission = 0;
				}

				if ( $forum_parent_id ) {
					$permission_forum_parent = $this->get_access_forum_id( $forum_parent_id );
					if ( $permission_forum_parent < $permission ) {
						return $permission;
					} else {
						return $permission_forum_parent;
					}
				} else {
					return $permission;
				}
			} else {
				return 0;
			}

		} else {
			$forum_parent_id  = bbp_get_forum_parent_id( $forum_id );
			$params           = get_post_meta( $forum_id, '_tps_params', true );
			$forum_permission = isset( $params['forum_permission'] ) ? $params['forum_permission'] : 0;
			if ( $forum_permission ) {
				switch ( $forum_permission ) {
					case 'bbp_moderator':
						$permission = 2;
						break;
					case 'bbp_keymaster':
						$permission = 3;
						break;
					case 'bbp_participant':
						$permission = 1;
						break;
					default :
						$permission = 0;
				}
			} else {
				$permission = 0;
			}

			/*Check forum parent*/
			if ( $forum_parent_id ) {
				$permission_forum_parent = $this->get_access_forum_id( $forum_parent_id );
				if ( $permission_forum_parent < $permission ) {
					return $permission;
				} else {
					return $permission_forum_parent;
				}
			} else {
				return $permission;
			}
		}
	}

	/**
	 * @param $user
	 */
	function CheckUserEnvato() {
		if ( !current_user_can( 'moderate' ) ) {
			return;
		}
		global $wpdb;
		$error  = array();
		$output = '';
		$user   = $_POST['data'];
		if ( !isset( $user['user'] ) ) {
			$error['check']   = 'error';
			$error['message'] = 'User is empty';
		} else {
			$meta_key   = '_purchase_code';
			$meta_value = strtoupper( $user['user'] );

			$users = $wpdb->get_row( "SELECT user_id as id
					FROM $wpdb->usermeta
					WHERE meta_key = '{$meta_key}'
					AND meta_value LIKE '%{$meta_value}%'
				", ARRAY_N );
			if ( count( $users ) > 0 ) {
				foreach ( $users as $user ) {
					$output .= '<li><i class="fa fa-user fa-fw"></i>' . bbp_get_user_profile_link( $user ) . '</li>';
				}
			}
			$error['check']   = 'done';
			$error['message'] = $output;
		}
		echo json_encode( $error );
		die;
	}

	/**
	 * Add Product Icon on user
	 */
	function add_icons() {

//		wp_enqueue_style( 'tps', TPS_CSS . 'tps.css' );
		$reply_id = bbp_get_reply_id();
		if ( !$reply_id ) {
			wp_enqueue_script( 'tps', TPS_JS . 'tps.js', array(), false, true );
			$user      = bbpress()->displayed_user;
			$author_id = $user->ID;

		} else {
			$author_id = get_post_field( 'post_author', $reply_id );
		}
		if ( user_can( $author_id, 'moderate' ) ) {
			return;
		}
		$purchased     = get_user_meta( $author_id, '_purchase_code', true );
		$global_params = get_option( '_tps_icons', array() );
		if ( $purchased ) {
			echo '<ul class="list-icons">';
			for ( $i = 0; $i < count( $purchased ); $i ++ ) {
				for ( $j = 0; $j < count( $global_params ); $j ++ ) {
					if ( $purchased[$i]['item_id'] == $global_params[$j]['id'] ) {
						echo '<li data-toggle="tooltip" data-placement="top" title="' . $global_params[$j]['name'] . '" class="tps-icon"><img src="' . $global_params[$j]['thumb'] . '"/></li>';
					}
				}
			}
			echo '</ul>';
		}
	}

	/**
	 * Add flag
	 */
	function add_geo_image() {
		if ( !current_user_can( 'moderate' ) ) {
			return;
		}
		$reply_id = bbp_get_reply_id();
		if ( $reply_id ) {
			$author_ip = get_post_meta( $reply_id, '_bbp_author_ip', true );
			$author_id = get_post_field( 'post_author', $reply_id );
			if ( !$author_id || !$author_ip ) {
				return;
			}
			$params = get_user_meta( $author_id, '_tps_location', true );
			$from   = get_user_meta( $author_id, '_tps_country', true );
			$img    = '';
			if ( $params && $from ) {
				$img = $params . ".png";
			} else {
				$ip_info = $this->geoCheckIP( $author_ip );
				if ( isset( $ip_info['country'] ) ) {
					update_user_meta( $author_id, '_tps_location', $ip_info['country'] );
					update_user_meta( $author_id, '_tps_country', $ip_info['from'] );
					$from = isset( $ip_info['from'] ) ? $ip_info['from'] : '';
					if ( $ip_info['country'] ) {
						$img = $ip_info['country'] . ".png";

					}
				}
				if ( isset( $ip_info['from'] ) ) {
					update_user_meta( $author_id, '_tps_country', $ip_info['from'] );
					$from = $ip_info['from'] ? $ip_info['from'] : '';
				}
			}
			if ( $img ) {
				echo "<div class='tps-user-flag tps-icon' data-placement='top' data-toggle='tooltip' data-original-title='" . $from . "'><img src='" . TPS_IMAGES . "flag/" . $img . "'/></div>";
			}
		} else {
			$user      = bbpress()->displayed_user;
			$author_id = $user->ID;
			$params    = get_user_meta( $author_id, '_tps_location', true );
			$from      = get_user_meta( $author_id, '_tps_country', true );
			if ( $params ) {
				$img = $params . ".png";
			} else {
				return;
			}
			if ( $img ) {
				echo "<div class='tps-user-flag tps-icon' data-placement='top' data-toggle='tooltip' data-original-title='" . $from . "'><img src='" . TPS_IMAGES . "flag/" . $img . "'/></div>";
			}
		}
	}

	/**
	 * Show list products in user profile
	 */
	function user_list_product() {
		global $current_user;
		$user_id = bbp_get_user_id();
		if ( $current_user->ID == $user_id || current_user_can( 'moderate' ) ) {
			$params    = get_user_meta( $user_id, '_purchase_code', true );
			$site_info = get_user_meta( $user_id, '_site_info', true );

			if ( $params ) {
				?>
				<h2 class="entry-title">
					<?php echo __( 'My Products', 'tps' ) ?>
				</h2>
				<div class="bbp-user-section">
					<ul>
						<?php foreach ( $params as $param ) { ?>
							<li><span>
								<?php
								preg_match( '/woo_/i', $param['item_id'], $woo_result );
								preg_match( '/edd_/i', $param['item_id'], $edd_result );
								if (count( array_filter( $woo_result ) )) {
								?>
									<i class="fa fa-fw fa-shopping-cart"></i></span>
								<?php } elseif ( count( array_filter( $edd_result ) ) ) { ?>
									<i class="fa fa-fw fa-download"></i></span>
								<?php } else { ?>
									<i class="fa fa-fw fa-cart-plus"></i></span>
								<?php } ?>

								<span class="item-name"><?php echo $param['item_name'] ?></span> &nbsp;|&nbsp;
								<span class="item-date"><?php echo $param['created_at'] ?></span> &nbsp;|&nbsp;
								<?php

								if ( count( array_filter( $woo_result ) ) ) {
									?>
									<span class="item-purchasedcode"><?php echo __( 'Order #', 'tps' ) . $param['purchase_code'] ?></span>
								<?php } elseif ( count( array_filter( $edd_result ) ) ) { ?>
									<span class="item-purchasedcode"><?php echo __( 'Download Order #', 'tps' ) . $param['purchase_code'] ?></span>
								<?php } else { ?>
									<span class="item-purchasedcode"><?php echo $param['purchase_code'] ?></span>&nbsp;|&nbsp;
									<span class="item-buy"><?php echo $param['buyer'] ?></span>
								<?php } ?>


							</li>
						<?php } ?>
					</ul>
				</div>
			<?php
			}
			if ( $site_info ) {
				?>
				<h2 class="entry-title">
					<?php echo __( 'My Site Information', 'tps' ) ?>
				</h2>
				<p class="help-block"><?php echo __( 'Your site information will be protected. Please change when your problems are solved.', 'tps' ) ?></p>
				<div class="bbp-user-section">
					<textarea class="form-control" rows="20" readonly><?php echo htmlspecialchars( $site_info ) ?></textarea>
				</div>
			<?php
			}
		}

	}

	/**
	 * Save customer's site info
	 */
	function update_info( $reply_id ) {
		global $current_user;

		$data     = $_POST['params'];
		$topic_id = $_POST['bbp_topic_id'];
		if ( $data && $topic_id ) {
			$params    = get_post_meta( $topic_id, '_tps_params', true );
			$user_data = get_user_meta( $current_user->ID, '_site_info', true );
			if ( trim( $data['site_info'] ) ) {
				$params['site_access'] = __( '#Updated ', 'tps' ) . current_time( "H:i:s m-d-Y" ) . __( ' by ', 'tps' ) . $current_user->data->user_login . " " . __( 'Reply #', 'tps' ) . $reply_id . "\n" . $data['site_info'] . " \n\n" . $params['site_access'];
				$user_data             = __( '#Updated ', 'tps' ) . current_time( "H:i:s m-d-Y" ) . "\n" . $data['site_info'] . "\n\n" . $user_data;
				update_post_meta( $topic_id, '_tps_params', $params );
				update_user_meta( $current_user->ID, '_site_info', $user_data );
			}
		}

		return;
	}

	/**
	 * Get data from single reply
	 */
	function get_reply_callback() {
		$error = array();
		if ( !current_user_can( 'moderate' ) ) {
			$error['check']   = 'error';
			$error['message'] = 'You have not permission';
		} else {
			$data = $_POST['data'];
			if ( isset( $data['reply_id'] ) ) {
				$error['check']   = 'done';
				$error['message'] = get_post_field( 'post_content', $data['reply_id'] );
			} else {
				$error['check']   = 'error';
				$error['message'] = 'Reply empty.';
			}
		}
		echo json_encode( $error );
		die;
	}

	/**
	 * Add list suggestion
	 */
	function topic_suggestions() {
		echo '<ul class="list-unstyle ob-list-suggestions"></ul>';
	}

	/**
	 * Function add class in Reply area
	 *
	 * @param $classes
	 *
	 * @return array
	 */
	function addClassRole( $classes ) {
		global $post;
		$classes[] = bbp_get_user_role( get_post_field( 'post_author', $post->ID ) );

		return $classes;
	}

	/**
	 * Function assign User when reply
	 *
	 * @param $topic_id
	 */
	function assign_user( $topic_id ) {
		global $current_user;
		if ( !current_user_can( 'moderate' ) ) {
			return;
		}
		$check_assign = get_post_meta( $topic_id, '_tps_user_assign', true );
		$check_status = get_post_meta( $topic_id, '_tps_status', true );
		if ( !$check_assign ) {
			update_post_meta( $topic_id, '_tps_user_assign', $current_user->ID );
		}
		if ( !$check_status ) {
			update_post_meta( $topic_id, '_tps_status', 3 );
		}
	}

	/**
	 * Guide on top reply form
	 */
	function note_autocomplete() {
		if ( !current_user_can( 'moderate' ) || get_post_type() != 'topic' ) {
			return;
		}
		?>
		<div class="bbp-template-notice">
			<p><?php echo __( 'Shortcode: <b>@kb:your_keyword</b> Auto load link from Knowlegde base follow keyword.', 'tps' ); ?></p>
		</div>
	<?php
	}

	/**
	 * Show user is assigned with topic
	 */
	function topic_show_staff() {
		global $post;
		$assign = get_post_meta( $post->ID, '_tps_user_assign', true );

		if ( $assign > 0 ) {
			$user_data = get_userdata( $assign );
			?>

			<span class="bbp-topic-started-by"><?php printf( __( ' | Supported by: %1$s', 'tps' ), '<a rel="nofollow" class="bbp-author-avatar" href="' . bbp_get_user_profile_url( $assign ) . '">' . get_avatar( $assign, 14 ) . '</a><a href="' . bbp_get_user_profile_url( $assign ) . '"> ' . $user_data->data->display_name . ' </a>' ); ?></span>
		<?php
		}

	}

	/**
	 * Function Search in reply content
	 */
	function reply_autosearch_callback() {
		global $wpdb;
		$error = array();
		if ( !current_user_can( 'moderate' ) ) {
			die;
		}
		$data = $_POST['data'];
		if ( isset( $data['keyword'] ) ) {
			$keyword = $data['keyword'];
			$keyword = strtoupper( $keyword );
			$ids     = $wpdb->get_col( "SELECT ID FROM $wpdb->posts WHERE UCASE(post_title) LIKE '%$keyword%' AND post_type='tps' AND post_status='publish'" );

			$search_query = array(
				'post__in'  => $ids,
				'order'     => 'ASC',
				'orderby'   => 'title',
				'post_type' => 'tps'
			);

			$search  = new WP_Query( $search_query );
			$newdata = array();
			foreach ( $search->posts as $post ) {
				$newdata[] = array(
					'title' => $post->post_title,
					'guid'  => get_permalink( $post->ID )
				);
			}
			$error['check']   = 'done';
			$error['message'] = $newdata;

		}
//		ob_end_clean();
		echo json_encode( $error );
		die;
	}

	/**
	 * Add auto searh KB
	 */
	function reply_autoCompleteForm() {
		?>
		<ul class="autocomplete-list" style="position: absolute;">

		</ul>
		<input id="text-pos" type="hidden" value="0" />
	<?php
	}

	/**
	 * Function update access site information
	 *
	 * @param $post_id
	 */
	function update_site_access( $post_id ) {
		global $current_user;
		$params = $params_forum = array();
		if ( is_user_logged_in() ) {
			if ( isset( $_POST['action'] ) == 'bbp-new-topic' ) {
				$data = $_POST['tps']['params'];
				if ( trim( $data['site_access'] ) ) {
					$params['site_access'] = $data['site_access'];
					$user_data             = get_user_meta( $current_user->ID, '_site_info', true );
					$user_data             = __( '#Updated ', 'tps' ) . current_time( "H:i:s m-d-Y" ) . "\n" . $data['site_access'] . "\n\n" . $user_data;
					update_post_meta( $post_id, '_tps_params', $params );
					update_user_meta( $current_user->ID, '_site_info', $user_data );
				}
			}
			$forum_id = $_POST['bbp_forum_id'];
			if ( $forum_id ) {
				$params_forum  = get_post_meta( $forum_id, '_tps_params', true );
				$params_global = get_option( '_tps_params', array() );
				if ( isset( $params_forum['mod'] ) ) {
					if ( count( $params_forum['mod'] ) > 0 ) {
						$mods = $params_forum['mod'];
						if ( isset( $params_global['subject'] ) && isset( $params_global['body'] ) ) {

							foreach ( $mods as $mod ) {
								$user = get_userdata( $mod );
								if ( $user ) {
									$subject = $this->emailReplace( $post_id, $user, $params_global['subject'] );
									$body    = $this->emailReplace( $post_id, $user, $params_global['body'] );
									$email   = $user->data->user_email;
									if ( !wp_mail( $email, $subject, $body ) ) {

									}
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Function shortcode will be replaced with real data
	 *
	 * @param $topic_id  topic ID
	 * @param $user      User profile
	 * @param $init_text Content need replace
	 *
	 * @return mixed
	 */
	protected function emailReplace( $topic_id, $user, $init_text ) {
		/**
		 * [user] Moderator name
		 * [topic-title] Topic 's Title
		 * [topic-content] Topic's Content
		 * [topic-url] Link to topic
		 * [topic-author] User who has posted this topic
		 * [site-name] This site name
		 */
		$init_array    = array(
			'/\[user\]/i',
			'/\[topic-title\]/i',
			'/\[topic-content\]/i',
			'/\[topic-url\]/i',
			'/\[topic-author\]/i',
			'/\[site-name\]/i'
		);
		$user_name     = $user->data->display_name;
		$topic_title   = get_post_field( 'post_title', $topic_id );
		$topic_content = get_post_field( 'post_content', $topic_id );
		$topic_url     = get_the_permalink( $topic_id );
		$topic_author  = get_the_author_meta( 'display_name', get_post_field( 'post_author', $topic_id ) );
		$site_name     = get_bloginfo( 'name' );
		$replace_data  = array(
			$user_name,
			$topic_title,
			$topic_content,
			$topic_url,
			$topic_author,
			$site_name
		);


		$init_text = preg_replace( $init_array, $replace_data, $init_text );

		return $init_text;
	}

	/**
	 * Show access information form
	 */
	function access_info_form() {
		$params             = get_option( '_tps_params', array() );
		$access_information = isset( $params['access_information'] ) ? $params['access_information'] : 0;
		if ( $access_information ) {
			return;
		}
		?>
		<h3><?php echo __( 'Site Information', 'tps' ) ?></h3>
		<p>
			<label for="bbp-topic-title"><?php echo __( 'Your site\'s access information:', 'tps' ); ?></label><br />
			<textarea class="form-control" name="tps[params][site_access]"></textarea>
		</p>
		<p><?php echo __( 'Note! All information will be protected.', 'tps' ) ?></p>
	<?php
	}

	/**
	 * Update status and assign user
	 */
	function update_assign_callback() {
		if ( !current_user_can( 'moderate' ) ) {
			return;
		}
		$data  = $_POST['data'];
		$error = $params = array();

		if ( count( $data ) > 0 ) {
			$post_id = $data['topic_id'];
			if ( $post_id ) {
				update_post_meta( $post_id, '_tps_user_assign', $data['user_assign'] );
				update_post_meta( $post_id, '_tps_status', $data['status'] );
				$error['check'] = 'done';
			} else {
				$error['check'] = 'error';
			}
		} else {
			$error['check'] = 'error';
		}
		//ob_end_clean();
		echo json_encode( $error );
		die;
	}

	/**
	 * Assign form on top topic
	 */
	function assign_form() {
		if ( get_post_type() != 'topic' || !is_user_logged_in() ) {
			return;
		}
		global $post, $current_user;
		if ( current_user_can( 'moderate' ) ) {
			$params['user_assign'] = get_post_meta( $post->ID, '_tps_user_assign', true );
			$params['status']      = get_post_meta( $post->ID, '_tps_status', true );
			$args                  = array(
				'role' => 'bbp_moderator'
			);
			$args_key              = array(
				'role' => 'bbp_keymaster'
			);
			$users_mod             = get_users( $args );
			$users_key             = get_users( $args_key );
			$users                 = array_merge( $users_mod, $users_key );
		}
		$params_global      = get_option( '_tps_params', array() );
		$access_information = isset( $params_global['access_information'] ) ? $params_global['access_information'] : 0;
		/*Check permisson*/
		$forum_id     = bbp_get_forum_id();
		$forum_params = get_post_meta( $forum_id, '_tps_params', true );
		$user_metas   = get_user_meta( $current_user->ID, '_purchase_code', true );
		$check_pur    = false;
		if ( $forum_params ) {
			if ( in_array( 99, $forum_params['envato_items'] ) || current_user_can( 'moderate' ) ) {
				$check_pur = true;
			} else {
				if ( $user_metas ) {
					foreach ( $user_metas as $user_meta ) {
						if ( in_array( $user_meta['item_id'], $forum_params['envato_items'] ) ) {
							$check_pur = true;
							break;
						}
					}
				}
			}
		} else {
			$check_pur = true;
		}
		?>
		<div class="bbp-topic-tools pull-right">
			<?php if ( current_user_can( 'moderate' ) ) { ?>
				<label><?php echo __( 'Assigned to', 'tps' ) ?></label>
				<select class="chosen-select user-assign">
					<option value="-1"><?php echo __( 'Unassigned', 'tps' ); ?></option>
					<?php foreach ( $users as $user ) { ?>
						<option value="<?php echo $user->ID ?>" <?php selected( @$params['user_assign'], $user->ID ) ?> >
							<?php echo $user->data->display_name ?>
						</option>
					<?php } ?>
				</select>
				<label><?php echo __( 'Topic Status', 'tps' ) ?></label>
				<select class="chosen-select topic-status">
					<option value="0" <?php selected( @$params['status'], 0 ) ?> ><?php echo __( 'Pending', 'tps' ) ?></option>
					<option value="3" <?php selected( @$params['status'], 3 ) ?> ><?php echo __( 'In processing', 'tps' ) ?></option>
					<option value="1" <?php selected( @$params['status'], 1 ) ?> ><?php echo __( 'Resolved', 'tps' ) ?></option>
					<option value="2" <?php selected( @$params['status'], 2 ) ?> ><?php echo __( 'Not solved', 'tps' ) ?></option>
				</select>
				<a type="submit" class="btn btn-primary btn-topic-update" data-loading-text="<?php echo __( 'Updating...', 'tps' ) ?>"><?php echo __( 'Update', 'tps' ) ?></a>
				<?php if ( !$access_information ) { ?>
					<a type="submit" href="#access-info" class="btn btn-primary access-info"><?php echo __( 'Access Info', 'tps' ) ?></a>
				<?php } ?>

			<?php
			}
			$author = get_post_field( 'post_author', $post->ID );
			if ( ( $current_user->ID == $author && $check_pur ) || current_user_can( 'moderate' ) ) {
				?>
				<a type="submit" href="#new-post" class="btn btn-primary site-info"><?php echo __( 'Send Access Info', 'tps' ) ?></a>
			<?php } ?>
		</div>
	<?php
	}

	/**
	 * Show Status before title topic
	 */
	function status_before_title() {
		global $post;
		$params           = get_post_meta( $post->ID, '_tps_params', true );
		$params['status'] = get_post_meta( $post->ID, '_tps_status', true );
		if ( isset( $params['status'] ) ) {
			switch ( $params['status'] ) {
				case 3:
					echo '<span class="ob-status in-processing"><i class="fa fa-fighter-jet fa-lg"></i></i></span>';
					break;
				case 1:
					echo '<span class="ob-status resolved"><i class="fa fa-check-circle-o fa-lg"></i></span>';
					break;
				case 2:
					echo '<span class="ob-status not-resolved"><i class="fa fa-times-circle-o fa-lg"></i></span>';
					break;
				default:
					echo '<span class="ob-status pending"><i class="fa fa-clock-o fa-lg"></i></span>';
			}
		} else {
			echo '<span class="ob-status pending"><i class="fa fa-clock-o fa-lg"></i></span>';
		}
	}

	/**
	 *     * Get data topic when select update best answer
	 */
	function get_topic_callback() {
		if ( !current_user_can( 'moderate' ) ) {
			return;
		}
		$error   = array();
		$post_id = $_POST['topic_id'];

		if ( $post_id ) {
			$data                    = get_post( $post_id );
			$tags                    = get_the_terms( $data->ID, 'tps_tag' );
			$categories              = get_the_terms( $data->ID, 'tps_category' );
			$error['check']          = 'done';
			$error['data']['id']     = $data->ID;
			$error['data']['ask']    = $data->post_excerpt;
			$error['data']['answer'] = $data->post_content;
			$error['data']['title']  = $data->post_title;
			if ( !is_array( $tags ) ) {
				$tags = array();
			}
			if ( !is_array( $categories ) ) {
				$categories = array();
			}
			if ( count( $tags ) > 0 ) {
				foreach ( $tags as $tag ) {
					$error['data']['tags'][] = $tag->name;
				}
				$error['data']['tags'] = implode( ',', $error['data']['tags'] );
			} else {
				$error['data']['tags'] = '';
			}
			if ( count( $categories ) > 0 ) {
				foreach ( $categories as $category ) {
					$error['data']['categories'][] = $category->term_id;
				}
				$error['data']['categories'] = implode( ',', $error['data']['categories'] );
			} else {
				$error['data']['categories'] = '';
			}
		} else {
			$error['check'] = 'error';
		}
		echo json_encode( $error );
		die;
	}

	/**
	 * function tick Best answer
	 */
	function best_answer_tick() {
		global $post;

		if ( bbp_is_replies_created() ) {
			return;
		}

		$params = get_post_meta( $post->ID, '_tps_params', true );
		if ( isset( $params['best_anwser'] ) ) {
			?>
			<script type="text/javascript">
				jQuery(document).ready(function () {
					jQuery('div.post-<?php echo $params['best_anwser'] ?>').addClass('best-anwser').append('<div class="best-answer-reply"><span><i class="fa fa-check"></i></span></div>')
				})
			</script>
		<?php
		}
	}

	/**
	 * Function Ajax best answer
	 */
	function best_answer_callback() {
		if ( !current_user_can( 'moderate' ) ) {
			return;
		}
		$data       = $_POST['data'];
		$categories = '';
		$terms      = $categories_text = array();
		$error      = $params = $params_topic = array();
		if ( isset( $data['kb_id'] ) ) {
			$id = $data['kb_id'];
		} else {
			$id = '';
		}
		if ( count( $data ) > 0 ) {
			// Create post object
			$categories = $data['categories'];

			$my_post = array(
				'post_title'   => $data['title'],
				'post_content' => stripslashes( $data['answer'] ),
				'post_excerpt' => stripslashes( $data['ask'] ),
				'post_status'  => 'publish',
				'post_author'  => 1,
				'post_type'    => 'tps',
				'ID'           => $id
			);

			// Insert the post into the database

			if ( isset( $data['kb_id'] ) ) {
				$params = get_post_meta( $data['kb_id'], '_tps_params', true );
			}

			if ( $categories ) {
				$categories = explode( ',', $categories );
				foreach ( $categories as $category ) {
					$cate_data         = get_term( $category, 'tps_category' );
					$categories_text[] = $cate_data->name;
				}
			}
			$post_id = wp_insert_post( $my_post );
			wp_set_object_terms( $post_id, $categories_text, 'tps_category' );

			$params['topic_id'][] = $data['topic_id'];
			$params['topic_id']   = array_unique( $params['topic_id'] );
			$params['status']     = 1;


			update_post_meta( $post_id, '_tps_params', $params );
			if ( isset( $data['topic_id'] ) ) {
				$params_topic                = get_post_meta( $data['topic_id'], '_tps_params', true );
				$params_topic['best_anwser'] = $data['ba_id'];
				update_post_meta( $data['topic_id'], '_tps_params', $params_topic );
			}


			$tags = $data['tags'];
			if ( $tags ) {
				$tags = explode( ',', $tags );
				wp_set_post_terms( $post_id, $tags, 'tps_tag' );
			}


			$error['check'] = 'done';
		} else {
			$error['check'] = 'error';
		}
		//ob_end_clean();
		echo json_encode( $error );
		die;
	}

	/**
	 * Function Ajax Create Topic
	 */
	function create_topic_callback() {
		global $current_user;
		$error       = $param_forum = array();
		$data        = $_REQUEST['data'];
		$user_metas  = get_user_meta( $current_user->ID, '_purchase_code', true );
		$param_forum = get_post_meta( $data['category'], '_tps_params', true );

		$check = false;
		if ( count( $data ) > 0 ) {
			if ( !isset( $data['title'] ) ) {
				$error['check']   = 'note';
				$error['message'] = 'Title is require field. It is not empty.';
				ob_end_clean();
				echo json_encode( $error );
				die;
			}
			if ( !isset( $data['content'] ) ) {
				$error['check']   = 'note';
				$error['message'] = 'Content is require field. It is not empty.';
				ob_end_clean();
				echo json_encode( $error );
				die;
			}
			if ( ( isset( $param_forum['envato_items'] ) ) ) {
				if ( in_array( 99, $param_forum['envato_items'] ) || current_user_can( 'moderate' ) ) {
					$check = true;
				} else {
					if ( $user_metas ) {
						foreach ( $user_metas as $user_meta ) {
							if ( in_array( $user_meta['item_id'], $param_forum['envato_items'] ) ) {
								$check = true;
								break;
							}
						}
					}
				}
			}
			if ( !$check ) {
				$error['check']   = 'note';
				$error['message'] = 'You can not create topic at this category forum. Please enter your purchased code.';
			} else {
				// Create post object
				$my_post  = array(
					'post_title'   => $data['title'],
					'post_content' => stripslashes( $data['content'] ),
					'post_status'  => 'publish',
					'post_parent'  => $data['category']
				);
				$my_forum = array(
					'forum_id' => $data['category']
				);
				$topic_id = bbp_insert_topic( $my_post, $my_forum );
				if ( $topic_id ) {
//					$forum_id       = 0;
//					$anonymous_data = bbp_filter_anonymous_post_data();
//					$author_id      = bbp_get_topic_author_id( $topic_id );
//					$is_edit        = (bool) isset( $_POST['save'] );
//
//					// Formally update the topic
//					bbp_update_topic( $topic_id, $forum_id, $anonymous_data, $author_id, $is_edit );
					$error['check']   = 'done';
					$error['message'] = __( 'Create topic successful.', 'tps' );
					/*Send Email*/
					$forum_id = $data['category'];
					if ( $forum_id ) {
						$params_forum  = get_post_meta( $forum_id, '_tps_params', true );
						$params_global = get_option( '_tps_params', array() );
						if ( isset( $params_forum['mod'] ) ) {
							if ( count( $params_forum['mod'] ) > 0 ) {
								$mods = $params_forum['mod'];
								if ( isset( $params_global['subject'] ) && isset( $params_global['body'] ) ) {

									foreach ( $mods as $mod ) {
										$user = get_userdata( $mod );
										if ( $user ) {
											$subject = $this->emailReplace( $topic_id, $user, $params_global['subject'] );
											$body    = $this->emailReplace( $topic_id, $user, $params_global['body'] );
											$email   = $user->data->user_email;
											if ( !wp_mail( $email, $subject, $body ) ) {

											}
										}
									}
								}
							}
						}
					}
				} else {
					$error['error']   = 'done';
					$error['message'] = __( 'Create topic error.', 'tps' );
				}
			}

		} else {
			$error['check']   = 'error';
			$error['message'] = 'Can not found data.';
		}
		//ob_end_clean();
		echo json_encode( $error );
		die;
	}

	/**
	 * Init script
	 */
	function ob_ajax_url() {
		?>
		<script type="text/javascript">
			var ob_ajax_url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php";
		</script>
	<?php
	}

	/**
	 * Function form
	 */
	function popup_form() {
		global $post, $current_user;
		if ( get_post_type() != 'topic' ) {
			return;
		}
		if ( current_user_can( 'moderate' ) ) {
			$tags = $this->getTopicTags();

			$args         = array(
				'post_type'      => 'tps',
				'post_status'    => 'publish',
				'orderby'        => "title",
				'posts_per_page' => 99,
			);
			$the_query    = new WP_Query( $args );
			$update_items = $the_query->posts;

			$params             = get_post_meta( $post->ID, '_tps_params', true );
			$args_categories    = array(
				'orderby'           => 'name',
				'order'             => 'ASC',
				'hide_empty'        => true,
				'exclude'           => array(),
				'exclude_tree'      => array(),
				'include'           => array(),
				'number'            => '',
				'fields'            => 'all',
				'slug'              => '',
				'parent'            => '',
				'hierarchical'      => true,
				'child_of'          => 0,
				'get'               => '',
				'name__like'        => '',
				'description__like' => '',
				'pad_counts'        => false,
				'offset'            => '',
				'search'            => '',
				'cache_domain'      => 'core'
			);
			$categories         = get_terms( 'tps_category', $args_categories );
			$params_global      = get_option( '_tps_params', array() );
			$access_information = isset( $params_global['access_information'] ) ? $params_global['access_information'] : 0;
			if ( !$access_information ) {
				?>

				<div id="access-info" class="row" style="display: none;width: 1200px;">
					<div class="col-md-12">
						<div class="col-md-12">
							<span class="ob-label"><?php echo __( 'Site Information', 'tps' ) ?></span>
							<textarea rows="20" readonly class="form-control"><?php echo isset( $params['site_access'] ) ? $params['site_access'] : '' ?></textarea>
						</div>
					</div>
				</div>
			<?php } ?>
			<div id="tps-bestanwser" class="row" style="display:none;width: 1200px;">
				<div class="col-md-12">
					<ul class="nav nav-tabs" role="tablist">
						<li class="active">
							<a href="#new-kb" role="tab" data-toggle="tab"><?php echo __( 'New Knowledge Base', 'tps' ) ?></a>
						</li>
						<li>
							<a href="#update-kb" role="tab" data-toggle="tab"><?php echo __( 'Update Knowledge Base', 'tps' ) ?></a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="new-kb">
							<h3><?php echo __( 'New Best Answer', 'tps' ) ?></h3>

							<div class="col-md-12">
								<span class="ob-label"><?php echo __( 'Title', 'tps' ) ?></span>

								<input type="text" class="ba-title form-control" value="<?php echo $post->post_title ?>" />
							</div>
							<div class="col-md-12">
								<span class="ob-label"><?php echo __( 'Ask Question', 'tps' ) ?></span>
								<textarea rows="5" class="ba-askquestion form-control"><?php echo get_post_field( 'post_content', $post->ID ) ?></textarea>
							</div>
							<div class="col-md-12">
								<span class="ob-label"><?php echo __( 'Answer Question', 'tps' ) ?></span>
								<textarea rows="5" class="ba-answerquestion form-control"></textarea>
							</div>
							<div class="col-md-12">
								<span class="ob-label"><?php echo __( 'Categories', 'tps' ) ?></span>
								<select multiple="true" class="ba-categories form-control chosen-select">
									<?php foreach ( $categories as $k => $category ) { ?>
										<option value="<?php echo $category->term_id ?>"><?php echo $category->name ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-12">
								<span class="ob-label"><?php echo __( 'Tags', 'tps' ) ?></span>

								<p>
									<input type="text" class="ba-tags form-control"
										   value="<?php if ( count( $tags ) > 0 ) {
											   echo implode( ',', $tags );
										   } ?>" />
								</p>
							</div>
							<div class="col-md-12">
								<input type="hidden" class="ba-id form-control" value="" />
								<input type="hidden" class="ba-topic-id form-control" value="<?php echo $post->ID ?>" />
								<button class="btn btn-primary ob-fancy-submit" data-loading-text="<?php echo __( 'Loading...', 'tps' ) ?>"><?php echo __( 'Submit', 'tps' ) ?></button>
								<button class="btn btn-danger ob-fancy-cancel"><?php echo __( 'Cancel', 'tps' ) ?></button>

							</div>
						</div>

						<div class="tab-pane" id="update-kb">
							<h3><?php echo __( 'UPDATE BEST ANSWER', 'tps' ) ?></h3>
							<select class="on-update-select chosen-select">
								<option><?php __( 'Please select knowledge base', 'tps' ) ?></option>
								<?php foreach ( $update_items as $update_item ) { ?>
									<option value="<?php echo $update_item->ID ?>"><?php echo $update_item->post_title ?></option>
								<?php } ?>
							</select>

							<div class="u-content" style="display: none">
								<div class="col-md-12">
									<span class="ob-label"><?php echo __( 'Title', 'tps' ) ?></span>

									<input type="text" class="u-ba-title form-control" value="" />
								</div>
								<div class="col-md-12">
									<span class="ob-label"><?php echo __( 'Ask Question', 'tps' ) ?></span>

									<textarea rows="5" class="u-ba-askquestion form-control"></textarea>
								</div>
								<div class="col-md-12">
									<span class="ob-label"><?php echo __( 'Answer Question', 'tps' ) ?></span>
									<textarea rows="5" class="u-ba-answerquestion form-control"></textarea>
								</div>
								<div class="col-md-12">
									<span class="ob-label"><?php echo __( 'Categories', 'tps' ) ?></span>
									<select multiple="true" class="u-categories form-control">
										<?php foreach ( $categories as $category ) { ?>
											<option value="<?php echo $category->term_id ?>"><?php echo $category->name ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-12">
									<span class="ob-label"><?php echo __( 'Tags', 'tps' ) ?></span>

									<p>
										<input type="text" class="u-ba-tags form-control" value="" />
									</p>
								</div>
								<div class="col-md-12">
									<input type="hidden" class="u-ba-id form-control" value="" />
									<input type="hidden" class="u-ba-topic-id form-control" value="<?php echo $post->ID; ?>" />
									<input type="hidden" class="u-kb-id form-control" value="" />
									<button class="btn btn-primary u-ob-fancy-submit" data-loading-text="<?php echo __( 'Updating...', 'tps' ) ?>"><?php echo __( 'Submit', 'tps' ) ?></button>
									<button class="btn btn-danger ob-fancy-cancel"><?php echo __( 'Cancel', 'tps' ) ?></button>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
	}

	/**
	 * Funcstion get Topic Tags
	 * @return array
	 */
	protected function getTopicTags() {
		global $post;
		$tags       = array();
		$topic_tags = wp_get_object_terms( $post->ID, 'topic-tag' );
		foreach ( $topic_tags as $topic_tag ) {
			$tags[] = $topic_tag->name;
		}

		return array_unique( $tags );

	}

	/**
	 * Add best anwser button in reply topic
	 */
	function best_anwser() {
		global $post;
		$position = bbp_get_reply_position( $post->ID );
		if ( $position == 1 ) {
			return;
		}
		if ( current_user_can( 'moderate' ) ) {
			?>
			<span class="bbp-admin-links">
				<a class="ob-popup" data-reply-id="<?php echo $post->ID ?>" href="#tps-bestanwser"><?php echo __( 'BEST ANSWER', 'tps' ) ?></a>&nbsp;|&nbsp;
			</span>
		<?php
		}
	}

	/**
	 * init Sript on front end
	 */
	function initScript() {
		wp_enqueue_script( 'jquery-fancybox', TPS_JS . 'jquery.fancybox.pack.js', array(), false, false );
		wp_enqueue_style( 'jquery-fancybox', TPS_CSS . 'jquery.fancybox.css' );
		if ( get_post_type() == 'topic' || ( get_post_type() == 'forum' ) || ( get_post_type() == 'reply' ) || bbp_is_replies_created() ) {

//			wp_enqueue_script( 'bootstraps', '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js', array(), false, false );
			wp_enqueue_script( 'jquery-caretposition', TPS_JS . 'jquery.caretposition.js', array(), false, false );
			wp_enqueue_script( 'jquery-easydrag', TPS_JS . 'jquery.easydrag.js', array(), false, false );
			wp_enqueue_script( 'jquery-caret', TPS_JS . 'jquery.caret.js', array(), false, false );
			wp_enqueue_script( 'jquery-chosen', TPS_JS . 'chosen.jquery.min.js', array(), false, false );
			wp_enqueue_script( 'jquery-autosize', TPS_JS . 'jquery.autosize.min.js', array(), false, false );
			wp_enqueue_script( 'tps-bbpress', TPS_JS . 'tps-bbpress.js', array(), false, false );

//			wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css' );
//			wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' );
			wp_enqueue_style( 'jquery-chosen', TPS_CSS . 'chosen.min.css' );

//			wp_enqueue_style( 'tps', TPS_CSS . 'tps.css' );
		}
	}

	//Get an array with geoip-infodata
	protected function geoCheckIP( $ip ) {
		//check, if the provided ip is valid
		if ( !filter_var( $ip, FILTER_VALIDATE_IP ) ) {
			throw new InvalidArgumentException( "IP is not valid" );
		}

		//contact ip-server
		$response = @file_get_contents( 'http://www.netip.de/search?query=' . $ip );
		if ( empty( $response ) ) {
			throw new InvalidArgumentException( "Error contacting Geo-IP-Server" );
		}

		//Array containing all regex-patterns necessary to extract ip-geoinfo from page
		$patterns            = array();
		$patterns["domain"]  = '#Domain: (.*?)&nbsp;#i';
		$patterns["country"] = '#Country: (.*?)&nbsp;#i';
		$patterns["state"]   = '#State/Region: (.*?)<br#i';
		$patterns["town"]    = '#City: (.*?)<br#i';

		//Array where results will be stored
		$ipInfo = array();

		//check response from ipserver for above patterns
		foreach ( $patterns as $key => $pattern ) {
			//store the result in array
			$ipInfo[$key] = preg_match( $pattern, $response, $value ) && !empty( $value[1] ) ? $value[1] : 'not found';
		}
		$ipInfo["from"]    = $ipInfo["country"];
		$ipInfo["country"] = strtolower( trim( preg_replace( '/-[\s\S]*/i', '', $ipInfo["country"] ) ) );

		return $ipInfo;
	}


}

?>