<?php

/*
Class Name: ThimPress Support Canned Response
Author: Andy Ha (tu@wpbriz.com)
Author URI: http://wpbriz.com
Copyright 2007-2014 wpBriz.com. All rights reserved.
*/

class TPSSearchLog {

	function __construct() {
		add_action( 'init', array( $this, 'init' ) );

	}

	/**
	 * Function init when run plugin+
	 */
	function init() {
		$this->register_post_type();
		$this->create_taxonomies();
	}

	/**
	 * Register post type
	 */
	function register_post_type() {

		if ( post_type_exists( 'tps-faq' ) ) {
			return;
		}

		$labels = array(
			'name'               => _x( 'Keyword', 'tps' ),
			'singular_name'      => _x( 'Keyword', 'tps' ),
			'menu_name'          => _x( 'Search Logs', 'Admin menu', 'tps' ),
			'name_admin_bar'     => _x( 'Search Log', 'Add new on Admin bar', 'tps' ),
			'add_new'            => _x( 'Add New', 'role', 'tps' ),
			'add_new_item'       => __( 'Keyword', 'tps' ),
			'new_item'           => __( 'Keyword', 'tps' ),
			'edit_item'          => __( 'Keyword', 'tps' ),
			'view_item'          => __( 'Keyword', 'tps' ),
			'all_items'          => __( 'Keyword', 'tps' ),
			'search_items'       => __( 'Search Search Logs', 'tps' ),
			'parent_item_colon'  => __( 'Parent Keyword', 'tps' ),
			'not_found'          => __( 'No Keyword found.', 'tps' ),
			'not_found_in_trash' => __( 'No Keyword found in Trash.', 'tps' )

		);
		$args   = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'tps-search' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title' ),
			'menu_icon'          => "dashicons-editor-help"
		);
		register_post_type( 'tps-search', $args );

	}

}

?>