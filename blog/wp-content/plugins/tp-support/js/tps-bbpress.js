﻿/*
 File Name: ThimPress Support Javascript
 Version: 1.0
 Author: Andy Ha (tu@wpbriz.com)
 Author URI: http://wpbriz.com
 Copyright 2007-2014 wpBriz.com. All rights reserved.
 */
"use strict";
jQuery(document).ready(function () {

	/**
	 * Auto height Textarea
	 */
	jQuery('#bbp_reply_content,#bbp_topic_content').autosize();
	/**
	 * Drag and drop attachments init
	 */
	//Add area place upload
//	jQuery('#wp-bbp_reply_content-editor-container').append('<div id="ob_drag_image" class="ob_drag_image" style="background: none repeat scroll 0 0 rgba(253, 253, 253, 0.8);     display: none;     height: 100%;     left: 0;     position: absolute;     top: 0;     width: 100%;     z-index: 9;">' +
//		'<div class="aC8">' +
//		'<div class="aC9">' +
//		'<div class="aDa"> DROP IMAGES HERE</div>' +
//		'</div>' +
//		'</div>' +
//		'</div>');
//	jQuery('body').append('<div class="ob_overlay" style="background: none repeat scroll 0 0 rgba(0, 0, 0, 0.5);     display: none;     height: 100%;     left: 0;     position: absolute;     top: 0;     width: 100%;     z-index: 1;opacity: 0; "></div>');
	/**
	 * Init Facybox
	 */
	jQuery('.tps-icon').tooltip();
	jQuery("a.access-info, .wp-caption a").fancybox({
		'transitionIn' : 'elastic',
		'transitionOut': 'elastic',
		'speedIn'      : 600,
		'speedOut'     : 200
	});
	jQuery("a.ob-popup").fancybox({
		'transitionIn' : 'elastic',
		'transitionOut': 'elastic',
		'speedIn'      : 600,
		'speedOut'     : 200,
		'beforeLoad'   : function () {
			var el = jQuery(this.element);
			var reply_id = el.data('reply-id');
			if (reply_id) {
				jQuery.ajax({
					type   : 'POST',
					data   : 'data[reply_id]=' + reply_id + '&action=get_reply',
					url    : ob_ajax_url,
					success: function (html) {
						var obj = jQuery.parseJSON(html);
						if (obj.check == 'done') {
							jQuery('.ba-answerquestion').val(obj.message);
						} else {
							console.log(html);
						}
					},
					error  : function (html) {

					}
				});
			}

		}
	});
	jQuery(".bbp-topic-reply-link,.bbp-reply-to-link, a.site-info,.btn-create-topic").fancybox({
		'transitionIn' : 'elastic',
		'transitionOut': 'elastic',
		'speedIn'      : 600,
		'speedOut'     : 200,
		'minWidth'     : 800,
		'helpers'      : {
			overlay: null
		},
		'afterShow'    : function () {
			jQuery(this.wrap).easydrag({
				'holdingHandler': true
			}).css({'position': 'fixed'});
		},
		'afterClose'   : function () {
			jQuery(this.href).show();
		}
	});

	jQuery("button.site-info").fancybox({
		'transitionIn' : 'elastic',
		'transitionOut': 'elastic',
		'speedIn'      : 600,
		'speedOut'     : 200,
		'minWidth'     : 800,
		'helpers'      : {
			overlay: null
		},
		'afterShow'    : function () {
			jQuery(this.wrap).easydrag({
				'holdingHandler': true
			}).css({'position': 'fixed'});
			jQuery('.fancybox-inner #new-post a[href="#topic-accessinfo"]').tab('show');
			jQuery('.fancybox-inner #bbp_reply_content').val('I have just added the access info.');
			jQuery('.fancybox-inner textarea[name="params[site_info]"]').focus();
		},
		'afterClose'   : function () {
			jQuery(this.href).show();
		}
	});

	/**
	 * Get information when click Best Answer
	 */
	jQuery("a.ob-popup").click(function () {
		var data = '';
		var str_id = jQuery(this).closest('div.bbp-reply-header').attr('id');
		var id = parseInt(str_id.replace('post-', ''));
		jQuery('.u-ba-id').val(id);
		jQuery('.ba-id').val(id);

	});

	/**
	 * Get data reply when click Best Answer
	 */
	jQuery('.on-update-select').change(function () {
		var topic_id = parseInt(jQuery(this).val());
		jQuery.ajax({
			type   : 'POST',
			data   : 'topic_id=' + topic_id + '&action=get_topic',
			url    : ob_ajax_url,
			success: function (html) {
				var obj = jQuery.parseJSON(html);
				if (obj.check == 'done') {
					jQuery('.u-ba-title').val(obj.data.title);
					jQuery('.u-ba-askquestion').val(obj.data.ask);
					jQuery('.u-ba-answerquestion').val(obj.data.answer);
					jQuery('.u-ba-tags').val(obj.data.tags);
					jQuery('.u-kb-id').val(obj.data.id);
					jQuery('.u-content').show();
					var categories = obj.data.categories;

					if (categories.search(',') > 0) {
						categories = categories.split(",");

					}
					jQuery('.u-categories').val(categories);
				} else {
					console.log(html);
				}
			},
			error  : function (html) {

			}
		});
	})
	/**
	 * Get data in update Best Answer
	 */
	jQuery('.btn-topic-update').click(function () {
		var btn = jQuery(this);
		btn.button('loading');
		var data_topic_id = parseInt(jQuery('.ba-topic-id').val());
		var data_user_assign = parseInt(jQuery('.user-assign').val());
		var data_stutus = parseInt(jQuery('.topic-status').val());
		jQuery.ajax({
			type   : 'POST',
			data   : 'data[topic_id]=' + data_topic_id + '&data[user_assign]=' + data_user_assign + '&data[status]=' + data_stutus + '&action=update_assign',
			url    : ob_ajax_url,
			success: function (html) {
				var obj = jQuery.parseJSON(html);
				if (obj.check == 'done') {
					btn.button('reset');
				} else {
					console.log(html);
				}
			},
			error  : function (html) {

			}
		});
	});

	/**
	 * Update Best Answer
	 */
	jQuery('.u-ob-fancy-submit').click(function () {
		var btn = jQuery(this);
		btn.button('loading');
		var data_ask = jQuery('.u-ba-askquestion').val();
		var data_answer = jQuery('.u-ba-answerquestion').val();
		var data_tags = jQuery('.u-ba-tags').val();
		var data_topic_id = jQuery('.u-ba-topic-id').val();
		var data_ba_id = jQuery('.u-ba-id').val();
		var data_title = jQuery('.u-ba-title').val();
		var data_kb_id = jQuery('.u-kb-id').val();
		var data_categories = jQuery('.u-categories').val();
		if (data_title == '' || data_title == 'undefined' || data_title == null) {
			btn.button('reset');
			alert('Categories are not empty.');
			jQuery('.u-ba-title').focus();
			return;
		}
		if (data_ask == '' || data_ask == 'undefined' || data_ask == null) {
			btn.button('reset');
			alert('Ask field is require.');
			jQuery('.u-ba-askquestion').focus();
			return;
		}
		if (data_answer == '' || data_answer == 'undefined' || data_answer == null) {
			btn.button('reset');
			alert('Question field is require.');
			jQuery('.u-ba-answerquestion').focus();
			return;
		}
		if (data_categories == '' || data_categories == 'undefined' || data_categories == null) {
			btn.button('reset');
			alert('Categories are not empty.');
			return;
		} else {
			data_categories = data_categories.join();
		}
		jQuery.ajax({
			type   : 'POST',
			data   : 'data[categories]=' + data_categories + '&data[ask]=' + data_ask + '&data[kb_id]=' + data_kb_id + '&data[topic_id]=' + data_topic_id + '&data[ba_id]=' + data_ba_id + '&data[title]=' + data_title + '&data[answer]=' + data_answer + '&data[tags]=' + data_tags + '&action=best_answer',
			url    : ob_ajax_url,
			success: function (html) {
				btn.button('reset');
				parent.jQuery.fancybox.close();
				var obj = jQuery.parseJSON(html);
				if (obj.check == 'done') {
					jQuery('body').find('.best-answer-reply').remove();
					jQuery('body').find('.best-anwser').removeClass('best-anwser');
					jQuery('div.post-' + data_ba_id).addClass('best-anwser').append('<div class="best-answer-reply"><span><i class="fa fa-check"></i></span></div>')
				} else {
					console.log(html);
				}
			},
			error  : function (html) {

			}
		});
	});

	/*Create new Best Answer*/
	jQuery('.ob-fancy-submit').click(function () {
		var btn = jQuery(this);
		btn.button('loading');
		var data_ask = jQuery('.ba-askquestion').val();
		var data_answer = jQuery('.ba-answerquestion').val();
		var data_tags = jQuery('.ba-tags').val();
		var data_topic_id = jQuery('.ba-topic-id').val();
		var data_ba_id = jQuery('.ba-id').val();
		var data_title = jQuery('.ba-title').val();
		var data_categories = jQuery('.ba-categories').val();
		if (data_title == '' || data_title == 'undefined' || data_title == null) {
			btn.button('reset');
			alert('Categories are not empty.');
			jQuery('.ba-title').focus();
			return;
		}
		if (data_ask == '' || data_ask == 'undefined' || data_ask == null) {
			btn.button('reset');
			alert('Ask field is require.');
			jQuery('.ba-askquestion').focus();
			return;
		}
		if (data_answer == '' || data_answer == 'undefined' || data_answer == null) {
			btn.button('reset');
			alert('Question field is require.');
			jQuery('.ba-answerquestion').focus();
			return;
		}
		if (data_categories == '' || data_categories == 'undefined' || data_categories == null) {
			btn.button('reset');
			alert('Categories are not empty.');
			return;
		} else {
			data_categories = data_categories.join();
		}
		jQuery.ajax({
			type   : 'POST',
			data   : 'data[categories]=' + data_categories + '&data[ask]=' + data_ask + '&data[topic_id]=' + data_topic_id + '&data[ba_id]=' + data_ba_id + '&data[title]=' + data_title + '&data[answer]=' + data_answer + '&data[tags]=' + data_tags + '&action=best_answer',
			url    : ob_ajax_url,
			success: function (html) {
				btn.button('reset');
				parent.jQuery.fancybox.close();
				var obj = jQuery.parseJSON(html);
				if (obj.check == 'done') {
					jQuery('body').find('.best-answer-reply').remove();
					jQuery('body').find('.best-anwser').removeClass('best-anwser');
					jQuery('div.post-' + data_ba_id).addClass('best-anwser').append('<div class="best-answer-reply"><span><i class="fa fa-check"></i></span></div>')
				} else {
					console.log(html);
				}
			},
			error  : function (html) {

			}
		});
	});
	/*Close Fanxy light box*/
	jQuery('.ob-fancy-cancel').click(function () {
		parent.jQuery.fancybox.close();
	});

	/*Init Chosen*/
	var config = {
		'.chosen-select'         : {width: "auto", disable_search_threshold: 8},
		'.chosen-select-deselect': {allow_single_deselect: true, width: "auto", disable_search_threshold: 3}
	}
	for (var selector in config) {
		jQuery(selector).chosen(config[selector]);
	}

	/* Live add question link */
	jQuery('#bbp_reply_content').on('keypress', function (event) {

		var cursorPosition = jQuery(this).prop("selectionStart");
		if (event.ctrlKey) {
			if (event.keyCode == 13) {
				jQuery('#new-post').submit();
				event.preventDefault();
			}
		} else {

			if (event.keyCode == 13) {
				var check = insertToBBCode(1);
				if (check) {
					jQuery('#bbp_reply_content').focus();
					event.preventDefault();
				}

			}
			if (event.keyCode == 38) {
				var selected = jQuery(".ob-selected");
				if (selected.length > 0) {
					if (jQuery(".autocomplete-list li").length > 1) {
						jQuery(".autocomplete-list li").removeClass("ob-selected");

						// if there is no element before the selected one, we select the last one
						if (selected.prev().length == 0) {
							selected.siblings().last().addClass("ob-selected");
						} else { // otherwise we just select the next one
							selected.prev().addClass("ob-selected");
						}
						event.preventDefault();
					}
				}
			}
			if (event.keyCode == 40) {
				var selected = jQuery(".ob-selected");
				if (selected.length > 0) {
					if (jQuery(".autocomplete-list li").length > 1) {
						jQuery(".autocomplete-list li").removeClass("ob-selected");

						// if there is no element before the selected one, we select the last one
						if (selected.next().length == 0) {
							selected.siblings().first().addClass("ob-selected");
						} else { // otherwise we just select the next one
							selected.next().addClass("ob-selected");
						}
						event.preventDefault();
					}
				}
			}
		}
	});
	jQuery('#bbp_reply_content').on('keyup', function (event) {

		clearTimeout(jQuery.data(this, 'timer'));
		if (event.ctrlKey || event.which == 17) {

		} else {
			if (event.which == 38) {
				if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15) {
					var selected = jQuery(".ob-selected");
					if (jQuery(".autocomplete-list li").length > 1) {
						jQuery(".autocomplete-list li").removeClass("ob-selected");

						// if there is no element before the selected one, we select the last one
						if (selected.prev().length == 0) {
							selected.siblings().last().addClass("ob-selected");
						} else { // otherwise we just select the next one
							selected.prev().addClass("ob-selected");
						}
					}
				}
			} else if (event.which == 40) {
				if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15) {
					var selected = jQuery(".ob-selected");
					if (jQuery(".autocomplete-list li").length > 1) {
						jQuery(".autocomplete-list li").removeClass("ob-selected");

						// if there is no element before the selected one, we select the last one
						if (selected.next().length == 0) {
							selected.siblings().first().addClass("ob-selected");
						} else { // otherwise we just select the next one
							selected.next().addClass("ob-selected");
						}
					}
				}
			} else {
				jQuery(this).data('timer', setTimeout(tpsSearch, 500));
			}
		}
	});

	/*Canned Response*/
	jQuery('.tps-canned').change(function () {
		var cursor = '/\{cursor\}/i';
		var reply_id = parseInt(jQuery(this).val());
		var reply_content = jQuery('.bpp-canned-' + reply_id).html();
		var textArea = jQuery('#bbp_reply_content');
		textArea.val(reply_content);
		var index = textArea.val().search('{cursor}');
		reply_content = textArea.val();
		reply_content = reply_content.replace('{cursor}', '');
		textArea.val(reply_content);
		if (index > 0) {
			textArea.selectRange(index, index);
		}
	});
	var replyFrom = jQuery('#bbp_reply_content');
	if (replyFrom.length > 0) {
		var index = replyFrom.val().search('{cursor}');
		var reply_content = replyFrom.val();
		reply_content = reply_content.replace('{cursor}', '');
		replyFrom.val(reply_content);
		if (index > 0) {
			replyFrom.selectRange(index, index);
		}
	}
	/**
	 * Short code submit reply
	 */
	jQuery('#bbp_topic_content').keypress(function (e) {
		if (e.ctrlKey) {
			if (e.keyCode == 13) {
				jQuery('#new-post').submit();
			}
		}
	});


	/*Suggestion on Create Topic*/

	jQuery('#bbp-topic-title').on('keyup', function (event) {

		clearTimeout(jQuery.data(this, 'timer'));
		if (event.which == 13) {
			event.preventDefault();
			jQuery(this).stop();
		} else if (event.which == 38) {
			if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15) {
				var selected = jQuery(".ob-selected");
				if (jQuery(".ob-list-search li").length > 1) {
					jQuery(".ob-list-search li").removeClass("ob-selected");

					// if there is no element before the selected one, we select the last one
					if (selected.prev().length == 0) {
						selected.siblings().last().addClass("ob-selected");
					} else { // otherwise we just select the next one
						selected.prev().addClass("ob-selected");
					}
				}
			}
		} else if (event.which == 40) {
			if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15) {
				var selected = jQuery(".ob-selected");
				if (jQuery(".ob-list-search li").length > 1) {
					jQuery(".ob-list-search li").removeClass("ob-selected");

					// if there is no element before the selected one, we select the last one
					if (selected.next().length == 0) {
						selected.siblings().first().addClass("ob-selected");
					} else { // otherwise we just select the next one
						selected.next().addClass("ob-selected");
					}
				}
			}
		} else if (event.which == 27) {
			jQuery('.ob-list-suggestions').html('');
			jQuery(this).val('');
			jQuery(this).stop();
		} else if (event.which == 8) {
			jQuery('.ob-list-suggestions').html('');
		} else {
			jQuery(this).data('timer', setTimeout(livesuggestionsTopic, 400));
		}
	});
	jQuery('#bbp-topic-title').on('keypress', function (event) {

		if (event.keyCode == 13) {
			var selected = jQuery(".ob-selected");
			if (selected.length > 0) {
				var ob_href = selected.find('a').first().attr('href');
				window.location.href = ob_href;
			}
			event.preventDefault();
		}
		if (event.keyCode == 27) {

		}
		if (event.keyCode == 38) {
			var selected = jQuery(".ob-selected");
			if (jQuery(".ob-list-search li").length > 1) {
				jQuery(".ob-list-search li").removeClass("ob-selected");

				// if there is no element before the selected one, we select the last one
				if (selected.prev().length == 0) {
					selected.siblings().last().addClass("ob-selected");
				} else { // otherwise we just select the next one
					selected.prev().addClass("ob-selected");
				}
			}
		}
		if (event.keyCode == 40) {
			var selected = jQuery(".ob-selected");
			if (jQuery(".ob-list-search li").length > 1) {
				jQuery(".ob-list-search li").removeClass("ob-selected");

				// if there is no element before the selected one, we select the last one
				if (selected.next().length == 0) {
					selected.siblings().first().addClass("ob-selected");
				} else { // otherwise we just select the next one
					selected.next().addClass("ob-selected");
				}
			}
		}
	});

	jQuery('.ob-list-suggestions,#bbp-topic-title').click(function (event) {
		event.stopPropagation();
	});

	jQuery(document).click(function () {
		jQuery(".ob-list-suggestions li").remove();
	});
});

/*Auto search in BBPress*/
function tpsSearch() {

	var word = /@kb:(\w+)$/i;
	var oField = jQuery('#bbp_reply_content');
	var pos = oField.caret();
	var content = oField.val(); //Content Box Data
	var part1 = content.substring(0, pos);
	var keyword = part1.match(word); //Content Matching @abc
	if (keyword) {
		keyword = keyword[1];
		jQuery.ajax({
			type   : 'POST',
			data   : 'data[keyword]=' + keyword + '&action=reply_autosearch',
			url    : ob_ajax_url,
			success: function (html) {
				var obj = jQuery.parseJSON(html);
				var items = obj.message;
				var data_li = '';
				if (obj.check == 'done') {
					if (items.length > 0) {
						jQuery.each(items, function (index) {
							if (index == 0) {
								data_li += '<li class="ui-menu-item ob-selected"><a  href="' + this['guid'] + '">' + this['title'] + '</a></li>';
							} else {
								data_li += '<li class="ui-menu-item"><a href="' + this['guid'] + '">' + this['title'] + '</a></li>';
							}
						});
					} else {
						data_li = '<li class="ui-menu-item ob-selected">Items not found.</li>';
					}
					jQuery('.autocomplete-list').html('').append(data_li);
					jQuery('#text-pos').val(pos);
					insertToBBCode(0);
					jQuery('#bbp-reply-content').focus();
				} else {
					console.log(html);
				}
			},
			error  : function (html) {
			}
		});
	} else {
		jQuery('.autocomplete-list').html('');
	}
}
function insertToBBCode(wait) {
	var pos = jQuery('#text-pos').val();
	var word = /@kb:(\w+)$/i;
	var oField = jQuery('#bbp_reply_content');
	var content = oField.val(); //Content Box Data
	var part1 = content.substring(0, pos);
	var part2 = content.substring(pos);

	var keyword = part1.match(word); //Content Matching @abc
	jQuery('.autocomplete-list li a').click(function (event) {
		event.preventDefault();
	});
	if (wait) {
		if (keyword) {
			keyword = keyword[0];
			var text_search = jQuery('.ob-selected').html();
			part1 = part1.replace(word, text_search);
			content = part1 + part2;
			oField.val(content);
			jQuery('.autocomplete-list').html('');
			oField.selectRange(part1.length, part1.length);
			return true;
		}
		return false;

	} else {
		jQuery('.autocomplete-list li').on('click', function (event) {

			if (keyword) {
				keyword = keyword[0];
				var text_search = jQuery(this).html();
				part1 = part1.replace(word, text_search);
				content = part1 + part2;
				oField.val(content);
				jQuery('.autocomplete-list').html('');
				oField.selectRange(part1.length, part1.length);
				return true;
			}
			event.preventDefault();
		});
	}
}

jQuery.fn.selectRange = function (start, end) {
	return this.each(function () {
		if (this.setSelectionRange) {
			this.focus();
			this.setSelectionRange(start, end);
		} else if (this.createTextRange) {
			var range = this.createTextRange();
			range.collapse(true);
			range.moveEnd('character', end);
			range.moveStart('character', start);
			range.select();
		}
	});
};
function stripslashes(str) {
	//       discuss at: http://phpjs.org/functions/stripslashes/
	//      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//      improved by: Ates Goral (http://magnetiq.com)
	//      improved by: marrtins
	//      improved by: rezna
	//         fixed by: Mick@el
	//      bugfixed by: Onno Marsman
	//      bugfixed by: Brett Zamir (http://brett-zamir.me)
	//         input by: Rick Waldron
	//         input by: Brant Messenger (http://www.brantmessenger.com/)
	// reimplemented by: Brett Zamir (http://brett-zamir.me)
	//        example 1: stripslashes('Kevin\'s code');
	//        returns 1: "Kevin's code"
	//        example 2: stripslashes('Kevin\\\'s code');
	//        returns 2: "Kevin\'s code"

	return (str + '')
		.replace(/\\(.?)/g, function (s, n1) {
			switch (n1) {
				case '\\':
					return '\\';
				case '0':
					return '\u0000';
				case '':
					return '';
				default:
					return n1;
			}
		});
}
function livesuggestionsTopic(waitKey) {
	var keyword = jQuery('#bbp-topic-title').val();

	if (keyword) {
		if (!waitKey && keyword.length < 3) {
			return;
		}
		jQuery.ajax({
			type   : 'POST',
			data   : 'action=live_search&keyword=' + keyword + '&from=create_topic',
			url    : ob_ajax_url,
			success: function (html) {
				var data_li = '';
				var items = jQuery.parseJSON(html);
				var elClass = '';

				jQuery.each(items, function (index) {
					switch (this['post_type']) {
						case 'post':
							elClass = ' fa-file-text ';
							break;
						case 'topic':
							elClass = ' fa-comment ';
							break;
						case 'tps':
							elClass = ' fa-book ';
							break;
						case 'tps-faq':
							elClass = ' fa-question ';
							break;
						default :
							elClass = '';
					}
					if (this['title'] == 'Results empty.') {
						return;
					}
					if (index == 0) {
						data_li += '<li class="ui-menu-item' + this['id'] + ' ob-selected"><a id="ui-id-' + this['id'] + '" class="ui-corner-all" href="' + this['guid'] + '"><i class="fa ' + elClass + ' fa-fw"></i><span class="search-title">' + this['title'] + '</span></a></li>';
					} else {
						data_li += '<li class="ui-menu-item' + this['id'] + '"><a id="ui-id-' + this['id'] + '" class="ui-corner-all" href="' + this['guid'] + '"><i class="fa ' + elClass + ' fa-fw"></i><span class="search-title">' + this['title'] + '</span></a></li>';
					}
				});
				jQuery('.ob-list-suggestions').html('').append(data_li);
			},
			error  : function (html) {
			}
		});
	}
}

/*
 *  Drag and Drop attachments
 */
//jQuery(window).load(function () {
//
//	//Drag on editor with body
//	jQuery(document).on('dragover', function (e) {
//		jQuery('.ob_overlay').show().animate({'opacity': '1'}, 200);
//		jQuery('.ob_drag_image').show();
//	});
//
//	var dropbox;
//
//	dropbox = document.getElementById("ob_drag_image");
//	dropbox.addEventListener("dragenter", dragenter, false);
//	dropbox.addEventListener("dragover", dragover, false);
//	dropbox.addEventListener("drop", drop, false);
//});
//function dragenter(e) {
//	e.stopPropagation();
//	e.preventDefault();
//}
//
//function dragover(e) {
//	e.stopPropagation();
//	e.preventDefault();
//}
//function drop(e) {
//	jQuery('.ob_overlay').hide().animate({'opacity': '0'});
//	jQuery('.ob_drag_image').hide();
//	e.stopPropagation();
//	e.preventDefault();
//
//	var dt = e.dataTransfer;
//	var files = dt.files;
//
//	handleFiles(files);
//}
//function handleFiles(files) {
//	console.log(files);
//
//}