﻿/*
 File Name: ThimPress Support Javascript
 Version: 1.0
 Author: Andy Ha (tu@wpbriz.com)
 Author URI: http://wpbriz.com
 Copyright 2007-2014 wpBriz.com. All rights reserved.
 */
"use strict";
jQuery(document).ready(function () {
	/*TimeWork*/
	setInterval(function () {
		var current_tz = 'current_timezone';
		var current_time = getdate(current_tz);
		var our_clock = current_time[1] + ":" + current_time[2] + ":" + current_time[3] + " " + current_time[0];
		jQuery("#spt_our_time").text(our_clock);
		jQuery('.spt-out-time').attr('title', 'Our Time: ' + current_time['Month'] + ' ' + current_time['dd'] + ', ' + current_time['YY'] + ' ' + our_clock);

		var timezone = '';
		var guest_time = getdate(timezone);
		var your_clock = guest_time[1] + ":" + guest_time[2] + ":" + guest_time[3] + " " + guest_time[0];
		jQuery("#spt_local_time").text(your_clock);
		jQuery('.spt-your-time').attr('title', 'Your Time: ' + guest_time['Month'] + ' ' + guest_time['dd'] + ', ' + guest_time['YY'] + ' ' + your_clock);
	}, 1000);
	/*Get date*/
	function getdate(timezone) {
		var guest_today = new Date();
		var today = (timezone == 'current_timezone') ? smart_today : guest_today;
		if (timezone == 'current_timezone') {
			today.setSeconds(today.getSeconds() + 1);
			today.setSeconds(guest_today.getSeconds());
		}
		var ap = '';
		var h = today.getHours();
		var m = today.getMinutes();
		var s = today.getSeconds();
		var time = [];
		time[0] = ap = ( h < 12 ) ? 'AM' : 'PM';
		time[1] = h = ( h <= 12 ) ? h : h - 12;
		time[1] = h = ( h < 10 ) ? '0' + h : h;
		time[2] = m = ( m < 10 ) ? '0' + m : m;
		time[3] = s = ( s < 10 ) ? '0' + s : s;
		time['dd'] = ( today.getDate() < 10 ) ? '0' + today.getDate() : today.getDate();
		time['mm'] = ( today.getMonth() + 1 < 10) ? '0' + ( today.getMonth() + 1 ) : today.getMonth() + 1;
		time['YY'] = today.getFullYear();
		time['Day'] = today.getDay();
		var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
		time['Month'] = monthNames[today.getMonth()];
		return time;
	};
});
