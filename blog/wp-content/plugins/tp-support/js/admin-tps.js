﻿/*
 File Name: ThimPress Support Javascript Admin
 Version: 1.0
 Author: Andy Ha (tu@wpbriz.com)
 Author URI: http://wpbriz.com
 Copyright 2007-2014 wpBriz.com. All rights reserved.
 */
"use strict";
jQuery(document).ready(function () {

	var config = {
		'.chosen-select'         : {width: "95%",disable_search_threshold: 8},
		'.chosen-select-deselect': {allow_single_deselect: true, width: "95%", disable_search_threshold: 8}
	}
	for (var selector in config) {
		jQuery(selector).chosen(config[selector]);
	}
	/**
	 * Add new field avatar
	 * @type {*}
	 */
	var $row = jQuery('#add_new_default_avatar').html();
	jQuery('#add_new_default_avatar').click(function () {
		var row2 = '<p>' + $row + '</p>';

		//get original unique id
		var regex = new RegExp(jQuery('#add_new_default_avatar_garbage').val(), "g");

		//replace with time-generated one
		var newDate = new Date;
		var k = newDate.getTime();

		row2 = row2.replace(regex, k);
		jQuery(this).before(row2);
		return false;
	});
	// Product gallery file uploads
	var product_icon;
	var $image_icon_ids = jQuery('#product-icon');
	var $product_icons = jQuery('#product_icons_container ul.product_icons');

	jQuery('.add_product_icon').on('click', 'a', function (event) {
		var $el = jQuery(this);
		var attachment_ids = $image_icon_ids.val();

		event.preventDefault();

		// If the media frame already exists, reopen it.
		if (product_icon) {
			product_icon.open();
			return;
		}

		// Create the media frame.
		product_icon = wp.media.frames.product_gallery = wp.media({
			// Set the title of the modal.
			title : $el.data('choose'),
			button: {
				text: $el.data('update')
			},
			states: [
				new wp.media.controller.Library({
					title     : $el.data('choose'),
					filterable: 'all',
					multiple  : true
				})
			]
		});

		// When an image is selected, run a callback.
		product_icon.on('select', function () {

			var selection = product_icon.state().get('selection');

			selection.map(function (attachment) {

				attachment = attachment.toJSON();

				if (attachment.id) {
					attachment_ids = attachment.id;

					$product_icons.html('\
					<li class="image" data-attachment_id="' + attachment.id + '">\
						<img src="' + attachment.url + '" />\
						<ul class="actions">\
							<li><a href="#" class="delete" title="' + $el.data('delete') + '">' + $el.data('text') + '</a></li>\
						</ul>\
					</li>');
				}

			});

			$image_icon_ids.val(attachment_ids);
		});

		// Finally, open the modal.
		product_icon.open();
	});
	// Remove images
	jQuery('#product_icons_container').on('click', 'a.delete', function () {
		jQuery(this).closest('li.image').remove();

		var attachment_ids = '';

		$product_icons.find('li').remove();

		$image_icon_ids.val('');

		runTipTip();
		event.preventDefault();
		return false;
	});
});

