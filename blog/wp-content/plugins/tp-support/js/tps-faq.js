﻿/*
 File Name: ThimPress Support Javascript
 Version: 1.0
 Author: Andy Ha (tu@wpbriz.com)
 Author URI: http://wpbriz.com
 Copyright 2007-2014 wpBriz.com. All rights reserved.
 */
"use strict";
jQuery(document).ready(function () {
	/*Check Open FAQ*/
	var anchor = window.location.hash.replace("#", "");
	if (anchor) {
		jQuery(".collapse").collapse('hide');
		jQuery("#" + anchor).collapse('show').prev().addClass("highlight");
	}

});