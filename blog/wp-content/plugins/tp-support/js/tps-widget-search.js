﻿/*
 File Name: ThimPress Support Javascript
 Version: 1.0
 Author: Andy Ha (tu@wpbriz.com)
 Author URI: http://wpbriz.com
 Copyright 2007-2014 wpBriz.com. All rights reserved.
 */
"use strict";
jQuery(document).ready(function () {
	 /*if (jQuery('#login_form').length > 0) {
		jQuery(".notlogin .btn-request-support, .form_register").fancybox({
			'transitionIn' : 'elastic',
			'transitionOut': 'elastic',
			'speedIn'      : 600,
			'speedOut'     : 200,
			'afterClose'   : function () {
				jQuery(this.href).show();
			}
		});
	} */
	// jQuery('input').tooltip(); 
	jQuery('.popular-keyword').click(function () {
		var text = jQuery(this).text();
		jQuery('#data-search').val(text).focus();
		jQuery('#data-search').data('timer', setTimeout(livesearch));
	});
	jQuery('#data-search').on('keyup', function (event) {

		clearTimeout(jQuery.data(this, 'timer'));
		if (event.which == 13) {
			event.preventDefault();
			jQuery(this).stop();
		} else if (event.which == 38) {
			if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15) {
				var selected = jQuery(".ob-selected");
				if (jQuery(".ob-list-search li").length > 1) {
					jQuery(".ob-list-search li").removeClass("ob-selected");

					// if there is no element before the selected one, we select the last one
					if (selected.prev().length == 0) {
						selected.siblings().last().addClass("ob-selected");
					} else { // otherwise we just select the next one
						selected.prev().addClass("ob-selected");
					}
				}
			}
		} else if (event.which == 40) {
			if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15) {
				var selected = jQuery(".ob-selected");
				if (jQuery(".ob-list-search li").length > 1) {
					jQuery(".ob-list-search li").removeClass("ob-selected");

					// if there is no element before the selected one, we select the last one
					if (selected.next().length == 0) {
						selected.siblings().first().addClass("ob-selected");
					} else { // otherwise we just select the next one
						selected.next().addClass("ob-selected");
					}
				}
			}
		} else if (event.which == 27) {
			jQuery('.ob-list-search').html('');
			jQuery(this).val('');
			jQuery(this).stop();
		} else if (event.which == 8) {
			jQuery('.ob-list-search').html('');
		} else {
			jQuery(this).data('timer', setTimeout(livesearch, 700));
		}
	}); 
	jQuery('#data-search').on('keypress', function (event) {

		if (event.keyCode == 13) {
			var selected = jQuery(".ob-selected");
			if (selected.length > 0) {
				var ob_href = selected.find('a').first().attr('href');
				window.location.href = ob_href;
			}
			event.preventDefault();
		}
		if (event.keyCode == 27) {

		}
		if (event.keyCode == 38) {
			var selected = jQuery(".ob-selected");
			// if there is no element before the selected one, we select the last one
			if (jQuery(".ob-list-search li").length > 1) {
				jQuery(".ob-list-search li").removeClass("ob-selected");
				if (selected.prev().length == 0) {
					selected.siblings().last().addClass("ob-selected");
				} else { // otherwise we just select the next one
					selected.prev().addClass("ob-selected");
				}
			}
		}
		if (event.keyCode == 40) {
			var selected = jQuery(".ob-selected");
			if (jQuery(".ob-list-search li").length > 1) {
				jQuery(".ob-list-search li").removeClass("ob-selected");

				// if there is no element before the selected one, we select the last one
				if (selected.next().length == 0) {
					selected.siblings().first().addClass("ob-selected");
				} else { // otherwise we just select the next one
					selected.next().addClass("ob-selected");
				}
			}
		}
	});

	jQuery('.ob-list-search,#data-search').click(function (event) {
		event.stopPropagation();
	});

	jQuery(document).click(function () {
		jQuery(".ob-list-search li").remove();
	});
	/*Button Search Tooltip*/
	jQuery('.notlogin .btn-request-support').hover(function () {
		//jQuery(this).tooltip('show');
	}, function () {
		//jQuery(this).tooltip('hide');
	});
	jQuery('#data-search').focus(function () {
		//jQuery(this).attr('placeholder', 'Please let us know your problem');
		jQuery(this).attr('placeholder', data_search_placeholder.focus );
	}).blur(function () {
		//jQuery(this).attr('placeholder', 'How do we help?')
		jQuery(this).attr('placeholder', data_search_placeholder.blur );
	});

});
function livesearch(waitKey) {
	var keyword = jQuery('#data-search').val();

	if (keyword) {
		if (!waitKey && keyword.length < 3) {
			return;
		}
		jQuery('.deskpress-smartsearch .fa-search').addClass('loading');
		jQuery('.btn-request-support').animate({'opacity': 0.4}, 500);
		jQuery('.btn-request-support').attr('disabled', 'disabled');
		//jQuery('.btn-request-support').tooltip('hide');
		jQuery.ajax({
			type   : 'POST',
			data   : 'action=live_search&keyword=' + keyword + '&from=search',
			url    : ob_ajax_url,
			success: function (html) {
				var data_li = '';
				var items = jQuery.parseJSON(html);
				var elClass = '';
				jQuery.each(items, function (index) {
					switch (this['post_type']) {
						case 'post':
							elClass = ' fa-file-text ';
							break;
						case 'topic':
							elClass = ' fa-comment ';
							break;
						case 'tps':
							elClass = ' fa-book ';
							break;
						case 'tps-faq':
							elClass = ' fa-question ';
							break;
						case 'showmore':
							elClass = 'showmore';
							break;
						default :
							elClass = '';
					}
					if (index == 0) {
						data_li += '<li class="ui-menu-item' + this['id'] + ' ob-selected"><a id="ui-id-' + this['id'] + '" class="ui-corner-all" href="' + this['guid'] + '"><i class="fa ' + elClass + ' fa-fw"></i><span class="search-title">' + this['title'] + '</span></a></li>';
					} else if (elClass == 'showmore') {
						data_li += '<li class="ui-menu-item' + this['id'] + ' ' + elClass + '"><a id="ui-id-' + this['id'] + '" class="ui-corner-all" href="' + this['guid'] + '"><span class="search-title">' + this['title'] + '</span></a></li>';
					} else {
						data_li += '<li class="ui-menu-item' + this['id'] + '"><a id="ui-id-' + this['id'] + '" class="ui-corner-all" href="' + this['guid'] + '"><i class="fa ' + elClass + ' fa-fw"></i><span class="search-title">' + this['title'] + '</span></a></li>';
					}
				});
				jQuery('.ob-list-search').html('').append(data_li);
				jQuery('button[data-target="#quick-create-topic"]').removeAttr('disabled').addClass('fadeIn');
				searchHover();
				jQuery('.deskpress-smartsearch .fa-search').removeClass('loading');

				jQuery('.btn-request-support').animate({'opacity': 1}, 500);
				jQuery('.btn-request-support').removeAttr('disabled');
				//jQuery('.btn-request-support').tooltip('show');
				var val = jQuery('#data-search').val();
				jQuery('.q-title').val(val);

			},
			error  : function (html) {
			}
		});
	}
}
function searchHover() {
	jQuery('.ob-list-search li').on('hover', function () {
		jQuery('.ob-list-search li').removeClass('ob-selected');
		jQuery(this).addClass('ob-selected');
	});
}