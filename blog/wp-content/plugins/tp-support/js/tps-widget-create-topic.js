﻿/*
 File Name: ThimPress Support Javascript
 Version: 1.0
 Author: Andy Ha (tu@wpbriz.com)
 Author URI: http://wpbriz.com
 Copyright 2007-2014 wpBriz.com. All rights reserved.
 */
"use strict";
jQuery(document).ready(function () {

	jQuery('.btn_createtopic').click(function () {
		var btn = jQuery(this);
		btn.button('loading')
		var data_title = jQuery('input[name="data[title]"]').val();
		if (data_title == null || data_title == '' || data_title == 'undefined') {
			btn.button('reset');
			jQuery('input[name="data[title]"]').focus();
			return;
		}
		var data_content = jQuery('textarea[name="data[content]"]').val();
		if (data_content == null || data_content == '' || data_content == 'undefined') {
			btn.button('reset');
			jQuery('textarea[name="data[content]"]').focus();
			return;
		}
		var data_categories = jQuery('select[name="data[category]"]').val();

		var el = jQuery(this);
		jQuery.ajax({
			type   : 'POST',
			data   : 'data[title]=' + data_title + '&data[category]=' + data_categories + '&data[content]=' + data_content + '&action=create_topic',
			url    : ob_ajax_url,
			success: function (html) {
				btn.button('reset');
				var obj = jQuery.parseJSON(html);
				if (obj.check == 'done') {
					jQuery('#quick-create-topic').modal('hide')
				} else if (obj.check == 'note') {
					jQuery('#quick-create-topic .modal-body').prepend('<div class="alert alert-warning" role="alert">' + obj.message + '</div>');
				}
			},
			error  : function (html) {

			}
		});
	});
	/*Suggestion on Create Topic*/

	jQuery('.q-title').on('keyup', function (event) {

		clearTimeout(jQuery.data(this, 'timer'));
		if (event.which == 13) {
			event.preventDefault();
			jQuery(this).stop();
		} else if (event.which == 38) {
			if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15) {
				var selected = jQuery(".ob-selected");
				if (jQuery(".ob-list-search li").length > 1) {
					jQuery(".ob-list-search li").removeClass("ob-selected");

					// if there is no element before the selected one, we select the last one
					if (selected.prev().length == 0) {
						selected.siblings().last().addClass("ob-selected");
					} else { // otherwise we just select the next one
						selected.prev().addClass("ob-selected");
					}
				}
			}
		} else if (event.which == 40) {
			if (navigator.userAgent.indexOf('Chrome') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Chrome') + 7).split(' ')[0]) >= 15) {
				var selected = jQuery(".ob-selected");
				if (jQuery(".ob-list-search li").length > 1) {
					jQuery(".ob-list-search li").removeClass("ob-selected");

					// if there is no element before the selected one, we select the last one
					if (selected.next().length == 0) {
						selected.siblings().first().addClass("ob-selected");
					} else { // otherwise we just select the next one
						selected.next().addClass("ob-selected");
					}
				}
			}
		} else if (event.which == 27) {
			jQuery('.ob-list-suggestions').html('');
			jQuery(this).val('');
			jQuery(this).stop();
		} else if (event.which == 8) {
			jQuery('.ob-list-suggestions').html('');
		} else {
			jQuery(this).data('timer', setTimeout(livesuggestions, 400));
		}
	});
	jQuery('.q-title').on('keypress', function (event) {

		if (event.keyCode == 13) {
			var selected = jQuery(".ob-selected");
			if (selected.length > 0) {
				var ob_href = selected.find('a').first().attr('href');
				window.location.href = ob_href;
			}
			event.preventDefault();
		}
		if (event.keyCode == 27) {

		}
		if (event.keyCode == 38) {
			var selected = jQuery(".ob-selected");
			if (jQuery(".ob-list-search li").length > 1) {
				jQuery(".ob-list-search li").removeClass("ob-selected");

				// if there is no element before the selected one, we select the last one
				if (selected.prev().length == 0) {
					selected.siblings().last().addClass("ob-selected");
				} else { // otherwise we just select the next one
					selected.prev().addClass("ob-selected");
				}
			}
		}
		if (event.keyCode == 40) {
			var selected = jQuery(".ob-selected");
			if (jQuery(".ob-list-search li").length > 1) {
				jQuery(".ob-list-search li").removeClass("ob-selected");

				// if there is no element before the selected one, we select the last one
				if (selected.next().length == 0) {
					selected.siblings().first().addClass("ob-selected");
				} else { // otherwise we just select the next one
					selected.next().addClass("ob-selected");
				}
			}
		}
	});

	jQuery('.ob-list-suggestions,.q-title').click(function (event) {
		event.stopPropagation();
	});

	jQuery(document).click(function () {
		jQuery(".ob-list-suggestions li").remove();
	});
})

function livesuggestions(waitKey) {
	var keyword = jQuery('.q-title').val();

	if (keyword) {
		if (!waitKey && keyword.length < 3) {
			return;
		}
		jQuery.ajax({
			type   : 'POST',
			data   : 'action=live_search&keyword=' + keyword + '&from=create_topic',
			url    : ob_ajax_url,
			success: function (html) {
				var data_li = '';
				var items = jQuery.parseJSON(html);
				var elClass = '';

				jQuery.each(items, function (index) {
					switch (this['post_type']) {
						case 'post':
							elClass = ' fa-file-text ';
							break;
						case 'topic':
							elClass = ' fa-comment ';
							break;
						case 'tps':
							elClass = ' fa-book ';
							break;
						case 'tps-faq':
							elClass = ' fa-question ';
							break;
						default :
							elClass = '';
					}
					if (this['title'] == 'Results empty.') {
						return;
					}
					if (index == 0) {
						data_li += '<li class="ui-menu-item' + this['id'] + ' ob-selected"><a id="ui-id-' + this['id'] + '" class="ui-corner-all" href="' + this['guid'] + '"><i class="fa ' + elClass + ' fa-fw"></i><span class="search-title">' + this['title'] + '</span></a></li>';
					} else {
						data_li += '<li class="ui-menu-item' + this['id'] + '"><a id="ui-id-' + this['id'] + '" class="ui-corner-all" href="' + this['guid'] + '"><i class="fa ' + elClass + ' fa-fw"></i><span class="search-title">' + this['title'] + '</span></a></li>';
					}
				});
				jQuery('.ob-list-suggestions').html('').append(data_li);
			},
			error  : function (html) {
			}
		});
	}
}