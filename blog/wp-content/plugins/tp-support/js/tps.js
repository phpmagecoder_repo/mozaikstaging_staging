﻿/*
 File Name: ThimPress Support Javascript
 Version: 1.0
 Author: Andy Ha (tu@wpbriz.com)
 Author URI: http://wpbriz.com
 Copyright 2007-2014 wpBriz.com. All rights reserved.
 */
"use strict";
jQuery(document).ready(function () {
	jQuery(".content-kb figure a").fancybox({
		'titleShow'     : false
	});

	jQuery('.tps-icon').tooltip();
	/**
	 * Search Envato User
	 */
	jQuery('.btn-search-users').click(function () {
		var btn = jQuery(this);
		btn.button('loading');
		var data_user = jQuery('#data-search-user').val();

		if (data_user == '' || data_user == 'undefined' || data_user == null) {
			btn.button('reset');
			alert('Input field can not empty.');
			jQuery('#data-search-user').focus();
			return;
		}

		jQuery.ajax({
			type   : 'POST',
			data   : 'data[user]=' + data_user + '&action=envato_user',
			url    : ob_ajax_url,
			success: function (html) {
				btn.button('reset');
				var obj = jQuery.parseJSON(html);
				console.log(obj);
				if (obj.check == 'done') {
					jQuery('.ob-list-users').append(obj.message);
				} else {
					console.log(html);
				}
			},
			error  : function (html) {

			}
		});
	});
});


function ArticleHelpful(id) {
	jQuery.ajax({
		type   : 'POST',
		data   : 'id=' + id + '&action=kbhelpful',
		url    : ob_ajax_url,
		success: function (html) {
			var obj = jQuery.parseJSON(html);
			var items = obj.message;
			if (obj.check == 'done') {
				location.reload();
			} else {
				console.log(html);
			}
		},
		error  : function (html) {
		}
	});
}
function ArticleNotHelpful(id) {
	jQuery.ajax({
		type   : 'POST',
		data   : 'id=' + id + '&action=kbunhelpful',
		url    : ob_ajax_url,
		success: function (html) {
			var obj = jQuery.parseJSON(html);
			var items = obj.message;
			if (obj.check == 'done') {
				location.reload();
			} else {
				console.log(html);
			}
		},
		error  : function (html) {
		}
	});
}