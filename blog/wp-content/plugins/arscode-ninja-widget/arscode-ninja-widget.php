<?php

/*
  Plugin Name: Ninja Widget
  Plugin URI: http://codecanyon.net/item/ninja-popups-for-wordpress/3476479?ref=arscode
  Description: Add Ninja Popup as Widget. Use [ninja-inline id=X] shortcode and add Ninja Popup everywhere!
  Version: 1.2
  Author: ARSCode
  Author URI: http://www.arscode.pro/
  License:
 */

function snp_ninja_inline_shortcode($attr, $content = null)
{
    extract(shortcode_atts(array('id' => ''), $attr));
    if (intval($id))
    {
        snp_run_popup($id, 'inline');
        $count		 = get_post_meta($id, 'snp_views');
        if (!$count || !$count[0])
            $count[0]	 = 0;
        update_post_meta($id, 'snp_views', $count[0] + 1);
        $ID_snp_theme	 = get_post_meta($id, 'snp_theme');
        if (!empty($ID_snp_theme) &&
            (
            $ID_snp_theme[0]['type'] == 'social' ||
            $ID_snp_theme[0]['type'] == 'likebox'
            )
        )
        {
            snp_enqueue_social_script();
        }
        return snp_create_popup($id, false, 'inline');
    }
}

add_shortcode('ninja-inline', 'snp_ninja_inline_shortcode');

function snp_ninja_widget_1()
{
    if (file_exists(dirname(__FILE__) . '/../arscode-ninja-popups/arscode-ninja-popups.php'))
    {
        $np_data = get_plugin_data(dirname(__FILE__) . '/../arscode-ninja-popups/arscode-ninja-popups.php');
        if (version_compare($np_data['Version'], '3.1', '>='))
        {
            return;
        }
    }
    echo "<div style=\"padding: 20px; background-color: #ef9999; margin: 40px; border: 1px solid #cc0000; \"><b>Requirement: you need to have version 3.1+ of Ninja Popup for Wordpress to run Ninja Widget.</b></div>";
}

add_action('admin_notices', 'snp_ninja_widget_1');

function snpw_register_tinymce_plugin($plugin_array)
{
    $plugin_array['snpw_button_addon'] = plugins_url('/admin/js/snp_button_addon.js', __FILE__);
    return $plugin_array;
}

function snpw_add_tinymce_button($buttons)
{
    $buttons[] = "snpw_button_addon";
    return $buttons;
}

function snpw_ajax_insert_shortcode()
{
    require_once plugin_dir_path(__FILE__) . '/include/snpw_insert_shortcode.php';
    die('');
}

function snpw_addon_setup()
{
    add_filter("mce_external_plugins", "snpw_register_tinymce_plugin");
    add_filter('mce_buttons', 'snpw_add_tinymce_button');
    add_action('wp_ajax_snpw_insert_shortcode', 'snpw_ajax_insert_shortcode');
}

add_action("init", "snpw_addon_setup");

// Creating the widget 
class snp_widget extends WP_Widget {

    function __construct() {
        parent::__construct('snp_widget', __('Ninja Popups - Widget', 'snp_widget_domain'), array( 'description' => __( 'Add Ninja Popup to your sidebar.', 'snp_widget_domain' ), ) );
    }

    public function widget( $args, $instance ) {
        //$title = apply_filters( 'widget_title', $instance['title'] );
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        //if ( ! empty( $title ) ) { echo $args['before_title'] . $title . $args['after_title']; }

        if ($instance['select']) {
            echo do_shortcode('[ninja-inline id='.$instance['select'].']');
        }
        echo $args['after_widget'];
    }
            
    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
            $select = $instance[ 'select' ];
        }
        else {
            $title = '';
            $select = '';
        }
// Widget admin form
?>
<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="hidden" value="<?php echo esc_attr( $title ); ?>" />
<p>
<?php $Popups = snp_get_popups(); ?>
<?php if (count($Popups) > 0) : ?>
<label for="<?php echo $this->get_field_id('select'); ?>"><?php _e('Select Popup', 'nhp-opts'); ?></label>
<select name="<?php echo $this->get_field_name('select'); ?>" id="<?php echo $this->get_field_id('select'); ?>" class="widefat" onchange="jQuery('#<?php echo $this->get_field_id( 'title' ); ?>').val( this.options[this.selectedIndex].text )">
<option value=""></option>
<?php foreach ((array) $Popups as $ID => $Name) : ?>
<option value="<?php echo $ID ?>"<?php echo $select==$ID ? ' selected="selected"' : '' ?>><?php echo (!empty($Name)) ? $Name : 'Popup-'.$ID ?></option>
<?php endforeach ?>
</select>
<?php else : ?>
<div><?php _e('Create some popups first', 'nhp-opts'); ?>...</div>
<?php endif ?>
</p>
<?php 
    }
        
    function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['select'] = $new_instance['select'];
		return $instance;
	}
}

// Register and load the widget
add_action( 'widgets_init', create_function('', 'return register_widget("snp_widget");') );