jQuery(document).ready(function($) {

    tinymce.create('tinymce.plugins.snpw_plugin', {
	init : function(ed, url) {
	    ed.addCommand('snpw_insert_shortcode', function() {
		ed.windowManager.open({
		    file : ajaxurl+"?action=snpw_insert_shortcode",
		    width : 380,
		    height : 130,
		    inline : 1
		}, {
		    plugin_url : url,
		    selected: tinyMCE.activeEditor.selection.getContent()
		});
	    
	    });
	    ed.addButton('snpw_button_addon', {
		title : 'Insert Ninja Widget shortcode', 
		cmd : 'snpw_insert_shortcode', 
		image: url + '/../img/snp_button_addon.png'
	    });
	}
    });
    tinymce.PluginManager.add('snpw_button_addon', tinymce.plugins.snpw_plugin);
});