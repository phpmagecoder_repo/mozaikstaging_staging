jQuery(function ($) {
	
	$('.pagecontainer > table:eq(1)').addClass('wrapper');
    $('.pagecontainer > table:eq(1) tr:first > td:first').addClass('container');
    $('.container > table:eq(1)').addClass('content-area');
	$('.container > table:eq(2)').addClass('content-area');
    $('.content-area').find('br[clear = none]').remove();
	$('.content-area').find('tr:eq(0)').remove();
	$('.content-area').find('tr:eq(0) > td:eq(1)').attr('width', '14');
	$('.content-area').find('tr:eq(1) > td:eq(1)').attr('width', '11');
	$('.fitem table.gallery a.gpvi img').each(function() {this.src = this.src.replace('80.jpg','140.jpg');})
	//$('.fitem .gallery tbody').find('tr:eq(1)').remove();
	
	//Top Bar
	$('#topBarInner').append(
	  '<div id="topBarLinks"> <a href="http://stores.ebay.com/mozaikunderwatercameras"><img class="transitionall3s" alt="Home" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_home.svg"></a><a href="http://stores.ebay.com/mozaikunderwatercameras/pages/aboutus">About</a><a href="http://contact.ebay.com/ws/eBayISAPI.dll?ContactUserNextGen&recipient=mozaikunderwater">Contact</a><a href="http://stores.ebay.com/mozaikunderwatercameras/pages/store-policy">Store Policy</a><a href="http://feedback.ebay.com/ws/eBayISAPI.dll?ViewFeedback2&userid=mozaikunderwater">View Feedback</a><a href="http://my.ebay.com/ws/eBayISAPI.dll?AcceptSavedSeller&sellerid=mozaikunderwater&refid=store&ssPageName=STORE:HTMLBUILDER:SIMPLEPAGE">Store Newsletter</a>'+
      '    <div class="clear"></div>'+
      '  </div>'+
      '  <div id="getInTouch">'+
      '    <div id="phoneTop">888-318-3550 <span>or</span> 360-529-0990</div>'+
      '    <div id="socialIconsTop"><span>Stay in Touch: </span><a href="http://www.facebook.com/Mozaik.Underwater.Cameras" target="_blank"><img class="transitionall3s" alt="Facebook" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_fb_c.svg"></a><a href="https://twitter.com/uwcameras" target="_blank"><img class="transitionall3s" alt="Twitter" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_tw_c.svg"></a>'+
      '      <div class="clear"></div>'+
      '    </div>'+
      '    <div class="clear"></div>'+
      '  </div>'+
      '  <div class="clear"></div>');
	
	//Header
	$('#headerInner').append(
	  '<div id="logo"><a href="http://stores.ebay.com/mozaikunderwatercameras"><img class="transitionall5s" alt="Mozaik Underwater Cameras" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logo.png"><span class="transitionall3s">Inspiring Divers to Become Photographers</span></a></div>'+
      '  <div id="search">'+
      '    <div id="searchInner">'+
      '      <form method="get" action="http://stores.ebay.com/mozaikunderwatercameras">'+
      '        <input type="hidden" name="_fsub" value="0">'+
      '        <input type="text" maxlength="100" id="searchField" name="_nkw" title="Search cameras, lights or accessories... (e.g G7X)" placeholder="Search cameras, lights or accessories... (e.g G7X)" value="">'+
      '        <button type="submit" id="searchButton"><img class="transitionall3s" alt="Search" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_magglass.svg"></button>'+
      '        <div class="clear"></div>'+
      '      </form>'+
      '    </div>'+
      '  </div>'+
      '  <div id="shippingBanner">'+
      '    <div id="shippingBannerInner"> <span id="shippingBanLine1">FREE SHIPPING</span> <span id="shippingBanLine2">OVER $499</span> <span id="shippingBanLine3">for US &amp; Canada</span> </div>'+
      '  </div>'+
      '  <div class="clear"></div>');
	
	//Top Menu
	$('#topMenu').append(
	  '<ul>'+
      '  <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=housing">Housings</a>'+
      '    <ul class="secondLevel">'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311167017"><span>Compact Housings</span></a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ2068272017">Compact Housings for Canon</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ2193951017">Other Compact Housings</a></li>'+
      '      <li>&nbsp;</li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311168017"><span>DSLR Housings</span></a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311330017">Other DSLR Housings</a></li>'+
      '      <li>&nbsp;</li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311166017"><span>UW Cameras - Cam &amp; Housing Set </span></a></li>'+
      '    </ul>'+
      '  </li>'+
      '  <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311170017">Strobes</a>'+
      '    <ul class="secondLevel">'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311170017"><span>UW Strobes</span></a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311337017">Strobe Packages</a></li>'+
      '    </ul>'+
      '  </li>'+
      '  <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=Lights">Lights</a>'+
      '    <ul class="secondLevel">'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ1349286017"><span>UW Video Lights</span></a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311171017"><span>Dive Lights</span></a></li>'+
      '    </ul>'+
      '  </li>'+
      '  <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311172017">Accessories</a>'+
      '    <ul class="secondLevel">'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311172017"><span>Misc UW Accessories</span></a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3312850017">Other Accessories</a></li>'+
      '    </ul>'+
      '  </li>'+
      '  <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ1023933017">Lenses &amp; Ports</a>'+
      '    <ul class="secondLevel">'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311173017"><span>Wet Lenses</span></a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ1023933017"><span>UW Lenses Ports Red Fliters</span></a></li>'+
      '    </ul>'+
      '  </li>'+
      '  <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311174017">Fiber Optic Cables &amp; Sync Cord</a></li>'+
      '  <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311175017">Tray/Arm Systems</a></li>'+
      '  <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ9505819017">Scuba Gear</a>'+
      '    <ul class="secondLevel">'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ9505819017"><span>Scuba Gear</span></a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ9505825017">Wetsuits</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ9505833017">Accessories</a></li>'+
      '    </ul>'+
      '  </li>'+
      '  <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?rt=p4634.c0.m14&_sop=10">New Arrivals</a></li>'+
      '</ul>'+
      '<div class="clear"></div>');
	
    //Menu Hover
	$('#topMenu ul li').has('ul.secondLevel').addClass('active');
	
	//Advanced search
	$('#advSearchSelects').append(
	  '<form action="http://search.stores.ebay.com/search/search.dll?GetResult" method="get" name="advsearch">'+
      '      <input type="hidden" name="srchdesc" value="n">'+
      '      <input type="hidden" name="sid" value="165975047">'+
      '      <input type="hidden" name="store" value="mozaikunderwatercameras">'+
      '      <input type="hidden" name="colorid" value="0">'+
      '      <input type="hidden" name="st" value="1">'+
      '      <input type="hidden" name="query">'+
      '      <div id="advSearch0">'+
      '        <select name="advtype" onchange="adv()">'+
      '          <option value="" selected disabled>CHOOSE A PRODUCT</option>'+
      '          <option value="Housing">Housings</option>'+
      '          <option value="Compact Housing">Compact Housings</option>'+
      '          <option value="Compact Housing Canon">--Compact Housings for Canon</option>'+
      '          <option value="Compact Housing -(Canon)">--Other Compact Housing</option>'+
      '          <option value="DSLR Housing">DSLR Housings</option>'+
      '          <option value="Camera Housing Set">UW Cameras - Cam &amp; Housing Sets</option>'+
      '          <option value="">-----------------------------------------------------</option>'+
      '          <option value="Strobe">UW Strobes</option>'+
      '          <option value="Strobe Package">Strobe Packages</option>'+
      '          <option value="">-----------------------------------------------------</option>'+
      '          <option value="Video Lights">UW Video Lights</option>'+
      '          <option value="Dive Lights">Dive Lights</option>'+
      '          <option value="Lens">Lenses</option>'+
      '          <option value="Fiber Optic Cable">Fiber Optic Cables</option>'+
      '          <option value="Sync Cord">Sync Cords</option>'+
      '          <option value="Tray System">Tray/Arm Systems</option>'+
      '          <option value="Wetsuit">Wetsuit</option>'+
      '          <option value=""></option>'+
      '        </select>'+
      '      </div>'+
      '      <div id="advSearch1">'+
      '        <select name="advbrand" onchange="adv()">'+
      '          <option value="" selected disabled>CHOOSE A BRAND</option>'+
      '          <option value="Amphibico">Amphibico</option>'+
      '          <option value="Aquatica">Aquatica Digital</option>'+
      '          <option value="BigBlue">BigBlue</option>'+
      '          <option value="Beneath Surface">Beneath Surface</option>'+
      '          <option value="Cressi">Cressi</option>'+
      '          <option value="Fantasea">Fantasea</option>'+
      '          <option value="Fisheye">Fisheye</option>'+
      '          <option value="Ikelite">Ikelite</option>'+
      '          <option value="Inon">Inon</option>'+
      '          <option value="Intova">Intova</option>'+
      '          <option value="I-torch">I-torch</option>'+
      '          <option value="Light Motion">Light Motion</option>'+
      '          <option value="Mangrove">Mangrove</option>'+
      '          <option value="Nauticam">Nauticam</option>'+
      '          <option value="Nautilus">Nautilus</option>'+
      '          <option value="Olympus">Olympus</option>'+
      '          <option value="Polar Pro">Polar Pro</option>'+
      '          <option value="ReefNet">ReefNet</option>'+
      '          <option value="Sea & Sea">Sea &amp; Sea</option>'+
      '          <option value="SeaLife">SeaLife</option>'+
      '          <option value="TUSA">TUSA</option>'+
      '          <option value="Vivid Pix">Vivid-Pix</option>'+
      '          <option value="Water Proof">Water Proof</option>'+
      '          <option value="Zen Underwater">Zen Underwater</option>'+
      '          <option value=""></option>'+
      '        </select>'+
      '      </div>'+
      '      <div id="advSearchButton">'+
      '        <input type="submit" name="submit" value="Search" class="transitionall3s">'+
      '      </div>'+
      '    </form>'+
      '    <div class="clear"></div>');
	  
	//Featured Categories
	$('#featuredCats').append(
	  '<div class="fCat" id="fCat1">'+
      '  <div class="fCatImg"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=housing"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/fcat1.jpg"></a></div>'+
      '  <div class="fCatTitle">UW Camera Housings</div>'+
      '  <div class="fCatSubTitle">Camera Housings for Diving &amp; Water Sports</div>'+
      '  <div class="fCatSubsBlock">'+
      '    <ul>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311166017">Camera &amp; Housing Sets</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311167017">Compact Housings</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311168017">DSLR Housings</a></li>'+
      '      <li></li>'+
      '    </ul>'+
      '  </div>'+
      '</div>'+
      '<div class="fCat" id="fCat2">'+
      '  <div class="fCatImg"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=light"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/fcat2.jpg"></a></div>'+
      '  <div class="fCatTitle">UW Video &amp; Photo Lights</div>'+
      '  <div class="fCatSubTitle">Professional Video Lights &amp; Hand Mounted Dive Lights</div>'+
      '  <div class="fCatSubsBlock">'+
      '    <ul>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=video+light">Video Lights</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=focus+light">Photo &amp; Focus Lights</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=video+light+set">Video Light Set</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ3311171017">Dive Lights</a></li>'+
      '    </ul>'+
      '  </div>'+
      '</div>'+
      '<div class="fCat" id="fCat3">'+
      '  <div class="fCatImg"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=lenses"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/fcat3.jpg"></a></div>'+
      '  <div class="fCatTitle">UW Optics</div>'+
      '  <div class="fCatSubTitle">Wide-Angle, Macro Lenses, Red/Pink Filters for Wet U/W Use</div>'+
      '  <div class="fCatSubsBlock">'+
      '    <ul>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=macro+lenses">Macro Lenses</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=fantasea+lenses">Fantasea Lenses</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=wide+angle+lens">Wide Angle Lenses</a></li>'+
      '      <li></li>'+
      '    </ul>'+
      '  </div>'+
      '</div>'+
      '<div class="fCat" id="fCat4">'+
      '  <div class="fCatImg"><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ9505819017"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/fcat4.jpg"></a></div>'+
      '  <div class="fCatTitle">Scuba Gear</div>'+
      '  <div class="fCatSubTitle">BCD\'s, Masks, Fins, Wetsuits, Snorkels, accessories and more</div>'+
      '  <div class="fCatSubsBlock">'+
      '    <ul>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ9505825017">Wetsuits</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=mask">Masks</a></li>'+
      '      <li><a href="http://stores.ebay.com/mozaikunderwatercameras_W0QQfsubZ9505833017">Accessories</a></li>'+
      '      <li></li>'+
      '    </ul>'+
      '  </div>'+
      '</div>'+
      '<div class="clear"></div>');  
	
	//Logo Scroll
	$('#brandLogoScroll').append(
      '<div id="brandLogosTitle">Our Brands (<span>Mozaik UW Cameras is an Authorized dealer of over 20 brands</span>)</div>'+
      '<div id="scroller" style="height: 90px;">'+
      '  <div class="innerScrollArea">'+
	  '    <ul>'+
	  '	     <li id="brandLogo1"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=amphibico"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo1.gif"></a></li>'+
      '      <li id="brandLogo2"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=aquatica"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo2.gif"></a></li>'+
      '      <li id="brandLogo3"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=bigblue"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo3.gif"></a></li>'+
      '      <li id="brandLogo4"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=beneath+surface"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo4.gif"></a></li>'+
      '      <li id="brandLogo5"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=cressi"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo5.gif"></a></li>'+
      '      <li id="brandLogo6"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=fantasea"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo6.gif"></a></li>'+
      '      <li id="brandLogo7"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=fisheye"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo7.gif"></a></li>'+
      '      <li id="brandLogo8"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=ikelite"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo8.gif"></a></li>'+
      '      <li id="brandLogo9"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=inon"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo9.gif"></a></li>'+
      '      <li id="brandLogo10"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=intova"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo10.gif"></a></li>'+
      '      <li id="brandLogo11"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=itorch"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo11.gif"></a></li>'+
      '      <li id="brandLogo12"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=light+motion"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo12.gif"></a></li>'+
      '      <li id="brandLogo13"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=mangrove"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo13.gif"></a></li>'+
      '      <li id="brandLogo14"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=mozaik"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo14.gif"></a></li>'+
      '      <li id="brandLogo15"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=nauticam"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo15.gif"></a></li>'+
      '      <li id="brandLogo16"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=nautilus"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo16.gif"></a></li>'+
      '      <li id="brandLogo17"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=olympus"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo17.gif"></a></li>'+
      '      <li id="brandLogo18"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=polar+pro"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo18.gif"></a></li>'+
      '      <li id="brandLogo19"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=reefnet"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo19.gif"></a></li>'+
      '      <li id="brandLogo20"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=sea+and+sea"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo20.gif"></a></li>'+
      '      <li id="brandLogo21"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=sealife"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo21.gif"></a></li>'+
      '      <li id="brandLogo22"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=tusa"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo22.gif"></a></li>'+
      '      <li id="brandLogo23"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=vivid+pix"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo23.gif"></a></li>'+
      '      <li id="brandLogo24"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=water+proof"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo24.gif"></a></li>'+
      '      <li id="brandLogo25"><a href="http://stores.ebay.com/mozaikunderwatercameras/_i.html?_nkw=zen+underwater"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip/logo25.gif"></a></li>'+
	  '   </ul>'+
      '  </div>'+
      '</div>');
	
	
	// Store Categories
    if ($('#categories').length > 0) {if ($('#LeftPanel .lcat').length > 0) { $('#categories').html($('#LeftPanel .lcat').html());}}	
	
	//Budget Form
	$('#budgetBox').append(
	  '<div class="catTitle">Shop by <span>Price</span></div>'+
      '      <form action="http://search.stores.ebay.com/search/search.dll?GetResult" method="get" name="searchbox">'+
      '        <div id="budgetSelect">'+
      '          <select>'+
      '            <option value="0" disabled selected>CHOOSE A CATEGORY</option>'+
      '            <option value="3311166017">UW Cameras - Cam &amp; Housing Set </option>'+
      '            <option value="3311167017">Compact Housings</option>'+
      '            <option value="2068272017">--Compact Housings for Canon</option>'+
      '            <option value="2193951017">--Other Compact Housings</option>'+
      '            <option>-------------------------------------------------------</option>'+
      '            <option value="3311168017">DSLR Housings</option>'+
      '            <option value="3311330017">--Other DSLR Housings</option>'+
      '            <option>-------------------------------------------------------</option>'+
      '            <option value="3311170017">UW Strobes</option>'+
      '            <option value="3311337017">--Strobe Packages</option>'+
      '            <option>-------------------------------------------------------</option>'+
      '            <option value="1349286017">UW Video Lights</option>'+
      '            <option value="3311171017">Dive Lights</option>'+
      '            <option value="3311172017">Misc UW Accessories</option>'+
      '            <option value="3312850017">--Other Accessories</option>'+
      '            <option>-------------------------------------------------------</option>'+
      '            <option value="3311173017">Wet Lenses</option>'+
      '            <option value="3311174017">Fiber Optic Cables &amp; Sync Cord</option>'+
      '            <option value="3311175017">Tray/Arm Systems</option>'+
      '            <option value="1023933017">UW Lenses Ports Red Fliters</option>'+
      '            <option value="9505819017">Scuba Gear</option>'+
      '            <option value="9505825017">--Wetsuits</option>'+
      '            <option value="9505833017">--Accessories</option>'+
      '            <option>-------------------------------------------------------</option>'+
      '            <option value="1">Other Items</option>'+
      '            <option value="0">All Items</option>'+
      '          </select>'+
      '        </div>'+
      '        <div id="budgetFields">'+
      '          <input type="text" id="saprclo" name="saprclo" placeholder="Min. USD">'+
      '          <input type="text" id="saprchi" name="saprchi" placeholder="Max. USD">'+
      '          <div class="clear"></div>'+
      '        </div>'+
      '        <div id="buttonPrice">'+
      '          <input type="submit" name="submit" value="Search" class="transitionall3s">'+
      '        </div>'+
      '        <input type="hidden" name="sid" value="165975047">'+
      '      </form>');
	  
	//Gallery Banner
	$('#bannerGallery').append(  
	  '<div id="bannerGalleryInner"><a href="http://housingcamera.com/gallery/" target="_blank"><img alt="View Gallery" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_gallery.svg"><span>Photo<br>Gallery</span></a></div>');
	
	//Promo
	$('#fitem3').append(
	  '<div id="promoBanner">'+
      '            <div id="promoBannerInner"> <span id="promoBanLine1">We are Divers &amp;</span> <span id="promoBanLine2">Underwater Photographers</span>'+
      '              <div id="promoBanLinks"> <a href="http://www.facebook.com/Mozaik.Underwater.Cameras" target="_blank">Look us up on Facebook</a> <a href="http://housingcamera.com/blog" target="_blank">Check Out our Blog</a> </div>'+
      '            </div>'+
      '</div>');
	
	//Featured Items Characters Count
	$('.fitem .details .g-std a').text(function (index, text) {
        return text.substr(0, 50) + '...';
    });
	 
	 
	// Footer
	var footer = '\n\r' +
    '<div id="footer">' +
    '  <div id="footerInner">' +
    '    <div class="footerCol" id="footerCol1">' +
    '      <div class="footerTitle">About Us</div>' +
    '      <div class="footerText">Mozaik Underwater Cameras is a Canadian and USA based Underwater Photo and Video store. As an authorized dealer of almost every major manufacturer in the industry we are committed to a high level of service &amp; quality which we are practicing day by day.</div>' +
    '    </div>' +
    '    <div class="footerCol" id="footerCol2">' +
    '      <div class="footerTitle">Store newsletter</div>' +
    '      <div class="footerText">Click below to become an elite customer and be notified first about new products, promotions and industry news.</div>' +
    '      <div id="buttonYellow"><a class="transitionall3s" href="http://my.ebay.com/ws/eBayISAPI.dll?AcceptSavedSeller&sellerid=mozaikunderwater&refid=store&ssPageName=STORE:HTMLBUILDER:SIMPLEPAGE">Signup</a></div>' +
    '    </div>' +
    '    <div class="footerCol" id="footerCol3">' +
    '      <div class="footerTitle">Customer service</div>' +
    '      <div class="footerContact" id="footerPhone">Tel: 1-888-318-3550<br>' +
    '        International: 1-360-529-0990</div>' +
    '      <div class="footerContact" id="footerMessage"><a href="http://contact.ebay.com/ws/eBayISAPI.dll?ContactUserNextGen&recipient=mozaikunderwater">Send a message via eBay</a></div>' +
    '      <div class="footerContact" id="footerLocation">8 14th St, Blaine, WA, 98230</div>' +
    '    </div>' +
    '    <div class="clear"></div>' +
    '    <div id="footerGray">' +
    '      <div id="footerSocials"> <a href="https://twitter.com/uwcameras" target="_blank"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_tw.svg"></a> <a href="http://www.facebook.com/Mozaik.Underwater.Cameras" target="_blank"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_fb.svg"></a> <a href="https://plus.google.com/+Housingcamera" target="_blank"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_gp.svg"></a> ' +
    '        <!--<a href="" target="_blank"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_fl.svg"></a>--> ' +
    '        <a href="http://www.pinterest.com/uwcameras/boards/" target="_blank"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_pn.svg"></a> <a href="http://www.youtube.com/user/uwcameras" target="_blank"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_yt.svg"></a> <a href="http://housingcamera.com/gallery" target="_blank"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_gal.svg"></a> <a href="http://housingcamera.com/blog/" target="_blank"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/icon_blog.svg"></a>' +
    '        <div class="clear"></div>' +
    '      </div>' +
    '      <div id="footerPayment"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/ccards.png"></div>' +
    '      <div class="clear"></div>' +
    '    </div>' +
    '    <div id="footerSeals"><img alt="" src="http://housingcamera.com/ebay/mozaikuwcameras/images/logostrip.png"></div>' +
    '    <div id="footerCredit">All Imagery &amp; Content &copy; ' + currentyear.getFullYear() + ' Mozaik Underwater Camera. Design by: <a href="http://lillystardesigns.com/" target="_blank" title="LillyStar - Professional and Affordable Web Design and Development"><img alt="LillyStar - Professional and Affordable Web Design and Development" src="http://housingcamera.com/ebay/mozaikuwcameras/images/lilly.png"> LillyStar</a></div>' +
    '  </div>' +
    '</div>';
	if ($('.content-area').length > 0) { $('.content-area').after(footer); }
	
	});
	
	
	
	//Advanced Search
	function adv() {
		document.advsearch.query.value = document.advsearch.advtype.value + " " + document.advsearch.advbrand.value; }
	
    // Item Slider
	    $(function(){
        var scroller = $('#scroller div.innerScrollArea');
        var scrollerContent = scroller.children('ul');
        scrollerContent.children().clone().appendTo(scrollerContent);
        var curX = 0;
        scrollerContent.children().each(function(){
            var $this = $(this);
            $this.css('left', curX);
            curX += $this.outerWidth(true);
        });
        var fullW = curX / 2;
        var viewportW = scroller.width();

        // Scrolling speed management
        var controller = {curSpeed:0, fullSpeed:2};
        var $controller = $(controller);
        var tweenToNewSpeed = function(newSpeed, duration)
        {
            if (duration === undefined)
                duration = 600;
            $controller.stop(true).animate({curSpeed:newSpeed}, duration);
        };

        // Pause on hover
        scroller.hover(function(){
            tweenToNewSpeed(0);
        }, function(){
            tweenToNewSpeed(controller.fullSpeed);
        });

        // Scrolling management; start the automatical scrolling
        var doScroll = function()
        {
            var curX = scroller.scrollLeft();
            var newX = curX + controller.curSpeed;
            if (newX > fullW*2 - viewportW)
                newX -= fullW;
            scroller.scrollLeft(newX);
        };
        setInterval(doScroll, 30);
        tweenToNewSpeed(controller.fullSpeed);
    });
	