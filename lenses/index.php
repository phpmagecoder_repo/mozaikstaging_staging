<?php

// error_reporting(E_ALL); 
// ini_set("display_errors", 1); 

session_start();

$nav0 = '';
$nav1 = '';
$nav2 = '';
$nav3 = '';
$offset = '';
$action = '';
$action_login = '';
$action_logout = '';
$action_cancel = '';
$action_edit = '';

require_once("inc/config.inc.php");
require_once('inc/tools.inc.php');

$con = mysql_connect("$db_server", "$db_user", "$db_passwd") or die("Ne morem se povezati s strenikom!");
$db = mysql_select_db("$db_database", $con) or die("Ne morem se povezati z bazo!");
$navArr = array( 'init' => 0);

//Router for Friendly URLS - Not Active Yet
foreach (explode ("/", $_SERVER['REQUEST_URI']) as $part)
{
    if (isset($part[0]) && isset($part[1]))
        $navArr[$part[0]] = $part[1];
    if (isset($part[2]) && isset($part[3]))
        $navArr[$part[2]] = $part[3];
    if (isset($part[4]) && isset($part[5]))
        $navArr[$part[4]] = $part[5];
    if (isset($part[6]) && isset($part[7]))
        $navArr[$part[6]] = $part[7];

    
}

//nav0 is the type of page (database|lenses|info|user|admin)
if (isset($navArr['nav0']))
    $nav0 = $navArr['nav0'];
if (isset($_GET['nav0']))
    $nav0 = $_GET['nav0'];
if (isset($_POST['nav0']))
    $nav0 = $_POST['nav0'];
//nav1 is the first param
if (isset($navArr['nav1']))
    $nav1 = $navArr['nav1'];
if (isset($_GET['nav1']))
    $nav1 = $_GET['nav1'];
if (isset($_POST['nav1']))
    $nav1 = $_POST['nav1'];

if (isset($navArr['nav2']))
    $nav2 = $navArr['nav2'];
if (isset($_GET['nav2']))
    $nav2 = $_GET['nav2'];
if (isset($_POST['nav2']))
    $nav2 = $_POST['nav2'];

if (isset($navArr['nav3']))
    $nav3 = $navArr['nav3'];
if (isset($_GET['nav3']))
    $nav3 = $_GET['nav3'];
if (isset($_POST['nav3']))
    $nav3 = $_POST['nav3'];

if (isset($navArr['id']))
    $id = $navArr['id'];
if (isset($_GET['id']))
    $id = $_GET['id'];
if (isset($_POST['id']))
    $id = $_POST['id'];

if (isset($navArr['offset']))
    $offset = $navArr['offset'];
if (isset($_GET['offset']))
    $offset = $_GET['offset'];
if (isset($_POST['offset']))
    $offset = $_POST['offset'];

// activate account link - 
if ($nav0 == 'act') {
    $nav0 = 'user';
    $nav1 = 'activate';
}
$user_ip = $_SERVER['REMOTE_ADDR'];

if (empty($_SESSION['user_id'])) {// session is new
    // cookie is set as intern user
    if (!empty($_COOKIE[$mycookie])) {
        // get md5 value from cookie 
        $md5id = $_COOKIE[$mycookie];
        $sql = "SELECT id FROM user WHERE md5(concat(id,'$system_pass')) = '$md5id'";

        $result = mysql_query($sql, $con);
        $num = mysql_num_rows($result);
        // does user exist in db
        if ($num) {
            $row = mysql_fetch_array($result);
            $user_id = $row['id'];
            $_SESSION['user_id'] = $user_id;
            $log = write_log('2', 'login: get cookie', $user_id, $con);
        } else {
            $_SESSION['user_id'] = '0';
            echo "could not get user_id from cookie";
            $log = write_log('2', 'login: cookie error', 0, $con);
        }
    } else {
        $_SESSION['user_id'] = '0';
    }
}


// constants
$current_user_id = $_SESSION['user_id'];

$db_current_datetime = date("Y-m-d H:i:s");
$db_current_date = date("Y-m-d");
$nice_current_date = date("d.m.Y");
$session_id = session_id();

// get user data / name
if ($current_user_id) {
    $sql = "SELECT name	FROM user WHERE id = '$current_user_id'";
    $result = mysql_query($sql, $con);
    $num = mysql_num_rows($result);
    if ($num == '1') {
        $row = mysql_fetch_array($result);
        $current_user_name = $row['name'];
    }
}

$meta_title ="Lens Database";
$meta_description ="Lens Database";
?>

<?php ///////////Include the Content according to the navigation parameters.//////////////////
$tplFile = "";
switch ($nav0) { //Router 
    case 'lenses':
    default:
        $nav0 = 'lenses';
        switch ($nav1) {
            default:
                $nav1 = 'list';
                include_once('inc/home.inc.php');
                $tplFile = 'inc/home.tpl.php';
                break;
            case 'add':
                unset($object_id);
                include_once('inc/database/lens_edit.inc.php');
                $tplFile = 'inc/database/lens_edit.tpl.php';
                break;
            case 'edit':
                $object_id = $id;
                include_once('inc/database/lens_edit.inc.php');
                $tplFile = 'inc/database/lens_edit.tpl.php';
                break;
            case 'editlink':
                $object_id = $id;
                include_once('inc/database/lens_link_edit.inc.php');
                $tplFile = 'inc/database/lens_link_edit.tpl.php';
                break;
            case 'view':
                $object_id = $id;
                include_once('inc/database/lens_view.inc.php');
                $tplFile = 'inc/database/lens_view.tpl.php';
                break;
        }
        break;
    case 'user':
        switch ($nav1) {
            case 'newpass':
                include_once('inc/users/newpass.inc.php');
                $tplFile = 'inc/users/newpass.tpl.php';
                break;

            case 'changepass':
                include_once('inc/users/changepass.inc.php');
                $tplFile = 'inc/users/changepass.tpl.ph';
                break;

            case 'register':
                include_once('inc/users/register.inc.php');
                $tplFile = 'inc/users/register.tpl.php';
                break;

            case 'activate':
                $object_id = $id;
                include_once('inc/users/activate.inc.php');
                $tplFile = 'inc/users/activate.tpl.php';
                break;
        }
        break;
    case 'info':
        switch ($nav1) {
            case 'dictionary':
            default:
                $nav1 = 'dictionary';
                $tplFile = 'inc/misc/dictionary.tpl.php';
                $meta_title ="Camera Lens Glossary of Terms";
                $meta_description ="Following table displays lens technologies used and comparison between similar features and markings among different manufacturers. Detailed descriptions of technologies can be found on separate pages for each lens manufacturer.";
                break;
            case 'canon':
                $tplFile = 'inc/misc/canon.tpl.php';
                $meta_title ="Canon Lens Types / Models and Specifications";
                $meta_description ="This page explains the different types of Lenses by Canon and how to understand the specs and models in the lens name";
                break;
            case 'nikon':
                $tplFile = 'inc/misc/nikon.tpl.php';
                $meta_title ="Nikon Lens Types / Models and Specifications";
                $meta_description ="This page explains the different types of Lenses by Nikon and how to understand the specs and models in the lens name";
                break;
            case 'sigma':
                $tplFile = 'inc/misc/sigma.tpl.php';
                $meta_title ="Sigma Lens Types / Models and Specifications";
                $meta_description ="This page explains the different types of Lenses by Sigma and how to understand the specs and models in the lens name";
                break;
            case 'tamron':
                $tplFile = 'inc/misc/tamron.tpl.php';
                $meta_title ="Tamron Lens Types / Models and Specifications";
                $meta_description ="This page explains the different types of Lenses by Tamron and how to understand the specs and models in the lens name";
                break;
        }
        break;
    case 'admin':
        switch ($nav1) {
            case 'log':
            default:
                $nav1 = 'log';
                include_once('inc/users/log.inc.php');
                $tplFile = 'inc/users/log.tpl.php';
                break;
            case 'users':
                include_once('inc/users/users.inc.php');
                $tplFile = 'inc/users/users.tpl.php';
                break;
            case 'useredit':
                $object_id = $id;
                include_once('inc/users/user_edit.inc.php');
                $tplFile = 'inc/users/user_edit.tpl.ph';
                break;
        }
        break;
}
?>
<html>
<head>
    <title><?php echo $meta_title; ?></title>
	
        <META http-equiv="Content-language" content="en">
        <META name="Description" content="<?php echo $meta_description; ?>">
        
        <META name="Audience" content="All">

        <META name="Robots" content="noindex,nofollow">
        <META name="Rating" content="General">
			
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        
        <?php require_once('inc/head.tpl.php'); ?>
        
	<link rel="stylesheet" href="lens_database.css" type="text/css">	
	<link rel="shortcut icon" href="http://www.lensdatabase.net/favicon.ico" type="image/x-icon">
	<link rel="icon" href="http://www.lensdatabase.net/favicon.ico" type="image/x-icon">

	<script type="text/javascript" src="js/fValidate.config.js"></script>
	<script type="text/javascript" src="js/fValidate.core.js"></script>
	<script type="text/javascript" src="js/fValidate.lang-enUS.js"></script>
	<script type="text/javascript" src="js/fValidate.validators.js"></script>
	<script type="text/javascript" src="js/fValidate.datetime.js"></script>	
	<script type="text/javascript" src="js/fValidate.controls.js"></script>	
	
	<script type="text/javascript">
		function submitAction( form, action, validate )
		{
	  		proceed = 1;
	  		if ( validate ) {
				proceed = validateForm( form, 0, 0, 0, 0, 8 );
	  		}
	  		if ( proceed ) {
				form.action.value = action;
				form.submit();
	  		}
		}
	</script>
	
	
        <script type="text/javascript">  
                function submitFormWithEnter(myfield,e, action)  
                {  
                var keycode;  
                        if (window.event)  
                {  keycode = window.event.keyCode;  }
                else if (e)  
                { 	keycode = e.which;  }  
                else  
                {  	return true;  }  

                if (keycode == 13)  
                {  	myfield.form.action.value = action;
                                myfield.form.submit();  
                        return false;  
                }  
                else  
                {  	return true;  }  
                }  
          </script>
</head>	
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
    <?php echo $headerBlock; ?>
    <div class="container clearer stretched lensdb">
        <?php // allways available scripts
              require_once('inc/header.inc.php'); 
              require_once('inc/header.tpl.php');?>

        <?php include_once($tplFile); ?>

        <?php require_once('inc/footer.tpl.php'); ?>
    </div>
    <?php
    	$layout = Mage::getSingleton('core/layout');
        $footerBlock = $layout->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
         echo $footerBlock;

	$footerScripts =  $layout->createBlock('core/template')->setTemplate('page/html/footer_theme_scripts.phtml')->toHtml();
	echo $footerScripts;
     ?>
</body>
</html>