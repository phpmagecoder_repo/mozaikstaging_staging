<?php

$mid_spacer = '10'; // space between items

$main_menu = array();

$i = 1;
$main_menu[$i]['url'] = $_SERVER['PHP_SELF'] . '?nav0=lenses';
$main_menu[$i]['title'] = 'Browse Lenses';
$main_menu[$i]['width'] = '160';
$main_menu[$i]['rights'] = 'P';
$main_menu[$i]['sel'] = 'lenses';


$i++;
$main_menu[$i]['url'] = $_SERVER['PHP_SELF'] . '?nav0=info';
$main_menu[$i]['title'] = 'Lens Info';
$main_menu[$i]['width'] = '100';
$main_menu[$i]['rights'] = 'P';
$main_menu[$i]['sel'] = 'info';

$j = 1;
$main_menu[$i]['items'][$j]['url'] = $_SERVER['PHP_SELF'] . '?nav0=info&nav1=dictionary';
$main_menu[$i]['items'][$j]['title'] = 'dictionary';
$main_menu[$i]['items'][$j]['rights'] = 'P';
$main_menu[$i]['items'][$j]['sel'] = 'dictionary';

$j++;
$main_menu[$i]['items'][$j]['url'] = $_SERVER['PHP_SELF'] . '?nav0=info&nav1=canon';
$main_menu[$i]['items'][$j]['title'] = 'Canon';
$main_menu[$i]['items'][$j]['rights'] = 'P';
$main_menu[$i]['items'][$j]['sel'] = 'canon';

$j++;
$main_menu[$i]['items'][$j]['url'] = $_SERVER['PHP_SELF'] . '?nav0=info&nav1=nikon';
$main_menu[$i]['items'][$j]['title'] = 'Nikon';
$main_menu[$i]['items'][$j]['rights'] = 'P';
$main_menu[$i]['items'][$j]['sel'] = 'nikon';

$j++;
$main_menu[$i]['items'][$j]['url'] = $_SERVER['PHP_SELF'] . '?nav0=info&nav1=sigma';
$main_menu[$i]['items'][$j]['title'] = 'Sigma';
$main_menu[$i]['items'][$j]['rights'] = 'P';
$main_menu[$i]['items'][$j]['sel'] = 'sigma';

$j++;
$main_menu[$i]['items'][$j]['url'] = $_SERVER['PHP_SELF'] . '?nav0=info&nav1=tamron';
$main_menu[$i]['items'][$j]['title'] = 'Tamron';
$main_menu[$i]['items'][$j]['rights'] = 'P';
$main_menu[$i]['items'][$j]['sel'] = 'tamron';


$i++;
$main_menu[$i]['url'] = $_SERVER['PHP_SELF'] . '?nav0=admin';
$main_menu[$i]['title'] = 'admin';
$main_menu[$i]['width'] = '100';
$main_menu[$i]['rights'] = 'A';
$main_menu[$i]['sel'] = 'admin';

$j = 1;
$main_menu[$i]['items'][$j]['url'] = $_SERVER['PHP_SELF'] . '?nav0=admin&nav1=log';
$main_menu[$i]['items'][$j]['title'] = 'log';
$main_menu[$i]['items'][$j]['rights'] = 'A';
$main_menu[$i]['items'][$j]['sel'] = 'log';

$j++;
$main_menu[$i]['items'][$j]['url'] = $_SERVER['PHP_SELF'] . '?nav0=admin&nav1=users';
$main_menu[$i]['items'][$j]['title'] = 'users';
$main_menu[$i]['items'][$j]['rights'] = 'A';
$main_menu[$i]['items'][$j]['sel'] = 'users';



// prepare menu for generation

$menu_level1 = array();
$k = 1;

$selected_item_id = false;

for ($i = 1; $i <= count($main_menu); $i++) {
    if (user_is_member($main_menu[$i]['rights'], $con)) {
        $menu_level1['url'][$k] = $main_menu[$i]['url'];
        $menu_level1['title'][$k] = $main_menu[$i]['title'];
        $menu_level1['width'][$k] = $main_menu[$i]['width'];

        if ($nav0 == $main_menu[$i]['sel']) {
            $menu_level1['sel'][$k] = true;
            $selected_item_id = $i;
        } else {
            $menu_level1['sel'][$k] = false;
        }

        $k++;
    }
}

$menu_level2 = array();
$k = 1;
if ($selected_item_id) {
    if (isset($main_menu[$selected_item_id]['items'])) {
        if (count($main_menu[$selected_item_id]['items'])) {
            $menu_level2 = array();

            for ($i = 1; $i <= count($main_menu[$selected_item_id]['items']); $i++) {
                if (user_is_member($main_menu[$selected_item_id]['items'][$i]['rights'], $con)) {
                    $menu_level2['url'][$k] = $main_menu[$selected_item_id]['items'][$i]['url'];
                    $menu_level2['title'][$k] = $main_menu[$selected_item_id]['items'][$i]['title'];
                    if ($nav1 == $main_menu[$selected_item_id]['items'][$i]['sel']) {
                        $menu_level2['sel'][$k] = true;
                    } else {
                        $menu_level2['sel'][$k] = false;
                    }
                    $k++;
                }
            }
        }
    }
} else {
    $menu_level2 = false;
}

	 /* echo '<pre>'; 	var_dump($menu_level2);	 	echo '</pre>'; */
?>