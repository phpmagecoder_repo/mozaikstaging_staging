<?php

$sql_and_add = '';

if(isset($_POST['action']))  { $action = $_POST['action']; }

// =================== sort menu options

$i = 1;
$sort['id'][$i]  = $i;
$sort['name'][$i]  = "announced";
$sort['sql'][$i]  = "lens.announced DESC";

$i++;
$sort['id'][$i]  = $i;
$sort['name'][$i]  = "weight";
$sort['sql'][$i]  = "lens.weight DESC";

$i++;
$sort['id'][$i]  = $i;
$sort['name'][$i]  = "aperture";
$sort['sql'][$i]  = "lens.lens_aperture_wmax_id ASC, lens.name";

$i++;
$sort['id'][$i]  = $i;
$sort['name'][$i]  = "focal wide";
$sort['sql'][$i]  = "lens.focal_min ASC";

$i++;
$sort['id'][$i]  = $i;
$sort['name'][$i]  = "focal tele";
$sort['sql'][$i]  = "lens.focal_max DESC";

$i++;
$sort['id'][$i]  = $i;
$sort['name'][$i]  = "zoom ratio";
$sort['sql'][$i]  = "lens.zoom_ratio DESC";

$i++;
$sort['id'][$i]  = $i;
$sort['name'][$i]  = "popularity";
$sort['sql'][$i]  = "lens.views DESC";


$i++;
$sort['id'][$i]  = $i;
$sort['name'][$i]  = "date added";
$sort['sql'][$i]  = "lens.added DESC";

$i++;
$sort['id'][$i]  = $i;
$sort['name'][$i]  = "date last modified";
$sort['sql'][$i]  = "lens.modified DESC";

if ($action == 'action_add_lens' && user_is_member("C,E,A", $con))
{
	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=database&nav1=add");
	exit;
}
// search
if ( ! isset($_SESSION['lens_search_txt']) ) { $_SESSION['lens_search_txt'] = ''; }

if($action == 'action_clear_search')
{
	$search_txt = '';
	$_SESSION['lens_search_txt'] = $search_txt;
}

if($action == 'action_search') 
{ 
	$search_txt = $_POST['search_txt'];
	$_SESSION['lens_search_txt'] = $search_txt;
}

$search_txt = $_SESSION['lens_search_txt'];

if($search_txt)
{
	// search log
	
	$session_string = substr($session_id,0,$sid_length);
	$session_string2 = $session_string.'%';

	$write_log = true;
	
	// get lens mount_type
	$sql = "SELECT action
			FROM user_log 
			WHERE action like '$session_string2'
			ORDER BY time DESC";
		
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	
	if($num)
	{
		//$row = mysql_fetch_array($result, $con);
		$row = mysql_fetch_array($result);
		$prev_log_action = substr($row['action'],$sid_length+2);
		if($prev_log_action == $search_txt) { $write_log = false; }
	}
	
	if($write_log)
	{
		$log = write_log('4', $session_string.": ".$search_txt, 0, $con);
	}
	
	
	// search
	$search = explode(" ", $search_txt);
	
	$sql_and_add .= " AND (";
	
	for($i=0; $i< count($search); $i++)
	{
		$sql_and_add .= " lens.name like '%".$search[$i]."%' ";
		if($i < (count($search)-1))
		{
			$sql_and_add .= " AND ";
		}
	
	}
	$sql_and_add .= " )";
}

// ----- SORT
if ( isset( $_POST['lens_sort_id'] ) ) $_SESSION['lens_sort_id'] = $_POST['lens_sort_id']; 
if ( ! isset($_SESSION['lens_sort_id']) ) 
{
		$_SESSION['lens_sort_id'] = '1';
}
$lens_sort_id = $_SESSION['lens_sort_id'];

$sql_sort_add = " ORDER BY ".$sort['sql'][$lens_sort_id];

// ----- LENS MAKER
if ( isset( $_POST['lens_maker_id'] ) ) $_SESSION['lens_maker_id'] = $_POST['lens_maker_id']; 
if ( ! isset($_SESSION['lens_maker_id']) ) 
{
		$_SESSION['lens_maker_id'] = '0';
}
$lens_maker_id = $_SESSION['lens_maker_id'];

if($lens_maker_id)
{
	// lens maker selected
	$sql_and_add .= " AND  lens.lens_maker_id = '$lens_maker_id' ";
}

// ----- MOUNT TYPE
if ( isset( $_POST['lens_mount_type_id'] ) ) $_SESSION['lens_mount_type_id'] = $_POST['lens_mount_type_id']; 
if ( ! isset($_SESSION['lens_mount_type_id']) ) 
{
		$_SESSION['lens_mount_type_id'] = '0';
}
$lens_mount_type_id = $_SESSION['lens_mount_type_id'];

if($lens_mount_type_id)
{
	// lens maker selected
	$sql_and_add .= " AND  lens.lens_mount_type_id = '$lens_mount_type_id' ";
}

// ----- FITS FOR
if ( isset( $_POST['lens_fitsfor_id'] ) ) $_SESSION['lens_fitsfor_id'] = $_POST['lens_fitsfor_id']; 
if ( ! isset($_SESSION['lens_fitsfor_id']) ) 
{
		$_SESSION['lens_fitsfor_id'] = '0';
}
$lens_fitsfor_id = $_SESSION['lens_fitsfor_id'];

if($lens_fitsfor_id)
{
	// lens maker selected
	$sql_and_add .= " AND  lens_lens_fitsfor.lens_fitsfor_id = '$lens_fitsfor_id' ";
}


// ----- TYPE
if ( isset( $_POST['lens_type_id'] ) ) $_SESSION['lens_type_id'] = $_POST['lens_type_id']; 
if ( ! isset($_SESSION['lens_type_id']) ) 
{
		$_SESSION['lens_type_id'] = '0';
}
$lens_type_id = $_SESSION['lens_type_id'];

if($lens_type_id)
{
	// lens maker selected
	$sql_and_add .= " AND  lens_lens_type.lens_type_id = '$lens_type_id' ";
}


// ----- APERTURE MINIMUM WIDE
if ( isset( $_POST['lens_aperture_id'] ) ) $_SESSION['lens_aperture_id'] = $_POST['lens_aperture_id']; 
if ( ! isset($_SESSION['lens_aperture_id']) ) 
{
		$_SESSION['lens_aperture_id'] = '0';
}
$lens_aperture_id = $_SESSION['lens_aperture_id'];

if($lens_aperture_id)
{
	// lens maker selected
	$sql_and_add .= " AND  lens.lens_aperture_wmax_id <= '$lens_aperture_id' ";
}


// ----- ZOOM
if ( isset( $_POST['zoom_id'] ) ) $_SESSION['zoom_id'] = $_POST['zoom_id']; 
if ( ! isset($_SESSION['zoom_id']) ) 
{
		$_SESSION['zoom_id'] = '0';
}
$zoom_id = $_SESSION['zoom_id'];

if($zoom_id)
{
	if($zoom_id == '2') { $selected_zoom_id = '0'; } else { $selected_zoom_id = '1'; }
	$sql_and_add .= " AND  lens.zoom = '$selected_zoom_id' ";
}


// ----- IS
if ( isset( $_POST['is_id'] ) ) $_SESSION['is_id'] = $_POST['is_id']; 
if ( ! isset($_SESSION['is_id']) ) 
{
		$_SESSION['is_id'] = '0';
}
$is_id = $_SESSION['is_id'];

if($is_id)
{
	if($is_id == '2') { $selected_is_id = '0'; } else { $selected_is_id = '1'; }
	$sql_and_add .= " AND  lens.stabilizer = '$selected_is_id' ";
}


// ----- DISCONTINUED
if ( isset( $_POST['discontinued_id'] ) ) $_SESSION['discontinued_id'] = $_POST['discontinued_id']; 
if ( ! isset($_SESSION['discontinued_id']) ) 
{
		$_SESSION['discontinued_id'] = '0';
}
$discontinued_id = $_SESSION['discontinued_id'];

if($discontinued_id)
{
	$sql_and_add .= " AND  lens.lens_status_id = '$discontinued_id' ";
}

// --- ONLY EDITOR and ADMIN can see PENDING lenses

if(! user_is_member("E,A", $con) )
{
	$sql_and_add .= " AND  lens.status_id = '2' ";
}

// =====================================================================
// =========== get lens data ===========================================
// =====================================================================


// count all lens
$sql = "SELECT id FROM lens";
$result = mysql_query($sql, $con);
$num_all_rows = mysql_num_rows($result);

$all_lenses = $num_all_rows;

// get lens data
$sql = "SELECT DISTINCT lens.id, lens.name, lens_maker.name maker, stabilizer, zoom, focal_min, focal_max, awide.name fwide, atele.name ftele, 
				weight, filter, announced, lens_focus.short lens_focus, lens.zoom_ratio, lens_status.name status, lens_status.icon status_icon, lens.status_id
		
		FROM (lens, lens_maker, lens_aperture awide, lens_aperture atele, lens_focus, lens_status)
		
		LEFT JOIN lens_lens_fitsfor ON lens_lens_fitsfor.lens_id = lens.id
		LEFT JOIN lens_lens_type ON lens_lens_type.lens_id = lens.id
		
		WHERE lens.lens_maker_id = lens_maker.id
		
		AND lens.lens_aperture_wmax_id = awide.id
		AND lens.lens_aperture_tmax_id = atele.id 
		AND lens.lens_status_id = lens_status.id
		AND lens.lens_focus_id = lens_focus.id ".
		$sql_and_add." ".$sql_sort_add;
		
$result = mysql_query($sql, $con);

// echo $sql;


$sql_limit = " LIMIT $offset,$limit";	

// get real data
	$result2 = mysql_query($sql);
	
	$numrows = mysql_num_rows($result2);	
	
	$displayed_lenses = $numrows;
	
	$result = mysql_query($sql.$sql_limit, $con);
	$num = mysql_num_rows($result);
	


$items = array();
for ($i=1; $i <= $num; $i++ )
{
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$items['id'][$i] = $row['id'];
	$tmp_id = $items['id'][$i];
	$items['maker'][$i] = $row['maker'];
	$items['lens_focus'][$i] = $row['lens_focus'];

	
	
	$items['status'][$i] = "<img title='".$row['status']."' src='img/".$row['status_icon']."'>";
	
	$items['name'][$i] = stripslashes($row['name']);
	if($row['stabilizer']) { $items['stabilizer'][$i] = 'IS'; } else { $items['stabilizer'][$i] = '&nbsp;'; } 
	if($row['zoom']) { $items['zoom'][$i] = 'zoom'; $zoom = true; } else { $items['zoom'][$i] = 'prime'; $zoom = false; } 

	if( $row['weight'] ) { $items['weight'][$i] = $row['weight']."g"; } 	else { $items['weight'][$i] = "&nbsp;"; } 
	if($row['filter']) { $items['filter'][$i] = $row['filter']; } else { $items['filter'][$i] = "&nbsp;"; } 
	if($row['announced'] != '0000-00-00' ) { $items['announced'][$i] = niceYearMonth($row['announced']); } else { $items['announced'][$i] = "&nbsp;"; } 
	if($row['status_id'] == '1') { $items['name'][$i] = "(Pending) ".$items['name'][$i]; }


	if($zoom)
	{	
		$items['focal'][$i] = $row['focal_min']."-".$row['focal_max'];
		$items['aperture'][$i] = $row['fwide'];
		if($row['fwide'] != $row['ftele'])
		{
			$items['aperture'][$i] .= "-".substr($row['ftele'],2);
		}
		$items['zoom_ratio'][$i] = round($row['zoom_ratio'],1)."x";
	} else
	{
		$items['focal'][$i] = $row['focal_min'];
		$items['aperture'][$i] = $row['fwide'];
		$items['zoom_ratio'][$i] = "&nbsp;";
	}
	
	// get lens image - if exist
	$sql2 = "SELECT lens_photo_version.filename, lens_photo_version.size_x, lens_photo_version.size_y, lens_photo.description
			FROM lens_photo_version, lens_photo
			WHERE lens_photo_version.lens_photo_id = lens_photo.id
			AND lens_photo.lens_id = '$tmp_id'
			AND lens_photo_version.no = '1'";
	
	$result2 = mysql_query($sql2);
	$num2 = mysql_num_rows($result2);
	
	if($num2)
	{
		//$row = mysql_fetch_array($result2, $con);
		$row = mysql_fetch_array($result2);
		$items['image_filename'][$i] = $dir_photo.$row['filename'];
		$items['image_sizex'][$i] = $row['size_x'];
		$items['image_sizey'][$i] = $row['size_y'];
		$items['image_description'][$i] = $row['description'];

	} else
	{
		$items['image_filename'][$i] = "img/shim.gif";
		$items['image_sizex'][$i] = '44';
		$items['image_sizey'][$i] = '33';
		$items['image_description'][$i] = "no photo available";
	}
	
	

}	


// ============= get last modified

	$sql = "SELECT modified FROM lens ORDER BY modified DESC";
		
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);

	$db_last_modified = "last update ".niceDateTime($row['modified']);




// =====================================================================
// =========== drop down menus =========================================
// =====================================================================


// get lens makers
$sql = "SELECT id, name
		FROM lens_maker
		ORDER BY name";

$sql = "SELECT lens.lens_maker_id maker_id, lens_maker.name, count(lens.id) counter
		FROM lens, lens_maker
		WHERE lens.lens_maker_id = lens_maker.id
		GROUP BY lens.lens_maker_id";

$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_maker = array();

$lens_maker['id']['1'] = '0';
$lens_maker['name']['1'] = '--- maker';	

$j=2;
for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_maker['id'][$j] = $row['maker_id'];
	$lens_maker['name'][$j] = $row['name']." (".$row['counter'].")";	
	
	// if($lens_maker['id'][$j] == $_SESSION['lens_maker_id']) { $lens_maker['name'][$j] = "*".$lens_maker['name'][$j];  }
	
	$j++;

}


// get lens mount_type
$sql = "SELECT id, name
		FROM lens_mount_type
		ORDER BY name";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_mount_type = array();

$lens_mount_type['id']['1'] = '0';
$lens_mount_type['name']['1'] = '--- sensor';	

$j=2;
for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_mount_type['id'][$j] = $row['id'];
	$lens_mount_type['name'][$j] = $row['name'];	
	$j++;
}


// get lens fits_for
$sql = "SELECT id, name
		FROM lens_fitsfor
		ORDER BY name";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_fitsfor = array();

$lens_fitsfor['id']['1'] = '0';
$lens_fitsfor['name']['1'] = '--- fits for';	

$j=2;
for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_fitsfor['id'][$j] = $row['id'];
	$lens_fitsfor['name'][$j] = $row['name'];	
	$j++;
}


// get lens type
$sql = "SELECT id, name
		FROM lens_type
		ORDER BY name";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_type = array();

$lens_type['id']['1'] = '0';
$lens_type['name']['1'] = '--- type';	

$j=2;
for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_type['id'][$j] = $row['id'];
	$lens_type['name'][$j] = $row['name'];	
	$j++;
}


// get lens max aperture
$sql = "SELECT id, name
		FROM lens_aperture
		ORDER BY value";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_aperture = array();

$lens_aperture['id']['1'] = '0';
$lens_aperture['name']['1'] = '--- aperture';	

$j=2;
for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_aperture['id'][$j] = $row['id'];
	$lens_aperture['name'][$j] = $row['name'];	
	$j++;
}


$zoom = array();
$i = 1;
$zoom['id'][$i] = '0';
$zoom['name'][$i] = '--- zoom';
$i++;
$zoom['id'][$i] = '1';
$zoom['name'][$i] = 'zoom lens';
$i++;
$zoom['id'][$i] = '2';
$zoom['name'][$i] = 'prime lens';


$discontinued = array();
$i = 1;
$discontinued['id'][$i] = '0';
$discontinued['name'][$i] = '--- all';
$i++;
$discontinued['id'][$i] = '1';
$discontinued['name'][$i] = 'in production';
$i++;
$discontinued['id'][$i] = '2';
$discontinued['name'][$i] = 'discontinued';


$is = array();
$i = 1;
$is['id'][$i] = '0';
$is['name'][$i] = '--- stabiliz.';
$i++;
$is['id'][$i] = '1';
$is['name'][$i] = 'IS/VR/OS';
$i++;
$is['id'][$i] = '2';
$is['name'][$i] = 'NO stabiliz.';


// ===================================================================================
// ======================================================= navigation
// ===================================================================================


$tpl_navigation_tmp = array();
$nav_limit = 9;
$k=1;
$tpl_navigation = "";

if ( $numrows > $limit ) 
	
{	
	// Calculate number of pages
	$pages = intval($numrows/$limit);
	if ($numrows%$limit)
	{
		$pages++;
	}
	
		
	// Display page links
	for ( $i=1; $i<=$pages; $i++ )
	{
				// Check if on current page
				if (($offset/$limit) == ($i-1)) {
				// $i is equal to current page, so don't display a link
				$tpl_navigation_tmp['item'][$k] = "&nbsp;<span class=\"notlink\">$i</span>&nbsp;";
				$tpl_navigation_tmp['sel'][$k] = TRUE;
				$tpl_navigation_sel=$k;
				$k++;
				}
				else
				{
				// $i is NOT the current page, so display a link to page $i
				$newoffset=$limit*($i-1);
				$tpl_navigation_tmp['item'][$k] = "&nbsp;<a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=".$newoffset."\"><b>$i</b></a>\n";
				$tpl_navigation_tmp['sel'][$k] = FALSE;
				$k++;
				}
				
								
				if($i == $pages)
				{
					$loffset = $newoffset;
				}
			
	}

	if ( !((($offset/$limit)+1) == $pages) && $pages != 1 )
		{	$tpl_navigation .= "<td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=0\"><img src='img/icon_pg_f1.gif' width='15' height='15' border='0'></a></td>"; }	
	else
		{ $tpl_navigation = "<td><img src='img/icon_pg_p0.gif' width='15' height='15' border='0'></td><td>"; }
		
	
	// Don't display PREV link if on first page
	if ( $offset != 0 )
	{
		$prevoffset = $offset-$limit;
		$tpl_navigation = "<td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=0\"><img src='img/icon_pg_f1.gif' width='15' height='15' border='0'></a></td>";
		$tpl_navigation .= "<td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=".$prevoffset."\"><img src='img/icon_pg_p1.gif' width='15' height='15' border='0'></a></td><td>";
	}
	else
	{
		$tpl_navigation = "<td><img src='img/icon_pg_p0.gif' width='15' height='15' border='0'></td>";
		$tpl_navigation .= "<td><img src='img/icon_pg_f0.gif' width='15' height='15' border='0'></td><td>";
	}

	
	$pre_dots = true;
	$post_dots = true;	
	
	
	for ($j=1; $j<=count($tpl_navigation_tmp['item']); $j++)
		{
			$diff = $j - $tpl_navigation_sel;
			
			if($diff < ($nav_limit*-1))
			{
				if($pre_dots) { $tpl_navigation .= "<span class='n_sort'>...</span>"; }
				$pre_dots = false; 
				
			} elseif(($diff > $nav_limit) )
			{
				if($post_dots) { $tpl_navigation .= "<span class='n_sort'>...</span>"; }
				$post_dots = false;
			} else
			{
				$tpl_navigation .= $tpl_navigation_tmp['item'][$j];
				
			}
		
		}

	// Check to see if current page is last page
	if ( !((($offset/$limit)+1) == $pages) && $pages != 1 )
	{
		$newoffset=$offset+$limit;
		$tpl_navigation .= "&nbsp;</td><td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=".$newoffset."\"><img src='img/icon_pg_n1.gif' width='15' height='15' border='0'></a></td>";	
		$tpl_navigation .= "<td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=".$loffset."\"><img src='img/icon_pg_l1.gif' width='15' height='15' border='0'></a></td>";
	}
	else
	{
		
		$tpl_navigation .= "&nbsp;</td><td><img src='img/icon_pg_n0.gif' width='15' height='15' border='0'></td>";
		$tpl_navigation .= "<td><img src='img/icon_pg_l0.gif' width='15' height='15' border='0'></td>";
	}
}

$tpl_navigation = "<table border='0'><tr>".$tpl_navigation."</tr></table>";


?>
