<?php
/**
 * @author      MagePsycho <info@magepsycho.com>
 * @website     http://www.magepsycho.com
 * @category    using Header / Footer outside of Magento
 */
$mageFilename = '/var/zpanel/hostdata/zadmin/public_html/uwphotography_ca/app/Mage.php';
require_once $mageFilename;
#Mage::setIsDeveloperMode(true);
#ini_set('display_errors', 1);
umask(0);
Mage::init();
Mage::getSingleton('core/session', array('name'=>'frontend'));

 
$block = Mage::getSingleton('core/layout');
 
# HEAD BLOCK
$headBlock  = $block->createBlock('page/html_head')->setTemplate('page/html/head_wp.phtml');// this wont give you the css/js inclusion

# add infortis js
$headBlock->addJs('prototype/prototype.js');
$headBlock->addJs('lib/ccard.js');
$headBlock->addJs('prototype/validation.js');
$headBlock->addJs('scriptaculous/builder.js');
$headBlock->addJs('scriptaculous/effects.js');
$headBlock->addJs('scriptaculous/dragdrop.js');
$headBlock->addJs('scriptaculous/controls.js');
$headBlock->addJs('scriptaculous/slider.js');
$headBlock->addJs('varien/js.js');
$headBlock->addJs('varien/form.js');
$headBlock->addJs('varien/menu.js');
$headBlock->addJs('mage/translate.js');
$headBlock->addJs('mage/cookies.js');

# add general js
$headBlock->addJs('infortis/jquery/jquery-1.7.2.min.js');
$headBlock->addJs('infortis/jquery/jquery-noconflict.js');
$headBlock->addJs('infortis/jquery/plugins/jquery.easing.min.js');
$headBlock->addJs('infortis/jquery/plugins/jquery.tabs.min.js');
$headBlock->addJs('infortis/jquery/plugins/jquery.accordion.min.js');
$headBlock->addJs('infortis/jquery/plugins/jquery.ba-throttle-debounce.min.js');
$headBlock->addJs('infortis/jquery/plugins/jquery.owlcarousel.min.js');
$headBlock->addJs('infortis/jquery/plugins/jquery.cookie.js');
$headBlock->addJs('infortis/jquery/plugins/jquery.reveal.js');
                        
# add css
$headBlock->addCss('css/styles.css');
$headBlock->addCss('css/styles-infortis.css');  
$headBlock->addCss('css/infortis/_shared/generic-cck.css');
$headBlock->addCss('css/infortis/_shared/accordion.css');
$headBlock->addCss('css/infortis/_shared/dropdown.css');
$headBlock->addCss('css/infortis/_shared/itemslider.css');
$headBlock->addCss('css/infortis/_shared/itemslider-old.css');
$headBlock->addCss('css/infortis/_shared/generic-nav.css');
$headBlock->addCss('css/infortis/_shared/icons.css');
$headBlock->addCss('css/infortis/_shared/itemgrid.css');
$headBlock->addCss('css/infortis/_shared/tabs.css');

$headBlock->addCss('css/infortis/ultra-megamenu/ultra-megamenu.css'); 
$headBlock->addCss('css/infortis/ultra-megamenu/wide.css');



$headBlock->addCss('css/icons-theme.css');
$headBlock->addCss('css/icons-social.css');
$headBlock->addCss('css/common.css');
$headBlock->addCss('css/_config/design_default.css');
$headBlock->addCss('css/override-components.css');
$headBlock->addCss('css/override-modules.css');
$headBlock->addCss('css/override-theme.css');
$headBlock->addCss('css/infortis/_shared/grid12.css');
$headBlock->addCss('css/_config/grid_default.css');
$headBlock->addCss('css/_config/layout_default.css');
$headBlock->addCss('css/reveal.css');
$headBlock->addCss('css/custom1.css');
      
$headBlock->getCssJsHtml();
$headBlock->getIncludes();
 
# HEADER BLOCK
$headerBlock = $block->createBlock('page/html_header')->setTemplate('page/html/header.phtml');

$linksBlock = $block->createBlock('cms/block')->setBlockId('block_header_top_left');
$headerBlock->setChild('block_header_top_left',$linksBlock);

$linksBlock1 = $block->createBlock('cms/block')->setBlockId('block_header_top_right');
$headerBlock->setChild('block_header_top_right',$linksBlock1);

$currencyBlock = $block->createBlock('directory/currency')->setTemplate('directory/currency.phtml');
$headerBlock->setChild('currency',$currencyBlock);

$cartBlock = $block->createBlock('cms/block')->setBlockId('external_cart_block');   // My custom cart Block
$headerBlock->setChild('cart_sidebar',$cartBlock);        

$searchBlock =  $block->createBlock('core/template')->setTemplate('catalogsearch/form.mini.phtml');
$headerBlock->setChild('topSearch',$searchBlock);

$navBlock = $block->createBlock('ultramegamenu/navigation')->setTemplate('infortis/ultramegamenu/mainmenu.phtml');
$headerBlock->setChild('topMenu',$navBlock);

$navBlockChild1 = $block->createBlock('cms/block')->setBlockId('block_header_nav_dropdown');
$navBlock->setChild('block_header_nav_dropdown',$navBlockChild1);

$navBlockChild2 = $block->createBlock('cms/block')->setBlockId('block_nav_links');
$navBlock->setChild('block_nav_links',$navBlockChild2);

$headerBlock = $headerBlock->toHtml();
 
?>
