<?php
// fetch post vars


if(isset($_POST['action']))  { $action = $_POST['action']; }

if ($action == 'action_cancel') { $action_cancel = true; }
if ($action == 'action_edit') { $action_edit = true; }




if ( $action_cancel )
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=home");
    	exit;
}


if ( $action_edit && user_is_member("E,A", $con) )
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=database&nav1=edit&id=".$object_id);
    	exit;
}


$keywords2 = "";

//==================== GET DATA / DEFAULT
if ( isset( $object_id ) )
{
	// urejanje objekta
	$sql = "SELECT lens.name name, lens.focal_min, lens.focal_max, 
					lens_maker.name lens_maker, weight, length, diameter, filter, announced, macro_ratio,
					stabilizer, zoom,
					lens_mount_type.name mount_type_name, lens_mount_type.description mount_type_description,
					awmax.name awmax, awmin.name awmin, atmax.name atmax, atmin.name atmin,
					lens_status.name lens_status,
					lens_focus.name lens_focus,
					
					lens_category_id, lens_category.name lens_category, lens.product_id, lens.discontinued, lens.lens_replaced_id,
					
					lens.angle, lens.construction, lens.blades, lens.blades_type, lens.minfocus, lens.afmotor, lens.focus_method,
					lens.zoom_method, lens.filter_desc, lens.notes, lens.notes_admin ,lens.stabilizer_type, lens.accessories,
					lens.added, lens.modified, user_add.username user_created, user_chg.username user_modified
					
					
			FROM lens, lens_maker, lens_mount_type, lens_aperture awmax, lens_aperture awmin, lens_aperture atmax, lens_aperture atmin, lens_status, 
				 lens_focus, user AS user_add, user AS user_chg, lens_category
				
			WHERE lens.id='$object_id'
			AND lens.user_add_id = user_add.id
			AND lens.user_chg_id = user_chg.id
			AND lens.lens_maker_id = lens_maker.id
			AND lens.lens_mount_type_id = lens_mount_type.id
			AND lens.lens_aperture_wmin_id = awmin.id
			AND lens.lens_aperture_wmax_id = awmax.id
			AND lens.lens_aperture_tmin_id = atmin.id
			AND lens.lens_aperture_tmax_id = atmax.id
			AND lens.lens_status_id = lens_status.id
			AND lens.lens_focus_id = lens_focus.id
			AND lens.lens_category_id = lens_category.id
			
			
			";
    $result = mysql_query($sql);
    //$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	
//	echo $sql;



	$name = stripslashes($row['name']);
	$focal_min = $row['focal_min'];
	$focal_max = $row['focal_max'];

	$lens_maker = $row['lens_maker'];
	$lens_mount_type = $row['mount_type_name']." (".$row['mount_type_description'].")";


	$lens_aperture_wmax = $row['awmax'];
	$lens_aperture_wmin = $row['awmin'];
	$lens_aperture_tmax = $row['atmax'];
	$lens_aperture_tmin = $row['atmin'];
	
	$lens_status = $row['lens_status'];
	$lens_category = $row['lens_category'];
	$lens_category_id = $row['lens_category_id'];
	
	$lens_focus = $row['lens_focus'];
	$product_id = stripslashes($row['product_id']);
	
	if($row['stabilizer']) { $stabilizer = 'image stabilizer'; $isis = true; } else { $stabilizer = 'none'; $isis = false; } 
	if($row['zoom']) { $zoom = 'zoom'; $iszoom = true; } else { $zoom = 'prime'; $iszoom = false;} 

	if(!$row['weight']) { $weight = '';} else { $weight = $row['weight']." g";  }
	if($row['length'] == '0') { $length = '';} else { $length = $row['length']." mm"; } 
	if($row['diameter'] == '0') { $diameter = '';}  else { $diameter = $row['diameter']." mm"; }
	if(!$row['filter']) { $filter = '';}  else { $filter = $row['filter']." mm"; }
	
	
	if(strlen($row['filter_desc'])) { $filter .= " / ".nl2br(stripslashes($row['filter_desc'])); }
	
	
	if($row['announced'] == '0000-00-00') { $announced = ''; } else { $announced = niceDate($row['announced']); }
	if($row['discontinued'] == '0000-00-00') { $discontinued = false; } else { $discontinued = niceDate($row['discontinued']); }
	
	$lens_replaced_id = $row['lens_replaced_id'];
	
	
	// get replacement for - if exist
	if(	$lens_replaced_id )
	{
		$sql2 = "SELECT name FROM lens WHERE id = '$lens_replaced_id'";
		$result2 = mysql_query($sql2);
		$num2 = mysql_num_rows($result2);
		
		if($num2 <> '1') 
		 { 
		 	$lens_replaced_txt = '<b>### error ###</b>'; 
		 } else
		 { 	
		 	//$row2 = mysql_fetch_array($result2, $con);
			$row2 = mysql_fetch_array($result2);
			$lens_replaced_txt = "<a href='".$_SERVER['PHP_SELF'].'?nav0=database&nav1=view&id='.$lens_replaced_id."' class='item'>".$row2['name']."</a>";
		 }
	} else
	{
		$lens_replaced_txt = "&nbsp;";
	}
	
	// get replaced by - if exist
	$sql2 = "SELECT id, name, announced FROM lens WHERE lens_replaced_id = '$object_id'";
	$result2 = mysql_query($sql2);
	$num2 = mysql_num_rows($result2);
	
	if($num)
	{
		//$row2 = mysql_fetch_array($result2, $con);
		$row2 = mysql_fetch_array($result2);
		$lens_replacedby_id = $row2['id'];
		$lens_replacedby_txt = "<a href='".$_SERVER['PHP_SELF'].'?nav0=database&nav1=view&id='.$lens_replacedby_id."' class='item'>".$row2['name']."</a>";
		
		if($row2['announced'] != '0000-00-00')
		{
			$lens_replacedby_txt .= " (".niceYearMonth($row2['announced']).")";
		}

	} else
	{
		$lens_replacedby_id = false;
	}
	
	
	
	if( strlen($row['macro_ratio']) )
	{
		$macro_ratio = stripslashes($row['macro_ratio']);
	} else
	{
		$macro_ratio = '';
	}
	
	
	$angle = nl2br(stripslashes($row['angle']));
	$construction = nl2br(stripslashes($row['construction']));
	
	if($row['blades']) 	{ $blades = $row['blades'];	} else 	{ $blades = ''; 	}
	if(strlen($row['blades_type'])) 
		{ 	
			if(strlen($blades)) { $blades .= " / "; }
			$blades .= nl2br(stripslashes($row['blades_type'])); 	
		}
	
	
	if($row['minfocus'] > '0')
	{
		$minfocus = $row['minfocus']." cm"; 
	} else
	{
		$minfocus = ''; 
	}
	$afmotor = nl2br(stripslashes($row['afmotor']));
	$focus_method = nl2br(stripslashes($row['focus_method']));
	$zoom_method = nl2br(stripslashes($row['zoom_method']));
	
	$stabilizer_type = nl2br(stripslashes($row['stabilizer_type']));
	$accessories = nl2br(stripslashes($row['accessories']));
	$notes = nl2br(stripslashes($row['notes']));
	$notes_admin = nl2br(stripslashes($row['notes_admin']));
	
	
	$time_added = niceDateTime($row['added']);
	$time_modified = niceDateTime($row['modified']);
	
	$user_created = $row['user_created'];
	$user_modified = $row['user_modified'];
	
	
	
	
	

	
	
	// =========  get selected lenstype
	$sql = "SELECT lens_type.name 
			FROM lens_type, lens_lens_type 
			WHERE lens_lens_type.lens_id = '$object_id'
			AND lens_lens_type.lens_type_id = lens_type.id";

	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	
	$lens_type = '';
	
	if($num)
	{
		for ($i=1; $i <= $num; $i++ )
		{	
			//$row = mysql_fetch_array($result, $con);
			$row = mysql_fetch_array($result);
			$lens_type .= $row['name'];
			if($i<$num) { $lens_type .= ', '; }
		}
	} else
	{
		$lens_type = 'n/d';
	}
	
	
	
	// =========  get selected fitsfor
	$sql = "SELECT lens_fitsfor.name 
			FROM lens_fitsfor, lens_lens_fitsfor 
			WHERE lens_lens_fitsfor.lens_id = '$object_id'
			AND lens_lens_fitsfor.lens_fitsfor_id = lens_fitsfor.id";

	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	
	$lens_fitsfor = '';
	
	if($num)
	{
		for ($i=1; $i <= $num; $i++ )
		{	
			//$row = mysql_fetch_array($result, $con);
			$row = mysql_fetch_array($result);
			$lens_fitsfor .= $row['name'];
			if($i<$num) { $lens_fitsfor .= ', '; }
		}
	} else
	{
		$lens_fitsfor = 'n/d';
	}	
	
	
	// get lens_image
	$sql = "SELECT lens_photo_version.filename, lens_photo_version.size_x, lens_photo_version.size_y, lens_photo.description
			FROM lens_photo_version, lens_photo
			WHERE lens_photo_version.lens_photo_id = lens_photo.id
			AND lens_photo.lens_id = '$object_id'
			AND lens_photo_version.no = '3'";
	
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	
	if($num)
	{
		//$row = mysql_fetch_array($result, $con);
		$row = mysql_fetch_array($result);
		$image_filename = $dir_photo.$row['filename'];
		$image_sizex = $row['size_x'];
		$image_sizey = $row['size_y'];
		$image_description = $row['description'];

	} else
	{
		$image_filename = false;
	}
	
	
	$lens_metadata = "lens record created: <span title='created by ".$user_created."'>".$time_added."</span><br>".
	                 "last modified: <span title='last modified by ".$user_modified."'>".$time_modified."</span>";
	

	
	// get lens external links
	if(user_is_member("E,A", $con)) { $sql_add = ""; } else { $sql_add = " AND status_id = '2' "; }
	$sql = "SELECT id, name, url, status_id
			FROM lens_link
			WHERE lens_id = '$object_id'
			".$sql_add." 
			ORDER BY added DESC";
	
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	
	if($num)
	{
		$links = array();
		for($i=1; $i<= $num; $i++)
		{
			//$row = mysql_fetch_array($result, $con);
			$row = mysql_fetch_array($result);
			$links['id'][$i] = $row['id'];
			$links['name'][$i] = stripslashes($row['name']);
			$links['url'][$i] = $row['url'];
			$links['status_id'][$i] = $row['status_id'];
			
			if($links['status_id'][$i] != '2')
			{ $links['name'][$i] = "*".$links['name'][$i]; }
		}

	} else
	{
		$links = false;
	}
	
	
	// add counter of lens views
	if(! inc_lens_view($object_id, $con))
	{
		$GLOBALS['error_msg'] = "Lens display error!";
	}

	// keywords: 
	$keywords2 .= $lens_maker.", ".$zoom.", ".$name;


}
else
{
	// id not defined - redirect to home
	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=home");
    exit;
}

$meta_title =$name . " | Lens Database by Mozaik UW";
$meta_description ="Info and Specifications about the " . $name . " Lens from the Lens Database by Mozaik UW";

?>
