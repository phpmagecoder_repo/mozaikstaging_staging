<?php


// don't log this page
$log_stats = false;

// if user is not admin
if(! user_is_member("C,E,A", $con))
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=home");
    	exit;
}


$action_deletephoto = '';
$action_deletelink = '';
$action_save = '';
$action_addphoto = '';
$action_addlink = '';



if(isset($_POST['action']))  { $action = $_POST['action']; }


if ($action == 'action_cancel') { $action_cancel = true; }
if ($action == 'action_save') { $action_save = true; }
if ($action == 'action_addphoto') { $action_addphoto = true; }
if ($action == 'action_deletephoto') { $action_deletephoto = true; }
if ($action == 'action_deletelink') { $action_deletelink = true; }
if ($action == 'action_addlink') { $action_addlink = true; }

$focal_min = '';
$focal_max = '';

if(isset($_POST['lens_maker_id'])) $lens_maker_id = $_POST['lens_maker_id'];
if(isset($_POST['name'])) $name = addslashes($_POST['name']);
if(isset($_POST['focal_min'])) $focal_min = $_POST['focal_min'];
if(isset($_POST['focal_max'])) $focal_max = $_POST['focal_max'];

if($focal_min && $focal_max) 
	{ $zoom_ratio = $focal_max/$focal_min; }
	else
	{ $zoom_ratio = 1;}

if(isset($_POST['lens_aperture_wmax_id'])) $lens_aperture_wmax_id = $_POST['lens_aperture_wmax_id'];
if(isset($_POST['lens_aperture_wmin_id'])) $lens_aperture_wmin_id = $_POST['lens_aperture_wmin_id'];
if(isset($_POST['lens_aperture_tmax_id'])) $lens_aperture_tmax_id = $_POST['lens_aperture_tmax_id'];
if(isset($_POST['lens_aperture_tmin_id'])) $lens_aperture_tmin_id = $_POST['lens_aperture_tmin_id'];

if(isset($_POST['lens_status_id'])) $lens_status_id = $_POST['lens_status_id'];
if(isset($_POST['status_id'])) $status_id = $_POST['status_id'];
if(isset($_POST['lens_mount_type_id'])) $lens_mount_type_id = $_POST['lens_mount_type_id'];
if(isset($_POST['lens_category_id'])) $lens_category_id = $_POST['lens_category_id'];

if(isset($_POST['stabilizer'])) $stabilizer = $_POST['stabilizer'];
if(isset($_POST['lens_focus_id'])) $lens_focus_id = $_POST['lens_focus_id'];
if(isset($_POST['zoom'])) $zoom = $_POST['zoom'];

if(isset($_POST['weight'])) $weight = $_POST['weight'];
if(isset($_POST['length'])) $length = $_POST['length'];
if(isset($_POST['diameter'])) $diameter = $_POST['diameter'];
if(isset($_POST['filter'])) $filter = $_POST['filter'];	
if(isset($_POST['announced'])) $announced = dbDate($_POST['announced']);
if(isset($_POST['discontinued'])) $discontinued = dbDate($_POST['discontinued']);
if(isset($_POST['macro_ratio'])) $macro_ratio = addslashes($_POST['macro_ratio']);

if(isset($_POST['lens_type_id'])) $lens_type_id = $_POST['lens_type_id'];
if(isset($_POST['lens_fitsfor_id'])) $lens_fitsfor_id = $_POST['lens_fitsfor_id'];
if(isset($_POST['lens_replaced_id'])) $lens_replaced_id = $_POST['lens_replaced_id'];
if(isset($_POST['product_id'])) $product_id = addslashes($_POST['product_id']);


if(isset($_POST['angle'])) $angle = addslashes($_POST['angle']);
if(isset($_POST['construction'])) $construction = addslashes($_POST['construction']);
if(isset($_POST['blades'])) $blades = $_POST['blades'];
if(isset($_POST['blades_type'])) $blades_type = addslashes($_POST['blades_type']);
if(isset($_POST['minfocus'])) $minfocus = $_POST['minfocus'];
if(isset($_POST['afmotor'])) $afmotor = addslashes($_POST['afmotor']);
if(isset($_POST['focus_method'])) $focus_method = addslashes($_POST['focus_method']);
if(isset($_POST['zoom_method'])) $zoom_method = addslashes($_POST['zoom_method']);
if(isset($_POST['filter_desc'])) $filter_desc = addslashes($_POST['filter_desc']);
if(isset($_POST['stabilizer_type'])) $stabilizer_type = addslashes($_POST['stabilizer_type']);
if(isset($_POST['accessories'])) $accessories = addslashes($_POST['accessories']);
if(isset($_POST['notes'])) $notes = addslashes($_POST['notes']);
if(isset($_POST['notes_admin'])) $notes_admin = addslashes($_POST['notes_admin']);




// ce je pritisnjen cancel gumb
if ( $action_cancel )
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=home");
    	exit;
}

// delete photo
if($action_deletephoto)
{
	$lens_photo_id = $_POST['itemid'];
	
	//get filenames for all photo versions
	$sql = "SELECT filename
			FROM lens_photo_version
			WHERE lens_photo_id='$lens_photo_id'";
			
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);

	for ($i=1; $i <= $num; $i++ )
	{
		//$row = mysql_fetch_array($result, $con);
		$row = mysql_fetch_array($result);
		$filename = $row['filename'];
		unlink($dir_photo.$filename); //delete file from disk
	}
	
	//delete data from versions table
	$sql = "DELETE FROM lens_photo_version WHERE lens_photo_id='$lens_photo_id'";
	$result = mysql_query($sql);
	
	//delete data from photo table
	$sql = "DELETE FROM lens_photo WHERE id='$lens_photo_id'";
	$result = mysql_query($sql);
	
}


// delete link
if($action_deletelink)
{
	$lens_link_id = $_POST['itemid'];
	
	//delete link
	$sql = "DELETE FROM lens_link WHERE id='$lens_link_id'";
	$result = mysql_query($sql);
	
}	


// ce je pritisnjen shrani gumb
if ( $action_save || $action_addphoto || $action_deletephoto || $action_addlink )
{

	if ( !empty( $object_id ) )
    {
		$action_insert = false;
        $sql = "UPDATE lens SET
					lens_maker_id = '$lens_maker_id',
					focal_min = '$focal_min',
					focal_max = '$focal_max',
					name = '$name',
					lens_aperture_tmin_id = '$lens_aperture_tmin_id',
					lens_aperture_tmax_id = '$lens_aperture_tmax_id',
					lens_aperture_wmin_id = '$lens_aperture_wmin_id',
					lens_aperture_wmax_id = '$lens_aperture_wmax_id',
					user_chg_id = '$current_user_id',
					lens_status_id = '$lens_status_id',
					status_id = '$status_id',
					lens_mount_type_id = '$lens_mount_type_id',
					lens_category_id = '$lens_category_id',
					stabilizer = '$stabilizer',
					lens_focus_id = '$lens_focus_id',
					zoom = '$zoom',
					weight = '$weight',
					length = '$length',
					diameter = '$diameter',
					filter = '$filter',
					announced = '$announced',
					discontinued = '$discontinued',
					macro_ratio = '$macro_ratio',
					modified = '$db_current_datetime',
					angle = '$angle',
					construction = '$construction',
					blades = '$blades',
					blades_type = '$blades_type',
					minfocus = '$minfocus',
					afmotor = '$afmotor',
					focus_method = '$focus_method',
					zoom_method = '$zoom_method',
					filter_desc = '$filter_desc',
					stabilizer_type = '$stabilizer_type',
					accessories = '$accessories',
					notes = '$notes',
					notes_admin = '$notes_admin',
					zoom_ratio = '$zoom_ratio',
					lens_replaced_id = '$lens_replaced_id',
					product_id = '$product_id'
					
                WHERE id='$object_id'";
				
		$log = write_log('3', 'edit', $object_id, $con);
    }
    else
    {
        $next_id = getNextID('lens', $con);
		$object_id = $next_id; 
		$action_insert = true;
		
        $sql = "INSERT INTO lens (
                    id,
					lens_maker_id,
                    focal_min,
					focal_max,
					
					name,
					lens_aperture_tmin_id,
					lens_aperture_tmax_id,
					lens_aperture_wmin_id,
					lens_aperture_wmax_id,
					
					user_chg_id,
					user_add_id,
					
					lens_status_id,
					status_id,
					lens_mount_type_id,
					lens_category_id,
					
					stabilizer,
					lens_focus_id,
					zoom,
					
					weight,
					length,
					diameter,
					filter,
					announced,
					discontinued,
					macro_ratio,
					modified,
					added,
					
					
					angle,
					construction,
					blades,
					blades_type,
					minfocus,
					afmotor,
					focus_method,
					zoom_method,
					filter_desc,
					stabilizer_type,
					accessories,
					notes,
					notes_admin,
					zoom_ratio,
					lens_replaced_id,
					product_id
					
					 )
                VALUES (
                    '$next_id',
					'$lens_maker_id',
                    '$focal_min',
					'$focal_max',
					
					'$name',
					'$lens_aperture_tmin_id',
					'$lens_aperture_tmax_id',
					'$lens_aperture_wmin_id',
					'$lens_aperture_wmax_id',
					
					'$current_user_id',
					'$current_user_id',
					
					'$lens_status_id',
					'$status_id',
					'$lens_mount_type_id',
					'$lens_category_id',
					
					'$stabilizer',
					'$lens_focus_id',
					'$zoom',

					'$weight',
					'$length',
					'$diameter',
					'$filter',
					'$announced',
					'$discontinued',
					'$macro_ratio',
					'$db_current_datetime',
					'$db_current_datetime',
					
					'$angle',
					'$construction',
					'$blades',
					'$blades_type',
					'$minfocus',
					'$afmotor',
					'$focus_method',
					'$zoom_method',
					'$filter_desc',
					'$stabilizer_type',
					'$accessories',
					'$notes',
					'$notes_admin',
					'$zoom_ratio',
					'$lens_replaced_id',
					'$product_id'

					 )";

		$log = write_log('3', 'insert', $next_id, $con);
    }
    $result = mysql_query($sql);
		
	// ================0 multiple selections
	
	// delete data lens_type
	$sql = "DELETE FROM  lens_lens_type WHERE lens_id ='$object_id'";
	$result = mysql_query($sql);
	
	// insert new data
	if ( is_array( $lens_type_id ) && count( $lens_type_id ) )
	{
		foreach ( $lens_type_id as $type_id )
		{
			$sql = "INSERT INTO lens_lens_type (
						lens_type_id,
						lens_id )
					VALUES (
						'$type_id',
						'$object_id')";				
			$result = mysql_query($sql);
		}
	}
	
	// delete data fits_for
	$sql = "DELETE FROM lens_lens_fitsfor WHERE lens_id ='$object_id'";
	$result = mysql_query($sql);
	
	// insert new data
	if ( is_array( $lens_fitsfor_id ) && count( $lens_fitsfor_id ) )
	{
		foreach ( $lens_fitsfor_id as $fitsfor_id )
		{
			$sql = "INSERT INTO lens_lens_fitsfor (
						lens_fitsfor_id,
						lens_id )
					VALUES (
						'$fitsfor_id',
						'$object_id')";				
			$result = mysql_query($sql);
		}
	}
}	
	
	
	
	
	
	
// adding link
if($action_addlink)
{	

	$link_name = $_POST['link_name'];
	$link_url = $_POST['link_url'];
	$link_status_id = $_POST['link_status_id'];
	
	
	if(strlen($link_name) > 5 && strlen($link_url) > 5)
	{
		// $url = fsockopen($link_url, 80, &$errno, &$errstr, 30); 

		// check link
//		$url = @fopen($link_url, 'r');

		

		
//		if($url)
//		if( is_valid_url( $link_url ) )
		if(true)
		{
//			fclose($url);
			$next_link_id = getNextID('lens_link', $con);
			$sql = "INSERT INTO lens_link (
						id,
						status_id,
						user_add_id,
						user_chg_id,
						lens_id,
						name,
						url,
						added,
						modified )
					VALUES (
						'$next_link_id ',
						'$link_status_id',
						'$current_user_id',
						'$current_user_id',
						'$object_id',
						'$link_name',
						'$link_url',
						'$db_current_datetime',
						'$db_current_datetime'
						)";				
			$result = mysql_query($sql);	
		} else
		{
			$GLOBALS['error_msg'] = "Error: URL Link '".$link_url."' Not valid!";
		}
		
		
	
	
	} else
	{
		$GLOBALS['error_msg'] = "Error: Please specify link and site description";
	}



}
	
	
// adding photo

// if($action_addphoto && !empty($_FILES['lens_photo']['name']) )
if($action_addphoto)
{
   // check in file was sent
		if( empty($_FILES['lens_photo']['name']) and $action_addphoto)
   		{
   			$all_ok = false;
			$GLOBALS['error_msg'] = "Error #1: Please specify image file (jpeg/image)";
   		}
   		$file = $_FILES['lens_photo'];

  // is uploaded file JPEG
  		if(($file['type'] != 'image/jpeg') and ($file['type'] != 'image/pjpeg') )
  		// jpeg (mozilla)  pjpeg (IE)
  		{
   			$all_ok = false;
			if (! $GLOBALS['error_msg']) 
			{ $GLOBALS['error_msg'] .= "Error #2: Please specify image in (jpeg/image) format"; }
  		}
   // is file uploaded OK
  		if(!is_uploaded_file($file['tmp_name']))
  		{
   			$all_ok = false;
			if (! $GLOBALS['error_msg']) 
			{ $GLOBALS['error_msg'] .= "Error #3: Error uploading file"; }
  		}
		
   // does this directory exist?
		if(!file_exists($dir_photo) or !is_dir($dir_photo))
  		{
   			$all_ok = false;
			if (! $GLOBALS['error_msg']) 
			{ $GLOBALS['error_msg'] .= "System Error #4: Images directory does not exist"; }
  		}

	
	if($all_ok) // #1
	{
		$photo_type_id = $_POST['photo_type_id'];
		$photo_priority = $_POST['photo_priority'];
		$photo_description = addslashes($_POST['photo_description']);
	
		// check what photo verisons needs to be created
		$sql = "SELECT * 
				FROM lens_photo_type_version
				WHERE lens_photo_type_id = '$photo_type_id'";
				
		$result = mysql_query($sql);
		$num_v = mysql_num_rows($result);
	
		$photo_types = array();
		for ($i=1; $i <= $num_v; $i++ )
		{
			//$row = mysql_fetch_array($result, $con);
			$row = mysql_fetch_array($result);
			$photo_types['max_x'][$i] = $row['max_x'];
			$photo_types['max_y'][$i] = $row['max_y'];
			$photo_types['no'][$i] = $row['no'];		
		}
		
		
		// set new photo name - ORIGINAL
		$next_id = getNextID('lens_photo', $con);
		$photo_number = sprintf("%09d",$next_id);
		$filename = 'img'.$photo_number.'_9.jpeg';

		$filename_orig = $dir_photo.$filename;
		
		
		// copy tmp photo to directory
  		if(!move_uploaded_file($file['tmp_name'], $filename_orig))
  		{
   			$all_ok = false;
			if (! $GLOBALS['error_msg']) 
			{ $GLOBALS['error_msg'] .= "Error #5: Error copying uploaded file"; }
  		}
		
		
		if($all_ok) // #2
		{
		
			// change access rights on system
			chmod($filename_orig,0664);
			
			// get image size
			$size = getimagesize($filename_orig);
			
			// generate original photo
			$original = imagecreatefromjpeg ($filename_orig);
	
			$width_original = $size[0];
			$height_original = $size[1];
			$border = 0;		
			$ratio = $width_original / $height_original;
		
		
		
		    // save image data into database
			$sql = "INSERT INTO lens_photo (
						id,
						status_id,
						user_add_id,
						user_chg_id,
						lens_id,
						lens_photo_type_id,
						description,
						priority,
						added,
						modified )
					VALUES (
						'$next_id',
						'2',
						'$current_user_id',
						'$current_user_id',
						'$object_id',
						'$photo_type_id',
						'$photo_description',
						'$photo_priority',
						'$db_current_datetime',
						'$db_current_datetime'
						 )";
			$result = mysql_query($sql);
			
			
			
			
			// shrani podatke o originalni varianti slike v bazo
			$next_id2 = getNextID('lens_photo_version', $con);
			$sql = "INSERT INTO lens_photo_version (
						id,
						lens_photo_id,
						filename,
						size_x,
						size_y,
						no )
					VALUES (
						'$next_id2',
						'$next_id',
						'$filename',
						'$width_original',
						'$height_original',
						'9' )";
			$result = mysql_query($sql);
		
			// generate foto version
			for ($i=1; $i <= count($photo_types['no']); $i++ )
			{
				// calculate new photo dimensions
				if ( ($height_original > $photo_types['max_y'][$i]) or ($width_original > $photo_types['max_x'][$i]) )
				{
					$img_height = $photo_types['max_y'][$i];
					$img_width = $photo_types['max_y'][$i]*$ratio;
				
					if ($img_width > $photo_types['max_x'][$i] )
					{
						$img_width = $photo_types['max_x'][$i];
						$img_height = $photo_types['max_x'][$i]/$ratio;
					}
				} else
				{
					$img_width = $width_original;
					$img_height = $height_original;
				}
			
				//create new image
				$image = imagecreatetruecolor($img_width, $img_height) or die("Error initializing image.");
			
				// resize
				imagecopyresampled($image, $original, 0, 0, 0, 0, $img_width, $img_height, imagesx($original), imagesy($original));
	
				// filename of new image
				$filename = 'img'.$photo_number.'_'.$photo_types['no'][$i].'.jpeg';
				$filename_imag = $dir_photo.$filename;
				$no = $photo_types['no'][$i];
	
				// save image to file
				imagejpeg ($image,$filename_imag,72);
				chmod($filename_imag,0664);
				
				// write data of each image version in database
				$next_id2 = getNextID('lens_photo_version', $con);
				$sql = "INSERT INTO lens_photo_version (
							id,
							lens_photo_id,
							filename,
							size_x,
							size_y,
							no )
						VALUES (
							'$next_id2',
							'$next_id',
							'$filename',
							'$img_width',
							'$img_height',
							'$no' )";
				$result = mysql_query($sql);
				
			
			}  // end for() generate
			
			$log = write_log('3', 'photo uploaded: id='.$next_id, $object_id, $con);
		
		}  // all ok #2
	
	} // all ok #1
}


// redirect on ADD IMAGE or ADD LINK
if( ($action_addphoto || $action_addlink) && $action_insert )
{
	header("Location: http://".$_SERVER['SERVER_NAME']
				.dirname2($_SERVER['PHP_SELF'])
				."/index.php?nav0=database&nav1=edit&id=".$object_id);
	exit; 
}
	

// redirect on SAVE	
if($action_save)
{
	header("Location: http://".$_SERVER['SERVER_NAME']
				.dirname2($_SERVER['PHP_SELF'])
				."/index.php?nav0=home");
	exit; 
}

	
//=============================================================================
//==================== GET DATA / DEFAULT
//=============================================================================

if ( isset( $object_id ) )
{
	// urejanje objekta
	$sql = "SELECT * FROM lens  WHERE id='$object_id'";
    $result = mysql_query($sql);

    //$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);

	$lens_maker_id = $row['lens_maker_id'];
	$name = htmlspecialchars(stripslashes($row['name']));
	$focal_min = $row['focal_min'];
	$focal_max = $row['focal_max'];

	$lens_aperture_wmax_id = $row['lens_aperture_wmax_id'];
	$lens_aperture_wmin_id = $row['lens_aperture_wmin_id'];
	$lens_aperture_tmax_id = $row['lens_aperture_tmax_id'];
	$lens_aperture_tmin_id = $row['lens_aperture_tmin_id'];
	
	$lens_status_id = $row['lens_status_id'];
	$status_id = $row['status_id'];
	$lens_mount_type_id = $row['lens_mount_type_id'];
	$stabilizer = $row['stabilizer'];
	$lens_focus_id = $row['lens_focus_id'];
	$lens_category_id = $row['lens_category_id'];
	$lens_replaced_id = $row['lens_replaced_id'];
	$zoom = $row['zoom'];

	$weight = $row['weight'];
	$length = $row['length'];
	$diameter = $row['diameter'];
	$filter = $row['filter'];	
	$announced = niceDate($row['announced']);
	$discontinued = niceDate($row['discontinued']);
	$macro_ratio = htmlspecialchars(stripslashes($row['macro_ratio']));
	
	$product_id = htmlspecialchars(stripslashes($row['product_id']));	
	
	$angle = htmlspecialchars(stripslashes($row['angle']));
	$construction = htmlspecialchars(stripslashes($row['construction']));
	$blades = $row['blades'];
	$blades_type = htmlspecialchars(stripslashes($row['blades_type']));
	$minfocus = $row['minfocus'];
	$afmotor = htmlspecialchars(stripslashes($row['afmotor']));
	$focus_method = htmlspecialchars(stripslashes($row['focus_method']));
	$zoom_method = htmlspecialchars(stripslashes($row['zoom_method']));
	$filter_desc = htmlspecialchars(stripslashes($row['filter_desc']));
	$stabilizer_type = htmlspecialchars(stripslashes($row['stabilizer_type']));
	$accessories = htmlspecialchars(stripslashes($row['accessories']));
	$notes = htmlspecialchars(stripslashes($row['notes']));
	$notes_admin = htmlspecialchars(stripslashes($row['notes_admin']));	

	// get replacement for - if exist
	if(	$lens_replaced_id )
	{
		$sql2 = "SELECT name FROM lens WHERE id = '$lens_replaced_id'";
		$result2 = mysql_query($sql2);
		$num2 = mysql_num_rows($result2);
		
		if($num2 <> '1') 
		 { 
		 	$lens_replaced_txt = '<b>### error ###</b>'; 
		 } else
		 { 	
		 	//$row2 = mysql_fetch_array($result2, $con);
			$row2 = mysql_fetch_array($result2);
			$lens_replaced_txt = "<a href='".$_SERVER['PHP_SELF'].'?nav0=database&nav1=view&id='.$lens_replaced_id."' class='item'>".$row2['name']."</a>";
		 }
	} else
	{
		$lens_replaced_txt = "&nbsp;";
	}
	
	
	// get selected lens type
	$sql = "SELECT lens_type_id
			FROM lens_lens_type
			WHERE lens_id = '$object_id'";


	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	$selected_type = array();
	
	for ($i=1; $i <= $num; $i++ )
	{	
		//$row = mysql_fetch_array($result, $con);
		$row = mysql_fetch_array($result);
		$selected_type['id'][$i] = $row['lens_type_id'];
	}	
	
	// get selected fitsfor
	$sql = "SELECT lens_fitsfor_id 
			FROM lens_lens_fitsfor
			WHERE lens_id = '$object_id'";

	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	$selected_fitsfor = array();
	
	for ($i=1; $i <= $num; $i++ )
	{	
		//$row = mysql_fetch_array($result, $con);
		$row = mysql_fetch_array($result);
		$selected_fitsfor['id'][$i] = $row['lens_fitsfor_id'];

	}	
	
	
	// get lens external links

	$sql = "SELECT lens_link.id, lens_link.name name, lens_link.url, lens_link.status_id, lens_link.added, user.name user, status.name status
			FROM lens_link, user, status
			WHERE lens_id = '$object_id'
			AND lens_link.user_add_id = user.id
			AND lens_link.status_id = status.id
			ORDER BY added DESC";
	
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	
	if($num)
	{
		$links = array();
		for($i=1; $i<= $num; $i++)
		{
			//$row = mysql_fetch_array($result, $con);
			$row = mysql_fetch_array($result);
			$links['id'][$i] = $row['id'];
			$links['name'][$i] = stripslashes($row['name']);
			$links['url'][$i] = $row['url'];
			$links['status'][$i] = $row['status'];
			$links['added'][$i] = niceDateTime($row['added']);
			$links['user'][$i] = $row['user'];
			
		}

	} else
	{
		$links = false;
	}
	
				

}
else
{
	// nov objekt
	$lens_maker_id = '1';
	$name = '';
	$focal_min = '';
	$focal_max = '';
	$lens_aperture_wmax_id = '10';
	$lens_aperture_wmin_id = '99';
	$lens_aperture_tmax_id = '13';
	$lens_aperture_tmin_id = '99';
	
	$lens_status_id = '1';
	$status_id = '2';
	$lens_mount_type_id = '2';
	$stabilizer = '0';
	$lens_focus_id = '1';
	$zoom = '1';
	
	$weight = '';
	$length = '';
	$diameter = '';
	$filter = '';
	$announced = '';
	$discontinued = '';
	$macro_ratio = '';
	
	$product_id = '';
	$lens_replaced_id = 0;
	
	$links = false;  // external links
}

// ================================== drop down menus

// get lens max aperture
$sql = "SELECT id, name
		FROM lens_aperture
		ORDER BY value";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_aperture = array();

$j=1;
for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_aperture['id'][$j] = $row['id'];
	$lens_aperture['name'][$j] = $row['name'];	
	$j++;
}


// get lens makers
$sql = "SELECT id, name
		FROM lens_maker
		ORDER BY name";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_maker = array();


for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_maker['id'][$i] = $row['id'];
	$lens_maker['name'][$i] = $row['name'];	

}


// get lens mount_type
$sql = "SELECT id, name, description
		FROM lens_mount_type
		ORDER BY name";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$mount_type = array();


for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$mount_type['id'][$i] = $row['id'];
	$mount_type['name'][$i] = $row['name']." (".$row['description'].")";	

}



// get status
$sql = "SELECT id, name
		FROM status
		ORDER BY name";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$status = array();


for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$status['id'][$i] = $row['id'];
	$status['name'][$i] = $row['name'];	
}

// get lens status
$sql = "SELECT id, name
		FROM lens_status
		ORDER BY id";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_status = array();


for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_status['id'][$i] = $row['id'];
	$lens_status['name'][$i] = $row['name'];	
}


// get lens category
$sql = "SELECT id, name
		FROM lens_category
		ORDER BY id";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_category = array();


for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_category['id'][$i] = $row['id'];
	$lens_category['name'][$i] = $row['name'];	
}


// get lens focus
$sql = "SELECT id, name, short
		FROM lens_focus
		ORDER BY id";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_focus = array();


for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_focus['id'][$i] = $row['id'];
	$lens_focus['name'][$i] = $row['name'].' ('.$row['short'].")";	
}



// get lens types
$sql = "SELECT id, name, description
		FROM lens_type
		ORDER BY id";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_type = array();

for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_type['id'][$i] = $row['id'];
	$lens_type['name'][$i] = $row['name']." (".$row['description'].")";	
	$lens_type['selected'][$i] = false;
	
	for($k=1; $k <= count($selected_type['id']); $k++)
	{
		if( $selected_type['id'][$k] == $lens_type['id'][$i] )
		{
			$lens_type['selected'][$i] = true;
		}
	}
}



// get fits for
$sql = "SELECT id, name
		FROM lens_fitsfor 
		ORDER BY id";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$lens_fitsfor = array();

for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_fitsfor['id'][$i] = $row['id'];
	$lens_fitsfor['name'][$i] = $row['name'];	
	$lens_fitsfor['selected'][$i] = false;
	
	for($k=1; $k <= count($selected_fitsfor['id']); $k++)
	{
		if( $selected_fitsfor['id'][$k] == $lens_fitsfor['id'][$i] )
		{
			$lens_fitsfor['selected'][$i] = true;
		}
	}
}


// get PHOTO TYPE
$sql = "SELECT id, name
		FROM lens_photo_type
		ORDER BY id";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$photo_type = array();

for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$photo_type['id'][$i] = $row['id'];
	$photo_type['name'][$i] = $row['name'];	
}


// ---------- get all attached photos

$sql = "SELECT lens_photo.id id, lens_photo.description description, lens_photo_type.name type, lens_photo_version.filename filename, lens_photo.priority priority
		FROM lens_photo, lens_photo_type, lens_photo_version
		WHERE lens_id = '$object_id'
		AND lens_photo.lens_photo_type_id = lens_photo_type.id
		AND lens_photo_version.lens_photo_id = lens_photo.id
		AND lens_photo_version.no = '1'
		ORDER BY priority";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);

$lens_photo = array();
for ($i=1; $i <= $num; $i++ )
{
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$lens_photo['id'][$i] = $row['id'];
	$lens_photo['description'][$i] = $row['description'];
	$lens_photo['type'][$i] = $row['type'];
	$lens_photo['priority'][$i] = $row['priority'];	
	$lens_photo['filename'][$i] = $dir_photo.$row['filename'];
}


?>
