
<form enctype="multipart/form-data"  action="<?php echo $_SERVER['PHP_SELF']?>" method="post" name="seznam" STYLE="padding:0px; spacing:0px;">

	<input type="hidden" name="id" value="<?php echo $object_id ?>">
	<input type="hidden" name="nav0" value="<?php echo $nav0 ?>">
	<input type="hidden" name="nav1" value="<?php echo $nav1 ?>">
	<input type="hidden" name="nav2" value="<?php echo $nav2 ?>">
	<input type="hidden" name="nav3" value="<?php echo $nav3 ?>">
	<input type="hidden" name="action" value="">
	<input type="hidden" name="itemid" value="">
	
	<table width="<?php echo $page_width; ?>" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td align="left" valign="top">
	
	
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="normal">
	<tr class="col_gray2">
	  <td width="150" nowrap class="table_header"><strong>publish status</strong></td>
	  <td><?php
	  
	  if( user_is_member("E,A", $con) )
	  { ?><select style="width:150px" class="inputfield" name="status_id">
          <?php for ( $i=1; $i <= count($status['id']); $i++ )
              { ?>
          <option <?php if($status['id'][$i] == $status_id) echo "selected"; ?> value="<?php echo $status['id'][$i]; ?>"> <?php echo $status['name'][$i] ?></option>
          <?php } ?>
      </select><?php
	  } else
	  { ?>
	  	<span class="table_header">Pending for approval!</span><input type="hidden" name="status_id" value="1">
<?php } ?></td>
      <td width="30">&nbsp;</td>
      <td width="150" nowrap class="table_header"><strong>lens status </strong></td>
      <td><select class="inputfield" style="width:150px" name="lens_status_id">
          <?php for ( $i=1; $i <= count($lens_status['id']); $i++ )
              { ?>
          <option <?php if($lens_status['id'][$i] == $lens_status_id) echo "selected"; ?> value="<?php echo $lens_status['id'][$i]; ?>"> <?php echo $lens_status['name'][$i] ?></option>
          <?php } ?>
      </select></td>
	</tr>
	<tr class="col_gray3">
	  <td nowrap><strong>name</strong></td>
	  <td colspan="4"><input style="width: 400px" name="name" type="text" class="inputfield" alt="length|5" emsg="Enter lens name!" value="<?php echo $name ?>"></td>
	  </tr>
	<tr>
	  <td nowrap><strong>maker</strong></td>
	  <td><select style="width:200px" class="inputfield" name="lens_maker_id">
          <?php for ( $i=1; $i <= count($lens_maker['id']); $i++ )
              { ?>
          <option <?php if($lens_maker['id'][$i] == $lens_maker_id) echo "selected"; ?> value="<?php echo $lens_maker['id'][$i]; ?>"> <?php echo $lens_maker['name'][$i] ?></option>
          <?php } ?>
      </select></td>
      <td>&nbsp;</td>
      <td nowrap><strong>weight (g) </strong></td>
      <td><input style="width: 100px" name="weight" type="text" tabindex="300" class="inputfield" value="<?php echo $weight ?>"></td>
	</tr>
	<tr>
	  <td nowrap><strong>mount type </strong></td>
	  <td><select style="width:200px" class="inputfield" name="lens_mount_type_id">
          <?php for ( $i=1; $i <= count($mount_type['id']); $i++ )
              { ?>
          <option <?php if($mount_type['id'][$i] == $lens_mount_type_id) echo "selected"; ?> value="<?php echo $mount_type['id'][$i]; ?>"> <?php echo $mount_type['name'][$i] ?></option>
          <?php } ?>
      </select></td>
      <td>&nbsp;</td>
      <td nowrap><strong>length (mm) </strong></td>
      <td><input style="width: 100px" name="length" type="text" tabindex="310" class="inputfield" value="<?php echo $length ?>"></td>
	</tr>
	<tr>
	  <td nowrap><strong>category </strong></td>
	  <td><select style="width:200px" class="inputfield" name="lens_category_id">
          <?php for ( $i=1; $i <= count($lens_category['id']); $i++ )
              { ?>
          <option <?php if($lens_category['id'][$i] == $lens_category_id) echo "selected"; ?> value="<?php echo $lens_category['id'][$i]; ?>"> <?php echo $lens_category['name'][$i] ?></option>
          <?php } ?>
      </select></td>
	  <td>&nbsp;</td>
	  <td nowrap><strong>diameter (mm) </strong></td>
	  <td><input style="width: 100px" name="diameter" tabindex="320" type="text" class="inputfield" value="<?php echo $diameter ?>"></td>
	</tr>
	<tr>
	  <td nowrap><strong>focal length (mm) </strong></td>
	  <td><input style="width: 70px" name="focal_min" alt="number|0|1|2600" emsg="Enter minimal focal distance!" type="text" class="inputfield" value="<?php echo $focal_min ?>">
	    (wide)
	    <input style="width: 70px" name="focal_max" alt="number|0|0|2600" emsg="Enter maximum focal distance!" type="text" class="inputfield" value="<?php echo $focal_max ?>">
	    (tele) </td>
	  <td>&nbsp;</td>
	  <td nowrap>&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>
	<tr>
	  <td valign="top" nowrap><strong>diagonal angle of view </strong></td>
	  <td><textarea name="angle" class="inputfield" style="width: 250px; height:40px;"><?php echo $angle ?></textarea></td>
	  <td>&nbsp;</td>
	  <td valign="top" nowrap><strong>lens construction </strong></td>
	  <td><textarea name="construction" class="inputfield" style="width: 250px; height:40px;"><?php echo $construction; ?></textarea></td>
	</tr>
	<tr>
	  <td nowrap class="line_t2"><strong>aperture wide </strong></td>
	  <td class="line_t2"><select style="width:80px" class="inputfield" name="lens_aperture_wmax_id">
          <?php for ( $i=1; $i <= count($lens_aperture['id']); $i++ )
              { ?>
          <option <?php if($lens_aperture['id'][$i] == $lens_aperture_wmax_id) echo "selected"; ?> value="<?php echo $lens_aperture['id'][$i]; ?>"> <?php echo $lens_aperture['name'][$i] ?></option>
          <?php } ?>
        </select>
	    (max)
  <select style="width:80px" class="inputfield" name="lens_aperture_wmin_id">
    <?php for ( $i=1; $i <= count($lens_aperture['id']); $i++ )
              { ?>
    <option <?php if($lens_aperture['id'][$i] == $lens_aperture_wmin_id) echo "selected"; ?> value="<?php echo $lens_aperture['id'][$i]; ?>"> <?php echo $lens_aperture['name'][$i] ?></option>
    <?php } ?>
  </select>
	    (min)</td>
		<td class="line_t2">&nbsp;</td>
	    <td class="line_t2" nowrap><strong>filter (mm) / note </strong></td>
	    <td class="line_t2"><input style="width: 40px" name="filter" type="text" class="inputfield" value="<?php echo $filter ?>">
            <input name="filter_desc" type="text" class="inputfield" style="width: 205px;" value="<?php echo $filter_desc; ?>"></td>
	</tr>
	<tr>
	  <td nowrap><strong>aperture tele </strong></td>
	  <td><select style="width:80px" class="inputfield" name="lens_aperture_tmax_id">
          <?php for ( $i=1; $i <= count($lens_aperture['id']); $i++ )
              { ?>
          <option <?php if($lens_aperture['id'][$i] == $lens_aperture_tmax_id) echo "selected"; ?> value="<?php echo $lens_aperture['id'][$i]; ?>"> <?php echo $lens_aperture['name'][$i] ?></option>
          <?php } ?>
        </select>
	    (max)
  <select style="width:80px" class="inputfield" name="lens_aperture_tmin_id">
    <?php for ( $i=1; $i <= count($lens_aperture['id']); $i++ )
              { ?>
    <option <?php if($lens_aperture['id'][$i] == $lens_aperture_tmin_id) echo "selected"; ?> value="<?php echo $lens_aperture['id'][$i]; ?>"> <?php echo $lens_aperture['name'][$i] ?></option>
    <?php } ?>
  </select>
	    (min)</td>
		<td>&nbsp;</td>
	    <td nowrap><strong>announced</strong></td>
	    <td><input style="width: 100px" name="announced" id="announced" type="text" alt="date|dd/mm/yyyy|.|bok" emsg="Invalid date..." class="inputfield" value="<?php echo $announced ?>">
            <img src="img/calendar.gif" alt="calendar" name="trigger_announced_date" id="trigger_announced_date" style="cursor: pointer;" 
	  title="Announced date" onMouseOver="this.style.background='blue';" onMouseOut="this.style.background=''" /> (dd.mm.yyyy) </td>
	</tr>
	<tr>
	  <td valign="top" nowrap><strong>blades (no / type) </strong></td>
	  <td><input style="width: 40px" name="blades" type="text" class="inputfield" value="<?php echo $blades ?>">
          <input style="width: 204px" name="blades_type" type="text" class="inputfield" value="<?php echo $blades_type ?>"></td>
	  <td>&nbsp;</td>
	  <td nowrap><strong>discontinued</strong></td>
	  <td><input style="width: 100px" name="discontinued" id="discontinued" type="text" alt="date|dd/mm/yyyy|.|bok" emsg="Invalid date..." class="inputfield" value="<?php echo $discontinued ?>">
          <img src="img/calendar.gif" alt="calendar" name="trigger_discontinued_date" id="trigger_discontinued_date" style="cursor: pointer;" 
	  title="Discontinued date" onMouseOver="this.style.background='blue';" onMouseOut="this.style.background=''" /> (dd.mm.yyyy) </td>
	</tr>
	<tr>
	  <td nowrap><strong>macro ratio</strong></td>
	  <td><input style="width: 200px" name="macro_ratio" type="text" class="inputfield" value="<?php echo $macro_ratio ?>"></td>
	  <td>&nbsp;</td>
	  <td nowrap><strong>replacement for (id) </strong></td>
	  <td><input name="lens_replaced_id" type="text" class="inputfield" style="width: 40px;" value="<?php echo $lens_replaced_id; ?>"> <?php echo  $lens_replaced_txt; ?></td>
	  </tr>
	<tr>
	  <td valign="top" nowrap><strong>minimum  focus (cm) </strong></td>
	  <td><input style="width: 100px" name="minfocus" type="text" class="inputfield" value="<?php echo $minfocus ?>"></td>
	  <td>&nbsp;</td>
	  <td nowrap><strong>zoom/prime</strong></td>
	  <td><input type="radio" name="zoom" value="1" <?php if($zoom == '1') { echo "checked"; } ?>>
	    zoom
	    <input type="radio" name="zoom" value="0" <?php if($zoom != '1') { echo "checked"; } ?>>
	    prime</td>
	</tr>
	<tr>
	  <td nowrap>&nbsp;</td>
	  <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td valign="top" nowrap><strong><span title="">zoom method</span></strong></td>
      <td><input name="zoom_method" type="text" class="inputfield" style="width: 250px;" value="<?php echo $zoom_method; ?>"></td>
	</tr>
	<tr>
	  <td nowrap><strong>model number</strong></td>
	  <td><input style="width: 200px" name="product_id" type="text" class="inputfield" value="<?php echo $product_id ?>"></td>
	  <td>&nbsp;</td>
	  <td nowrap><strong>focusing type </strong></td>
	  <td><select style="width:200px" class="inputfield" name="lens_focus_id">
          <?php for ( $i=1; $i <= count($lens_focus['id']); $i++ )
              { ?>
          <option <?php if($lens_focus['id'][$i] == $lens_focus_id) echo "selected"; ?> value="<?php echo $lens_focus['id'][$i]; ?>"> <?php echo $lens_focus['name'][$i] ?></option>
          <?php } ?>
      </select></td>
	</tr>
	<tr>
	  <td nowrap><strong>image stabilizer </strong></td>
	  <td><input name="stabilizer" type="checkbox" value="1" <?php if($stabilizer == '1') { echo "checked"; } ?>>
	    (IS / VR / OS)</td>
	  <td>&nbsp;</td>
      <td valign="top" nowrap><strong>focus method </strong></td>
      <td><input name="focus_method" type="text" class="inputfield" style="width: 250px;" value="<?php echo $focus_method; ?>"></td>
	</tr>
	<tr>
	  <td valign="top" nowrap><strong>stabilizer type</strong></td>
	  <td><textarea name="stabilizer_type" class="inputfield" style="width: 250px; height:50px;"><?php echo $stabilizer_type; ?></textarea></td>
      <td>&nbsp;</td>
	  <td valign="top" nowrap><strong>AF  motor type</strong></td>
	  <td><textarea name="afmotor" class="inputfield" style="width: 250px; height:50px;"><?php echo $afmotor; ?></textarea></td>
	</tr>
	<tr>
      <td valign="top" nowrap class="line_t2"><strong>lens type </strong><br>
        (hold CTRL <br>
        to select multiple) </td>
	  <td class="line_t2"><select class="inputfield" style="width:190px;" name="lens_type_id[]" multiple size="8">
          <?php for ( $i=1; $i <= count($lens_type['id']); $i++ )
			  { ?>
          <option value="<?php echo $lens_type['id'][$i]; ?>" <?php if($lens_type['selected'][$i]) echo 'selected'?>><?php echo $lens_type['name'][$i] ?></option>
          <?php } ?>
      </select></td>
	  <td class="line_t2">&nbsp;</td>
	  <td class="line_t2" valign="top" nowrap><strong>fits for</strong><br>
	    (hold CTRL <br>
	    to select multiple) </td>
	  <td class="line_t2"><select class="inputfield" style="width:190px;" name="lens_fitsfor_id[]" multiple size="8">
          <?php for ( $i=1; $i <= count($lens_fitsfor['id']); $i++ )
			  { ?>
          <option value="<?php echo $lens_fitsfor['id'][$i]; ?>" <?php if($lens_fitsfor['selected'][$i]) echo 'selected'?>><?php echo $lens_fitsfor['name'][$i] ?></option>
          <?php } ?>
      </select></td>
	  </tr>
	<tr>
      <td class="line_t2" valign="top" nowrap><strong>accessories</strong></td>
	  <td class="line_t2" colspan="4"><textarea name="accessories" class="inputfield" style="width: 400px; height:40px;"><?php echo $accessories; ?></textarea></td>
      </tr>
	<tr>
      <td valign="top" nowrap><strong>notes</strong></td>
	  <td colspan="4"><textarea name="notes" class="inputfield" style="width: 400px; height:60px;"><?php echo $notes; ?></textarea></td>
      </tr>
	<tr>
      <td valign="top" nowrap><strong>admin notes </strong></td>
	  <td colspan="4"><textarea name="notes_admin" class="inputfield" style="width: 400px; height:60px;"><?php echo $notes_admin; ?></textarea></td>
      </tr>
	<tr class="col_gray3">
	  <td nowrap>&nbsp;</td>
	  <td><input class="gumb" type="button" style="width:90px" name="action_save" onClick="submitAction(this.form, 'action_save', 1)" value="Save"><input class="gumb" type="button" style="width:90px" name="new" onClick="submitAction(this.form, 'action_cancel', 0)" value="Cancel"></td>
      <td>&nbsp;</td>
      <td nowrap>&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>
</table>	

�
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="normal">
  <tr class="col_gray2">
    <td width="150" nowrap class="table_header"><strong>links</strong></td>
  </tr>
</table>

<?php if($links)
		{ ?>
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="normal">  
<?php for($i=1; $i<= count($links['id']); $i++)
	{ ?>
  <tr>
    <td width="150" align="right" nowrap><img src="img/bullet1.gif" width="15" height="7"></td>
    <td><a href="<?php echo $links['url'][$i]; ?>" class="item"><?php echo $links['name'][$i]; ?></a></td>
	<td><?php echo $links['url'][$i]; ?></td>
    <td class="small_info2"><?php echo $links['user'][$i].' @'.$links['added'][$i]; ?></td>
	<td><?php echo $links['status'][$i]; ?></td>
	<td width="12"><a href="<?php echo $_SERVER['PHP_SELF'].'?nav0=database&nav1=editlink&id='.$links['id'][$i] ?>"><img src="img/icon_edit.gif" width="12" height="13" border="0"></a></td>
	<td><INPUT name="submit" TYPE="image" onClick="submitActionItemId(this.form, 'action_deletelink', <?php echo $links['id'][$i] ?>,0)" src="img/icon_delete.gif" title="Delete link" width="11" height="13"></td>
  </tr>
<?php } ?>  
</table>
<?php 	} ?>  


<table width="100%" border="0" cellpadding="3" cellspacing="0" class="normal">  
  <tr class="col_gray3">
    <td width="150" align="right" nowrap><strong>name</strong></td>
    <td width="400"><input style="width: 400px" name="link_name" type="text" class="inputfield" value=""></td>
    <td class="small_info2">describe link content eg: DPreview - lens test, Canon Europe - specifications </td>
  </tr>
  <tr class="col_gray3">
    <td align="right" nowrap><strong>url</strong></td>
    <td><input style="width: 400px" name="link_url" type="text" class="inputfield" value=""></td>
    <td class="small_info2">direct link to page including http://....</td>
  </tr>
  <tr class="col_gray3">
    <td nowrap>&nbsp;</td>
    <td><select style="width:150px" class="inputfield" name="link_status_id">
      <?php for ( $i=1; $i <= count($status['id']); $i++ )
              { ?>
      <option <?php if($status['id'][$i] == '2') echo "selected"; ?> value="<?php echo $status['id'][$i]; ?>"> <?php echo $status['name'][$i] ?></option>
      <?php } ?>
    </select>
    <input class="gumb" type="button" style="width:90px" name="action_addlink" onClick="submitAction(this.form, 'action_addlink', 1)" value="add link"></td>
    <td>&nbsp;</td>
  </tr>
</table>

�&nbsp;
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="normal">
  <tr class="col_gray2">
    <td width="150" nowrap class="table_header"><strong>add photo</strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td nowrap class="table_header">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="col_gray3">
    <td width="150" align="right" nowrap><strong>type</strong></td>
    <td><select style="width:150px" class="inputfield" name="photo_type_id">
      <?php for ( $i=1; $i <= count($photo_type['id']); $i++ )
              { ?>
      <option <?php if($photo_type['id'][$i] == $photo_type_id) echo "selected"; ?> value="<?php echo $photo_type['id'][$i]; ?>"> <?php echo $photo_type['name'][$i] ?></option>
      <?php } ?>
    </select></td>
    <td width="30">&nbsp;</td>
    <td width="150" align="right" nowrap><strong>priority</strong></td>
    <td><input style="width: 100px" name="photo_priority" type="text" tabindex="200" class="inputfield" value="10"></td>
  </tr>
  <tr class="col_gray3">
    <td align="right" nowrap><strong>description</strong></td>
    <td colspan="4"><input style="width: 400px" name="photo_description" type="text" class="inputfield" value="<?php echo $name ?>"></td>
  </tr>
  <tr class="col_gray3">
    <td nowrap>&nbsp;</td>
    <td colspan="4"><input name="lens_photo" type="file" class="inputfield"> <input class="gumb" type="button" style="width:90px" name="action_addphoto" onClick="submitAction(this.form, 'action_addphoto', 1)" value="add photo"></td>
    </tr>
</table>
&nbsp;
<?php if(count($lens_photo['id']))
		{ ?>
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="normal">
  <tr class="col_gray2">
    <td nowrap class="table_header"><strong>photos</strong></td>
    <td>&nbsp;</td>
    <td nowrap class="table_header">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<?php for($i=1; $i<= count($lens_photo['id']); $i++)
		{ ?>  
  <tr class="col_gray3">
    <td><img src="<?php echo $lens_photo['filename'][$i] ?>"></td>
    <td><?php echo $lens_photo['description'][$i] ?></td>
    <td><?php echo $lens_photo['priority'][$i] ?></td>
    <td><INPUT name="submit" TYPE="image" onClick="submitActionItemId(this.form, 'action_deletephoto', <?php echo $lens_photo['id'][$i] ?>,1)" src="img/icon_delete.gif" title="Delete photo" width="11" height="13"></td>
  </tr>
<?php	} ?>  
</table>
<?php } ?>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p></td>
<!--		  <td width="10"><img src="img/shim.gif" width="10" height="10"></td>
		  <td width="160"><img src="img/google_ads.png" width="160" height="600"></td> -->
		</tr>
	</table>


</form>
<script type="text/javascript">
    Calendar.setup({
        inputField     :    "announced",     // id of the input field
        button         :    "trigger_announced_date"  // trigger for the calendar (button ID)
    });
    Calendar.setup({
        inputField     :    "discontinued",     // id of the input field
        button         :    "trigger_discontinued_date"  // trigger for the calendar (button ID)
    });	
	

</script>
