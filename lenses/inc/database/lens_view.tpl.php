<?php
$def_title = "Lens Database - " . $name;
$def_description = "Lens Database";
$def_keywords1 = "Lens Database";
$def_keywords2 = "Lens Database";
?>

<form enctype="multipart/form-data"  action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" name="seznam" STYLE="padding:0px; spacing:0px;">

    <input type="hidden" name="id" value="<?php echo $object_id ?>">
    <input type="hidden" name="nav0" value="<?php echo $nav0 ?>">
    <input type="hidden" name="nav1" value="<?php echo $nav1 ?>">
    <input type="hidden" name="nav2" value="<?php echo $nav2 ?>">
    <input type="hidden" name="nav3" value="<?php echo $nav3 ?>">
    <input type="hidden" name="action" value="">
    <table width="<?php echo $page_width; ?>" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" valign="top">

                <table width="100%" border="0" cellpadding="2" cellspacing="0" class="normal">
                    <tr>
                        <td colspan="3"><h1><?php echo $name ?></h1></td>
                    </tr>
<?php if ($image_filename) {
    ?>
                        <tr>
                            <td colspan="3"><img src="<?php echo $image_filename ?>" alt="<?php echo $image_description ?>" width="<?php echo $image_sizex ?>" height="<?php echo $image_sizey ?>" border="0"></td>
                        </tr>		
<?php } ?>			 
                    <tr>	
                        <td width="50%" valign="top"><table border="0" cellpadding="2" cellspacing="0" class="normal" width="100%">
                                <tr>
                                    <td width="120" align="right" valign="top" nowrap class="line_t2"><strong>focal length:</strong></td>
                                    <td valign="top" class="line_t2"><?php echo $focal_min ?> 
<?php if ($iszoom) {
    echo " - " . $focal_max;
} ?> 
                                        mm</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" nowrap><strong>aperture<?php if ($iszoom) {
    echo ' wide';
} ?>:</strong></td>
                                    <td valign="top"><?php echo $lens_aperture_wmax ?> - <?php echo $lens_aperture_wmin ?></td>
                                </tr>
                                <?php if ($iszoom) { ?>			
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>aperture tele: </strong></td>
                                        <td valign="top"><?php echo $lens_aperture_tmax ?> - <?php echo $lens_aperture_tmin ?></td>
                                    </tr>
                                <?php } ?>		
<?php if (strlen($blades)) { ?>  	 
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>blades (no / type): </strong></td>
                                        <td valign="top"><?php echo $blades ?></td>
                                    </tr>
<?php } ?>				
                                <tr>
                                    <td align="right" valign="top" nowrap class="line_t2"><strong>image stabilizer: </strong></td>
                                    <td valign="top" class="line_t2"><?php echo $stabilizer ?></td>
                                </tr>
<?php if ($isis && strlen($stabilizer_type)) { ?>
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>stabilizer type:</strong></td>
                                        <td valign="top"><?php echo $stabilizer_type ?></td>
                                    </tr>
<?php } ?>
                                <tr>
                                    <td align="right" valign="top" nowrap class="line_t2"><strong>focusing:</strong></td>
                                    <td class="line_t2" valign="top"><?php echo $lens_focus ?></td>
                                </tr>
                                <?php if (strlen($afmotor)) { ?>  			  
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>AF  motor type:</strong></td>
                                        <td valign="top"><?php echo $afmotor ?></td>
                                    </tr>
                                <?php } ?>		  
                                <?php if (strlen($focus_method)) { ?>  			  
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>focus method: </strong></td>
                                        <td valign="top"><?php echo $focus_method ?></td>
                                    </tr>
                                <?php } ?>		  
                                <?php if ($iszoom && strlen($zoom_method)) { ?>
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>zoom method: </strong></td>
                                        <td valign="top"><?php echo $zoom_method ?></td>
                                    </tr>
                                <?php } ?>
                                <?php if (strlen($macro_ratio)) { ?>  	
                                    <tr>
                                        <td align="right" valign="top" nowrap class="line_t2"><strong>macro ratio:</strong></td>
                                        <td class="line_t2" valign="top"><?php echo $macro_ratio ?></td>
                                    </tr>
                                <?php } ?>		  
                                <?php if (strlen($minfocus)) { ?>  	  
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>minimum  focus (cm): </strong></td>
                                        <td valign="top"><?php echo $minfocus ?></td>
                                    </tr>
                                <?php } ?>		  

                                <?php if (strlen($accessories)) { ?>          
                                    <tr>
                                        <td align="right" valign="top" nowrap class="line_t2"><strong>accessories:</strong></td>
                                        <td class="line_t2" valign="top"><?php echo $accessories ?></td>
                                    </tr>
                                <?php } ?>		

                                <?php if (strlen($product_id)) { ?>  	
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>model number:</strong></td>
                                        <td valign="top"><?php echo $product_id ?></td>
                                    </tr>
                                <?php } ?>	

<?php if (strlen($notes)) { ?>  	
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>notes:</strong></td>
                                        <td valign="top"><?php echo $notes ?></td>
                                    </tr>
                                <?php } ?>					
                                <tr>
                                    <td class="line_t2" align="right" valign="top" nowrap>&nbsp;</td>
                                    <td class="line_t2" valign="top">&nbsp;</td>
                                </tr>

<?php if ($current_user_id == '1') {
    ?>

<?php } ?>
                            </table></td>
                        <td width="20"><img src="img/shim.gif" width="20" height="10"></td>
                        <td width="50%" valign="top"><table border="0" cellpadding="2" cellspacing="0" class="normal" width="100%">
                                <tr>
                                    <td width="120" align="right" valign="top" nowrap class="line_t2"><strong>maker:</strong></td>
                                    <td valign="top" class="line_t2"><?php echo $lens_maker ?></td>
                                </tr>
                                <tr>
                                    <td  align="right" valign="top" nowrap><strong>fits for:</strong></td>
                                    <td valign="top" ><?php echo $lens_fitsfor ?></td>
                                </tr>
                                <?php if ($lens_category_id > 1) { ?>	
                                    <tr>
                                        <td  align="right" valign="top" nowrap><strong>category:</strong></td>
                                        <td valign="top" ><?php echo $lens_category ?></td>
                                    </tr>		  
                                <?php } ?>		  
                                <?php if (strlen($announced)) { ?>	   		  
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>announced:</strong></td>
                                        <td valign="top"><?php echo $announced ?></td>
                                    </tr>
                                <?php } ?>	
                                <?php if (strlen($discontinued)) { ?>	   	  
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>discontinued:</strong></td>
                                        <td valign="top"><?php echo $discontinued ?></td>
                                    </tr>
                                <?php } ?>

                                <?php if ($lens_replaced_id) { ?>	   	  
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>replacement for:</strong></td>
                                        <td valign="top"><?php echo $lens_replaced_txt ?></td>
                                    </tr>
                                <?php } ?>	

<?php if ($lens_replacedby_id) { ?>	   	  
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>replaced by:</strong></td>
                                        <td valign="top"><?php echo $lens_replacedby_txt ?></td>
                                    </tr>
<?php } ?>	

                                <tr>
                                    <td align="right" valign="top" nowrap><strong>status:</strong></td>
                                    <td valign="top"><?php echo $lens_status ?></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" nowrap><strong>zoom/prime:</strong></td>
                                    <td valign="top"><?php echo $zoom ?></td>
                                </tr>

                                <tr>
                                    <td align="right" valign="top" nowrap><strong>mount type: </strong></td>
                                    <td valign="top"><?php echo $lens_mount_type ?></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" nowrap><strong>lens type: </strong></td>
                                    <td valign="top"><?php echo $lens_type ?></td>
                                </tr>
                                <?php if (strlen($weight)) { ?>	          
                                    <tr>
                                        <td align="right" valign="top" nowrap class="line_t2"><strong>weight:  </strong></td>
                                        <td class="line_t2" valign="top"><?php echo $weight ?></td>
                                    </tr>
                                <?php } ?>
                                <?php if (strlen($length)) { ?>	
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>length: </strong></td>
                                        <td valign="top"><?php echo $length ?></td>
                                    </tr>
                                <?php } ?>
                                <?php if (strlen($diameter)) { ?>	
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>diameter: </strong></td>
                                        <td valign="top"><?php echo $diameter ?></td>
                                    </tr>
                                <?php } ?>
                                <?php if (strlen($filter)) { ?>	
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>filter / note: </strong></td>
                                        <td valign="top"><?php echo $filter ?></td>
                                    </tr>
                                <?php } ?>		  
                                <?php if (strlen($construction)) { ?>   		  
                                    <tr>
                                        <td align="right" valign="top" nowrap class="line_t2"><strong>lens construction: </strong></td>
                                        <td class="line_t2" valign="top"><?php echo $construction ?></td>
                                    </tr>
                                <?php } ?>

                                <?php if (strlen($angle)) { ?>		  
                                    <tr>
                                        <td align="right" valign="top" nowrap><strong>diagonal angle of view:</strong></td>
                                        <td valign="top"><?php echo $angle ?></td>
                                    </tr>
                                            <?php } ?>

                                            <?php if ($links) {
                                                ?>
                                    <tr>
                                        <td class="line_t2" align="right" valign="top" nowrap><strong>external links:</strong></td>
                                        <td class="line_t2" valign="top"><table border="0" cellpadding="1" cellspacing="0" class="normal" width="100%">
                                                <?php for ($i = 1; $i <= count($links['id']); $i++) {
                                                    ?>
                                                    <tr>
                                                        <td align="center" valign="middle"><img src="img/bullet1.gif" width="15" height="7"></td>
                                                        <td><a href="<?php echo $links['url'][$i]; ?>" class="item" target="_blank"><?php echo $links['name'][$i]; ?></a></td>
                                                    </tr>
                                    <?php } ?>		
                                            </table></td>
                                    </tr>
                                <?php } ?>


<?php if ($current_user_id == '1') {
    ?>

<?php } ?>
                            </table></td>
                    </tr>
                    <tr>
                        <td align="left" class="small_info">&copy; All lens photos are copyright of their owners</td>
                        <td>&nbsp;</td>
                        <td align="right" class="small_info"><?php echo $lens_metadata ?></td>
                    </tr>	
                </table>	


                <table border="0" cellpadding="2" cellspacing="0" class="normal">
                    <tr>
                        <td width="120">&nbsp;</td>
                        <td><input class="gumb" type="button" style="width:150px" name="new" onClick="submitAction(this.form, 'action_cancel', 0)" value="Back to list"></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
<?php if (user_is_member("E,A", $con)) {
    ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input class="gumb" type="button" style="width:100px" name="new" onClick="submitAction(this.form, 'action_edit', 0)" value="Edit"></td>		
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

<?php } ?>	
                </table>			
            </td>
        </tr>
    </table>	

</form>
