<table width="<?php echo $page_width; ?>" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left" valign="top"><h1>Tamron - Lens Dictionary</h1>

            <h2><a name="vc"></a><strong>VC (Vibration Compensation)</strong></h2>
            <p>The highly evaluated VC (Vibration Compensation)  mechanism originally developed by Tamron delivers exceedingly  stabilized viewfinder images for remarkable hand-held photography  without annoying motion delay. The VC mechanism exhibits its  camera-shake compensation performance to the maximum extent at dusk, in  dimly-lit indoor situations, or in hand-held shooting at the  ultra-telephoto settings.</p>

            <h2><a name="di"></a><strong>Di (Digitally Integrated)</strong></h2>
            <p>Featuring coating optimized for digital SLRs, but still usable on 24�36mm sensors (35mm, �full� or double frame)</p>

            <h2><a name="diii"></a><strong>Di II</strong></h2>
            <p>Lenses for DSLRs with APS-C sized sensors only</p>

            <h2><a name="usd"></a><strong>USD - Ultrasonic Silent Drive</strong></h2>
            <p>Lens equipped with Tamron's ultrasonic auto-focus   drive USD (Ultrasonic Silent Drive) achieves faster focusing making   lens perfect for photography of sports, motor racing, and   other fast-moving subjects. With advanced motor technology and newly   developed software, Tamron's USD delivers precise and noiseless focusing   at turbo speed. <br>
                <br>
                Tamron's USD works with the high-frequency ultrasonic vibrations that   are produced by a ring called a 'stator'. Energy from the vibrations is   used to rotate an attached metallic ring known as the 'rotor'.   Piezoelectric ceramic, an element that produces ultrasonic vibrations   when voltage of a specific frequency is applied is arranged in a ring   formation on the stator. This electrode configuration of piezoelectric   ceramic causes two ultrasonic vibrations to occur in the stator.<br>
                By effectively combining these two ultrasonic vibrations, it is possible   to convert the energy from the vibrations that produced simple motion   into energy known as 'deflective traveling waves', which then moves   around the circumference (rotation direction) of the ring.<br>
                With the USD, the friction between these deflective traveling waves   created on the metallic surface of the stator and the surface of the   rotor produce force, causing the rotor to rotate. The focusing ring   lens, which is linked to the rotor, is thus moved, creating a fast and   smooth auto-focus drive.</p>			  

            <h2><a name="sp"></a><strong>SP</strong> - Super Performance</h2>
            <p>Tamron's "SP" designation stands for "Super  				  Performance". Tamron's original SP symbol is shown above left. Tamron's current  				  SP symbol is shown to the right of the original symbol. In the past, Tamron's  				  lenses received the SP designation for any of or a combination of the following  				  reasons:</p>

            <ul>
                <li>Superior sharpness and contrast due to an advanced or unique  					 optical design.</li>
                <li>Low dispersion element(s) for apochromatic or near  					 apochromatic performance.</li>
                <li>Internal focusing (compared to similar lenses of the  					 era).</li>
                <li>Superior macro performance (compared to similar lenses of the  					 era).</li>
                <li>An exceptional zoom range compared to similar lenses.</li>
                <li>A larger maximum aperture compared to similar lenses.</li>
            </ul>
            <p>Today, many lenses produced by Tamron and other  				  manufactures feature low dispersion elements and internal focusing. It appears  				  that Tamron currently reserves its SP designation for lenses which meet any or  				  a combination of the following criteria:</p>
            <ul>
                <li>Superior sharpness and contrast due to an advanced or unique  					 optical design.</li>
                <li>Superior macro performance compared to similar lenses.</li>
                <li>An exceptional zoom range compared to similar lenses.</li>
                <li>A larger maximum aperture compared to similar lenses.</li>
            </ul>
            <p>Older Tamron SP lenses were simply marked with  				  the letters "SP" somewhere on the lens barrel (usually above the aperture  				  ring), and many SP lenses also had "TAMRON SP" marked on the front element  				  retaining ring and/or on the included built-in or standard accessory lens hood.  				  Today's modern Tamron SP lenses are distinctively marked with a thin gold ring  				  about the top of the lens barrel.</p>
            <h2><a name="if"></a><strong>IF - Internal Focus</strong></h2>
            <p>Tamron's "IF" designation stands for "Internal  				  Focusing". Tamron lenses with an IF desination incorporate internal focusing  				  wherein the length of the lens does not change while the lens is being focused.  				  This is because a group of internal focus elements are moved, in order to  				  acheive focus, rather than moving the frontmost lens elements. Movement of  				  these internal focus elements is very similar to moving the variator and  				  compensator lens groups within a zoom lens, but instead the lens is being  				  focused rather than being zoomed. Some Tamron IF lenses also featured a  				  variable IF cam, much like the variable zoom cams found within zoom lenses,  				  which produced a faster and much more natural focusing collar movement at very  				  close focus distances. This is a useful feature in close-up and macro  				  photography since it allows the photographer to quickly focus the lens while in  				  macro mode. Tamron's older IF lenses were marked with the letters "IF"  				  somewhere on the lens barrel.</p>

            <h2><a name="xr"></a>XR - Extra Refractive Index</h2>
            <p>Tamron's "XR" designation stands for "Extra  				  Refractive Index" lens optics. Lenses with XR optics for the frontmost elements  				  are somewhat more compact than similar lenses which use optics with less  				  powerful refractive indices. XR optics allow wide angle lenses, wide angle  				  zooms and wide angle to telephoto zooms to have a somewhat smaller overall  				  diameter. XR optics can potentially allow long telephoto lenses to be shorter  				  in overall length, thus reducing weight.</p>


            <h2><a name="ld"></a><strong>LD (Low Dispersion)</strong></h2>
            <p>Extra Refractive Index glass. Tamron's "LD" designation stands for "Low Dispersion". Tamron lenses with a LD desination incorporate one or more very low dispersion elements. Almost always, Tamron used two low dispersion elements to achieve apochromatic performance which was similar to lenses which incorporated a fluorite element. Tamron's older LD lenses were distintively marked with a thin bright green ring around the top of the lens barrel and featured the letters "LD" somewhere on the lens barrel. Tamron has done away with the thin bright green ring for designating its modern lenses which incorporate LD lens elements.</p>

            <h2><a name="zl"></a><strong>ZL (Zoom Lock)</strong></h2>
            <p>Tamron's "ZL" designation stands for "Zoom-Lock" mechanism. Many of today's zoom lenses utilize two or more zoom cams and feature large front elements which also move when the lens is zoomed. In some zoom lens designs, it is simply unavoidable for the zoom setting not to creep when the lens is tilted up or down. Tamron neatly solved this problem by incorporating a zoom-lock mechanism which locks the zoom setting in place.</p>

            <h2>source: <a href="http://www.tamron.com/lenses/prod/all_in_one_zooms.asp">Tamron Lenses</a>, <a href="http://en.wikipedia.org/wiki/Tamron">Wikipedia</a>, <a href="http://www.adaptall-2.com/">Adaptall</a></h2>
            <br>
            <br> 
        </td>
      
    </tr>
</table>	
