
<table width="<?php echo $page_width; ?>" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left" valign="top"><h1>Sigma - Lens Dictionary</h1>

            <h2><a name="asp"></a><strong>ASP (Aspherical Lens)</strong></h2>
            <p>The aspherical lens complex allows freedom of design, improved performance, a reduced number of component lenses and a compact size.</p>

            <h2><a name="apo"></a><strong>APO Lens</strong></h2>
            <p>In order to attain the highest quality images, the APO lens has been made using special low-dispersion (SLD) glass and is designed to minimize color aberration.</p>

            <h2><a name="os"></a><strong>OS (Optical Stabilizer)</strong></h2>
            <p>This function utilizes a built-in mechanism that compensates for camera shake. It dramatically expands photographic possibilities by alleviating camera movement when shooting by hand held camera.</p>

            <h2><a name="hsm"></a><strong>HSM (Hyper-Sonic Motor)</strong></h2>
            <p>This lens uses a motor driven by ultrasonic waves to provide a quiet, highspeed AF.</p>

            <h2><a name="rf"></a><strong>RF (Rear Focus)</strong></h2>
            <p>This lens is equipped with a system that moves the rear lens group for highspeed , silent focusing.</p>

            <h2><a name="if"></a><strong>IF (Inner Focus)</strong></h2>
            <p>To ensure stability in focusing, this lens moves the inner lens group or groups without changing the lens' physical length.</p>

            <h2><a name="conv"></a><strong>Conv (APO Teleconverter EX)</strong></h2>
            <p>This lens can be used with the APO Teleconverter EX. It can increase the focal length and will interface with the camera's AE (automatic exposure) function.</p>

            <h2><a name="ex"></a><strong>EX Lens</strong></h2>
            <p>The exterior of this lens is EX-finished to denote the superior build and optical quality, and to enhance its appearance.</p>


            <h2><a name="dg"></a><strong>DG Lens</strong></h2>
            <p>These are large-aperture lenses with wide angles and short minimum focusing distances. With an abundance of peripheral illumination, they are ideal lenses for Digital SLR Cameras whilst retaining suitability for traditional 35mm SLRs.</p>

            <h2><a name="dc"></a><strong>DC Lens</strong></h2>
            <p>These are special lenses designed so that the image circle matches the smaller size of the image sensor of most digital SLR cameras. Their specialized design gives these lenses the ideal properties for digital cameras, the compact and lightweight construction is an added bonus ! including compact and lightweight construction. </p>
            <p>source: <a href="http://www.sigmaphoto.com/lenses/lenses_all.asp">Sigma Lenses</a></p>
            <br>
            <br> 
        </td>
       
    </tr>
</table>	
