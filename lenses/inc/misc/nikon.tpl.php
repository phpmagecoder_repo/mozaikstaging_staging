<table width="<?php echo $page_width; ?>" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left" valign="top"><h1>Nikon - Lens Dictionary</h1>

            <h2><a name="ed"></a>ED glass (Extra-low Dispersion)</h2>
            <p><b>Essential element of NIKKOR telephoto lenses.</b> Nikon developed ED (Extra-low Dispersion) glass to enable the production of lenses that offer superior sharpness and color correction by minimizing chromatic aberration. Put simply, chromatic aberration is a type of image and color dispersion that occurs when light rays of varying wavelengths pass through optical glass. In the past, correcting this problem for telephoto lenses required special optical elements that offer anomalous dispersion characteristics - specifically calcium fluoride crystals. However, fluorite easily cracks and is sensitive to temperature changes that can adversely affect focusing by altering the lens� refractive index. So Nikon designers and engineers put their heads together and came up with ED glass, which offers all the benefits, yet none of the drawbacks of calcium fluorite-based glass. With this innovation, Nikon developed several types of ED glass suitable for various lenses. They deliver stunning sharpness and contrast even at their largest apertures. In this way, NIKKOR�s ED-series lenses exemplify Nikon�s preeminence in lens innovation and performance. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#ed">source</a>)</p>


            <h2><a name="sic"></a>SIC (Super Integrated Coating)</h2>
            <p><b>Nikon Super Integrated Coating ensures exceptional performance.</b> To enhance the performance of its optical lens elements, Nikon employs an exclusive multilayer lens coating that helps reduce ghost and flare to a negligible level. Nikon Super Integrated Coating achieves a number of objectives, including minimized reflection in the wider wavelength range and superior color balance and reproduction. Nikon Super Integrated Coating is especially effective for lenses with a large number of elements, like our Zoom-NIKKOR lenses. Also, Nikon's multilayer coating process is tailored to the design of each particular lens. The number of coatings applied to each lens element is carefully calculated to match the lens type and glass used, and also to assure the uniform color balance that characterizes NIKKOR lenses. This results in lenses that meet much higher standards than the rest of the industry. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#sic">source</a>)</p>

            <h2><a name="ncc"></a>N (Nano Crystal Coat)</h2>
            <p>Nano Crystal Coat is an antireflective coating that originated in the development of NSR-series (Nikon Step and Repeat) semiconductor manufacturing devices. It virtually eliminates internal lens element reflections across a wide range of wavelengths, and is particularly effective in reducing ghost and flare peculiar to ultra-wideangle lenses. Nano Crystal Coat employs multiple layers of Nikon�s outstanding extra-low refractive index coating, which features ultra-fine crystallized particles of nano size (one nanometer equals one millionth of a mm). Nikon now proudly marks a world first by applying this coating technology to a wide range of lenses for use in consumer optical products. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#n">source</a>)</p>

            <h2><a name="asp"></a>ASP (Aspherical lens elements)</h2>
            <p>Nikon introduced the first photographic lens with aspherical lens elements in 1968. What sets them apart? Aspherical lenses virtually eliminate the problem of coma and other types of lens aberration - even when used at the widest aperture. They are particularly useful in correcting the distortion in wideangle lenses. In addition, use of aspherical lenses contributes to a lighter and smaller lens design. Nikon employs three types of aspherical lens elements. Precision-ground aspherical lens elements are the finest expression of lens-crafting art, demanding extremely rigorous production standards. Hybrid lenses are made of a special plastic molded onto optical glass. Molded glass aspherical lenses are manufactured by molding a unique type of optical glass using a special metal die technique. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#asp">source</a>)</p>

            <h2><a name="crc"></a>CRC (Close-Range Correction system)</h2>
            <p>The Close-Range Correction (CRC) system is one of Nikon�s most important focusing innovations, for it provides superior picture quality at close focusing distances and increases the focusing range. With CRC, the lens elements are configured in a "floating element" design wherein each lens group moves independently to achieve focusing. This ensures superior lens performance even when shooting at close distances. The CRC system is used in fisheye, wideangle, Micro, and selected medium telephoto NIKKOR lenses. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#crc">source</a>)</p>

            <h2><a name="if"></a>IF (Internal Focusing)</h2>
            <p>Imagine being able to focus a lens without it changing in size. Nikon�s IF technology enables just that. All internal optical movement is limited to the interior of the nonextending lens barrel. This allows for a more compact, lightweight construction as well as a closer focusing distance. In addition, a smaller and lighter focusing lens group is employed to ensure faster focusing. The IF system is featured in most NIKKOR telephoto and selected NIKKOR zoom lenses. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#if">source</a>)</p>

            <h2><a name="rf"></a>RF (Rear Focusing)</h2>
            <p>With Nikon�s Rear Focusing (RF) system, all the lens elements are divided into specific lens groups, with only the rear lens group moving for focusing. This makes autofocusing operation smoother and faster. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#rf">source</a>)</p>

            <h2><a name="dc"></a>DC (Defocus-image Control)</h2>
            <p><b>AF DC-NIKKOR lenses � unique NIKKOR lenses for unique portraits.</b> AF DC-NIKKOR lenses feature exclusive Nikon Defocus-image Control technology. This allows photographers to control the degree of spherical aberration in the foreground or background by rotating the lens� DC ring. This will create a rounded out-of-focus blur that is ideal for portrait photography. No other lenses in the world offer this special technique. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#dc">source</a>)</p>

            <h2><a name="d"></a>D (Distance information)</h2>
            <p>D-type and G-type NIKKOR lenses relay subject-to-camera distance information to AF Nikon camera bodies. This then makes possible advances like 3D Matrix Metering and 3D Multi-Sensor Balanced Fill-Flash. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#d">source</a>)</p>

            <h2><a name="d"></a>G (G-type Nikkor)</h2>
            <p>The G-type NIKKOR has no aperture ring; aperture should be selected from camera body. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#g">source</a>)</p>

            <h2><a name="swm"></a>SWM (Silent Wave Motor)</h2>
            <p>Nikon's AF-S technology is yet another reason professional photographers like NIKKOR telephoto lenses. AF-S NIKKOR lenses feature Nikon�s SWM which converts �traveling waves� into rotational energy to focus the optics. This enables high-speed autofocusing that�s extremely accurate and super quiet. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#swm">source</a>)</p>

            <h2><a name="ma"></a>M/A mode</h2>
            <p>AF-S NIKKOR lenses feature Nikon�s exclusive M/A mode, that allows switching from autofocus to manual operation with virtually no time lag - even during AF servo operation and regardless of AF mode in use. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#ma">source</a>)</p>

            <h2><a name="vr"></a>VR (Vibration Reduction)</h2>
            <p>This innovative VR system minimizes image blur caused by camera shake, and offers the equivalent of shooting at a shutter speed three stops (eight times) faster (As determined by Nikon performance tests). It allows handheld shooting at dusk, at night, and even in poorly lit interiors. The lens� VR system also detects automatically when the photographer pans � no special mode is required. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#vr">source</a>)</p>

            <h2><a name="dx"></a>DX</h2>
            <p>Compact and lightweight DX NIKKOR lenses featuring a smaller image circle are specially designed and optimized for Nikon D2-series, D1-series, D100 and D70s/D70 digital SLR cameras. These are ideal options for landscape photographers and others who need to shoot expansive scenes with Nikon DX-Format digital SLRs. (<a href="http://imaging.nikon.com/products/imaging/lineup/lens/glossary2.htm#dx">source</a>)</p>

            <h2><a name="pc"></a>PC (Perspective Control)</h2>
            <p>PC-E NIKKOR lenses, with their Perspective Control capability, are equipped with a tilt/shift mechanism that enables photographers to emphasise or correct far and near perspective, control depth of field, resolve distortions caused by the camera angle, and achieve the desired focus throughout the entire subject plane, even when it is not parallel to the camera&rsquo;s focal plane. Furthermore, these lenses incorporate numerous state-of-the-art Nikon optical features and technologies that ensure superior picture quality even at the maximum tilt and/or shift settings. The electromagnetic diaphragm employed in these lenses enables auto aperture control when used with compatible camera models. In addition to applications with subject matter that requires perspective control, such as architecture, landscapes and commercial products, these lenses are also ideal for various other situations, such as close-up photography. (<a href="http://www.nikonusa.com/Find-Your-Nikon/Camera-Lenses/Manual/Perspective-Control.page">source</a>)</p>

            <br>
            <br> 
        </td>
       
    </tr>
</table>	
