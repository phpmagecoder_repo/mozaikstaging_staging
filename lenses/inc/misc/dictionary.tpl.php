<table width="<?php echo $page_width; ?>" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left" valign="top"><h1>Lens Dictionary</h1>
            <p>Following table displays lens technologies used and comparison between  similar features and markings among different manufacturers. Detailed  descriptions of technologies can be found on separate pages for each  lens manufacturer.</p>
            <table width="100%" border="0" cellpadding="4" cellspacing="0" class="normal">
                <tr class="col_gray3">
                    <td class="line_b2">&nbsp;</td>
                    <td class="line_b2"><strong><a href="index.php?nav0=info&nav1=canon">Canon</a></strong></td>
                    <td class="line_b2"><strong><a href="index.php?nav0=info&nav1=nikon">Nikon</a></strong></td>
                    <td class="line_b2"><strong><a href="index.php?nav0=info&nav1=sigma">Sigma</a></strong></td>
                    <td class="line_b2"><strong><a href="index.php?nav0=info&nav1=tamron">Tamron</a></strong></td>				
                    <td class="line_b2"><strong>Olympus</strong></td>
                    <td class="line_b2"><strong>Pentax</strong></td>
                    <td class="line_b2"><strong>Sony</strong></td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>image <br>
                            stabilization </strong></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=canon#is">IS</a> <br>
                        (Image Stabilization) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=nikon#vr">VR</a> <br>
                        (Vibration Reduction) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=sigma#os">OS</a> <br>
                        (Optical Stabilizer) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=tamron#vc">VC</a> <br>
                        (Vibration Compensation) </td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>focusing <br>
                            motor </strong></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=canon#usm">USM</a> <br>
                        (Ultrasonic Motor) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=nikon#swm">SWM</a> <br>
                        (Silent Wave Motor)</td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=sigma#hsm">HSM</a> <br>
                        (Hyper-Sonic Motor) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=tamron#usd">USD</a> <br>
                        (Ultrasonic Silent Drive)</td>
                    <td valign="top" class="line_b2">SWD <br>
                        (Supersonic Wave Drive) </td>
                    <td valign="top" class="line_b2">SDM <br>
                        (Supersonic Drive Motor)</td>
                    <td valign="top" class="line_b2">SSM 	(SuperSonic Motor<br>
                        SAM 	(Smooth Autofocus Motor)</td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>focusing<br>
                            technology</strong></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=canon#fs">Floating System </a><br>
                        <a href="index.php?nav0=info&nav1=canon#if">IF &amp; RF</a> (Inner &amp; Rear Focusing) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=nikon#if">IF</a> (Internal Focusing)<br>
                        <a href="index.php?nav0=info&nav1=nikon#rf">RF</a> (Rear Focusing)</td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=sigma#if">IF</a> (Inner Focus)<br>
                        <a href="index.php?nav0=info&nav1=sigma#rf">RF</a> (Rear Focus)</td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=tamron#if">IF</a> (Internal Focusing)</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>professional <br>
                            series </strong></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=canon#lseries">L</a> (Luxury) </td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=sigma#ex">EX</a> Lens </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=tamron#sp">SP</a> (Super Performance) </td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">DA*</td>
                    <td valign="top" class="line_b2">G (high quality)<br> 
                        ZA (Zeiss-designed) </td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>full frame / <br>
                            crop (APS-C) </strong></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=canon#ef">EF</a> (full frame) <br>
                        <a href="index.php?nav0=info&nav1=canon#efs">EF-S</a> (crop only) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=nikon#dx">DX</a> (crop only) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=sigma#dg">DG</a> (full frame)<br>
                        <a href="index.php?nav0=info&nav1=sigma#dc">DC</a> (crop only) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=tamron#di">Di</a> (full frame)<br>
                        <a href="index.php?nav0=info&nav1=tamron#diii">Di-II</a> (crop only) </td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">DT (crop only) </td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>glass technology </strong></td>
                    <td valign="top" class="line_b2">
                        <a href="index.php?nav0=info&nav1=canon#aspherical">Aspherical Lenses</a><br>
                        <a href="index.php?nav0=info&nav1=canon#ud">Fluorite and UD Lenses</a><br>
                        <a href="index.php?nav0=info&nav1=canon#do">DO</a> (Diffractive Optics)<br></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=nikon#asp">ASP</a> (Aspherical lens elements)<br>
                        <a href="index.php?nav0=info&nav1=nikon#ed">ED glass</a> (Extra-low Dispersion)</td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=sigma#asp">ASP</a> (Aspherical Lens)<br>
                        <a href="index.php?nav0=info&nav1=sigma#apo">APO</a> (APO Lens)</td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=tamron#ld">LD</a> (Low Dispersion)<br>
                        <a href="index.php?nav0=info&nav1=tamron#xr">XR</a> (Extra Refractive Index glass)</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">APO (Apochromatic correction)<br>
                        HS-APO (APO with higher-speed)<br></td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>glass coatings</strong></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=canon#ssc">Super Spectra Coating</a></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=nikon#sic">SIC</a> (Super Integrated Coating)<br>
                        <a href="index.php?nav0=info&nav1=nikon#ncc">N</a> (Nano Crystal Coat)</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">T* (Carl Zeiss coating)</td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>tilt / shift </strong></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=canon#ftm">TS</a> (tilt shift) </td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=nikon#pc">PC</a> (Perspective Control) </td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>manual focus </strong></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=canon#ftm">Full-Time Manual Focus</a></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=nikon#ma">M/A</a> mode</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" class="line_b2"><strong>other<br>
                            technologies</strong> </td>
                    <td valign="top" class="line_b2"><br></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=nikon#crc">CRC</a> (Close-Range Correction system) <br>
                        <a href="index.php?nav0=info&nav1=nikon#dc">DC</a> (Defocus-image Control) <br>
                        <a href="index.php?nav0=info&nav1=nikon#d">D</a> (Distance information)<br>
                        <a href="index.php?nav0=info&nav1=nikon#g">G</a> (G-type)<br></td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=sigma#conv">Conv</a> (APO Teleconverter EX)</td>
                    <td valign="top" class="line_b2"><a href="index.php?nav0=info&nav1=tamron#zl">ZL</a> (Zoom Lock) </td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">&nbsp;</td>
                    <td valign="top" class="line_b2">STF (Smooth Transmission Focus)<br>
                        (D) 
                        Distance encoder<br>
                        (xi) 
                        Motorized zoom</td>
                </tr>
            </table>
            <!--
                          <p>&nbsp;</p>
                          <table width="100%" border="1" cellpadding="2" cellspacing="2" class="normal">
                            <tr>
                              <td>&nbsp;</td>
                              <td><strong>image stabilisation </strong></td>
                              <td><strong>focusing motor </strong></td>
                              <td><strong>professional series </strong></td>
                              <td><strong>full frame / crop (APS-C) </strong></td>
                            </tr>
                            <tr>
                              <td><strong>Canon</strong></td>
                              <td>IS (Image Stabilisation) </td>
                              <td>USM (Ultrasonic Motor) </td>
                              <td>L (Luxury) </td>
                              <td>EF (full frame) <br>
            EF-S (crop only) </td>
                            </tr>
                            <tr>
                              <td><strong>Nikon</strong></td>
                              <td>VR (Vibration Reduction) </td>
                              <td>SWM (Silent Wave Motor)</td>
                              <td>&nbsp;</td>
                              <td>DX (crop only) </td>
                            </tr>
                            <tr>
                              <td><strong>Sigma</strong></td>
                              <td>OS (Optical Stabilizer) </td>
                              <td>HSM ( Hyper-Sonic Motor) </td>
                              <td>EX</td>
                              <td>DG (full frame)<br>
            DC (crop only) </td>
                            </tr>
                            <tr>
                              <td><strong>Tamron</strong></td>
                              <td>VC (Vibration Compensation) </td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>Di (full frame)<br>
            Di-II (crop only) </td>
                            </tr>
                            <tr>
                              <td><strong>Olympus</strong></td>
                              <td>&nbsp;</td>
                              <td>SWD (Supersonic Wave Drive) </td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td><strong>Pentax</strong></td>
                              <td>&nbsp;</td>
                              <td>SDM (Supersonic Drive Motor)</td>
                              <td>DA*</td>
                              <td>&nbsp;</td>
                            </tr>
                          </table> -->
            <br>
            <br> 
        </td>
    </tr>
</table>	
