<?php

$action_change_successfull = false;

$display_login_module = false;


// fetch post vars

if(isset($_POST['action']))  { $action = $_POST['action']; }

if ($action == 'action_cancel') { $action_cancel = true; }
if ($action == 'action_reset') { $action_reset = true; }

$forgot_username = $_POST['forgot_username'];
$forgot_email = $_POST['forgot_email'];


// check verification
	require_once('lib/php-captcha.inc.php');

	if ( PhpCaptcha::Validate($_POST['captcha']) ) {
		$captcha_ok = true;
	} else {
		$captcha_ok = false;
	}

// redirect to home
if($action_cancel)
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=home");
    	exit;
}


// button new passwor pressed
if($action_reset)
{
	if($captcha_ok)
	{
		// entered username or email
		if( strlen($forgot_username) || strlen($forgot_email) )
		{
			if(strlen($forgot_username)) { 	$sql = "SELECT id, name, email FROM user WHERE username = '$forgot_username'"; $type = 'username';  $forgot = $forgot_username; }
			if(strlen($forgot_email)) { 	$sql = "SELECT id, name, email FROM user WHERE email = '$forgot_email'"; $type = 'email'; $forgot = $forgot_email; }
			
			$result = mysql_query($sql);
			$num = mysql_num_rows($result);
			
			
			// only one user exist in DB
			if($num == '1')
			{
				//$row = mysql_fetch_array($result, $con);
				$row = mysql_fetch_array($result);
				$forgot_user_id = $row['id'];
				$forgot_user_name = $row['name'];
				$forgot_user_email = $row['email'];
		
				$new_pass = generatePassword($length=6, $strength=4);
				$new_md5_pass = md5($new_pass);
				
				// change password in database
				$sql = "UPDATE user SET
							password = '$new_md5_pass'
						WHERE id='$forgot_user_id'";	
	
				$result = mysql_query($sql);	
				
				$log = write_log('1', 'reset pswd ('.$type.' '.$forgot.')', $forgot_user_id, $con);
				
				// send email with new password
				if($send_email)
				{
					$msg = "<html>
							<body>
							Hey ".$forgot_user_name.", <br><br>"
						   ."You recently requested a new password for <a href='http://www.lensdatabase.net'>www.lensdatabase.com</a><br>"
						   ."Your password was reset and your new password is: ".$new_pass."<br>"
						   ."If you did not request new password, please note that password was changed and you must login using new password.<br>"
						   ."For security reasons please change your password immediatly!<br><br>"
						   ."Please contact <a href='mailto:info@lensdatabase.net'>info@lensdatabase.net</a> with any questions.<br><br>"
						   ."Thanks,<br>"
						   ."The LensDatabase Team<br>";
					   
							mail($forgot_user_email, "LensDatabase Password reset", $msg,
									"From: ".$email_from."\n"
									."Reply-To: ".$email_replyto."\n"
									."Bcc: matej@estrela.si\n"	
									."MIME-Version: 1.0\n"	
									."Content-type: text/html; charset=iso-8859-1\n"
									."X-Mailer: PHP/" . phpversion());
				} else
				{
					// $GLOBALS['error_msg'] = "Your new password is:".$new_pass;
				
				}
				$action_change_successfull = true;
			} else
			{
				$GLOBALS['error_msg'] = "User with this ".$type." does not exist!";
				$log = write_log('1', 'pswd reset err: no '.$type.' '.$forgot.' exist', $forgot_user_id, $con);
			}
			
		} else
		{
			$GLOBALS['error_msg'] = "To reset your password please enter your username or email!!!";	
		
		}
	} else
	{
		// captcha not OK
		$GLOBALS['error_msg'] = "Please type correct verification data!!!";	
	}
}


?>
