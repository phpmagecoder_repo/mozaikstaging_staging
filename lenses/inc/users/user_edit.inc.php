<?php

// don't log this page
$log_stats = false;

// if user is not admin
if($current_user_id != '1')
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=home");
    	exit;
}

$action_save = '';
$action_cancel = '';


if(isset($_POST['action']))  { $action = $_POST['action']; }


if ($action == 'action_cancel') { $action_cancel = true; }
if ($action == 'action_save') { $action_save = true; }


if(isset($_POST['name'])) $name = addslashes($_POST['name']);
if(isset($_POST['username'])) $username = addslashes($_POST['username']);
if(isset($_POST['email'])) $email = addslashes($_POST['email']);

if(isset($_POST['confirmed'])) $confirmed = $_POST['confirmed'];
if(isset($_POST['status_id'])) $status_id = $_POST['status_id'];
if(isset($_POST['user_group_id'])) $user_group_id = $_POST['user_group_id'];


// ce je pritisnjen cancel gumb
if ( $action_cancel )
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=admin&nav1=users");
    	exit;
}



// ce je pritisnjen shrani gumb
if ( $action_save )
{

	if ( !empty( $object_id ) )
    {
        $sql = "UPDATE user SET
					name = '$name',
					username = '$username',
					email = '$email',
					status_id = '$status_id',
					confirmed = '$confirmed'
                WHERE id='$object_id'";
				
		// $log = write_log('3', 'edit', $object_id, $con);
    }
    else
    {
        $next_id = getNextID('user', $con);
		$object_id = $next_id; 
		
        $sql = "INSERT INTO user (
                    id,
					name,
                    username,
					email,
					status_id,
					confirmed
					 )
                VALUES (
                    '$next_id',
					'$name',
                    '$username',
					'$email',
					'$status_id',
					'$confirmed'
					 )";

		// $log = write_log('3', 'insert', $next_id, $con);
    }
    $result = mysql_query($sql);
		
	// ================ multiple selections
	
	// delete data user groups
	$sql = "DELETE FROM  user_user_group WHERE user_id ='$object_id'";
	$result = mysql_query($sql);
	
	// insert new data
	if ( is_array( $user_group_id ) && count( $user_group_id ) )
	{
		foreach ( $user_group_id as $group_id )
		{
			$sql = "INSERT INTO user_user_group (
						user_group_id,
						user_id )
					VALUES (
						'$group_id',
						'$object_id')";				
			$result = mysql_query($sql);
		}
	}

	

}	
	



// redirect on SAVE	
if($action_save)
{
	header("Location: http://".$_SERVER['SERVER_NAME']
				.dirname2($_SERVER['PHP_SELF'])
				."/index.php?nav0=admin&nav1=users");
	exit; 
}

	

//==================== GET DATA / DEFAULT
if ( isset( $object_id ) )
{
	// urejanje objekta
	$sql = "SELECT * FROM user  WHERE id='$object_id'";
    $result = mysql_query($sql);

    //$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);

	$status_id = $row['status_id'];
	$name = htmlspecialchars(stripslashes($row['name']));
	$username = htmlspecialchars(stripslashes($row['username']));
	$email = htmlspecialchars(stripslashes($row['email']));
	
	$confirmed = $row['confirmed'];
	
	
	// get selected user groups
	$sql = "SELECT user_group_id
			FROM user_user_group
			WHERE user_id = '$object_id'";


	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	$selected_groups = array();
	
	for ($i=1; $i <= $num; $i++ )
	{	
		//$row = mysql_fetch_array($result, $con);
		$row = mysql_fetch_array($result);
		$selected_groups['id'][$i] = $row['user_group_id'];
	}	

}
else
{
	$status_id = '2';
	$name = '';
	$username = '';
	$email = '';
}

// ================================== drop down menus


// get status
$sql = "SELECT id, name
		FROM status
		ORDER BY name";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$status = array();
for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$status['id'][$i] = $row['id'];
	$status['name'][$i] = $row['name'];	
}


// get user groups
$sql = "SELECT id, name
		FROM user_group
		ORDER BY id";
		
$result = mysql_query($sql);
$num = mysql_num_rows($result);
$user_group = array();

for ($i=1; $i <= $num; $i++ )
{	
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$user_group['id'][$i] = $row['id'];
	$user_group['name'][$i] = $row['name'];	
	$user_group['selected'][$i] = false;
	
	for($k=1; $k <= count($selected_groups['id']); $k++)
	{
		if( $selected_groups['id'][$k] == $user_group['id'][$i] )
		{
			$user_group['selected'][$i] = true;
		}
	}
}


?>
