
<form enctype="multipart/form-data"  action="<?php echo $_SERVER['PHP_SELF']?>" method="post" name="seznam" STYLE="padding:0px; spacing:0px;">

	<input type="hidden" name="id" value="<?php echo $object_id ?>">
	<input type="hidden" name="nav0" value="<?php echo $nav0 ?>">
	<input type="hidden" name="nav1" value="<?php echo $nav1 ?>">
	<input type="hidden" name="nav2" value="<?php echo $nav2 ?>">
	<input type="hidden" name="nav3" value="<?php echo $nav3 ?>">
	<input type="hidden" name="action" value="">
	

<table width="<?php echo $page_width; ?>" border="0" align="center" cellpadding="0" cellspacing="0" class="normal">
<tr>
	<td>


<table width="500" border="0" align="center" cellpadding="0" cellspacing="4" class="normal">	
	<tr>
	  <td colspan="2"><h1>Reset password</h1></td>
	  </tr>
<?php	
if( $action_change_successfull )
{ ?>	


	<tr>
	  <td colspan="2"><p><strong>Your password has been reset. </strong><br> 
	      <br>
	      Message with your new temporary password was sent to your email. Please change this password immediatly. <br>
      For security reasons this transaction was logged.</p>
      <p>Return to <a href="index.php">home page</a>.</p></td>
    </tr>
<?php	
} else
{ ?>
	<tr>
	  <td colspan="2"><strong>To reset your password, enter your username or email and click &quot;Send new password&quot; button. 
      New password will be sent to your email. </strong></td>
	  </tr>
	<tr>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  </tr>
	<tr>
	  <td align="right">username:</td>
	  <td><input style="width: 180px" name="forgot_username" alt="length|5|bok" emsg="Username must be at least 5 characters long!" type="text" class="inputfield" value=""></td>
	</tr>
	<tr>
	  <td align="right">email:</td>
	  <td><input style="width: 180px" name="forgot_email" alt="length|7|bok" emsg="Email must be at least 7 characters long!" type="text" class="inputfield" value=""></td>
    </tr>
	
    <tr>
      <td align="right" valign="middle">enter verification:</td>
      <td valign="middle"><input name="captcha" type="text" size="8" maxlength="5" value="" alt="length|4" emsg="Enter verification!">
<img src="captcha.php" width="110" height="30" alt="CAPTCHA" /></td>
    </tr>
	<tr>
	  <td>&nbsp;</td>
	  <td><input class="gumb" type="button" style="width:190px" name="action_save" onClick="submitAction(this.form, 'action_reset', 1)" value="Send new password"><input class="gumb" type="button" style="width:80px" name="new" onClick="submitAction(this.form, 'action_cancel', 0)" value="Cancel"></td>
    </tr>
	<tr>
	  <td colspan="2">&nbsp;</td>
	  </tr>
	<tr>
      <td colspan="2"><em>Note: For security reasons this action is being logged. <br>
        Your current IP address is: <?php echo $user_ip; ?></em></td>
	  </tr>


<?php	} ?>
</table>	


	</td>
	</tr>
</table>	

</form>
