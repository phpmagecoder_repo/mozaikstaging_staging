<?php


// fetch post vars

// new users disabled - redirect to home
if ( ! $allow_new_users_register )
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=home");
    	exit;
}

$action_submit_successfull = false;

$display_login_module = false;


if(isset($_POST['action']))  { $action = $_POST['action']; }

if ($action == 'action_cancel') { $action_cancel = true; }
if ($action == 'action_register') { $action_register = true; }

$username = $_POST['new_username'];
$name = $_POST['new_name'];
$email = $_POST['new_email'];
$pass1 = $_POST['new_pass1'];
$pass2 = $_POST['new_pass2'];


// check verification
	require_once('lib/php-captcha.inc.php');

	if ( PhpCaptcha::Validate($_POST['captcha']) ) {
		$captcha_ok = true; 
	} else {
		$captcha_ok = false;
	}

// redirect to home
if($action_cancel)
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=home");
    	exit;
}


// button new passwor pressed
if($action_register)
{
	// set success variable to true
	$action_submit_successfull = true;
	
	if( strlen($username) < 5 ) { $action_submit_successfull = false; }
	if( strlen($name) < 5 ) { $action_submit_successfull = false; }
	if( strlen($email) < 5 ) { $action_submit_successfull = false; }
	if( strlen($pass1) < 5 ) { $action_submit_successfull = false; }
	if( strlen($pass2) < 5 ) { $action_submit_successfull = false; }
	if( $pass1 != $pass2) { $action_submit_successfull = false; } else { $md5pass = md5($pass1); }
	
	
	if(! $action_submit_successfull)
	{
		$GLOBALS['error_msg'] = "Please fill all form data!!!";	
	}
	
	if(! $captcha_ok)
	{
		$action_submit_successfull = false;
		$GLOBALS['error_msg'] = "Please type correct verification data!!!";	
	}
	
	if($action_submit_successfull)
	{
		$sql = "SELECT id FROM user WHERE username = '$username'";
		$result = mysql_query($sql);
		$num = mysql_num_rows($result);
		if($num)
		{
			// user already exist
			$action_submit_successfull = false;
			$GLOBALS['error_msg'] = "Username <b>".$username."</b> is not available!";	
			$log = write_log('1', 'register username exist: '.$username, 0, $con);
		} else
		{
			// user with this username does not exist
			
			// check email
			$sql = "SELECT id FROM user WHERE email = '$email'";
			$result = mysql_query($sql);
			$num = mysql_num_rows($result);
			if($num)
			{
				// user with this email exist
				$log = write_log('1', 'register email exist: '.$email, 0, $con);
				$action_submit_successfull = false;
				$GLOBALS['error_msg'] = "User with email <b>".$email."</b> already exist in our system!<br> Please use <a href='index.php?nav0=user&nav1=newpass'>forgot password</a> form.";	
				
			} else
			{
				// Registration OK ... proceed
				
				$next_id = getNextID('user', $con);

        		$sql = "INSERT INTO user (
                    		id,
							status_id,
							name,
							username,
							password,
							email,
							reg_ip,
							reg_time
					 		)
                		VALUES (
							'$next_id',
							'2',
							'$name',
							'$username',
							'$md5pass',
							'$email',
							'$user_ip',
							'$db_current_datetime'
					 		)";
				$result = mysql_query($sql);
				$log = write_log('1', 'register success: '.$username, $next_id, $con);
				$activate_string = md5($next_id.$system_pass);
				
				
				if($send_email)
				{
						$msg = "<html>
								<body>
								Hey ".$name.", <br><br>"
							   ."you have registred to LensDatabase using this email address. To complete your registration, follow the link bellow:<br>"
							   ."<a href='http://www.lensdatabase.net/index.php?nav0=act&id=".$activate_string."'>http://www.lensdatabase.net/index.php?nav0=act&id=".$activate_string."</a> <br>"
							   ."(If clicking on the link doesn't work, try copying and pasting it into your browser.) <br><br>"
							   ."If you did not register for LensDatabase, please disregard this message.<br>"
							   ."Please contact <a href='mailto:info@lensdatabase.net'>info@lensdatabase.net</a> with any questions.<br><br>"
							   ."Thanks,<br>"
							   ."The LensDatabase Team<br>";
						   
								mail($email, "Account activation for LensDatabase", $msg,
										"From: ".$email_from."\n"
										."Reply-To: ".$email_replyto."\n"
										."Bcc: matej@estrela.si\n"	
										."MIME-Version: 1.0\n"	
										."Content-type: text/html; charset=iso-8859-1\n"
										."X-Mailer: PHP/" . phpversion());
				
				}
			}
		
		
		}
	}
}

?>
