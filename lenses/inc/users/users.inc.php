<?php


// if user is not admin
if($current_user_id != '1')
{
    	header("Location: http://".$_SERVER['SERVER_NAME']
                      .dirname2($_SERVER['PHP_SELF'])
                      ."/index.php?nav0=home");
    	exit;
}

if(isset($_POST['action']))  { $action = $_POST['action']; }


// =====================================================================
// =========== get users data ===========================================
// =====================================================================


// count all users
$sql = "SELECT id FROM user";
$result = mysql_query($sql, $con);
$num_all_rows = mysql_num_rows($result);

// get users data
$sql = "SELECT user.id, user.name, user.username, user.email, status.name status, confirmed, reg_time
		FROM user, status
		WHERE  user.status_id = status.id 
		ORDER BY user.id";
		
$result = mysql_query($sql, $con);

$sql_limit = " LIMIT $offset,$limit";	

// get real data
	$result2 = mysql_query($sql);
	$numrows = mysql_num_rows($result2);	
	
	$result = mysql_query($sql.$sql_limit, $con);
	$num = mysql_num_rows($result);

$items = array();
for ($i=1; $i <= $num; $i++ )
{
	//$row = mysql_fetch_array($result, $con);
	$row = mysql_fetch_array($result);
	$items['id'][$i] = $row['id'];
	
	$tmp_id = $row['id'];
	
	$items['name'][$i] = $row['name'];
	$items['username'][$i] = $row['username'];
	$items['email'][$i] = $row['email'];
	$items['status'][$i] = $row['status'];
	$items['registred'][$i] = niceDateTime($row['reg_time']);
	if($row['confirmed'])
	{ $items['confirmed'][$i] = "true"; } else { $items['confirmed'][$i] = "false"; }
	
	// get access data
	$sql7 = "SELECT user_group.name 
			FROM user_group, user_user_group
			WHERE user_user_group.user_group_id = user_group.id
			AND user_user_group.user_id = '$tmp_id'";
	$result7 = mysql_query($sql7);
	$num7 = mysql_num_rows($result7);
	
	if($num7)
	{
		$items['access'][$i] = '';
		for($k=1; $k<= $num7; $k++)
		{
			//$row7 = mysql_fetch_array($result7, $con);
			$row7 = mysql_fetch_array($result7);
			$items['access'][$i] .= $row7['name'];
			if($k < $num7) { $items['access'][$i] .= ", "; }
		}
	
	} else
	{
		$items['access'][$i] = "n/d";
	}
	
}	


// ===================================================================================
// ======================================================= navigation
// ===================================================================================


$tpl_navigation_tmp = array();
$nav_limit = 10;
$k=1;
$tpl_navigation = "";

if ( $numrows > $limit ) 
	
{	
	// Calculate number of pages
	$pages = intval($numrows/$limit);
	if ($numrows%$limit)
	{
		$pages++;
	}
	
		
	// Display page links
	for ( $i=1; $i<=$pages; $i++ )
	{
				// Check if on current page
				if (($offset/$limit) == ($i-1)) {
				// $i is equal to current page, so don't display a link
				$tpl_navigation_tmp['item'][$k] = "&nbsp;<span class=\"notlink\">$i</span>&nbsp;";
				$tpl_navigation_tmp['sel'][$k] = TRUE;
				$tpl_navigation_sel=$k;
				$k++;
				}
				else
				{
				// $i is NOT the current page, so display a link to page $i
				$newoffset=$limit*($i-1);
				$tpl_navigation_tmp['item'][$k] = "&nbsp;<a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=".$newoffset."&sort=".$sort."\"><b>$i</b></a>\n";
				$tpl_navigation_tmp['sel'][$k] = FALSE;
				$k++;
				}
				
								
				if($i == $pages)
				{
					$loffset = $newoffset;
				}
			
	}

	if ( !((($offset/$limit)+1) == $pages) && $pages != 1 )
		{	$tpl_navigation .= "<td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=0&sort=".$sort."\"><img src='img/icon_pg_f1.gif' width='13' height='13' border='0'></a></td>"; }	
	else
		{ $tpl_navigation = "<td><img src='img/icon_pg_p0.gif' width='13' height='13' border='0'></td><td>"; }
		
	
	// Don't display PREV link if on first page
	if ( $offset != 0 )
	{
		$prevoffset = $offset-$limit;
		$tpl_navigation = "<td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=0&sort=".$sort."\"><img src='img/icon_pg_f1.gif' width='13' height='13' border='0'></a></td>";
		$tpl_navigation .= "<td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=".$prevoffset."&sort=".$sort."\"><img src='img/icon_pg_p1.gif' width='13' height='13' border='0'></a></td><td>";
	}
	else
	{
		$tpl_navigation = "<td><img src='img/icon_pg_p0.gif' width='13' height='13' border='0'></td>";
		$tpl_navigation .= "<td><img src='img/icon_pg_f0.gif' width='13' height='13' border='0'></td><td>";
	}

	
	$pre_dots = true;
	$post_dots = true;	
	
	
	for ($j=1; $j<=count($tpl_navigation_tmp['item']); $j++)
		{
			$diff = $j - $tpl_navigation_sel;
			
			if($diff < ($nav_limit*-1))
			{
				if($pre_dots) { $tpl_navigation .= "<span class='n_sort'>...</span>"; }
				$pre_dots = false; 
				
			} elseif(($diff > $nav_limit) )
			{
				if($post_dots) { $tpl_navigation .= "<span class='n_sort'>...</span>"; }
				$post_dots = false;
			} else
			{
				$tpl_navigation .= $tpl_navigation_tmp['item'][$j];
				
			}
		
		}

	// Check to see if current page is last page
	if ( !((($offset/$limit)+1) == $pages) && $pages != 1 )
	{
		$newoffset=$offset+$limit;
		$tpl_navigation .= "</td><td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=".$newoffset."&sort=".$sort."\"><img src='img/icon_pg_n1.gif' width='13' height='13' border='0'></a></td>";	
		$tpl_navigation .= "<td><a class=\"sublink\" href=\"".$_SERVER['PHP_SELF']."?nav0=".$nav0."&nav1=".$nav1."&nav2=".$nav2."&nav3=".$nav3."&offset=".$loffset."&sort=".$sort."\"><img src='img/icon_pg_l1.gif' width='13' height='13' border='0'></a></td>";
	}
	else
	{
		
		$tpl_navigation .= "</td><td><img src='img/icon_pg_n0.gif' width='13' height='13' border='0'></td>";
		$tpl_navigation .= "<td><img src='img/icon_pg_l0.gif' width='13' height='13' border='0'></td>";
	}
}

$tpl_navigation = "<table border=0><tr>".$tpl_navigation."</tr></table>";


?>
