
    <ul class="mainmenu1">
        <?php
        for ($i = 1; $i <= count($menu_level1['url']); $i++) {
            if ($menu_level1['sel'][$i]) {
                ?>	  
                <li class="active"><a href="<?php echo $menu_level1['url'][$i]; ?>" ><?php echo $menu_level1['title'][$i]; ?></a>
                <?php if ($i < count($menu_level1['url']) && $i>=1) echo "<span> | </span>"; ?></li>
            <?php } else {
                ?>
                <li><a href="<?php echo $menu_level1['url'][$i]; ?>" ><?php echo $menu_level1['title'][$i]; ?></a>
                    <?php if ($i < count($menu_level1['url']) && $i>=1) echo "<span> | </span>"; ?></li>
            <?php } ?>
            
        <?php } //for ?>	
    </ul>
    <ul class="mainmenu2">
        <?php
           if (isset($menu_level2['url'])) {
               for ($i = 1; $i <= count($menu_level2['url']); $i++) {
                   if ($menu_level2['sel'][$i]) {
                       ?>
                       <li class="active"><a href="<?php echo $menu_level2['url'][$i]; ?>" ><?php echo $menu_level2['title'][$i]; ?></a>
                       <?php if ($i < count($menu_level2['url']) && $i>=1) echo "<span> | </span>"; ?></li>
                   <?php } else {
                       ?>
                       <li><a href="<?php echo $menu_level2['url'][$i]; ?>"><?php echo $menu_level2['title'][$i]; ?></a>
                       <?php if ($i < count($menu_level2['url']) && $i>=1) echo "<span> | </span>"; ?></li>
                       <?php
                   }
                  
               }
           }
           ?>   
    </ul>
