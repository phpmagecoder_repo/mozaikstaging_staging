<?php
if (isset($_POST['action'])) {
    $action = $_POST['action'];
}

if ($action == 'action_login') {
    $action_login = true;
}
if ($action == 'action_logout') {
    $action_logout = true;
}

if ($action_login) {
    if ((strlen($_POST['lens_user']) > 4 ) && (strlen($_POST['lens_pswd']) > 4 )) {
        $lens_user = $_POST['lens_user'];
        $lens_pswd = md5($_POST['lens_pswd']);

        $sql = "SELECT id, status_id, confirmed, password, name
				FROM user
				WHERE username = '$lens_user'";


        $result = mysql_query($sql, $con);
        $num = mysql_num_rows($result);
        if ($num == '1') {
            //$row = mysql_fetch_array($result, $con);
            $row = mysql_fetch_array($result);

            $user_id = $row['id'];
            $status_id = $row['status_id'];
            $confirmed = $row['confirmed'];
            $password = $row['password'];
            $name = $row['name'];



            // is account confirmed ?
            if ($confirmed == '1') {
                // is acount active? - status_id = 2
                if ($status_id == '2') {
                    if ($password == $lens_pswd) {
                        $_SESSION['user_id'] = $user_id;
                        $current_user_name = $name;
                        // set cookie for users to stay loged in
                        // cookie data = md5 ( user_id + system password)

                        setcookie($mycookie, md5($user_id . $system_pass), time() + (3600 * 24 * 365 * 3));

                        $log = write_log('2', 'login: OK ' . $lens_user, $user_id, $con);
                    } else {
                        $log = write_log('2', 'login: user ' . $lens_user . ' psswd error', $user_id, $con);
                        $GLOBALS['error_msg'] = "Password error!";
                        $_SESSION['user_id'] = '0';
                    }
                } else {
                    $log = write_log('2', 'login: user ' . $lens_user . ' disabled', $user_id, $con);
                    $GLOBALS['error_msg'] = "Account for user <b>" . $lens_user . "</b> is disabled!";
                    $_SESSION['user_id'] = '0';
                }
            } else {
                $log = write_log('2', 'login: not activated ' . $lens_user, $user_id, $con);
                $GLOBALS['error_msg'] = "Account for user <b>" . $lens_user . "</b> has not been activated yet. <br>Please check your email to activate your account!";
                $_SESSION['user_id'] = '0';
            }
        } else {
            $log = write_log('2', 'login: no user ' . $lens_user . ' in DB', 0, $con);
            $GLOBALS['error_msg'] = "User <b>" . $lens_user . "</b> does not exist!";
            $_SESSION['user_id'] = '0';
        }
    } else {
        $GLOBALS['error_msg'] = "Please enter your username and password!";
        $_SESSION['user_id'] = '0';
    }
}

// logout action
if ($action_logout) {
    // write logout to log
    $log = write_log('2', 'logout', $_SESSION['user_id'], $con);

    $_SESSION['user_id'] = 0;
    $current_user_id = $_SESSION['user_id'];

    session_unset();
    setcookie($mycookie, $current_user_id, time() - (3600 * 10));
}

$current_user_id = $_SESSION['user_id'];

require_once("menu.inc.php");

?>
