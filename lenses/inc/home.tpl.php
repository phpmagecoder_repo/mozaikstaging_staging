<h1>Lens Database</h1>

<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" name="list" STYLE="padding:0px; spacing:0px;">
    <input type="hidden" name="nav0" value="<?php echo $nav0 ?>">
    <input type="hidden" name="nav1" value="<?php echo $nav1 ?>">
    <input type="hidden" name="nav2" value="<?php echo $nav2 ?>">
    <input type="hidden" name="nav3" value="<?php echo $nav3 ?>">
    <input type="hidden" name="action" value="">
    <input type="hidden" name="value" value="">

    <table width="<?php echo $page_width; ?>" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <table width="100%" border="0" cellpadding="3" cellspacing="0" class="normal">
                    <tr >
                        <td align="right" nowrap class="table_header" width="160"><input onKeyPress="return submitFormWithEnter(this, event, 'action_search')" style="width: 150px" name="search_txt" type="text" class="inputfield<?php if (strlen($search_txt)) {
    echo "_sel";
} ?>" value="<?php echo $search_txt ?>"></td>
                        <td width="220">
                            <input class="gumb" type="button" style="width:100px" name="new" onClick="submitAction(this.form, 'action_search', 0)" value="Search lens">

                            <input class="gumb" type="button" style="width:100px" name="new" onClick="submitAction(this.form, 'action_clear_search', 0)" value="Clear search" <?php if (!strlen($search_txt)) {
    echo "disabled='disabled'";
} ?>> </td>
                        <td>&nbsp;</td>
                        <td width="70" align="right" nowrap class="normal"><strong>sort by:</strong></td> 
                        <td width="140"><select style="width:140px" onChange="submitAction(this.form, 'action_find', 0)" class="dropmenu" name="lens_sort_id">
                                <?php for ($i = 1; $i <= count($sort['id']); $i++) {
                                    ?>
                                    <option <?php if ($sort['id'][$i] == $_SESSION['lens_sort_id']) echo "selected"; ?> value="<?php echo $sort['id'][$i]; ?>"> <?php echo $sort['name'][$i] ?></option>
<?php } ?>
                            </select></td>

                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="0" class="normal">
                    <tr >
                        <td width="100">
                            <select style="width:100px" onChange="submitAction(this.form, 'action_find', 0)" class="inputfield<?php if ($_SESSION['lens_maker_id']) {
    echo "_sel";
} ?>" name="lens_maker_id">
<?php for ($i = 1; $i <= count($lens_maker['id']); $i++) {
    ?>
                                    <option style="background-color: #ffffff;" <?php if ($lens_maker['id'][$i] == $_SESSION['lens_maker_id']) echo "selected"; ?> value="<?php echo $lens_maker['id'][$i]; ?>"> <?php echo $lens_maker['name'][$i] ?></option>
                                <?php } ?>
                            </select></td>	

                        <td width="100">
                            <select style="width:100px" onChange="submitAction(this.form, 'action_find', 0)" class="inputfield<?php if ($_SESSION['lens_mount_type_id']) {
                                    echo "_sel";
                                } ?>" name="lens_mount_type_id">
                                <?php for ($i = 1; $i <= count($lens_mount_type['id']); $i++) {
                                    ?>
                                    <option style="background-color: #ffffff;" <?php if ($lens_mount_type['id'][$i] == $_SESSION['lens_mount_type_id']) echo "selected"; ?> value="<?php echo $lens_mount_type['id'][$i]; ?>"> <?php echo $lens_mount_type['name'][$i] ?></option>
                                <?php } ?>
                            </select></td>	
                        <td width="100">
                            <select style="width:100px" onChange="submitAction(this.form, 'action_find', 1)" class="inputfield<?php if ($_SESSION['lens_fitsfor_id']) {
                                    echo "_sel";
                                } ?>" name="lens_fitsfor_id">
                                <?php for ($i = 1; $i <= count($lens_fitsfor['id']); $i++) {
                                    ?>
                                    <option style="background-color: #ffffff;" <?php if ($lens_fitsfor['id'][$i] == $_SESSION['lens_fitsfor_id']) echo "selected"; ?> value="<?php echo $lens_fitsfor['id'][$i]; ?>"> <?php echo $lens_fitsfor['name'][$i] ?></option>
<?php } ?>
                            </select></td>	
                        <td width="100">
                            <select style="width:100px" onChange="submitAction(this.form, 'action_find', 1)" class="inputfield<?php if ($_SESSION['lens_type_id']) {
    echo "_sel";
} ?>" name="lens_type_id">
                                <?php for ($i = 1; $i <= count($lens_type['id']); $i++) {
                                    ?>
                                    <option style="background-color: #ffffff;" <?php if ($lens_type['id'][$i] == $_SESSION['lens_type_id']) echo "selected"; ?> value="<?php echo $lens_type['id'][$i]; ?>"> <?php echo $lens_type['name'][$i] ?></option>
                                <?php } ?>
                            </select></td>		  

                        <td width="100">
                            <select style="width:100px" onChange="submitAction(this.form, 'action_find', 0)" class="inputfield<?php if ($_SESSION['lens_aperture_id']) {
                                    echo "_sel";
                                } ?>" name="lens_aperture_id">
                                <?php for ($i = 1; $i <= count($lens_aperture['id']); $i++) {
                                    ?>
                                    <option style="background-color: #ffffff;" <?php if ($lens_aperture['id'][$i] == $_SESSION['lens_aperture_id']) echo "selected"; ?> value="<?php echo $lens_aperture['id'][$i]; ?>"> <?php echo $lens_aperture['name'][$i] ?></option>
                                <?php } ?>
                            </select></td>	
                        <td width="100">
                            <select style="width:100px" onChange="submitAction(this.form, 'action_find', 0)" class="inputfield<?php if ($_SESSION['zoom_id']) {
                                    echo "_sel";
                                } ?>" name="zoom_id">
                                <?php for ($i = 1; $i <= count($zoom['id']); $i++) {
                                    ?>
                                    <option style="background-color: #ffffff;" <?php if ($zoom['id'][$i] == $_SESSION['zoom_id']) echo "selected"; ?> value="<?php echo $zoom['id'][$i]; ?>"> <?php echo $zoom['name'][$i] ?></option>
<?php } ?>
                            </select></td>
                        <td width="100">
                            <select style="width:100px" onChange="submitAction(this.form, 'action_find', 0)" class="inputfield<?php if ($_SESSION['is_id']) {
    echo "_sel";
} ?>" name="is_id">
                        <?php for ($i = 1; $i <= count($is['id']); $i++) {
                            ?>
                                    <option style="background-color: #ffffff;" <?php if ($is['id'][$i] == $_SESSION['is_id']) echo "selected"; ?> value="<?php echo $is['id'][$i]; ?>"> <?php echo $is['name'][$i] ?></option>
<?php } ?>
                            </select></td>	
                        <td width="100">
                            <select style="width:100px" onChange="submitAction(this.form, 'action_find', 0)" class="inputfield<?php if ($_SESSION['discontinued_id']) {
    echo "_sel";
} ?>" name="discontinued_id">
<?php for ($i = 1; $i <= count($discontinued['id']); $i++) {
    ?>
                                    <option style="background-color: #ffffff;" <?php if ($is['id'][$i] == $_SESSION['discontinued_id']) echo "selected"; ?> value="<?php echo $discontinued['id'][$i]; ?>"> <?php echo $discontinued['name'][$i] ?></option>
<?php } ?>
                            </select></td>	  
                        <td>&nbsp;</td> 


<?php if (user_is_member("C,E,A", $con)) {
    ?><td width="90"><input class="gumb" type="button" style="width:90px" name="new" onClick="submitAction(this.form, 'action_add_lens', 1)" value="Add lens"></td>
<?php } ?>

                    </tr>
                </table>	
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="col_white">
                        <td><img src="img/shim.gif" width="1" height="1" /></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="2" cellspacing="0" class="normal">

                    <tr class="col_orange1">
                        <td colspan="3" class="table_header">lens</td>
                        <td align="center" class="table_header">zoom</td>
                        <td class="table_header">focal</td>
                        <td class="table_header">zoom ratio</td>
                        <td class="table_header">aperture</td>
                        <td align="center" class="table_header">IS</td>
                        <td align="center" class="table_header">focus</td>
                        <td align="center" class="table_header">filter</td>
                        <td align="center" class="table_header">weight</td>
                        <td align="center" class="table_header">announced</td>
                        <td align="center" class="table_header">&nbsp;</td>
<?php if (user_is_member("E,A", $con)) {
    ?>
                            <td align="center" class="table_header">&nbsp;</td>
                        <?php } ?>	
                    </tr>
                        <?php
                        for ($i = 1; $i <= count($items['id']); $i++) {
                            ?>	
                        <tr>
                            <td width="<?php echo $iconx; ?>"><a href="<?php echo $_SERVER['PHP_SELF'] . '?nav0=database&nav1=view&id=' . $items['id'][$i] ?>" class="item"><img src="<?php echo $items['image_filename'][$i]; ?>" alt="<?php echo $items['image_description'][$i]; ?>" width="<?php echo $items['image_sizex'][$i]; ?>" height="<?php echo $items['image_sizey'][$i]; ?>" border="0"></a></td>
                            <td width="1"><img src="img/shim.gif" width="1" height="<?php echo $icony; ?>" border="0" /></td>
                            <td><a href="<?php echo $_SERVER['PHP_SELF'] . '?nav0=database&nav1=view&id=' . $items['id'][$i] ?>" class="item"><?php echo $items['name'][$i] ?></a></td>
                            <td align="center"><?php echo $items['zoom'][$i] ?></td>
                            <td><?php echo $items['focal'][$i] ?></td>
                            <td><?php echo $items['zoom_ratio'][$i] ?></td>
                            <td><?php echo $items['aperture'][$i] ?></td>
                            <td align="center"><?php echo $items['stabilizer'][$i] ?></td>
                            <td align="center"><?php echo $items['lens_focus'][$i] ?></td>
                            <td align="center"><?php echo $items['filter'][$i] ?></td>
                            <td align="center"><?php echo $items['weight'][$i] ?></td>
                            <td align="center"><?php echo $items['announced'][$i] ?></td>
                            <td align="center"><?php echo $items['status'][$i] ?></td>
    <?php if (user_is_member("E,A", $con)) {
        ?>
                                <td align="center" class="table_header"><a href="<?php echo $_SERVER['PHP_SELF'] . '?nav0=database&nav1=edit&id=' . $items['id'][$i] ?>"><img src="img/icon_edit.gif" width="12" height="13" border="0"></a></td>
    <?php } ?>	
                        </tr>
<?php } ?>	
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="normal">
                    <tr class="col_orange1">
                        <td><img src="img/shim.gif" width="1" height="4" /></td>
                    </tr>
                    <tr class="col_white">
                        <td><img src="img/shim.gif" width="1" height="1" /></td>
                    </tr>

                </table>
                <div class="tbl-bottom"><?php echo $tpl_navigation ?></div>
                <div class="tbl-bottom"><?php echo $displayed_lenses ?></b> lenses matches search / total <b><?php echo $all_lenses ?></b> lenses</div>
                <table width="100%" border="0" cellpadding="1" cellspacing="0" class="tbl-container">
                    <tr>
                        <td style="text-align:left;" class="small_info">&copy; All lens photos are copyright of their owners</td>
                        <td style="text-align:right;" class="small_info"><?php echo $db_last_modified; ?></td>
                    </tr>
                </table>
    </table>
</form>

