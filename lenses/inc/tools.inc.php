<?php

require_once('inc/config.inc.php');


//funkcija za izracun prvega prostega ID-ja v tabeli
function getNextID( $table, $con )
{
	$sql = "SELECT MAX(ID) FROM $table";
	$result = mysql_query($sql, $con);
	$next_id = mysql_result( $result, 0, 0 )+1;
	return $next_id;
}


// converts from "1.12.2003" to "2003-12-01" / "" to "0000-00-00"
function dbDate( $date )
{
	if(strlen($date) )
	{
   		$date = explode(".", $date);
   		return sprintf("%04d",$date[2]).'-'.sprintf("%02d",$date[1]).'-'.sprintf("%02d",$date[0]);
	} else
	{
		return "0000-00-00";
	}
}

// convert DateTime from 2005-04-05 08:40:00 to 05.04.2005 08:40:00
function niceDateTimeSec( $datum_old )
{
   $time = substr($datum_old,1+strpos($datum_old,' '));
   $tmp = strtotime($datum_old); 
   $date = date('d.m.Y',$tmp);
   return $date.' '.$time;
}

// convert DateTime from 2005-04-05 08:40:00 to 05.04.2005 08:40 !!! BREZ SEKUND
function niceDateTime( $datum_old )
{
   $datetime = explode(" ", $datum_old);   
   $date = explode("-",$datetime[0]);
   $time = explode(":",$datetime[1]);
   
   return $date[2].".".$date[1].".".$date[0]." ".$time[0].":".$time[1];
}

// convert 2005-04-05 to 05.04.2005 / "0000-00-00" to ""
function niceDate($datum)
{
		 if($datum == '0000-00-00' || ( strlen($datum)<4 ) )  { 	return ""; }
		 else
		 {
			$date = explode("-", $datum);
			return $date[2].'.'.$date[1].'.'.$date[0];
		}
}

// convert 2005-04-05 to 04/2005 / "0000-00-00" to ""
function niceYearMonth($datum)
{
		 if($datum == '0000-00-00' || ( strlen($datum)<4 ) )  { 	return ""; }
		 else
		 {
			$date = explode("-", $datum);
			return $date[1].'/'.$date[0];
		}
}


// returns year from mysql format of date 2009-01-01 vrne 2009
function mysql_year($date)
{
	return(substr($date,0,4));
}

//vpis v dnevnik
function write_log($action_id, $action, $object_id, $con)
{
		$user_id = $_SESSION['user_id'];
		$time = date("Y-m-d H:i:s"); 
		$ip = $_SERVER['REMOTE_ADDR'];
		$next_id = getNextID('user_log', $con);

        $sql = "INSERT INTO user_log (
                    id,
                    user_log_action_id,
					ip,
					time,
					action,
					user_id,
					object_id )
                VALUES (
                    '$next_id',
                    '$action_id',
					'$ip',
                    '$time',
                    '$action',
                    '$user_id',
					'$object_id' ) ";
			$result = mysql_query($sql);
			return $result;
}





// shortens string and adds "..." at the end
function skrajsaj_string($str, $limit)
{
	if(strlen($str) <= $limit)
	{ 
		return $str;
	} else
	{
		return substr($str,0,$limit)."...";
	}
	
}


function dirname2($dir)
{
	$tempdir = dirname($dir);
	if ($tempdir == '/') 
	{ return ""; }
	else
	{ return $tempdir; }
}

function generatePassword($length=9, $strength=0) {
	$vowels = 'aeuy';
	$consonants = 'bdghjmnpqrstvz';
	if ($strength & 1) {
		$consonants .= 'BDGHJLMNPQRSTVWXZ';
	}
	if ($strength & 2) {
		$vowels .= "AEUY";
	}
	if ($strength & 4) {
		$consonants .= '23456789';
	}
	if ($strength & 8) {
		$consonants .= '@#$%';
	}
 
	$password = '';
	$alt = time() % 2;
	for ($i = 0; $i < $length; $i++) {
		if ($alt == 1) {
			$password .= $consonants[(rand() % strlen($consonants))];
			$alt = 0;
		} else {
			$password .= $vowels[(rand() % strlen($vowels))];
			$alt = 1;
		}
	}
	return $password;
}

// adds / subtractd days from date
function daycalc($date,$days)
{
	$date2 = strtotime($date);
	$date3 = mktime(0, 0, 0, date("m", $date2), date("d",$date2)+$days, date("Y",$date2));
	$date4 = date ('Y-m-d',$date3);
	return $date4;
}

/*
function can_access($rights,$con)
{
	$user_id = $_SESSION['user_id'];
	
	if($rights)
	{
		if($user_id == '1')
		{
			return true;
		} else
		{
			return false;
		}
	} else
	{
		// no access rights necessary to acces module
		return true;
	}
}

*/

// adds &nbsp if blank
function check_blank($value)
{
	if(strlen($value))
	{
		return $value;
	} else
	{
		return '&nbsp;';
	}
}

// increase number of lens views
function inc_lens_view($lens_id, $con)
{
	$sql = "SELECT views FROM lens WHERE id = '$lens_id'";
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	
	if($num)
	{
		//$row = mysql_fetch_array($result, $con);
		$row = mysql_fetch_array($result);
		$views = $row['views']+1;
		$sql = "UPDATE lens SET views = '$views' WHERE id='$lens_id'";
		$result = mysql_query($sql);
		return true;
		
	} else
	{
		return false;
	} 
}

function user_is_member($groups, $con)
{
	// P - public
	// R - registred user
	// C - contributor
	// E - editor
	// A - admin

	$user_id = $_SESSION['user_id'];
	$group = explode(",", $groups);


	// groups are define
	if(count($group))
	{
		for($i=0; $i<count($group);$i++)
		{
			// public users
			if($group[$i] == 'P') 	{ return true; }
			// access for registred users
			if($group[$i] == 'R' && $user_id )  { return true; }
		}
	
		if($user_id)
		{
			// get groups members data
			$sql = "SELECT user_group.short 
					FROM user_group, user_user_group
					WHERE user_user_group.user_id = '$user_id'
					AND user_user_group.user_group_id = user_group.id";
	
			
			
			$access = array();		
			$result = mysql_query($sql);
			$num = mysql_num_rows($result);
			
			if($num)
			{
				// access rights for this user are defined
				for($i=1; $i<= $num; $i++)
				{
					//$row = mysql_fetch_array($result, $con);
					$row = mysql_fetch_array($result);
					$access[$i] = $row['short'];
					
					for($j=0; $j<count($group);$j++)
					{
						if( $access[$i] == $group[$j]) { return true; }
					}
				}
				
				// not member of group
				return false;
				
			} else
			{
				// no access rights defined for this user
				return false;
			}
		} else
		{
			// not logged in
			return false;
		}
	} else
	{
		// no groups defined
		return false;
	}
}


//preveri ali je url naslov aktiven	
function is_valid_url ( $url )
{
		$url = @parse_url($url);

		if ( ! $url) {
			return false;
		}

		$url = array_map('trim', $url);
		$url['port'] = (!isset($url['port'])) ? 80 : (int)$url['port'];
		$path = (isset($url['path'])) ? $url['path'] : '';

		if ($path == '')
		{
			$path = '/';
		}

		$path .= ( isset ( $url['query'] ) ) ? "?$url[query]" : '';

		if ( isset ( $url['host'] ) AND $url['host'] != gethostbyname ( $url['host'] ) )
		{
			if ( PHP_VERSION >= 5 )
			{
				$headers = get_headers("$url[scheme]://$url[host]:$url[port]$path");
			}
			else
			{
				$fp = fsockopen($url['host'], $url['port'], $errno, $errstr, 30);

				if ( ! $fp )
				{
					return false;
				}
				fputs($fp, "HEAD $path HTTP/1.1\r\nHost: $url[host]\r\n\r\n");
				$headers = fread ( $fp, 128 );
				fclose ( $fp );
			}
			$headers = ( is_array ( $headers ) ) ? implode ( "\n", $headers ) : $headers;
			return ( bool ) preg_match ( '#^HTTP/.*\s+[(200|301|302)]+\s#i', $headers );
		}
		return false;
}

?>
