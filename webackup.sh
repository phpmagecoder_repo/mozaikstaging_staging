#!/bin/sh 
### Change all these
BACKUP_LIST=/webackup/backup.list 
EXCLUDE_FILE=/webackup/exclude.list 
OUTPUT_DIR=/webackup/ftp 
OUTPUT_FILE=$(date +%Y-%m-%d).tar
#CRYPT_KEY="y0uR cRypT!KeY in H3re"
FTP_USER=uwp 
FTP_PASS=1q2w3e4r5t6y 
FTP_SERVER=108.61.59.242 
FTP_DIR=/C/HostingSpaces/uwp
### Dont change these
FTP_OK_MSG="^226 " 
FTP_LOG=$0.ftp.log 
OUTPUT_ZIPFILE=$OUTPUT_FILE.gz
#OUTPUT_ENCRYPTED=$OUTPUT_ZIPFILE.gpg
doBackup(){
    SOURCE=$1
    if [ ! -e $SOURCE ]; then
        echo "$0 WARNING file $SOURCE could not be found" 1>&2
    else
        echo backing up $SOURCE to $OUTPUT_FILE
        tar -rPf $OUTPUT_FILE --exclude-from=$EXCLUDE_FILE $SOURCE
    fi
}
reportFileSize(){
    FILE=$1
    MSG=$2
    echo $MSG $FILE is `ls -lah $FILE | awk '{ print $5}'` bytes
}
 
###################################^M
# Prepare everything...
###################################^M
cd $OUTPUT_DIR

rm -f $OUTPUT_FILE
rm -f $OUTPUT_ZIPFILE
#rm -f $OUTPUT_ENCRYPTED
rm -f $FTP_LOG

if [ ! -e $BACKUP_LIST ]; then
    echo "$0 could not find the backup list $BACKUP_LIST" 1>&2
    exit 1
fi

if [ ! -e $EXCLUDE_FILE ]; then
    # We need the file to exist otherwise the tar command fails^M
    touch $EXCLUDE_FILE
fi

###################################^M
# Backup the files into an archive and compress it^M
###################################^M
echo Running backup with the following excludes...
cat $EXCLUDE_FILE

# Create the archive and put a copy of the backup list into it^M
tar -cPf $OUTPUT_FILE $BACKUP_LIST

# Read the entries from the BACKUP_LIST file, and add each one into the archive^M
while read ENTRY
do
    doBackup $ENTRY
done < $BACKUP_LIST

reportFileSize $OUTPUT_FILE "Before compression"
 
# Compress the archive
gzip $OUTPUT_FILE

reportFileSize $OUTPUT_ZIPFILE "After compression"

###################################^M
# Encrypt backup file^M
###################################^M
#gpg -c --passphrase "$CRYPT_KEY" $OUTPUT_ZIPFILE

#reportFileSize $OUTPUT_ENCRYPTED "After encryption"

###################################^M
# FTP backup file^M
###################################^M
ftp -nv $FTP_SERVER > $FTP_LOG << EOF
    user $FTP_USER $FTP_PASS
    cd $FTP_DIR
    put $OUTPUT_ZIPFILE
    bye
EOF

OK_MSG_COUNT=`grep -c "$FTP_OK_MSG" $FTP_LOG`
if [ $OK_MSG_COUNT = 1 ]; then
    echo FTP transfer completed ok
    EXIT_CODE=0
else
    echo FTP transfer failed! 1>&2
    cat $FTP_LOG 1>&2
    EXIT_CODE=1
fi
 
###################################^M
# Clean up and exit (leave the zipped backup file in place)^M
###################################^M
rm -f $OUTPUT_FILE
#rm -f $OUTPUT_ENCRYPTED
rm -f $FTP_LOG

exit $EXIT_CODE
